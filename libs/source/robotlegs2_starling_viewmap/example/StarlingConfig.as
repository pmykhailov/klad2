package {
	import view.game.StarlingGameViewMediator;
	import view.stage.StarlingStageMediator;
	import view.StarlingSubViewMediator;

	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.framework.api.IConfig;

	import starling.display.Stage;

	import view.game.StarlingGameView;
	import view.StarlingSubView;

	/**
	 * @author jamieowen
	 */
	public class StarlingConfig implements IConfig
	{
		[Inject]
		public var mediatorMap:IMediatorMap;
		
		public function configure() : void
		{
			mediatorMap.map( StarlingGameView ).toMediator(StarlingGameViewMediator);
			mediatorMap.map( Stage ).toMediator(StarlingStageMediator);
			mediatorMap.map( StarlingSubView ).toMediator(StarlingSubViewMediator);
		}
	}
}
