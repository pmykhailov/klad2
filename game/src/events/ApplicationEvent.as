/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
package events {
import flash.events.Event;

public class ApplicationEvent extends Event {

    public static const INIT:String = "applicationEventInit";
    public static const INIT_COMPLETE:String = "applicationEventInitComplete";

    private var _data:Object;

    public function ApplicationEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);

        _data = data;
    }

    override public function clone():Event {
        return new ApplicationEvent(type, _data, bubbles, cancelable);
    }

    public function get data():Object {
        return _data;
    }
}
}
