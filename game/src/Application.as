package 
{

import controller.commands.init.InitGameCommand;
import controller.managers.GameManagers;
import controller.managers.pause.PauseManager;

import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.display.Sprite;
import flash.events.Event;
import flash.geom.Rectangle;

import misc.AILogicUtils;

import model.GameModel;

import model.config.game.GameConfig;

import robotlegs.bender.bundles.mvcs.MVCSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.impl.Context;
import robotlegs.extensions.starlingViewMap.StarlingViewMapExtension;

import sound.SoundGroupEnum;

import starling.core.Starling;
import starling.events.Event;

import treefortress.sound.SoundAS;

import utils.ScreenUtils;

import view.ViewDimensions;
import view.game.StarlingGameView;
import view.map.item.CellTextures;
import view.user_controlls.UserControllsManager;

use namespace SoundAS;

[SWF(width="1280", height="920", backgroundColor="0x000000")]
	public class Application extends Sprite
	{
        private var _context:IContext;
		private var _starling:Starling;

        private var _userControllsManager: UserControllsManager;

		public function Application()
		{
            GlobalEventDispatcher.instance.init(this);
            GlobalEventDispatcher.instance.addEventListener(GlobalEventDispatcherEvent.TYPE_TO_GAME, onToGameEventHandler);

            addEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStageHandler);
            addEventListener(flash.events.Event.REMOVED_FROM_STAGE, onRemovedFromStageHandler);
		}

        private function onToGameEventHandler(event:GlobalEventDispatcherEvent):void
        {
            switch (event.data.type)
            {
                case GlobalEventDispatcherEvent.INIT_GAME :
                    // TODO: set config in different way
                    InitGameCommand.config = event.data.data as GameConfig;
                    break;

                case GlobalEventDispatcherEvent.DESTROY_GAME :
                    destroy();
                    break;

                case GlobalEventDispatcherEvent.PAUSE_GAME :
                    pause();
                    break;

                case GlobalEventDispatcherEvent.RESUME_GAME :
                    resume();
                    break;

                case GlobalEventDispatcherEvent.CHANGE_GAME_SCREEN :
                     changeGameScreen(event.data.data);
                    break;

                case GlobalEventDispatcherEvent.SPAWN_CHARACTER :
                     // handled in CharacterViewMediator
                    break;
            }
        }

        private function onAddedToStageHandler(event:flash.events.Event):void {
            removeEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStageHandler);
            init();
        }
		
		private function init():void
        {
            ScreenUtils.instance.stage = stage;
            // When this flag is true starling error "the application lost the device context" isn't appear
            Starling.handleLostContext = true;

            _starling = new Starling(StarlingGameView, stage, new Rectangle(0, 0, ViewDimensions.APPLICATION.width, ViewDimensions.APPLICATION.height));

			_context = new Context()
				.install( MVCSBundle, StarlingViewMapExtension )
				.configure(StarlingGameConfig, new ContextView( this ), _starling);

            _starling.addEventListener(starling.events.Event.ROOT_CREATED, onRootCreatedHandler);
            _starling.start();
		}

        private function onRootCreatedHandler(event: starling.events.Event):void
        {
            _starling.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreatedHandler);

            _starling.root.dispatchEvent(new starling.events.Event(starling.events.Event.ROOT_CREATED));

            fitGameToWholeScreen();

            _userControllsManager = new UserControllsManager();
            _userControllsManager.init(stage);

            PauseManager.instance.moveController = _userControllsManager.moveController;
            PauseManager.instance.fireController = _userControllsManager.fireController;

            var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.GAME_INIT_COMPLETE);
            GlobalEventDispatcher.instance.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_FROM_GAME, data));
        }

        private function fitGameToWholeScreen():void
        {
            var viewPortRectangle:Rectangle = new Rectangle();

            viewPortRectangle.width = ScreenUtils.instance.screenDimensions.width;
            viewPortRectangle.height = ScreenUtils.instance.screenDimensions.height;

            Starling.current.viewPort = viewPortRectangle;
        }

        private function changeGameScreen(data:Object):void
        {
            var viewPortRectangle:Rectangle = Starling.current.viewPort.clone();

            if (data.hasOwnProperty("paddingTop")) {
                viewPortRectangle.y = data.paddingTop;
                viewPortRectangle.height -= data.paddingTop;
            }

            if (data.hasOwnProperty("paddingBottom")) {
                viewPortRectangle.height -= data.paddingBottom;
            }

            if (data.hasOwnProperty("paddingLeft")) {
                viewPortRectangle.x = data.paddingLeft;
                viewPortRectangle.width -= data.paddingLeft;
            }

            if (data.hasOwnProperty("paddingRight")) {
                viewPortRectangle.width -= data.paddingRight;
            }

            Starling.current.viewPort = viewPortRectangle;
        }

        private function pause():void {
            PauseManager.instance.pause();
        }

        private function resume():void {
            PauseManager.instance.resume();
        }

        private function destroy():void {
        }

        private function onRemovedFromStageHandler(event:flash.events.Event):void {
            removeEventListener(flash.events.Event.REMOVED_FROM_STAGE, onAddedToStageHandler);

            // ----------------------------------------------
            //    1. Remove listeners (e.g. enter frames)
            // ----------------------------------------------
            GlobalEventDispatcher.instance.removeEventListener(GlobalEventDispatcherEvent.TYPE_TO_GAME, onToGameEventHandler);

            // ----------------------------------------------
            //    2. Dispose objects
            // ----------------------------------------------

            // 2.1 Singletons
            // GlobalEventDispatcher.instance.destroy(); moved down to the end
            ScreenUtils.instance.destroy();
            AILogicUtils.instance.destroy();
            PauseManager.instance.destroy();

            // 2.2 Misc
            var gameModel: GameModel = _context.injector.getInstance(GameModel);
            gameModel.destroy();

            var gameManagers: GameManagers = _context.injector.getInstance(GameManagers);
            gameManagers.destroy();

            (Starling.current.root as StarlingGameView).destroy();

            CellTextures.instance.destroy();

            _starling.dispose();
            _userControllsManager.destroy();
            _context.destroy();

            //SoundAS.group(SoundGroupEnum.GROUP_MUSIC).removeAll();
            //SoundAS.group(SoundGroupEnum.GROUP_SFX).removeAll();

            GlobalEventDispatcher.instance.destroy();

            // ----------------------------------------------
            //    3. remove references
            // ----------------------------------------------
            InitGameCommand.config = null;

            _starling = null;
            _userControllsManager = null;
            _context = null;
        }

    }
}
