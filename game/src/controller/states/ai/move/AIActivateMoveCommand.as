/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.ai.move {
import controller.states.movableunit.move.*;
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.events.MovableUnitStateEvent;

import flash.events.IEventDispatcher;
import flash.geom.Point;
import flash.ui.Keyboard;

import model.map.cell.CellModel;

import model.map.MapModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;

import misc.LogicUtils;

import view.game.StarlingGameView;
import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.character.CharacterView;

import view.events.ViewEvent;

public class AIActivateMoveCommand extends MovableUnitActivateMoveCommand {


    public function AIActivateMoveCommand() {
        super();
    }

    override protected function getStateControllerClass():Class
    {
        return AIMoveController;
    }
}
}
