/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.ai.move
{
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;
import controller.states.movableunit.move.MovableUnitMoveController;

import flash.geom.Point;
import flash.ui.Keyboard;

import misc.AILogicUtils;

import model.GameModel;

import model.map.cell.CellModel;

import model.map.MapModel;

import starling.display.Sprite;

import starling.events.Event;

import misc.LogicUtils;

import view.moveble_units.character.MovableUnitsStateEnum;

import view.moveble_units.character.CharacterView;
import view.game.StarlingGameView;
import view.map.MapView;
import view.map.item.ICellView;
import view.map.item.simple.SimpleCellView;

    public class AIMoveController extends MovableUnitMoveController
    {
        public function AIMoveController(gameView:StarlingGameView, gameModel:GameModel,data:MovableUnitStateControllerData)
        {
            super(gameView, gameModel, data);
        }

        override protected function perform():void
        {
            super.perform();
            /*
            var character: Sprite = _gameView.getMovableUnitViewByType(MovableUnitTypeEnum.TYPE_CHARACTER);
            var ai:Sprite = _gameView.getMovableUnitViewByType(MovableUnitTypeEnum.TYPE_AI, _data.movableUnitID);

            if (Math.abs(character.x - ai.x) <= _gameModel.characterSpeed)
            {
                trace("AI :: AIMoveController distance between character and ai is small");
                //AILogicUtils.instance.run();


                if (Math.abs(character.y - ai.y) == 0) //<=_gameModel.characterSpeed)
                {
                    trace("AI :: cought played");
                } else
                {
                }


            }
             */
        }
    }
}
