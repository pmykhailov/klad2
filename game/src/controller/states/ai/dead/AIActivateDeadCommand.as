/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.ai.dead {
import controller.managers.ai.AIManager;
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import controller.states.movableunit.dead.*;
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;

import model.map.cell.CellModel;

import starling.display.Sprite;



import view.map.item.ICellView;

import view.moveble_units.character.MovableUnitsStateEnum;

public class AIActivateDeadCommand extends MovableUnitActivateDeadCommand {
    public function AIActivateDeadCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        changeState(MovableUnitsStateEnum.STATE_SPAWN);
    }

}
}
