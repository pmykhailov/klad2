/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.ai.climbmove {
import controller.states.movableunit.climbmove.MovableUnitActivateClimbMoveCommand;

public class AIActivateClimbMoveCommand extends MovableUnitActivateClimbMoveCommand {


    public function AIActivateClimbMoveCommand() {
        super();
    }

    override protected function getStateControllerClass():Class
    {
        return AIClimbMoveController;
    }
}
}
