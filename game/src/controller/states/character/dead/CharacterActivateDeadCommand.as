/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.character.dead {
import controller.managers.character.CharacterManager;
import controller.states.movableunit.dead.*;

import sound.GameSoundEnum;
import sound.SoundGroupEnum;

import view.moveble_units.character.MovableUnitsStateEnum;
import treefortress.sound.SoundAS;

public class CharacterActivateDeadCommand extends MovableUnitActivateDeadCommand {
    public function CharacterActivateDeadCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(GameSoundEnum.LOSE);
    }


    override protected function clear():void {
        super.clear();

        (movableUnitManager as CharacterManager).positionChangeHandler.stop();
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitDeadController;
    }

}
}
