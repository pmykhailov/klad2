/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.move {
import controller.states.movableunit.base.MovableUnitDeactivateStateBaseCommand;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;

import view.game.StarlingGameView;
import view.moveble_units.character.CharacterView;

import view.events.ViewEvent;

public class MovableUnitDeactivateMoveCommand extends MovableUnitDeactivateStateBaseCommand {


    public function MovableUnitDeactivateMoveCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        moveableUnitView.removeAnimation();
    }
}
}
