/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.move {
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.events.MovableUnitStateEvent;

import flash.events.IEventDispatcher;
import flash.geom.Point;
import flash.ui.Keyboard;

import model.map.cell.CellModel;

import model.map.MapModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;

import misc.LogicUtils;

import view.game.StarlingGameView;
import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.character.CharacterView;

import view.events.ViewEvent;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitActivateMoveCommand extends MovableUnitActivateStateBaseCommand {


    public function MovableUnitActivateMoveCommand() {
        super();
    }

    override public function execute():void {
        moveableUnitView.lookDirection = movableUnitManager.currentMoveDirection;
        moveableUnitView.startAnimation(MovableUnitAnimationsEnum.MOVE);

        super.execute();
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_MOVE;
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitMoveController;
    }

    override protected function createMoveVector():Point
    {
        return (movableUnitManager.currentMoveDirection == Keyboard.RIGHT) ? new Point(movableUnitManager.speed,0) : new Point(-movableUnitManager.speed,0);
    }
}
}
