/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.move
{
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.geom.Point;
import flash.ui.Keyboard;

import model.GameModel;

import model.map.cell.CellModel;

import model.map.MapModel;
import model.map.cell.CellTypeEnum;

import starling.events.Event;

import misc.LogicUtils;

import view.moveble_units.character.MovableUnitsStateEnum;

import view.moveble_units.character.CharacterView;
import view.game.StarlingGameView;
import view.map.MapView;
import view.map.item.ICellView;
import view.map.item.simple.SimpleCellView;

    public class MovableUnitMoveController extends MovableUnitStateController
    {
        private var _newMovableUnitCoordinates:Point;

        public function MovableUnitMoveController(gameView:StarlingGameView, gameModel:GameModel,data:MovableUnitStateControllerData)
        {
            super(gameView, gameModel, data);

            _newMovableUnitCoordinates  = new Point();
        }

        override protected function perform():void
        {
            super.perform();

            _newMovableUnitCoordinates.x = moveableUnitView.x + _moveVector.x;
            _newMovableUnitCoordinates.y = moveableUnitView.y + _moveVector.y;

            var cellWithMaxContributionVO:Object = LogicUtils.getCellWithMaxContributionVO(_newMovableUnitCoordinates, mapModel, _gameView.map);
            var characterCell:CellModel = cellWithMaxContributionVO.cellModel as CellModel;
            var cellUnderCharacterCell:CellModel = mapModel.getCell(characterCell.row + 1, characterCell.col);

            if (LogicUtils.areCellsForMoving(_newMovableUnitCoordinates, mapModel))
            {
                moveableUnitView.x = _newMovableUnitCoordinates.x;
                moveableUnitView.y = _newMovableUnitCoordinates.y;

                if (
                        (cellUnderCharacterCell.type == CellTypeEnum.TYPE_EMPTY || cellUnderCharacterCell.type == CellTypeEnum.TYPE_WATTER) &&
                        cellWithMaxContributionVO.contribution > 85
                   )
                {
                    var cellWithMaxContributionView:ICellView = _gameView.map.getCellView(characterCell.row, characterCell.col);

                    moveableUnitView.x = cellWithMaxContributionView.x;
                    moveableUnitView.y = cellWithMaxContributionView.y;

                   _data.changeStateCallback(MovableUnitsStateEnum.STATE_FALL);
                }
            }else
            {
                // We reach watter cell while moving
                // [E] [W]
                // [C] [C]
                // Then we will die

                var characterCells:Vector.<CellModel> = LogicUtils.getCharacterCells(_newMovableUnitCoordinates, mapModel);
                var contributions:Array = LogicUtils.calcContributions(_newMovableUnitCoordinates, mapModel, mapView);

                if (
                        (characterCells[0].type == CellTypeEnum.TYPE_WATTER /*&& contributions[0] > 50*/ )||
                        (contributions[1] && characterCells[1].type == CellTypeEnum.TYPE_WATTER /*&& contributions[1] > 50*/)
                   )
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_DEAD);
                    return;
                }

                _data.changeStateCallback(MovableUnitsStateEnum.STATE_IDLE);
            }
        }



        override public function reactOnSignal(signal:Signal):void
        {
            super.reactOnSignal(signal);

            if (signal.type == Signal.TYPE_KEY_UP)
            {
                _data.changeStateCallback(MovableUnitsStateEnum.STATE_IDLE);
            }
        }
    }
}
