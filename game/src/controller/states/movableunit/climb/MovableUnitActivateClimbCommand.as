/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climb {

import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;

import flash.geom.Point;
import flash.ui.Keyboard;

import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;

import misc.LogicUtils;

import view.moveble_units.character.MovableUnitsStateEnum;
import view.map.item.ICellView;
import view.map.item.simple.SimpleCellView;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitActivateClimbCommand extends MovableUnitActivateStateBaseCommand {
    public function MovableUnitActivateClimbCommand() {
        super();
    }

    override public function execute():void {

        moveableUnitView.startAnimation(MovableUnitAnimationsEnum.CLIMB);

        var cellToSnapModel:CellModel;
        var characterCellsModels:Vector.<CellModel> = LogicUtils.getCharacterCells(moveableUnitView, mapModel);
        var characterCellsContributions:Array = LogicUtils.calcContributions(moveableUnitView, mapModel, gameView.map);
        var cellWithMaxContributionVO:Object = LogicUtils.getCellWithMaxContributionVO(moveableUnitView, mapModel, gameView.map);
        var characterCellModelWithMaxContribution:CellModel = cellWithMaxContributionVO.cellModel as CellModel;
        var underCharacterCellModelWithMaxContribution = mapModel.getCell(characterCellModelWithMaxContribution.row + 1, characterCellModelWithMaxContribution.col);
        var isFullyInLadder:Boolean = false;

        // Snap to ladder cell
        // If we are not on the middle of ladder of course
        /*/
        if (characterCellsModels[0].type == CellTypeEnum.TYPE_LADDER && characterCellsContributions[0] == 100)
        {
            isFullyInLadder = true;
        }else
        if (
                characterCellsContributions[1] == 0 && characterCellsContributions[2] == 0 &&
                characterCellsModels[0].type == CellTypeEnum.TYPE_LADDER && characterCellsModels[3].type == CellTypeEnum.TYPE_LADDER
            )
        {
            isFullyInLadder = true;
        }
        */

        isFullyInLadder = (
                vo.prevState == MovableUnitsStateEnum.STATE_CLIMB_IDLE ||
                // At the end of STATE_CLIMB_MOVE movable unit snaps ot leaser cell so
                // it is fully on leader
                vo.prevState == MovableUnitsStateEnum.STATE_CLIMB_MOVE
            );

        if (isFullyInLadder)
        {
            cellToSnapModel = null;
        }else
        if (movableUnitManager.currentMoveDirection == Keyboard.UP)
        {
            if (characterCellModelWithMaxContribution.type == CellTypeEnum.TYPE_LADDER)
            {
                cellToSnapModel = characterCellModelWithMaxContribution;
            }
        }else
        if (movableUnitManager.currentMoveDirection == Keyboard.DOWN)
        {
            if (underCharacterCellModelWithMaxContribution.type == CellTypeEnum.TYPE_LADDER)
            {
                cellToSnapModel = characterCellModelWithMaxContribution;
            }
        }

        if (cellToSnapModel)
        {
            var cellView:ICellView = gameView.map.getCellView(cellToSnapModel.row, cellToSnapModel.col);

            moveableUnitView.x = cellView.x;
            moveableUnitView.y = cellView.y;
        }

        if (cellToSnapModel || isFullyInLadder)
        {
            super.execute();
        }else
        {
            changeState(MovableUnitsStateEnum.STATE_IDLE);
        }
    }


    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_CLIMB;
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitClimbController;
    }

    override protected function createMoveVector():Point
    {
        return (movableUnitManager.currentMoveDirection == Keyboard.DOWN) ? new Point(0,movableUnitManager.speed) : new Point(0,-movableUnitManager.speed);
    }
}
}
