/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climb
{
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.geom.Point;
import flash.ui.Keyboard;

import model.GameModel;

import model.map.cell.CellModel;

import model.map.MapModel;
import model.map.cell.CellTypeEnum;

import starling.events.Event;

import misc.LogicUtils;

import view.moveble_units.character.MovableUnitsStateEnum;

import view.moveble_units.character.CharacterView;
import view.game.StarlingGameView;
import view.map.MapView;

    public class MovableUnitClimbController extends MovableUnitStateController
    {
        private var _newCharacterCoordinates:Point;

        public function MovableUnitClimbController(gameView:StarlingGameView, gameModel:GameModel, data:MovableUnitStateControllerData)
        {
            super(gameView, gameModel, data);

            _newCharacterCoordinates  = new Point();
        }

        override protected function perform():void
        {
            super.perform();

            _newCharacterCoordinates.x = moveableUnitView.x + _moveVector.x;
            _newCharacterCoordinates.y = moveableUnitView.y + _moveVector.y;

            var characterCells:Vector.<CellModel> = LogicUtils.getCharacterCells(_newCharacterCoordinates, mapModel);
            var contributions: Array = LogicUtils.calcContributions(_newCharacterCoordinates, mapModel, mapView);

            /*
             **********************************
             ****** BOTTOM OF LADDER CASE *****
             **********************************
             */
            var isSlightlyOnLadder:Boolean = characterCells[0].type == CellTypeEnum.TYPE_LADDER && contributions[0] < 30;
            var cellUnderZeroCell:CellModel = mapModel.getCell(characterCells[0].row + 1, characterCells[0].col);

            if (isSlightlyOnLadder && cellUnderZeroCell.type != CellTypeEnum.TYPE_LADDER)
            {
                // Fall from bottom of the ladder
                if (cellUnderZeroCell.type == CellTypeEnum.TYPE_EMPTY)
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_FALL);
                    return;
                }
                // Die if we touch watter
                if (cellUnderZeroCell.type == CellTypeEnum.TYPE_WATTER)
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_DEAD);
                    return;
                }
            }

            /*
             **********************************
             ****** TOP OF LADDER CASE *****
             **********************************
             */
            // Probably impossible case but still let's have it
            if (characterCells[0].type == CellTypeEnum.TYPE_WATTER)
            {
                _data.changeStateCallback(MovableUnitsStateEnum.STATE_DEAD);
            }

            /*
             **************************************
             ****** CONTINUE OR STOP CLIMBING *****
             **************************************
             */

            var climbingAllowed:Boolean = false;
            var isOnLadder:Boolean = (characterCells[0].type == CellTypeEnum.TYPE_LADDER);
            var isOnLadderTop:Boolean = (characterCells[0].type == CellTypeEnum.TYPE_EMPTY) && ((characterCells[3] && characterCells[3].type == CellTypeEnum.TYPE_LADDER) || !characterCells[3])
            var isLadderBottomReached:Boolean = (characterCells[0].type == CellTypeEnum.TYPE_LADDER) && !characterCells[3] && mapModel.getCell(characterCells[0].row + 1, characterCells[0].col).type != CellTypeEnum.TYPE_LADDER;
            var isLadderTopReached:Boolean = (characterCells[0].type == CellTypeEnum.TYPE_EMPTY) && !characterCells[3];

             // Add not so strict conditions. That condition is added after character speed became real number
            var isLadderTopReachedWeakCondition:Boolean = _moveVector.y < 0 && (characterCells[0].type == CellTypeEnum.TYPE_EMPTY && contributions[0] > 85) && (characterCells[3] && characterCells[3].type == CellTypeEnum.TYPE_LADDER);
            isLadderTopReached = isLadderTopReached || isLadderTopReachedWeakCondition;

            var isLadderBottomReachedWeakCondition:Boolean = _moveVector.y > 0 && (characterCells[3] && characterCells[3].type == CellTypeEnum.TYPE_LADDER && contributions[3] > 85) && mapModel.getCell(characterCells[3].row + 1, characterCells[3].col).type != CellTypeEnum.TYPE_LADDER;
            isLadderBottomReached = isLadderBottomReached || isLadderBottomReachedWeakCondition;

            climbingAllowed = isOnLadder || isOnLadderTop;


            if (climbingAllowed)
            {
                moveableUnitView.x = _newCharacterCoordinates.x;
                moveableUnitView.y = _newCharacterCoordinates.y;
            }

            if (isLadderTopReached || isLadderBottomReached)
            {
                // If weak condition of reaching ladder top fulfilled than we should snap to empty cell on top of the ladder
                if (isLadderTopReached && isLadderTopReachedWeakCondition) {
                    moveableUnitView.x = _gameView.map.getCellViewByModel(characterCells[0]).x;
                    moveableUnitView.y = _gameView.map.getCellViewByModel(characterCells[0]).y;
                }

                if (isLadderBottomReached && isLadderBottomReachedWeakCondition) {
                    moveableUnitView.x = _gameView.map.getCellViewByModel(characterCells[3]).x;
                    moveableUnitView.y = _gameView.map.getCellViewByModel(characterCells[3]).y;
                }

                _data.changeStateCallback(MovableUnitsStateEnum.STATE_IDLE);
            }
        }


        override public function reactOnSignal(signal:Signal):void
        {
            super.reactOnSignal(signal);

            if (signal.type == Signal.TYPE_KEY_UP)
            {
                _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB_IDLE);
            }
        }
    }
}
