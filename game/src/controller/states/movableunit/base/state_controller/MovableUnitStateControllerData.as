/**
 * Created by Pasha on 26.07.2015.
 */
package controller.states.movableunit.base.state_controller {
public class MovableUnitStateControllerData {

    public var changeStateCallback:Function;
    public var movableUnitType:String;
    public var movableUnitID:int;

    public function MovableUnitStateControllerData() {
    }
}
}
