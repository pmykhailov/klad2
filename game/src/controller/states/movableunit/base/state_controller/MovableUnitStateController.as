/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.base.state_controller
{
import controller.managers.pause.IPausable;
import controller.managers.pause.PauseManager;
import controller.states.movableunit.base.*;

import flash.geom.Point;
import flash.ui.Keyboard;

import misc.IDestroyable;

import model.GameModel;

import model.map.cell.CellModel;

import model.map.MapModel;

import starling.display.Sprite;

import starling.events.Event;

import misc.LogicUtils;


import view.moveble_units.character.CharacterView;
import view.game.StarlingGameView;
import view.map.MapView;

    public class MovableUnitStateController implements IPausable, IDestroyable
    {
        protected var _gameView:StarlingGameView;
        protected var _gameModel:GameModel;
        protected var _moveVector:Point;
        protected var _data:MovableUnitStateControllerData;
        private var _enterFrameEventProvider:Sprite;
        private var _isStarted:Boolean;
        private var _isPaused:Boolean;

        public function MovableUnitStateController(gameView:StarlingGameView, gameModel:GameModel,data:MovableUnitStateControllerData)
        {
            super();

            _gameView = gameView;
            _gameModel = gameModel;
            _data = data;
        }

        public function start():void
        {
            _isStarted = true;
            _isPaused = false;

            PauseManager.instance.addToStateControllers(this);

            performSart();
        }

        public function stop():void
        {
            _isStarted = false;

            PauseManager.instance.removeFromStateControllers(this);

            performStop();
        }

        public function pause():void {
            if (_enterFrameEventProvider) {
                _isPaused = true;
                performStop();
            }
        }

        public function resume():void {
            _isPaused = false;
            performSart();
        }

        public function destroy():void {
            if (_isStarted && !_isPaused) {
                stop();
            }
        }

        private function performSart():void {
            _enterFrameEventProvider = new Sprite();
            _enterFrameEventProvider.addEventListener(starling.events.Event.ENTER_FRAME, onEnterFrameHandler);
            _gameView.addChild(_enterFrameEventProvider);

            perform();
        }
        private function performStop():void {
            _enterFrameEventProvider.removeEventListener(starling.events.Event.ENTER_FRAME, onEnterFrameHandler);
            _gameView.removeChild(_enterFrameEventProvider);
            _enterFrameEventProvider = null;
        }

        public function reactOnSignal(signal:Signal):void
        {

        }

        protected function onEnterFrameHandler(event: starling.events.Event):void
        {
            perform();
        }

        protected function perform():void {

        }

        protected function get mapModel():MapModel
        {
            return _gameModel.mapModel;
        }

        protected function get mapView():MapView
        {
            return _gameView.map;
        }

        protected function get moveableUnitView():Sprite
        {
            return _gameView.getMovableUnitViewByType(_data.movableUnitType, _data.movableUnitID);
        }

        public function get moveVector():Point
        {
            return _moveVector;
        }

        public function set moveVector(value:Point):void
        {
            _moveVector = value;
        }

        public function get isStarted():Boolean {
            return _isStarted;
        }

    }
}
