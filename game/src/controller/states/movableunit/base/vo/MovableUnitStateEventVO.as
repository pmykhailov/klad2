/**
 * Created by Pasha on 26.07.2015.
 */
package controller.states.movableunit.base.vo {
import controller.managers.base.MovableUnitManager;

public class MovableUnitStateEventVO {

    private var _state:String;
    private var _manager:MovableUnitManager;

    public function MovableUnitStateEventVO(state:String, manager:MovableUnitManager) {
        _state = state;
        _manager = manager;
    }

    public function get state():String {
        return _state;
    }

    public function get manager():MovableUnitManager {
        return _manager;
    }
}
}
