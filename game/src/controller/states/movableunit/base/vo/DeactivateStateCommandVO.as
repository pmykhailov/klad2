/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 30.06.15
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.base.vo {
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import controller.managers.base.MovableUnitManager;

public class DeactivateStateCommandVO {

    public var movableUnitManager:MovableUnitManager;

    public function DeactivateStateCommandVO() {
    }
}
}
