/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.base.events {
import flash.events.Event;

public class MovableUnitStateEvent extends Event {

    public static const STATE_CHANGE:String = "characterStateEventStateChange";

    private var _data:Object;

    public function MovableUnitStateEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }


    override public function clone():Event {
        return new MovableUnitStateEvent(type, data, bubbles, cancelable);
    }
}
}
