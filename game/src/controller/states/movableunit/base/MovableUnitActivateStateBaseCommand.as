/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.base {
import controller.managers.GameManagers;
import controller.managers.base.MovableUnitManager;
import controller.managers.pause.PauseManager;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;
import controller.states.movableunit.base.vo.ActivateStateCommandVO;
import controller.states.movableunit.base.events.MovableUnitStateEvent;
import controller.states.movableunit.base.vo.MovableUnitStateEventVO;


import flash.events.IEventDispatcher;
import flash.geom.Point;
import flash.ui.Keyboard;

import model.GameModel;


import model.map.MapModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;

import misc.LogicUtils;

import starling.display.Sprite;

import view.game.StarlingGameView;
import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.character.CharacterView;

import view.events.ViewEvent;
import view.moveble_units.common.BaseMovableUnitView;

public class MovableUnitActivateStateBaseCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var gameModel:GameModel;

    [Inject]
    public var event:ViewEvent;

    public function MovableUnitActivateStateBaseCommand() {
        super();
    }

    override public function execute():void
    {
        super.execute();

        activate();
    }

    protected function activate():void
    {
        movableUnitManager.stateID = getStateId();

        movableUnitManager.stateController = createStateController();
        movableUnitManager.stateController.moveVector = createMoveVector();
        movableUnitManager.stateController.start();
    }

    protected function getStateId():String
    {
        return null;
    }

    protected function getStateControllerClass():Class
    {
        return MovableUnitStateController;
    }

    protected function createStateController():MovableUnitStateController
    {
        var StateControllerClass:Class = getStateControllerClass();
        var data:MovableUnitStateControllerData = new MovableUnitStateControllerData();

        data.changeStateCallback = changeState;
        data.movableUnitType = movableUnitManager.type;
        data.movableUnitID = movableUnitManager.id;

        return new StateControllerClass(gameView, gameModel, data);
    }

    protected function createMoveVector():Point
    {
        return null;
    }

    protected function changeState(state:String):void
    {
        var vo:MovableUnitStateEventVO = new MovableUnitStateEventVO(state, movableUnitManager);

        dispatcher.dispatchEvent(new MovableUnitStateEvent(MovableUnitStateEvent.STATE_CHANGE, vo));
    }

    protected function get movableUnitManager():MovableUnitManager
    {
        return (event.data as ActivateStateCommandVO).movableUnitManager;
    }

    protected function get gameView():StarlingGameView
    {
        return Starling.current.root as StarlingGameView;
    }

    protected function get moveableUnitView():BaseMovableUnitView
    {
        return gameView.getMovableUnitViewByType(movableUnitManager.type, movableUnitManager.id);
    }

    protected function get mapModel():MapModel
    {
        return gameModel.mapModel;
    }

    protected function get vo():ActivateStateCommandVO
    {
        return event.data as ActivateStateCommandVO;
    }
}
}
