/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.base
{
import controller.states.movableunit.base.events.MovableUnitStateEvent;
import controller.states.movableunit.base.vo.DeactivateStateCommandVO;
import controller.states.movableunit.base.vo.MovableUnitStateEventVO;
import controller.managers.GameManagers;
import controller.states.movableunit.base.vo.ActivateStateCommandVO;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;

import view.game.StarlingGameView;
import view.events.ViewEvent;

public class MovableUnitStateChangeCommand extends Command
    {
        [Inject]
        public var context:IContext;

        [Inject]
        public var event:MovableUnitStateEvent;

        [Inject]
        public var dispatcher:IEventDispatcher;

        [Inject]
        public var gameController:GameManagers;

        public function MovableUnitStateChangeCommand()
        {
        }

        override public function execute():void {
            super.execute();

            var vo:MovableUnitStateEventVO = event.data as MovableUnitStateEventVO;

            var activateStateCommandVO:ActivateStateCommandVO = new ActivateStateCommandVO();
            activateStateCommandVO.prevState = vo.manager.stateID;
            activateStateCommandVO.movableUnitManager = vo.manager;

            var deactivateStateCommandVO:DeactivateStateCommandVO = new DeactivateStateCommandVO();
            deactivateStateCommandVO.movableUnitManager = vo.manager;

            //if (vo.manager.type.toUpperCase() == "CHARACTER")
            trace(vo.manager.type.toUpperCase() + " transition from " + activateStateCommandVO.prevState + " to " + vo.state);

            var type:String = vo.manager.type;
            var deactivateEventName:String = ViewEvent.getDeactivateMovableUnitStateEventName(activateStateCommandVO.prevState, type);
            var activateEventName:String = ViewEvent.getActivateMovableUnitStateEventName(vo.state, type);

            dispatcher.dispatchEvent(new ViewEvent(deactivateEventName, deactivateStateCommandVO));
            dispatcher.dispatchEvent(new ViewEvent(activateEventName, activateStateCommandVO));
        }
}
}
