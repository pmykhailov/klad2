package controller.states.movableunit.base
{
    public class Signal
    {
        public static const TYPE_KEY_DOWN:String = "keyDown";
        public static const TYPE_KEY_UP:String = "keyUp";

        private var _type:String;
        private var _data:Object;

        public function Signal(type:String, data:Object = null)
        {
            _type = type;
            _data = data;
        }

        public function get type():String {
            return _type;
        }

        public function get data():Object {
            return _data;
        }
    }
}
