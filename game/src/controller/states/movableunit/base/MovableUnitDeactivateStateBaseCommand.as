/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:57
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.base {
import controller.managers.base.MovableUnitManager;
import controller.states.movableunit.base.vo.DeactivateStateCommandVO;


import robotlegs.bender.bundles.mvcs.Command;

import starling.core.Starling;


import view.events.ViewEvent;
import view.game.StarlingGameView;
import view.moveble_units.common.BaseMovableUnitView;

public class MovableUnitDeactivateStateBaseCommand extends Command {

    [Inject]
    public var event:ViewEvent;

    public function MovableUnitDeactivateStateBaseCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        if (movableUnitManager.stateController.isStarted) {
            movableUnitManager.stateController.stop();
        }
    }

    protected function get gameView():StarlingGameView
    {
        return Starling.current.root as StarlingGameView;
    }

    protected function get movableUnitManager():MovableUnitManager
    {
        return (event.data as DeactivateStateCommandVO).movableUnitManager;
    }

    protected function get moveableUnitView():BaseMovableUnitView
    {
        return gameView.getMovableUnitViewByType(movableUnitManager.type, movableUnitManager.id);
    }



}
}
