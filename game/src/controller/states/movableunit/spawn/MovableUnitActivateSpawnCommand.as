/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.spawn {
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;

import model.map.cell.CellModel;


import view.map.item.ICellView;

import view.moveble_units.character.MovableUnitsStateEnum;

public class MovableUnitActivateSpawnCommand extends MovableUnitActivateStateBaseCommand {
    public function MovableUnitActivateSpawnCommand() {
        super();
    }

    override public function execute():void {
        movableUnitManager.stateID = getStateId();

        var cellModel:CellModel = movableUnitManager.spawnCell;
        var cellView:ICellView = gameView.map.getCellView(cellModel.row, cellModel.col);

        moveableUnitView.x = cellView.x;
        moveableUnitView.y = cellView.y;

        changeState(MovableUnitsStateEnum.STATE_IDLE);
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_SPAWN;
    }

}
}
