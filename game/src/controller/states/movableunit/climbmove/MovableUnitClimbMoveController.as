/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climbmove
{
    import controller.states.movableunit.base.state_controller.MovableUnitStateController;
    import controller.states.movableunit.base.Signal;
import controller.managers.base.IMoveDirectionProvider;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.geom.Point;

import flash.ui.Keyboard;

import misc.LogicUtils;

import misc.LogicUtils;
import misc.vo.CellWithMaxContributionVO;

import model.GameModel;
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;

import starling.events.Event;

import view.moveble_units.character.MovableUnitsStateEnum;

import view.game.StarlingGameView;
import view.map.item.ICellView;
import view.map.item.simple.SimpleCellView;
import view.moveble_units.common.BaseMovableUnitView;


public class MovableUnitClimbMoveController extends MovableUnitStateController
    {
        private var _newCharacterCoordinates:Point;
        private var _moveDirectionProvider:IMoveDirectionProvider;

        public function MovableUnitClimbMoveController(gameView:StarlingGameView, gameModel:GameModel, data:MovableUnitStateControllerData)
        {
            super(gameView, gameModel, data);

            _newCharacterCoordinates  = new Point();
        }

        override protected function perform():void
        {
            super.perform();

            _newCharacterCoordinates.x = moveableUnitView.x + _moveVector.x;
            _newCharacterCoordinates.y = moveableUnitView.y + _moveVector.y;

            var canMove:Boolean = LogicUtils.areCellsForMoving(_newCharacterCoordinates, mapModel);

            // But this is strict condition of such case
            // [L] [B]
            // [L] [ ] -> Character can not stand always directly in front of empty cell
            // [L] [B]

            // So we will snap to biggest leader cell and try if move is possible in this case
            if (!canMove)
            {
                var cellWithMaxContributionVO:CellWithMaxContributionVO = LogicUtils.getCellWithMaxContributionVO(_newCharacterCoordinates, mapModel, mapView);

                if (cellWithMaxContributionVO.contribution > 60 && (cellWithMaxContributionVO.cellModel.type == CellTypeEnum.TYPE_LADDER || cellWithMaxContributionVO.cellModel.type == CellTypeEnum.TYPE_EMPTY))
                {
                    var movableUnitTmpCoordinates:Point = new Point();

                    movableUnitTmpCoordinates.x = mapView.getCellViewByModel(cellWithMaxContributionVO.cellModel).x + _moveVector.x;
                    movableUnitTmpCoordinates.y = mapView.getCellViewByModel(cellWithMaxContributionVO.cellModel).y + _moveVector.y;

                    canMove = LogicUtils.areCellsForMoving(movableUnitTmpCoordinates, mapModel);

                    if (canMove)
                    {
                        _newCharacterCoordinates.x = movableUnitTmpCoordinates.x;
                        _newCharacterCoordinates.y = movableUnitTmpCoordinates.y;
                    }
                }
            }

            if (canMove)
            {
                var characterCells:Vector.<CellModel> = LogicUtils.getCharacterCells(_newCharacterCoordinates, mapModel);
                var contributions: Array = LogicUtils.calcContributions(_newCharacterCoordinates, mapModel, mapView);

                moveableUnitView.x = _newCharacterCoordinates.x;
                moveableUnitView.y = _newCharacterCoordinates.y;

                var cellModel:CellModel;
                var i:int;

                /*
                 *******************************
                 ****** HIT THE WATER CASE *****
                 *******************************
                 */
                for (i = 0; i < characterCells.length; i++)
                {
                    cellModel = characterCells[i];
                    if (cellModel && cellModel.type == CellTypeEnum.TYPE_WATTER)
                    {
                        _data.changeStateCallback(MovableUnitsStateEnum.STATE_DEAD);
                        return;
                    }
                }

                var maxLeaderCellContribution:int = 0;
                /*
                 *******************************
                 ****** GO OUT FROM LADDER *****
                 *******************************
                 */
                for (i = 0; i < characterCells.length; i++)
                {
                    cellModel = characterCells[i];
                    if (cellModel && cellModel.type == CellTypeEnum.TYPE_LADDER)
                    {
                        if (contributions[i] > maxLeaderCellContribution)
                        {
                            maxLeaderCellContribution = contributions[i];
                        }
                    }
                }

                if (maxLeaderCellContribution < 10)
                {
                    // Snap to cell ...
                    for (i = 0; i < characterCells.length; i++)
                    {
                        cellModel = characterCells[i];
                        if (cellModel && cellModel.type == CellTypeEnum.TYPE_LADDER)
                        {
                            var cellToSnap:ICellView;

                            if (_moveDirectionProvider.currentMoveDirection == Keyboard.RIGHT)
                            {
                                cellToSnap = mapView.getCellView(cellModel.row, cellModel.col + 1);
                            }else
                            {
                                cellToSnap = mapView.getCellView(cellModel.row, cellModel.col - 1);
                            }

                            moveableUnitView.x = cellToSnap.x;

                            break;
                        }

                    }

                    var isGonnaFall:Boolean;
                    var cellWithMaxContribution:CellModel = LogicUtils.getCellWithMaxContributionVO(moveableUnitView, mapModel, mapView).cellModel;
                    var cellUnderCellWithMaxContribution: CellModel = mapModel.getCell(cellWithMaxContribution.row + 1, cellWithMaxContribution.col);

                    if (
                        cellUnderCellWithMaxContribution.type == CellTypeEnum.TYPE_EMPTY
                       )
                    {
                        isGonnaFall = true;
                    }

                    // ... and fall
                    if (isGonnaFall)
                    {
                        _data.changeStateCallback(MovableUnitsStateEnum.STATE_FALL);
                    }else
                    // ... or move
                    {
                        moveableUnitView.y = mapView.getCellViewByModel(cellWithMaxContribution).y;

                        _data.changeStateCallback(MovableUnitsStateEnum.STATE_MOVE);
                    }


                }
            } else {
                _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB_IDLE);
            }
        }

        override public function reactOnSignal(signal:Signal):void
        {
            super.reactOnSignal(signal);

            if (signal.type == Signal.TYPE_KEY_UP)
            {
                (moveableUnitView as BaseMovableUnitView).pauseAnimation();
                stop();
            }else
            {
                if (signal.type == Signal.TYPE_KEY_DOWN)
                {
                    if (signal.data == Keyboard.LEFT || signal.data == Keyboard.RIGHT)
                    {
                        _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB_MOVE);
                    }else
                    if (signal.data == Keyboard.UP || signal.data == Keyboard.DOWN)
                    {
                        // Snap to leader cell

                        var characterCells:Vector.<CellModel> = LogicUtils.getCharacterCells(_newCharacterCoordinates, mapModel);
                        var contributions: Array = LogicUtils.calcContributions(_newCharacterCoordinates, mapModel, mapView);
                        var cellModelToSnap:CellModel;

                        if (characterCells[0].type == CellTypeEnum.TYPE_LADDER && characterCells[1] && characterCells[1].type == CellTypeEnum.TYPE_LADDER)
                        {
                            if (contributions[1] > contributions[0])
                            {
                                cellModelToSnap = characterCells[1];
                            }else
                            {
                                cellModelToSnap = characterCells[0];
                            }
                        }else
                        if (characterCells[0].type == CellTypeEnum.TYPE_LADDER)
                        {
                            cellModelToSnap = characterCells[0];
                        }else
                        if (characterCells[1] && characterCells[1].type == CellTypeEnum.TYPE_LADDER)
                        {
                            cellModelToSnap = characterCells[1];
                        }else
                        if (characterCells[2] && characterCells[2].type == CellTypeEnum.TYPE_LADDER)
                        {
                            cellModelToSnap = characterCells[2];
                        }else
                        if (characterCells[3] && characterCells[3].type == CellTypeEnum.TYPE_LADDER)
                        {
                            cellModelToSnap = characterCells[3];
                        }

                        moveableUnitView.x = mapView.getCellView(cellModelToSnap.row, cellModelToSnap.col).x;

                        // And climb
                        _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB);
                    }
                }
            }
        }

        public function set moveDirectionProvider(value:IMoveDirectionProvider):void {
            _moveDirectionProvider = value;
        }
    }
}
