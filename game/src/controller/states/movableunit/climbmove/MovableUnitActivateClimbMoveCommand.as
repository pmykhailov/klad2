/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climbmove {
import controller.managers.base.MoveDirectionsEnum;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;

import flash.geom.Point;
import flash.ui.Keyboard;

import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitActivateClimbMoveCommand extends MovableUnitActivateStateBaseCommand
{
    public function MovableUnitActivateClimbMoveCommand() {
        super();
    }

    override public function execute():void
    {
        moveableUnitView.lookDirection = movableUnitManager.currentMoveDirection;
        moveableUnitView.startAnimation(MovableUnitAnimationsEnum.MOVE);

        super.execute();
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_CLIMB_MOVE;
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitClimbMoveController;
    }

    override protected function createStateController():MovableUnitStateController
    {
        var characterStateController:MovableUnitClimbMoveController = super.createStateController() as MovableUnitClimbMoveController;

        characterStateController.moveDirectionProvider = movableUnitManager;

        return characterStateController;
    }

    override protected function createMoveVector():Point
    {
        return (movableUnitManager.currentMoveDirection == Keyboard.RIGHT) ? new Point(movableUnitManager.speed,0) : new Point(-movableUnitManager.speed,0);
    }
}
}
