/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climbmove
{
    import controller.states.movableunit.base.MovableUnitDeactivateStateBaseCommand;

import robotlegs.bender.bundles.mvcs.Command;

    public class MovableUnitDeactivateClimbMoveCommand extends MovableUnitDeactivateStateBaseCommand
    {
        public function MovableUnitDeactivateClimbMoveCommand()
        {
            super();
        }

        override public function execute():void
        {
            super.execute();

            moveableUnitView.removeAnimation();
        }
    }
}
