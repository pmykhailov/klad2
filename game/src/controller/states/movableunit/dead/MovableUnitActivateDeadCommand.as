/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.dead {

import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;

import events.GlobalEventDispatcher;

import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;


import view.moveble_units.character.MovableUnitsStateEnum;

public class MovableUnitActivateDeadCommand extends MovableUnitActivateStateBaseCommand {
    public function MovableUnitActivateDeadCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        clear();
        notifyMovableUnitDead();
    }

    protected function clear():void
    {
        //moveableUnitView.removeAnimation();
    }

    protected function notifyMovableUnitDead():void
    {
        var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.MOVABLE_UNIT_DEAD, {type:movableUnitManager.type});
        GlobalEventDispatcher.instance.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_FROM_GAME, data));
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitDeadController;
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_DEAD;
    }

}
}
