package controller.states.movableunit.dead {
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.ui.Keyboard;
import model.GameModel;

import starling.display.Quad;

import view.ViewDimensions;

import view.game.StarlingGameView;

public class MovableUnitDeadController extends MovableUnitStateController {
    public function MovableUnitDeadController(gameView:StarlingGameView, gameModel:GameModel,data:MovableUnitStateControllerData) {
        super(gameView, gameModel, data);
    }

    override public function start():void
    {
        //var blackScreen:Quad = new Quad(ViewDimensions.APPLICATION.width, ViewDimensions.APPLICATION.height, 0x000000);
        //blackScreen.alpha = 0.3;

        //_gameView.addChild(blackScreen);
    }

    override public function reactOnSignal(signal:Signal):void
    {
        super.reactOnSignal(signal);
    }
}
}
