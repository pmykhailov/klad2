/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climbidle {
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;

import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitActivateClimbIdleCommand extends MovableUnitActivateStateBaseCommand
{
    public function MovableUnitActivateClimbIdleCommand() {
        super();
    }

    override public function execute():void
    {
        moveableUnitView.startAnimation(MovableUnitAnimationsEnum.CLIMB);
        moveableUnitView.pauseAnimation();

        super.execute();
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_CLIMB_IDLE;
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitClimbIdleController;
    }
}
}
