/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.climbidle
{
    import controller.states.movableunit.base.state_controller.MovableUnitStateController;
    import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.ui.Keyboard;

import model.GameModel;

import view.moveble_units.character.MovableUnitsStateEnum;

import view.game.StarlingGameView;


    public class MovableUnitClimbIdleController extends MovableUnitStateController
    {
        public function MovableUnitClimbIdleController(gameView:StarlingGameView, gameModel:GameModel, data:MovableUnitStateControllerData)
        {
            super(gameView, gameModel, data);
        }


        override public function reactOnSignal(signal:Signal):void {
            super.reactOnSignal(signal);

            if (signal.type == Signal.TYPE_KEY_DOWN)
            {
                if (signal.data == Keyboard.UP || signal.data == Keyboard.DOWN)
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB);
                }else
                if (signal.data == Keyboard.LEFT || signal.data == Keyboard.RIGHT)
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB_MOVE);
                }
            }
        }
    }
}
