/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.05.15
 * Time: 13:58
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.fall
{
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.geom.Point;
import flash.ui.Keyboard;

import misc.LogicUtils;

import model.GameModel;

import model.map.cell.CellModel;

import model.map.MapModel;
import model.map.cell.CellTypeEnum;

import starling.events.Event;

import misc.LogicUtils;

import view.moveble_units.character.MovableUnitsStateEnum;

import view.moveble_units.character.CharacterView;
import view.game.StarlingGameView;
import view.map.MapView;
import view.map.item.ICellView;
import view.map.item.simple.SimpleCellView;

public class MovableUnitFallController extends MovableUnitStateController
    {
        private var _newCharacterCoordinates:Point;

        public function MovableUnitFallController(gameView:StarlingGameView, gameModel:GameModel,data:MovableUnitStateControllerData)
        {
            super(gameView, gameModel, data);


            _newCharacterCoordinates  = new Point();
        }

        override protected function perform():void
        {
            super.perform();

            _newCharacterCoordinates.x = moveableUnitView.x + _moveVector.x;
            _newCharacterCoordinates.y = moveableUnitView.y + _moveVector.y;

            var characterCells:Vector.<CellModel> = LogicUtils.getCharacterCells(_newCharacterCoordinates, mapModel);
            var cellModelForChecking: CellModel;

            if (characterCells[3])
            {
                cellModelForChecking = characterCells[3];
            }else
            {
                cellModelForChecking = mapModel.getCell(characterCells[0].row + 1, characterCells[0].col);
            }

            // Stop falling ...
            if (
                    cellModelForChecking.type == CellTypeEnum.TYPE_BRICK ||
                    cellModelForChecking.type == CellTypeEnum.TYPE_CONCRETE ||
                    cellModelForChecking.type == CellTypeEnum.TYPE_LADDER ||
                    cellModelForChecking.type == CellTypeEnum.TYPE_WATTER
                )
            {
                // Snap to last empty cell
                var cellView: ICellView = _gameView.map.getCellView(characterCells[0].row, characterCells[0].col);

                moveableUnitView.x = cellView.x;
                moveableUnitView.y = cellView.y;

                if (cellModelForChecking.type == CellTypeEnum.TYPE_WATTER)
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_DEAD);
                }else
                {
                    _data.changeStateCallback(MovableUnitsStateEnum.STATE_IDLE);
                }

            }else
            // ... or continue falling
            {
                moveableUnitView.x = _newCharacterCoordinates.x;
                moveableUnitView.y = _newCharacterCoordinates.y;
            }
        }

    }
}
