/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.fall {
import controller.states.movableunit.base.MovableUnitDeactivateStateBaseCommand;

import robotlegs.bender.bundles.mvcs.Command;

import starling.core.Starling;

import view.game.StarlingGameView;

import view.moveble_units.common.BaseMovableUnitView;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitDeactivateFallCommand extends MovableUnitDeactivateStateBaseCommand {
    public function MovableUnitDeactivateFallCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        moveableUnitView.removeAnimation();
    }

}
}
