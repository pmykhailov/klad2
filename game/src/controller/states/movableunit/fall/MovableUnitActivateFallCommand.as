/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.fall {
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;

import flash.geom.Point;

import view.moveble_units.character.CharacterView;

import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitActivateFallCommand extends MovableUnitActivateStateBaseCommand {
    public function MovableUnitActivateFallCommand() {
        super();
    }

    override public function execute():void {
        moveableUnitView.startAnimation(MovableUnitAnimationsEnum.FALL);

        super.execute();
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_FALL;
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitFallController;
    }

    override protected function createMoveVector():Point
    {
        return new Point(0,movableUnitManager.speed);
    }

}
}
