/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 29.05.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.states.movableunit.idle {
import controller.managers.base.MoveDirectionsEnum;
import controller.states.movableunit.base.MovableUnitActivateStateBaseCommand;
import view.moveble_units.character.CharacterView;
import view.moveble_units.character.MovableUnitsStateEnum;
import view.moveble_units.common.MovableUnitAnimationsEnum;

public class MovableUnitActivateIdleCommand extends MovableUnitActivateStateBaseCommand {
    public function MovableUnitActivateIdleCommand() {
        super();
    }

    override public function execute():void {
        movableUnitManager.currentMoveDirection = MoveDirectionsEnum.NONE;

        moveableUnitView.startAnimation(MovableUnitAnimationsEnum.MOVE);
        moveableUnitView.pauseAnimation();

        super.execute();
    }

    override protected function getStateControllerClass():Class
    {
        return MovableUnitIdleController;
    }

    override protected function getStateId():String
    {
        return MovableUnitsStateEnum.STATE_IDLE;
    }

}
}
