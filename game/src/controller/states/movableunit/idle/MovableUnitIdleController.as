package controller.states.movableunit.idle {
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.state_controller.MovableUnitStateControllerData;

import flash.ui.Keyboard;
import model.GameModel;
import view.moveble_units.character.MovableUnitsStateEnum;
import view.game.StarlingGameView;

public class MovableUnitIdleController extends MovableUnitStateController {
    public function MovableUnitIdleController(gameView:StarlingGameView, gameModel:GameModel,data:MovableUnitStateControllerData) {
        super(gameView, gameModel, data);
    }

    override public function reactOnSignal(signal:Signal):void
    {
        super.reactOnSignal(signal);

        if (signal.type == Signal.TYPE_KEY_DOWN)
        {
            if (signal.data == Keyboard.UP || signal.data == Keyboard.DOWN)
            {
                _data.changeStateCallback(MovableUnitsStateEnum.STATE_CLIMB);
            }else
            if (signal.data == Keyboard.LEFT || signal.data == Keyboard.RIGHT)
            {
                _data.changeStateCallback(MovableUnitsStateEnum.STATE_MOVE);
            }
        }
    }
}
}
