/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.07.15
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */
package controller.handlers {
import controller.managers.GameManagers;

import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.events.IEventDispatcher;
import flash.ui.Keyboard;

import misc.IDestroyable;

import misc.LogicUtils;
import misc.vo.CellWithMaxContributionVO;

import model.GameModel;
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.map.MapModel;
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;
import model.map.cell.featured.DoorCellModel;
import model.map.cell.featured.ExitCellModel;
import model.map.cell.featured.ScoreItemCellModel;

import sound.GameSoundEnum;
import sound.SoundGroupEnum;

import starling.display.Sprite;
import starling.events.Event;

import treefortress.sound.SoundAS;

import view.events.ViewEvent;
import view.game.StarlingGameView;
import view.map.MapView;
import view.map.item.ICellView;

public class PositionChangeHandler implements IDestroyable{

    private var _gameView:StarlingGameView;
    private var _gameModel:GameModel;
    private var _gameManagers:GameManagers;
    private var _dispatcher:IEventDispatcher;

    public function PositionChangeHandler(gameView:StarlingGameView, gameModel:GameModel, gameManagers:GameManagers, dispatcher:IEventDispatcher) {
        _gameView = gameView;
        _gameModel = gameModel;
        _gameManagers = gameManagers;
        _dispatcher = dispatcher;
    }

    public function start():void {
        _gameView.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
    }

    public function stop():void {
        _gameView.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
    }

    public function destroy():void {
        stop();

        _gameView= null;
        _gameModel = null;
        _gameManagers = null;
        _dispatcher = null;
    }

    private function onEnterFrameHandler(event:Event):void {
        var mapModel:MapModel = _gameModel.mapModel;
        var mapView:MapView = _gameView.map;
        var characterView:Sprite = _gameView.getMovableUnitViewByType(MovableUnitTypeEnum.TYPE_CHARACTER);
        var cellWithMaxContributionVO:CellWithMaxContributionVO = LogicUtils.getCellWithMaxContributionVO(characterView, mapModel, mapView);
        var cellModel:CellModel = cellWithMaxContributionVO.cellModel;

        // Exit level
        if (cellModel is ExitCellModel) {
            stop();

            _dispatcher.dispatchEvent(new ViewEvent(ViewEvent.EXIT_REACHED));
        }

        // Collect scores
        if (cellModel is ScoreItemCellModel) {
            var scoreItemCellModel:ScoreItemCellModel = cellModel as ScoreItemCellModel;

            if (!scoreItemCellModel.collected) {
                SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(GameSoundEnum.COIN);

                scoreItemCellModel.collected = true;

                // Collect key
                if (scoreItemCellModel.hasKey) {
                    _gameModel.keyHasCollected = true;
                }

                var scoreItemCellView:ICellView = mapView.getCellView(scoreItemCellModel.row, scoreItemCellModel.col);
                scoreItemCellView.update(scoreItemCellModel);

                _dispatcher.dispatchEvent(new ViewEvent(ViewEvent.SCORE_ITEM_COLLECTED, scoreItemCellView));

                // Notify that score item was collected
                var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.SCORE_ITEM_COLLECTED);
                GlobalEventDispatcher.instance.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_FROM_GAME, data));
            }
        }


        var doorCellModel:DoorCellModel;

        // Check if next cell is door cell
        if (cellWithMaxContributionVO.contribution >= 95) {

            if (
                    _gameManagers.characterManager.currentMoveDirection == Keyboard.LEFT &&
                    LogicUtils.isInMapRange(cellModel.row, cellModel.col - 1, mapModel) &&
                    (mapModel.getCell(cellModel.row, cellModel.col - 1) is DoorCellModel)
                    ) {
                doorCellModel = mapModel.getCell(cellModel.row, cellModel.col - 1) as DoorCellModel;
            } else if (
                    _gameManagers.characterManager.currentMoveDirection == Keyboard.RIGHT &&
                    LogicUtils.isInMapRange(cellModel.row, cellModel.col + 1, mapModel) &&
                    (mapModel.getCell(cellModel.row, cellModel.col + 1) is DoorCellModel)
                    ) {
                doorCellModel = mapModel.getCell(cellModel.row, cellModel.col + 1) as DoorCellModel;
            }
        }

        if (doorCellModel) {
            // Go through door
            if (
                    (doorCellModel.requireKey && _gameModel.keyHasCollected && !doorCellModel.isOpen) ||
                    (!doorCellModel.requireKey && !doorCellModel.isOpen)
                    ) {
                doorCellModel.isOpen = true;
                doorCellModel.type = CellTypeEnum.TYPE_EMPTY;
                mapView.getCellView(doorCellModel.row, doorCellModel.col).update(doorCellModel);
            }
        }
    }
}
}
