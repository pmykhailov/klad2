/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 10.08.15
 * Time: 17:56
 * To change this template use File | Settings | File Templates.
 */
package controller.commands {
import controller.managers.GameManagers;
import controller.managers.ai.AIManager;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.events.MovableUnitStateEvent;
import controller.states.movableunit.base.vo.MovableUnitStateEventVO;

import misc.AILogicUtils;

import model.config.game.movable_items.base.MovableUnitTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;
import starling.events.Event;

import view.game.StarlingGameView;
import view.moveble_units.character.MovableUnitsStateEnum;

public class AIStateChangeAnalyzerCommand extends Command {


    [Inject]
    public var event:MovableUnitStateEvent;

    [Inject]
    public var context:IContext;

    [Inject]
    public var aiLogicUtils:AILogicUtils;

    public function AIStateChangeAnalyzerCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        if ((event.data as MovableUnitStateEventVO).manager.type == MovableUnitTypeEnum.TYPE_CHARACTER) {
            aiLogicUtils.run();
        }
    }
}
}
