/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.06.15
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */
package controller.commands.key {
import controller.managers.GameManagers;
import controller.states.movableunit.base.Signal;

import flash.events.IEventDispatcher;
import flash.ui.Keyboard;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

import starling.core.Starling;

import view.events.ViewEvent;
import view.game.StarlingGameView;

public class KeyDownCommand extends Command {


    public function KeyDownCommand() {
        super();
    }

    [Inject]
    public var event:ViewEvent;
    [Inject]
    public var context:IContext;
    [Inject]
    public var dispatcher:IEventDispatcher;
    [Inject]
    public var gameManagers:GameManagers;

    protected function get gameView():StarlingGameView {
        return Starling.current.root as StarlingGameView;
    }

    override public function execute():void {
        super.execute();

        var root:StarlingGameView = Starling.current.root as StarlingGameView;
        var keyCode:uint = event.data as uint;

        //trace("KEY DOWN : " + TraceUtils.getKeyName(keyCode));

        if (keyCode == Keyboard.SPACE) {
            gameManagers.characterManager.bulletsManager.addBullet();
        } else {
            gameManagers.characterManager.currentMoveDirection = keyCode;
            gameManagers.characterManager.stateController.reactOnSignal(new Signal(Signal.TYPE_KEY_DOWN, event.data));
        }

    }

}
}
