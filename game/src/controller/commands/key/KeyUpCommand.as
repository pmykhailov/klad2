/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.06.15
 * Time: 16:46
 * To change this template use File | Settings | File Templates.
 */
package controller.commands.key {
import controller.managers.GameManagers;
import controller.states.movableunit.base.Signal;

import flash.events.IEventDispatcher;
import flash.ui.Keyboard;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

import view.events.ViewEvent;

public class KeyUpCommand extends Command {

    public function KeyUpCommand() {
        super();
    }

    [Inject]
    public var event:ViewEvent;
    [Inject]
    public var context:IContext;
    [Inject]
    public var dispatcher:IEventDispatcher;
    [Inject]
    public var gameManagers:GameManagers;

    override public function execute():void {
        super.execute();

        var keyCode:uint = event.data as uint;

        //trace("KEY UP : " + TraceUtils.getKeyName(keyCode));

        if (keyCode != Keyboard.SPACE) {
            gameManagers.characterManager.stateController.reactOnSignal(new Signal(Signal.TYPE_KEY_UP));
        }
    }

}
}
