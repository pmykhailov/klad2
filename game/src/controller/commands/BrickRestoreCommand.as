/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.09.15
 * Time: 10:21
 * To change this template use File | Settings | File Templates.
 */
package controller.commands {
import model.events.GameModelEvent;
import model.map.cell.featured.BrickCellModel;

import robotlegs.bender.bundles.mvcs.Command;

import starling.core.Starling;

import view.game.StarlingGameView;
import view.map.item.complex.BrickCellView;

public class BrickRestoreCommand extends Command {

    [Inject]
    public var event:GameModelEvent;

    public function BrickRestoreCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        var brickCellModel:BrickCellModel = event.data as BrickCellModel;
        var brickCellView: BrickCellView = gameView.map.getCellViewByModel(brickCellModel) as BrickCellView;

        brickCellView.update(brickCellModel);
    }

    protected function get gameView():StarlingGameView
    {
        return Starling.current.root as StarlingGameView;
    }
}
}
