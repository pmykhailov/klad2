/**
 * Created by Pasha on 30.08.2015.
 */
package controller.commands {
import controller.managers.GameManagers;
import controller.states.movableunit.base.events.MovableUnitStateEvent;
import controller.states.movableunit.base.vo.MovableUnitStateEventVO;

import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

import view.moveble_units.character.MovableUnitsStateEnum;

public class SpawnCharacterCommand extends Command {

    [Inject]
    public var gameController:GameManagers;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var gameManagers:GameManagers;

    public function SpawnCharacterCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var vo:MovableUnitStateEventVO;
        vo = new MovableUnitStateEventVO(MovableUnitsStateEnum.STATE_SPAWN, gameController.characterManager);
        dispatcher.dispatchEvent(new MovableUnitStateEvent(MovableUnitStateEvent.STATE_CHANGE, vo));

        gameManagers.characterManager.positionChangeHandler.start();
    }
}
}
