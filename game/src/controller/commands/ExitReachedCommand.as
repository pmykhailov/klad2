/**
 * Created by Pasha on 30.08.2015.
 */
package controller.commands {
import controller.managers.GameManagers;

import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.events.TimerEvent;
import flash.utils.Timer;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

import sound.GameSoundEnum;
import sound.SoundGroupEnum;

import starling.core.Starling;

import treefortress.sound.SoundAS;

import view.game.StarlingGameView;
import view.moveble_units.common.BaseMovableUnitView;

use namespace SoundAS;

public class ExitReachedCommand extends Command {

    public function ExitReachedCommand() {
        super();
    }

    [Inject]
    public var context:IContext;
    [Inject]
    public var gameManagers:GameManagers;
    private var _timer:Timer;

    protected function get gameView():StarlingGameView {
        return Starling.current.root as StarlingGameView;
    }

    override public function execute():void {
        super.execute();

        var i:int;

        context.detain(this);

        _timer = new Timer(1000, 1);
        _timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandler);
        _timer.start();

        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(GameSoundEnum.EXIT);

        // Stop animations
        var movableUnitsViews:Array = gameView.getMovableUnitsViews();
        for (i = 0; i < movableUnitsViews.length; i++) {
            var movableUnitView:BaseMovableUnitView = movableUnitsViews[i];
            movableUnitView.pauseAnimation();
        }

        // Stop state controllers
        gameManagers.characterManager.stateController.stop();
        for (var i:int = 0; i < gameManagers.aiManagers.length; i++) {
            gameManagers.aiManagers[i].stateController.stop();
        }
    }

    private function onTimerCompleteHandler(event:TimerEvent):void {
        context.release(this);

        _timer.removeEventListener(TimerEvent.TIMER_COMPLETE, onTimerCompleteHandler);

        var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.EXIT_REACHED);
        GlobalEventDispatcher.instance.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_FROM_GAME, data));
    }

}
}
