/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.04.15
 * Time: 13:34
 * To change this template use File | Settings | File Templates.
 */
package controller.commands.init {
import controller.commands.AIStateChangeAnalyzerCommand;
import controller.handlers.PositionChangeHandler;
import controller.managers.ai.AIManager;
import controller.managers.pause.PauseManager;
import controller.states.movableunit.base.MovableUnitStateChangeCommand;
import controller.states.movableunit.base.events.MovableUnitStateEvent;
import controller.states.movableunit.base.vo.MovableUnitStateEventVO;
import controller.managers.GameManagers;

import events.ApplicationEvent;

import flash.events.IEventDispatcher;

import misc.AILogicUtils;

import model.GameModel;
import model.config.game.GameConfig;
import model.config.game.movable_items.AIConfig;
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;
import model.map.cell.featured.BrickCellModel;


import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.framework.api.IInjector;

import starling.core.Starling;

import controller.managers.BulletsManager;

import view.events.ViewEvent;

import view.game.StarlingGameView;
import view.moveble_units.character.MovableUnitsStateEnum;

public class InitGameCommand extends Command {

    public static var config:GameConfig;

    [Inject]
    public var injector: IInjector;

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var eventCommandMap: IEventCommandMap;

    public function InitGameCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        // Some default data if game is loaded not from lobby
        if (!config)
        {
            config = new GameConfig();
            config.map = [
                [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,2,2,4,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,0,0,0,0,0,0,0,0,0,0,0,0,4,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
                [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
            ];

            config.rows = config.map.length;
            config.cols = config.map[0].length;

            config.exitCell = new CellModel(CellTypeEnum.TYPE_EXIT, 1, 30);

            config.characterData.spawnCellModel = new CellModel(-1,21,1);

            var aiConfig:AIConfig = new AIConfig();
            aiConfig.type = MovableUnitTypeEnum.TYPE_AI;
            aiConfig.spawnCellModel = new CellModel(-1,1,10);

            config.aisData.push(aiConfig);
            /*
            var aiConfig:AIConfig = new AIConfig();
            aiConfig.type = MovableUnitTypeEnum.TYPE_AI;
            aiConfig.spawnCellModel = new CellModel(-1,3,10);

            config.aisData.push(aiConfig);
            */
        }

        injector.map(GameModel).asSingleton();
        var gameModel: GameModel = injector.getInstance(GameModel);
        gameModel.init(config);

        // Add all bricks to pause manager
        for (var i:int = 0; i<gameModel.mapModel.rows; i++) {
            for (var j:int = 0; j<gameModel.mapModel.cols; j++) {
               if (gameModel.mapModel.getCell(i,j) is BrickCellModel) {
                   PauseManager.instance.bricks.push(gameModel.mapModel.getCell(i,j));
               }
            }
        }


        var gameView:StarlingGameView = Starling.current.root as StarlingGameView;
        gameView.init(gameModel);

        injector.map(GameManagers).asSingleton();
        var gameManagers: GameManagers = injector.getInstance(GameManagers);
        gameManagers.init(gameModel, gameView);

        gameManagers.characterManager.positionChangeHandler = new PositionChangeHandler(gameView, gameModel, gameManagers, dispatcher);
        gameManagers.characterManager.bulletsManager = new BulletsManager(gameView, gameModel, dispatcher);

        PauseManager.instance.bulletsManager = gameManagers.characterManager.bulletsManager;

        // Move ai logic setup
        injector.map(AILogicUtils).asSingleton();
        var aiLogicUtils:AILogicUtils = injector.getInstance(AILogicUtils) as AILogicUtils;
        aiLogicUtils.gameManagers = gameManagers;
        aiLogicUtils.gameModel = gameModel;
        aiLogicUtils.dispatcher = dispatcher;
        aiLogicUtils.run();

        PauseManager.instance.aiLogicUtils = aiLogicUtils;

        // AIStateChangeAnalyzerCommand mapping MUST GO BEFORE MovableUnitStateChangeCommand mapping
        // MovableUnitStateChangeCommand notifys lobby that character is dead. If player has no more lives
        // game unloads and robotlegs destroys. In this case command AIStateChangeAnalyzerCommand will be
        // executed when robotlegs is destroyed.
        // Thats why order of following two mappings to MovableUnitStateEvent.STATE_CHANGE is important

        eventCommandMap.map(MovableUnitStateEvent.STATE_CHANGE).toCommand(AIStateChangeAnalyzerCommand);
        eventCommandMap.map(MovableUnitStateEvent.STATE_CHANGE).toCommand(MovableUnitStateChangeCommand);

        // Character setup
        var vo:MovableUnitStateEventVO;

        // Spawn character
        dispatcher.dispatchEvent(new ViewEvent(ViewEvent.SPAWN_CHARACTER));

        // AI setup
        var aiManagers:Vector.<AIManager> = gameManagers.aiManagers;
        var aiManager:AIManager;

        for (var i:int = 0; i < aiManagers.length; i++) {
            aiManager = aiManagers[i];
            vo = new MovableUnitStateEventVO(MovableUnitsStateEnum.STATE_SPAWN, aiManager);
            dispatcher.dispatchEvent(new MovableUnitStateEvent(MovableUnitStateEvent.STATE_CHANGE, vo));
        }

        dispatcher.dispatchEvent(new ApplicationEvent(ApplicationEvent.INIT_COMPLETE));
    }
}
}
