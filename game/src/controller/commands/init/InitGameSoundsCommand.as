package controller.commands.init {

    import flash.events.IEventDispatcher;
import flash.system.ApplicationDomain;
import flash.utils.getDefinitionByName;

import utils.ClassUtils;

    import robotlegs.bender.bundles.mvcs.Command;

    import sound.GameSoundEnum;
    import sound.SoundGroupEnum;

    import treefortress.sound.SoundAS;

    public class InitGameSoundsCommand extends Command {

        [Inject]
        public var dispatcher: IEventDispatcher;

        override public function execute(): void {

            coin;
            exit;
            shoot;
            lose;
            walk;

            background;

            trace("InitGameSoundsCommand");

            var sounds: Array = GameSoundEnum.ALL_FX;
            var n: int = sounds.length;
            for (var i: int = 0; i < n; i++) {
                _addSoundToGroup(sounds[i], SoundGroupEnum.GROUP_SFX);
            }


            _addSoundToGroup(GameSoundEnum.BACKGROUND, SoundGroupEnum.GROUP_MUSIC);

            // If sounds volumes inited in lobby unfortunatly have to do such thing
            SoundAS.group(SoundGroupEnum.GROUP_MUSIC).masterVolume = SoundAS.group(SoundGroupEnum.GROUP_MUSIC).masterVolume;
            SoundAS.group(SoundGroupEnum.GROUP_SFX).masterVolume = SoundAS.group(SoundGroupEnum.GROUP_SFX).masterVolume;

            SoundAS.group( SoundGroupEnum.GROUP_MUSIC).playLoop(GameSoundEnum.BACKGROUND);
        }


        private function _addSoundToGroup(soundClassName: String, group:String): void {

            var SoundClass:Class = ApplicationDomain.currentDomain.getDefinition(soundClassName) as Class;
            SoundAS.group(group).addSound(soundClassName, new SoundClass());
        }

    }
}
