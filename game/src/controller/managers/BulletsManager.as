/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.07.15
 * Time: 12:48
 * To change this template use File | Settings | File Templates.
 */
package controller.managers {
import controller.managers.pause.IPausable;

import flash.events.IEventDispatcher;
import flash.ui.Keyboard;

import misc.IDestroyable;

import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.map.cell.CellTypeEnum;

import sound.GameSoundEnum;
import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import view.bullets.*;

import misc.LogicUtils;

import model.GameModel;
import model.map.cell.CellModel;
import model.map.cell.featured.BrickCellModel;

import starling.events.Event;

import view.ViewDimensions;
import view.events.ViewEvent;
import view.map.item.ICellView;
import view.moveble_units.character.CharacterView;
import view.game.StarlingGameView;

public class BulletsManager implements IPausable, IDestroyable{

    private var _bullets: Vector.<Bullet>;
    private var _gameModel: GameModel;
    private var _gameView: StarlingGameView;
    private var _dispatcher:IEventDispatcher;

    public function BulletsManager(gameView:StarlingGameView, gameModel:GameModel, dispatcher:IEventDispatcher) {
        _gameView = gameView;
        _gameModel = gameModel;
        _dispatcher = dispatcher;

        _bullets = new Vector.<Bullet>();
    }

    public function start():void
    {
        _gameView.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
    }

    public function stop():void
    {
        _gameView.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
    }

    private function onEnterFrameHandler(event:Event):void
    {
        update();
    }

    public function pause():void {
        stop();
    }

    public function resume():void {
        start();
    }

    public function destroy():void {
        stop();

        _bullets = null;
        _gameModel = null;
        _gameView = null;
        _dispatcher = null;
    }

    public function update():void {

        var hasNotActiveBullets:Boolean;
        var i:int;

        for (i = 0; i < _bullets.length; i++) {
            var bullet:Bullet = _bullets[i];
            var bulletCellModel: CellModel;

            bullet.x += bullet.speed.x;
            bullet.y += bullet.speed.y;

            bulletCellModel = LogicUtils.getCellModelByPoint(bullet, _gameModel.mapModel);

            if (
                    bulletCellModel.type == CellTypeEnum.TYPE_CONCRETE ||
                    bulletCellModel.type == CellTypeEnum.TYPE_BRICK ||
                    bulletCellModel.type == CellTypeEnum.TYPE_WATTER
                )
            {
                bullet.isActive = false;
                hasNotActiveBullets = true;

                if (bulletCellModel.type == CellTypeEnum.TYPE_BRICK && !(bulletCellModel as BrickCellModel).isHited)
                {
                    (bulletCellModel as BrickCellModel).hit();
                    var cellView:ICellView = _gameView.map.getCellView(bulletCellModel.row, bulletCellModel.col);
                    cellView.update(bulletCellModel);
                    _dispatcher.dispatchEvent(new ViewEvent(ViewEvent.BULLET_HITS_BRICK, cellView));
                }
            }
        }

        // TO DEBUG THIS
        while (hasNotActiveBullets)
        {
            for (i = 0; i < _bullets.length; i++)
            {
                if (!_bullets[i].isActive)
                {
                    _gameView.bulletsLayer.removeChild(_bullets[i]);

                    _bullets[i].dispose();

                    _bullets.splice(i,1);
                    i--;
                }
            }

            hasNotActiveBullets = false;
            for (i = 0; i < _bullets.length; i++)
            {
                if (!_bullets[i].isActive)
                {
                    hasNotActiveBullets = true;
                }
            }
        }

        if (_bullets.length == 0) {
            stop();
        }
    }

    public function addBullet():void {

        if (_bullets.length >= _gameModel.maxBullets) return;

        var characterView: CharacterView = _gameView.getMovableUnitViewByType(MovableUnitTypeEnum.TYPE_CHARACTER) as CharacterView;
        var bullet: Bullet = new Bullet();

        bullet.x = characterView.x + ViewDimensions.CELL.width / 2;
        bullet.y = characterView.y + ViewDimensions.CELL.height / 2;

        var sign:int = characterView.lookDirection == Keyboard.LEFT ? -1 : 1;
        bullet.speed.x = sign * _gameModel.bulletSpeed;

        _bullets.push(bullet);

        _gameView.bulletsLayer.addChild(bullet);

        if (_bullets.length == 1)
        {
            start();
        }

        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(GameSoundEnum.SHOOT);
    }

}
}
