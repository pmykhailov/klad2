/**
 * Created by Pasha on 25.07.2015.
 */
package controller.managers.character {
import controller.managers.BulletsManager;
import controller.handlers.PositionChangeHandler;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;
import controller.managers.base.IMoveDirectionProvider;
import controller.managers.base.MovableUnitManager;

public class CharacterManager extends MovableUnitManager{

    public var positionChangeHandler: PositionChangeHandler;

    public var bulletsManager: BulletsManager;

    public function CharacterManager() {
    }

    override public function destroy():void {
        super.destroy();

        bulletsManager.destroy();
        positionChangeHandler.destroy();

        bulletsManager = null;
        positionChangeHandler = null;
    }
}
}
