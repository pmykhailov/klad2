/**
 * Created by Pasha on 25.07.2015.
 */
package controller.managers.ai {
import controller.managers.base.*;
import controller.states.movableunit.base.state_controller.MovableUnitStateController;

import misc.Dimensions;

import model.map.cell.CellModel;

public class AIManager extends MovableUnitManager{

    private var _moveDistance:Dimensions;

    public function AIManager():void{
        _moveDistance = new Dimensions(0,0);
    }

    public function get moveDistance():Dimensions {
        return _moveDistance;
    }

    public function set moveDistance(value:Dimensions):void {
        _moveDistance = value;
    }
}
}
