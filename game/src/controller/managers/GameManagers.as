/**
 * Created by Pasha on 25.07.2015.
 */
package controller.managers {
import controller.managers.ai.AIManager;

import misc.IDestroyable;

import model.config.game.movable_items.base.MovableUnitTypeEnum;
import controller.managers.character.CharacterManager;
import controller.managers.base.MovableUnitManager;

import model.GameModel;
import model.ai.AIModel;

import view.game.StarlingGameView;

public class GameManagers implements IDestroyable{

    private var _gameModel:GameModel;
    private var _gameView:StarlingGameView;

    private var _characterManager:CharacterManager;
    private var _aiManagers:Vector.<AIManager>;

    public function GameManagers() {

    }

    public function init(gameModel:GameModel, gameView:StarlingGameView):void {
        _gameModel = gameModel;
        _gameView = gameView;
        initManagers();
    }

    public function destroy():void {

        characterManager.destroy();

        for (var i:int = 0; i < aiManagers.length; i++) {
            aiManagers[i].destroy();
        }

        _gameModel = null;
        _gameView = null;
        _characterManager = null;
        _aiManagers = null;
    }

    private function initManagers():void {
        _characterManager = new CharacterManager();
        _characterManager.type = MovableUnitTypeEnum.TYPE_CHARACTER;
        _characterManager.id = -1;
        _characterManager.spawnCell = _gameModel.characterSpawnCellModel;
        _characterManager.speed = _gameModel.characterSpeed;

        _aiManagers = new Vector.<AIManager>();
        var aiModels:Vector.<AIModel> = _gameModel.aiModels;

        for (var i:int = 0; i < aiModels.length; i++) {
            var aiModel:AIModel = aiModels[i];
            var aiManager:AIManager = new AIManager();

            aiManager.type = MovableUnitTypeEnum.TYPE_AI;
            aiManager.id = aiModel.id;
            aiManager.spawnCell = aiModel.spawnCellModel;
            aiManager.speed = _gameModel.aiSpeed;
            _aiManagers.push(aiManager);
        }
    }

    public function get characterManager():CharacterManager {
        return _characterManager as CharacterManager;
    }

    public function get aiManagers():Vector.<AIManager> {
        return _aiManagers;
    }
}
}
