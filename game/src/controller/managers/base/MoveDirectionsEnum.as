/**
 * Created by Pasha on 25.06.2016.
 */
package controller.managers.base {
import flash.ui.Keyboard;

public class MoveDirectionsEnum {
    public static const NONE:uint = 0;
    public static const LEFT:uint = Keyboard.LEFT;
    public static const RIGHT:uint = Keyboard.RIGHT;
    public static const UP:uint = Keyboard.UP;
    public static const DOWN:uint = Keyboard.DOWN;
}
}
