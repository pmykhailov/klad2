/**
 * Created by Pasha on 25.07.2015.
 */
package controller.managers.base {
public interface IMoveDirectionProvider {
    function get currentMoveDirection():int;
}
}
