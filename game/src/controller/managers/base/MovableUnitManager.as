/**
 * Created by Pasha on 25.07.2015.
 */
package controller.managers.base {
import controller.states.movableunit.base.state_controller.MovableUnitStateController;

import misc.IDestroyable;

import model.map.cell.CellModel;

public class MovableUnitManager implements IMoveDirectionProvider, IDestroyable{

    /** Current state **/
    private var _stateID:String;

    private var _id:int;

    private var _type:String;

    /** Constants from MoveDirectionsEnum **/
    private var _currentMoveDirection:int;

    private var _stateController:MovableUnitStateController;

    private var _spawnCell:CellModel;

    private var _speed:Number;

    public function MovableUnitManager() {
    }

    public function get currentMoveDirection():int {
        return _currentMoveDirection;
    }

    public function set currentMoveDirection(value:int):void {
        _currentMoveDirection = value;
    }

    public function get id():int {
        return _id;
    }

    public function set id(value:int):void {
        _id = value;
    }

    public function get stateController():MovableUnitStateController {
        return _stateController;
    }

    public function set stateController(value:MovableUnitStateController):void {
        _stateController = value;
    }

    public function get stateID():String {
        return _stateID;
    }

    public function set stateID(value:String):void {
        _stateID = value;
    }

    public function get type():String {
        return _type;
    }

    public function set type(value:String):void {
        _type = value;
    }

    public function get spawnCell():CellModel {
        return _spawnCell;
    }

    public function set spawnCell(value:CellModel):void {
        _spawnCell = value;
    }

    public function get speed():Number {
        return _speed;
    }

    public function set speed(value:Number):void {
        _speed = value;
    }

    public function destroy():void {
        _stateController.destroy();
    }
}
}
