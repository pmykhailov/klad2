/**
 * Created by Pasha on 04.07.2016.
 */
package controller.managers.pause {
import controller.managers.BulletsManager;

import misc.IDestroyable;

public class PauseManager implements IPausable, IDestroyable{

    private static var _instance:PauseManager;

    private var _isPaused:Boolean;

    private var _moveController:IPausable;
    private var _fireController:IPausable;
    private var _starlingStageMediator:IPausable;
    private var _aiLogicUtils:IPausable;
    [ArrayElementType("controller.managers.pause.IPausable")]
    private var _stateControllers:Array;
    private var _characterView:IPausable;
    [ArrayElementType("controller.managers.pause.IPausable")]
    private var _aiViews:Array;
    private var _bulletsManager:BulletsManager;
    [ArrayElementType("controller.managers.pause.IPausable")]
    private var _bricks:Array;

    public function PauseManager() {
        _instance = this;
        _stateControllers = [];
        _aiViews = [];
        _bricks = [];
    }

    public static function get instance():PauseManager {
        if (!_instance) _instance = new PauseManager();
        return _instance;
    }

    public function destroy():void {
        _moveController = null;
        _fireController = null;
        _starlingStageMediator = null;
        _aiLogicUtils = null;
        _stateControllers = null;
        _characterView = null;
        _aiViews = null;
        _bulletsManager = null;
        _bricks = null;

        _instance = null;
    }

    public function pause():void {
        var i:int;

        _moveController.pause();
        _fireController.pause();
        _starlingStageMediator.pause();
        _aiLogicUtils.pause();
        for (i = 0; i < _stateControllers.length; i++) {
            _stateControllers[i].pause();
        }
        _characterView.pause();
        for (i = 0; i < _aiViews.length; i++) {
            _aiViews[i].pause();
        }
        _bulletsManager.pause();
        for (i = 0; i < _bricks.length; i++) {
            _bricks[i].pause();
        }

        _isPaused = true;
    }

    public function resume():void {
        var i:int;

        _moveController.resume();
        _fireController.resume();
        _starlingStageMediator.resume();

        // Order is important here. stateControllers then aiLogicUtils
        for (i = 0; i < _stateControllers.length; i++) {
            _stateControllers[i].resume();
        }
        _aiLogicUtils.resume();
        _characterView.resume();
        for (i = 0; i < _aiViews.length; i++) {
            _aiViews[i].resume();
        }
        _bulletsManager.resume();
        for (i = 0; i < _bricks.length; i++) {
            _bricks[i].resume();
        }

        _isPaused = false;
    }

    public function get isPaused():Boolean {
        return _isPaused;
    }

    public function set moveController(value:IPausable):void {
        _moveController = value;
    }

    public function set fireController(value:IPausable):void {
        _fireController = value;
    }

    public function set starlingStageMediator(value:IPausable):void {
        _starlingStageMediator = value;
    }

    public function set aiLogicUtils(value:IPausable):void {
        _aiLogicUtils = value;
    }

    public function addToStateControllers(controller:IPausable):void {
        _stateControllers.push(controller);
    }

    public function removeFromStateControllers(controller:IPausable):void {
        for (var i:int = 0; i < _stateControllers.length; i++) {
            if (_stateControllers[i] == controller) {
                _stateControllers.splice(i,1);
                break;
            }
        }
    }

    public function set characterView(value:IPausable):void {
        _characterView = value;
    }

    public function get aiViews():Array {
        return _aiViews;
    }

    public function set bulletsManager(value:BulletsManager):void {
        _bulletsManager = value;
    }

    public function get bricks():Array {
        return _bricks;
    }
}
}
