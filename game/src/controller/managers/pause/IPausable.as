/**
 * Created by Pasha on 04.07.2016.
 */
package controller.managers.pause {
public interface IPausable {
    function pause():void;
    function resume():void;
}
}
