package {
import controller.commands.BrickRestoreCommand;
import controller.commands.ExitReachedCommand;
import controller.commands.SpawnCharacterCommand;
import controller.commands.init.InitGameCommand;
import controller.commands.init.InitGameSoundsCommand;
import controller.commands.key.KeyDownCommand;
import controller.commands.key.KeyUpCommand;
import controller.states.ai.climbmove.AIActivateClimbMoveCommand;
import controller.states.ai.dead.AIActivateDeadCommand;
import controller.states.ai.move.AIActivateMoveCommand;
import controller.states.character.dead.CharacterActivateDeadCommand;
import controller.states.movableunit.climb.MovableUnitActivateClimbCommand;
import controller.states.movableunit.climb.MovableUnitDeactivateClimbCommand;
import controller.states.movableunit.climbidle.MovableUnitActivateClimbIdleCommand;
import controller.states.movableunit.climbidle.MovableUnitDeactivateClimbIdleCommand;
import controller.states.movableunit.climbmove.MovableUnitActivateClimbMoveCommand;
import controller.states.movableunit.climbmove.MovableUnitDeactivateClimbMoveCommand;
import controller.states.movableunit.dead.MovableUnitActivateDeadCommand;
import controller.states.movableunit.dead.MovableUnitDeactivateDeadCommand;
import controller.states.movableunit.fall.MovableUnitActivateFallCommand;
import controller.states.movableunit.fall.MovableUnitDeactivateFallCommand;
import controller.states.movableunit.idle.MovableUnitActivateIdleCommand;
import controller.states.movableunit.idle.MovableUnitDeactivateIdleCommand;
import controller.states.movableunit.move.MovableUnitActivateMoveCommand;
import controller.states.movableunit.move.MovableUnitDeactivateMoveCommand;
import controller.states.movableunit.spawn.MovableUnitActivateSpawnCommand;
import controller.states.movableunit.spawn.MovableUnitDeactivateSpawnCommand;

import events.ApplicationEvent;

import flash.events.IEventDispatcher;

import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.events.GameModelEvent;

import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IInjector;

import starling.display.Stage;

import view.events.ViewEvent;
import view.game.StarlingGameView;
import view.game.StarlingGameViewMediator;
import view.moveble_units.character.CharacterView;
import view.moveble_units.character.CharacterViewMediator;
import view.moveble_units.character.MovableUnitsStateEnum;
import view.particles_layer.ParticlesLayer;
import view.particles_layer.ParticlesLayerMediator;
import view.stage.controll_keys.StarlingStageMediator;

public class StarlingGameConfig implements IConfig
	{
		[Inject]
		public var mediatorMap:IMediatorMap;

        [Inject]
        public var eventCommandMap: IEventCommandMap;

        [Inject]
        public var injector: IInjector;

        [Inject]
        public var dispatcher: IEventDispatcher;

        public function configure() : void
		{
            mapMediators();
            mapCommands();
            mapInjections();
        }

        private function mapInjections():void {
        }

        private function mapMediators():void {
            mediatorMap.map( StarlingGameView ).toMediator(StarlingGameViewMediator);
            mediatorMap.map( Stage ).toMediator(StarlingStageMediator);
            mediatorMap.map( CharacterView ).toMediator(CharacterViewMediator);
            mediatorMap.map( ParticlesLayer ).toMediator(ParticlesLayerMediator);
        }

        private function mapCommands():void {
            // Character state change commands [BEGIN]
            MovableUnitActivateSpawnCommand;
            MovableUnitDeactivateSpawnCommand;

            MovableUnitActivateIdleCommand;
            MovableUnitDeactivateIdleCommand;

            MovableUnitActivateMoveCommand;
            MovableUnitDeactivateMoveCommand;

            MovableUnitActivateClimbCommand;
            MovableUnitDeactivateClimbCommand;

            MovableUnitActivateClimbIdleCommand;
            MovableUnitDeactivateClimbIdleCommand;

            MovableUnitActivateClimbMoveCommand;
            MovableUnitDeactivateClimbMoveCommand;

            MovableUnitActivateFallCommand;
            MovableUnitDeactivateFallCommand;

            MovableUnitActivateDeadCommand;
            MovableUnitDeactivateDeadCommand;

            // Character specific commands
            CharacterActivateDeadCommand;

            // AI specific commands
            AIActivateMoveCommand;
            AIActivateClimbMoveCommand;
            AIActivateDeadCommand;

            var states:Array = MovableUnitsStateEnum.STATES_ALL;
            var types:Array = MovableUnitTypeEnum.TYPES_ALL;

            for (var i:int = 0; i < states.length; i++) {
                for (var j:int = 0; j < types.length; j++) {
                    eventCommandMap.map(ViewEvent.getActivateMovableUnitStateEventName(states[i], types[j])).toCommand(ViewEvent.getActivateMovableUnitStateCommandClass(states[i], types[j]));
                    eventCommandMap.map(ViewEvent.getDeactivateMovableUnitStateEventName(states[i], types[j])).toCommand(ViewEvent.getDeactivateMovableUnitStateCommandClass(states[i], types[j]));
                }
            }

            // Character state change commands [END]

            eventCommandMap.map(ViewEvent.KEY_DOWN).toCommand(KeyDownCommand);
            eventCommandMap.map(ViewEvent.KEY_UP).toCommand(KeyUpCommand);

            eventCommandMap.map(ViewEvent.SPAWN_CHARACTER).toCommand(SpawnCharacterCommand);
            eventCommandMap.map(ViewEvent.EXIT_REACHED).toCommand(ExitReachedCommand);

            eventCommandMap.map(ApplicationEvent.INIT).toCommand(InitGameSoundsCommand);
            eventCommandMap.map(ApplicationEvent.INIT).toCommand(InitGameCommand);

            eventCommandMap.map(GameModelEvent.BRICK_RESTORE).toCommand(BrickRestoreCommand);
        }

	}
}
