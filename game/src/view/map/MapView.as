/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.04.15
 * Time: 18:45
 * To change this template use File | Settings | File Templates.
 */
package view.map {
import misc.IDestroyable;
import misc.MapUtils;

import model.map.cell.CellModel;
import model.map.MapModel;

import starling.display.DisplayObject;

import starling.display.Sprite;

import view.map.item.CellsViewFactory;
import view.map.item.ICellView;
import view.map.item.simple.View_EmptyCell;

public class MapView extends Sprite implements IDestroyable{

    private var _cells:Array;

    public function MapView() {
        super();
    }

    public function init(mapModel:MapModel, exitCell:CellModel):void
    {
        var cols:int = mapModel.cols;
        var rows:int = mapModel.rows;
        var cellView:ICellView;
        var cellModel:CellModel;

        _cells = [];

        for (var i:int = 0; i < rows; i++)
        {
            _cells[i] = [];

            for (var j:int = 0; j < cols; j++)
            {
                cellModel = mapModel.getCell(i, j) as CellModel;

                cellView = CellsViewFactory.getCellView(cellModel);

                cellView.x = j * cellView.width;
                cellView.y = i * cellView.height;

                cellView.row = i;
                cellView.col = j;

                _cells[i][j] = cellView;

                addChild(cellView as DisplayObject);
            }
        }

        // Detect top water cells
        MapUtils.setTopWaterCells(_cells, rows, cols);

        // Exit cell
        var exitCellRow:int = exitCell.row;
        var exitCellCol:int =exitCell.col;
        if (exitCellRow < 0 || exitCellRow >= rows) {
            _cells[exitCellRow] = [];
        }
        cellView = new View_EmptyCell();
        _cells[exitCellRow][exitCellCol] = cellView;
        cellView.x = exitCellCol * cellView.width;
        cellView.y = exitCellRow * cellView.height;
    }

    public function getCellView(row:int, col:int):ICellView
    {
        return _cells[row][col] as ICellView;
    }

    public function getCellViewByModel(cellModel:CellModel): ICellView
    {
        return getCellView(cellModel.row, cellModel.col);
    }

    public function destroy():void {

        var rows:int = _cells.length;
        var cols:int = _cells[0].length;

        for (var i:int = 0; i < rows; i++)
        {
            for (var j:int = 0; j < cols; j++) {
                (_cells[i][j] as ICellView).destroy();
            }
        }
    }
}
}
