/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
package view.map.item.complex {
import model.map.cell.CellModel;
import model.map.cell.featured.DoorCellModel;

import starling.display.Image;

import view.map.item.CellTextures;

public class DoorItemCellView extends ComplexCellView {

    private var _open:Image;
    private var _closed:Image;

    public function DoorItemCellView() {
        super();

        _open = new Image(CellTextures.instance.openDoorTexture);
        _closed = new Image(CellTextures.instance.closeDoorTexture);

        addChild(_closed);
    }

    override public function update(cellModel:CellModel):void {
        var doorCellModel:DoorCellModel = cellModel as DoorCellModel;

        if (doorCellModel.isOpen && contains(_closed))
        {
            removeChild(_closed);
            addChild(_open);
        }
    }
}
}
