/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 10:42
 * To change this template use File | Settings | File Templates.
 */
package view.map.item.complex
{
    import model.map.cell.CellModel;
import model.map.cell.featured.ScoreItemCellModel;

import starling.display.Image;

import view.map.item.CellTextures;

public class ScoreItemCellView extends ComplexCellView
    {
        private var _background:Image;
        private var _scoreItem:Image;
        private var _key:Image;

        public function ScoreItemCellView()
        {
            super();

            _background = new Image(CellTextures.instance.emptyCellTexture);
            _scoreItem = new Image(CellTextures.instance.scoreItemTexture);
            _key = new Image(CellTextures.instance.keyTexture);

            addChild(_background);
            addChild(_scoreItem);
        }

        override public function update(cellModel:CellModel):void
        {
            super.update(cellModel);

            var scoreItemCellModel:ScoreItemCellModel = cellModel as ScoreItemCellModel;
            if (scoreItemCellModel.collected && contains(_scoreItem))
            {
                removeChild(_scoreItem);
            }

            if (scoreItemCellModel.hasKey && scoreItemCellModel.collected && !contains(_key))
            {
                addChild(_key);
            }
        }
}
}
