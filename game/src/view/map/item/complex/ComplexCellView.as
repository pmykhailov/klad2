/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
package view.map.item.complex
{
import model.map.cell.CellModel;

import starling.display.Sprite;

    import view.map.item.ICellView;

    public class ComplexCellView extends Sprite implements ICellView
    {

        private var _row:int;
        private var _col:int;

        public function ComplexCellView()
        {
        }

        public function get row():int
        {
            return _row;
        }

        public function set col(value:int):void
        {
            _col = value;
        }

        public function set row(value:int):void
        {
            _row = value;
        }

        public function get col():int
        {
            return _col;
        }

        public function update(cellModel:CellModel):void
        {
        }

        public function destroy():void {
        }
    }
}
