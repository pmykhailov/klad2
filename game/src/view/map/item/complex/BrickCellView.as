/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 11:21
 * To change this template use File | Settings | File Templates.
 */
package view.map.item.complex {
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;
import model.map.cell.featured.BrickCellModel;
import model.map.cell.featured.DoorCellModel;

import starling.display.Image;

import view.map.item.CellTextures;

public class BrickCellView extends ComplexCellView {

    private var _image:Image;

    public function BrickCellView() {
        super();

        _image = new Image(CellTextures.instance.brickCellTexture);
        addChild(_image);
    }

    override public function update(cellModel:CellModel):void {
        var brickCellModel:BrickCellModel = cellModel as BrickCellModel;

        if (brickCellModel.isHited)
        {
            if (brickCellModel.restorePhaseCurrent == 0) {
                _image.alpha = 0;
            }

            //brickCellModel.restorePhaseTotal
            switch (brickCellModel.restorePhaseCurrent)
            {
                case 1:
                    _image.alpha = 0.25;
                break;

                case 2:
                    _image.alpha = 0.5;
                break;

                case 3:
                    _image.alpha = 0.73;
                break;
            }
        }else
        if (brickCellModel.currentHitCount > 0)
        {
            _image.alpha = 1 - 0.1 * brickCellModel.currentHitCount;
        }else
        {
            _image.alpha = 1;
        }
    }
}
}
