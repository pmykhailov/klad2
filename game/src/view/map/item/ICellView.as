/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 10:28
 * To change this template use File | Settings | File Templates.
 */
package view.map.item
{
import misc.IDestroyable;

import model.map.cell.CellModel;

public interface ICellView extends IDestroyable
    {
        function get x():Number;

        function set x(value:Number):void

        function get y():Number;

        function set y(value:Number):void

        function get width():Number;

        function set width(value:Number):void

        function get height():Number;

        function set height(value:Number):void

        function get row():int;

        function set row(value:int):void;

        function get col():int;

        function set col(value:int):void;

        function update(cellModel:CellModel):void

    }
}
