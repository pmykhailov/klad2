/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.04.15
 * Time: 15:08
 * To change this template use File | Settings | File Templates.
 */
package view.map.item.simple {
import model.map.cell.CellModel;

import view.map.item.*;

import starling.display.Image;
import starling.textures.Texture;

public class SimpleCellView extends Image implements ICellView {

    private var _row:int;
    private var _col:int;

    public function SimpleCellView(texture:Texture) {
        super(texture);
    }

    public function get row():int {
        return _row;
    }

    public function set col(value:int):void {
        _col = value;
    }

    public function set row(value:int):void {
        _row = value;
    }

    public function get col():int {
        return _col;
    }

    public function update(cellModel:CellModel):void
    {
    }

    public function destroy():void {
    }
}
}
