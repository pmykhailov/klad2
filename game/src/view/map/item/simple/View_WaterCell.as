/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 20.04.15
 * Time: 14:06
 * To change this template use File | Settings | File Templates.
 */
package view.map.item.simple {
import view.map.item.*;

import flash.display.BitmapData;

import starling.display.Image;
import starling.display.Quad;
import starling.textures.Texture;

public class View_WaterCell extends SimpleCellView {
    public function View_WaterCell() {
        super(getRandomTexture(CellTextures.instance.waterCellTextures));
    }

    public function set isWaterTop(value:Boolean):void {
        texture = value ? getRandomTexture(CellTextures.instance.waterTopCellTextures) : getRandomTexture(CellTextures.instance.waterCellTextures);
    }

    private function getRandomTexture(textures:Array):Texture {
        var rndIndex:int = int(textures.length * Math.random());
        return textures[rndIndex];
    }
}
}
