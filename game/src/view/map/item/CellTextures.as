/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 21.04.15
 * Time: 14:13
 * To change this template use File | Settings | File Templates.
 */
package view.map.item {
import flash.display.BitmapData;
import flash.utils.getDefinitionByName;

import misc.IDestroyable;

import starling.textures.Texture;

public class CellTextures implements IDestroyable{


    private static var _instance:CellTextures;

    public var emptyCellTexture:Texture;
    public var wallCellTexture:Texture;
    public var brickCellTexture:Texture;
    public var ladderCellTexture:Texture;
    public var woodCellTexture:Texture;
    public var waterCellTextures:Array;
    public var waterTopCellTextures:Array;
    public var scoreItemTexture:Texture;
    public var keyTexture:Texture;
    public var openDoorTexture:Texture;
    public var closeDoorTexture:Texture;

    public function CellTextures() {
        init();
    }

    private function init():void {
        var bd:BitmapData;
        var i:int;

        bd = new EmtyCellBitmapData();
        emptyCellTexture = Texture.fromBitmapData(bd);

        bd = new WallCellBitmapData();
        wallCellTexture = Texture.fromBitmapData(bd);

        bd = new BrickCellBitmapData();
        brickCellTexture = Texture.fromBitmapData(bd);

        bd = new LadderCellBitmapData();
        ladderCellTexture = Texture.fromBitmapData(bd);

        bd = new WoodCellBitmapData();
        woodCellTexture = Texture.fromBitmapData(bd);

        WaterCellBitmapData_0;
        WaterCellBitmapData_1;
        WaterCellBitmapData_2;
        WaterCellBitmapData_3;
        WaterCellBitmapData_4;
        WaterCellBitmapData_5;
        WaterCellBitmapData_6;
        WaterCellBitmapData_7;
        WaterCellBitmapData_8;
        waterCellTextures = [];
        for (i = 0; i < 9;i++) {
            var WaterCellBitmapDataClass:Class = getDefinitionByName("WaterCellBitmapData_" + i) as Class;
            bd = new WaterCellBitmapDataClass();
            waterCellTextures.push(Texture.fromBitmapData(bd));
        }

        WaterTopCellBitmapData_0;
        WaterTopCellBitmapData_1;
        WaterTopCellBitmapData_2;
        waterTopCellTextures = [];
        for (i = 1; i < 3;i++) {
            var WaterTopCellBitmapDataClass:Class = getDefinitionByName("WaterTopCellBitmapData_" + i) as Class;
            bd = new WaterTopCellBitmapDataClass();
            waterTopCellTextures.push(Texture.fromBitmapData(bd));
        }

        bd = new ScoreItemBitmapData();
        scoreItemTexture = Texture.fromBitmapData(bd);

        bd = new KeyBitmapData();
        keyTexture = Texture.fromBitmapData(bd);

        bd = new OpenedDoorBitmapData();
        openDoorTexture = Texture.fromBitmapData(bd);

        bd = new ClosedDoorBitmapData();
        closeDoorTexture = Texture.fromBitmapData(bd);
    }


    public static function get instance():CellTextures {

        if (!_instance) _instance = new CellTextures();

        return _instance;
    }

    public function destroy():void {
        _instance = null;

        emptyCellTexture.dispose();
        wallCellTexture.dispose();
        brickCellTexture.dispose();
        ladderCellTexture.dispose();
        woodCellTexture.dispose();

        var i:int;
        for (i = 0; i < waterCellTextures.length; i++) {
            waterCellTextures[i].dispose();
        }

        for (i = 0; i < waterTopCellTextures.length; i++) {
            waterTopCellTextures[i].dispose();
        }

        scoreItemTexture.dispose();
        keyTexture.dispose();
        openDoorTexture.dispose();
        closeDoorTexture.dispose();
    }
}
}
