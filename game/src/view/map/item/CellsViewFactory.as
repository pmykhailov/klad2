/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 20.04.15
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */
package view.map.item {
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;
import model.map.cell.featured.DoorCellModel;
import model.map.cell.featured.ScoreItemCellModel;

import starling.display.DisplayObject;
import starling.display.DisplayObjectContainer;

import view.map.item.complex.BrickCellView;

import view.map.item.complex.ComplexCellView;
import view.map.item.complex.DoorItemCellView;
import view.map.item.complex.ScoreItemCellView;

import view.map.item.simple.View_EmptyCell;
import view.map.item.simple.View_LadderCell;
import view.map.item.simple.View_WallCell;
import view.map.item.simple.View_WaterCell;
import view.map.item.simple.View_WoodCell;

public class CellsViewFactory {
    public function CellsViewFactory() {
    }

    public static function getCellView(cellModel:CellModel):ICellView
    {
        if (cellModel is ScoreItemCellModel)
        {
            var scoreItemCellView:ScoreItemCellView = new ScoreItemCellView();
            scoreItemCellView.update(cellModel);
            return scoreItemCellView;
        }else
        if (cellModel is DoorCellModel)
        {
            var doorItemCellView:DoorItemCellView = new DoorItemCellView();
            doorItemCellView.update(cellModel);
            return doorItemCellView;
        }else
        if (cellModel.type == CellTypeEnum.TYPE_BRICK)
        {
            return new BrickCellView();
        }else
        {
            switch (cellModel.type)
            {
                case CellTypeEnum.TYPE_EMPTY:
                    return new View_EmptyCell();
                    break;

                case CellTypeEnum.TYPE_CONCRETE:
                    return new View_WallCell();
                    break;

                case CellTypeEnum.TYPE_WOOD:
                    return new View_WoodCell();
                    break;

                case CellTypeEnum.TYPE_LADDER:
                    return new View_LadderCell();
                    break;

                case CellTypeEnum.TYPE_WATTER:
                    return new View_WaterCell();
                    break;
            }
        }

        return null;
    }
}
}
