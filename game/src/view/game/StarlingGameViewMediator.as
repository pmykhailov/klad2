package view.game {
import events.ApplicationEvent;
import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherEvent;

import flash.events.Event;

import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;

import starling.events.Event;

import view.events.ViewEvent;

/**
	 * @author jamieowen
	 */
	public class StarlingGameViewMediator extends StarlingMediator
	{
		
		public function StarlingGameViewMediator()
		{
			
		}
		
		override public function initialize():void
		{
			super.initialize();
			trace( "StarlingGameViewMediator initialised");

            viewComponent.addEventListener(starling.events.Event.ROOT_CREATED, onRootCreatedHandler);
		}

		override public function destroy():void
		{
			super.destroy();
			trace( this + ", " + "destroy()" );
		}

        private function onRootCreatedHandler(event: starling.events.Event):void
        {

            trace( "StarlingGameViewMediator onRootCreatedHandler");

            viewComponent.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreatedHandler);

            dispatch(new ApplicationEvent(ApplicationEvent.INIT));
        }
	}
}
