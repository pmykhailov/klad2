package view.game {
import controller.managers.pause.PauseManager;

import misc.IDestroyable;

import model.GameModel;
import model.ai.AIModel;
import model.config.game.movable_items.base.MovableUnitTypeEnum;

import starling.display.Sprite;

import view.map.MapView;
import view.moveble_units.ai.AIView;
import view.moveble_units.character.CharacterView;
import view.moveble_units.common.BaseMovableUnitView;
import view.particles_layer.ParticlesLayer;

public class StarlingGameView extends Sprite implements IDestroyable {

    private var _character:CharacterView;
    private var _ais:Vector.<AIView>;
    private var _particlesLayer:ParticlesLayer;

    private var _map:MapView;

    public function StarlingGameView() {
        super();
    }

    public function get map():MapView {
        return _map;
    }

    private var _bulletsLayer:Sprite;

    public function get bulletsLayer():Sprite {
        return _bulletsLayer;
    }

    public function init(gameModel:GameModel):void {
        _map = new MapView();
        _map.init(gameModel.mapModel, gameModel.exitCell);
        addChild(_map);

        _character = new CharacterView();
        addChild(_character);
        PauseManager.instance.characterView = _character;

        var aiModels:Vector.<AIModel> = gameModel.aiModels;
        _ais = new Vector.<AIView>();
        for (var i:int = 0; i < aiModels.length; i++) {
            var ai:AIView = new AIView(aiModels[i].id);
            _ais.push(ai);
            addChild(ai);

            PauseManager.instance.aiViews.push(ai);
        }

        _bulletsLayer = new Sprite();
        addChild(_bulletsLayer);

        _particlesLayer = new ParticlesLayer();
        addChild(_particlesLayer);
    }

    public function getMovableUnitViewByType(type:String, id:int = -1):BaseMovableUnitView {
        switch (type) {
            case MovableUnitTypeEnum.TYPE_CHARACTER:
                return _character;
                break;

            case MovableUnitTypeEnum.TYPE_AI:
                return _ais[id];
                break;

        }

        return null;
    }

    public function getMovableUnitsViews():Array/*BaseMovableUnitView*/ {
        var res:Array = [];
        for (var i:int = 0; i < _ais.length; i++) {
            res.push(_ais[i]);
        }
        res.push(_character);

        return res;
    }

    public function destroy():void {
        _map.destroy();
    }
}
}
