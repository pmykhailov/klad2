/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.05.15
 * Time: 11:25
 * To change this template use File | Settings | File Templates.
 */
package view {
import misc.Dimensions;

public class ViewDimensions {

    public static const APPLICATION:Dimensions = new Dimensions(1280,920);

    public static const CELL:Dimensions = new Dimensions(40,40);
    public static const CHARACTER:Dimensions = new Dimensions(40,40);

    public function ViewDimensions() {
    }
}
}
