/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.04.15
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
package view.events {

import model.config.game.movable_items.base.MovableUnitTypeEnum;

import flash.events.Event;
import flash.utils.getDefinitionByName;

public class ViewEvent extends Event {

    public static const KEY_DOWN:String = "viewEventKeyDown";
    public static const KEY_UP:String = "viewEventKeyUp";

    private static const ACTIVATE:String = "Activate";
    private static const DEACTIVATE:String = "Deactivate";

    public static const SPAWN_CHARACTER:String = "viewEventSpawnCharacter";
    public static const BULLET_HITS_BRICK:String = "viewEventBulletHitsBrick";
    public static const SCORE_ITEM_COLLECTED:String = "viewEventScoreItemCollected";

    public static const EXIT_REACHED:String = "viewEventExitReached";

    private var _data:Object;

    public function ViewEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);

        _data = data;
    }

    override public function clone():Event {
        return new ViewEvent(type, _data, bubbles, cancelable);
    }

    public function get data():Object {
        return _data;
    }

    public static function getActivateMovableUnitStateEventName(state:String, type:String):String
    {
        return getMovableUnitStateEventName(ACTIVATE, state, type);
    }

    public static function getDeactivateMovableUnitStateEventName(state:String, type:String):String
    {
        return getMovableUnitStateEventName(DEACTIVATE, state, type);
    }

    public static function getActivateMovableUnitStateCommandClass(state:String, type:String):Class
    {
        return getPhaseMovableUnitStateCommandClass(ACTIVATE, state, type);
    }

    public static function getDeactivateMovableUnitStateCommandClass(state:String, type:String):Class
    {
        return getPhaseMovableUnitStateCommandClass(DEACTIVATE, state, type);
    }

    public static function getMovableUnitStateEventName(phase:String, state:String, type:String):String
    {
        return type + phase + state + "Event";
    }

    public static function getPhaseMovableUnitStateCommandClass(phase:String, state:String, type:String):Class
    {
        var eventName:String = type + phase + state + "Command";
        var name:String = "controller.states." + type.toLowerCase() + "." + state.toLowerCase() + "." + eventName;
        var ResultClass:Class;

        try
        {
            ResultClass = getDefinitionByName(name) as Class;
        }catch(e:Error)
        {
            // Class doesn't exist
        }


        if (!ResultClass && type != MovableUnitTypeEnum.TYPE_GENERAL)
        {
            return getPhaseMovableUnitStateCommandClass(phase, state, MovableUnitTypeEnum.TYPE_GENERAL);
        }

        return ResultClass;
    }


}
}
