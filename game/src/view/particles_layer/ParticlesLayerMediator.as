package view.particles_layer {
import events.ApplicationEvent;
import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherEvent;

import flash.events.Event;

import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;

import starling.core.Starling;

import starling.events.Event;
import starling.extensions.PDParticleSystem;
import starling.textures.Texture;

import view.ViewDimensions;

import view.events.ViewEvent;
import view.map.item.ICellView;

/**
	 * @author jamieowen
	 */
	public class ParticlesLayerMediator extends StarlingMediator
	{
        [Embed(source = "/../res/particle/brick_hit/texture.png")]
        private const StarParticle:Class;

        [Embed(source="/../res/particle/brick_hit/particle.pex", mimeType="application/octet-stream")]
        public const StarConfig:Class;

        [Embed(source = "/../res/particle/score_collected/texture.png")]
        private const StarParticle2:Class;

        [Embed(source="/../res/particle/score_collected/particle.pex", mimeType="application/octet-stream")]
        public const StarConfig2:Class;
		
		public function ParticlesLayerMediator()
		{
			
		}
		
		override public function initialize():void
		{
			super.initialize();
			trace( this + ", " + "initialise()" );

            addContextListener(ViewEvent.BULLET_HITS_BRICK, onBulletHitsBrickHandler);
            addContextListener(ViewEvent.SCORE_ITEM_COLLECTED, onScoreItemCollectedHandler);
		}

		override public function destroy():void
		{
			super.destroy();
			trace( this + ", " + "destroy()" );
		}

        private function onBulletHitsBrickHandler(event:ViewEvent):void
        {
            var cellView:ICellView = event.data as ICellView;
            addParticles(cellView, StarConfig, StarParticle);
        }

        private function onScoreItemCollectedHandler(event:ViewEvent):void
        {
            var cellView:ICellView = event.data as ICellView;
            addParticles(cellView, StarConfig2, StarParticle2);
        }

        private function addParticles(cellView:ICellView, ConfigClass:Class, ParticleClass:Class):void
        {
            // load the XML config file
            var psConfig:XML = XML(new ConfigClass());
            // create the particle texture
            var psTexture:Texture = Texture.fromBitmap(new ParticleClass());
            // create the particle system out of the texture and XML description
            var mParticleSystem:PDParticleSystem = new PDParticleSystem(psConfig, psTexture);
            // positions the particles starting point
            mParticleSystem.emitterX = cellView.x + ViewDimensions.CELL.width / 2;
            mParticleSystem.emitterY = cellView.y + ViewDimensions.CELL.height / 2;
            // start the particles

            // show them
            viewComponent.addChild(mParticleSystem);
            // animate them
            Starling.juggler.add(mParticleSystem);

            mParticleSystem.addEventListener(starling.events.Event.COMPLETE, onEmittingCompleteHandler);
            mParticleSystem.start(0.3);
        }

        private function onEmittingCompleteHandler(event:starling.events.Event):void {
            var mParticleSystem:PDParticleSystem = event.target as PDParticleSystem;

            mParticleSystem.removeEventListener(starling.events.Event.COMPLETE, onEmittingCompleteHandler);

            Starling.juggler.remove(mParticleSystem);
        }

	}
}
