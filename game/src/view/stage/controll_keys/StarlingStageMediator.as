package view.stage.controll_keys {
import controller.managers.pause.IPausable;
import controller.managers.pause.PauseManager;

import events.GlobalEventDispatcher;

import flash.events.Event;

import flash.ui.Keyboard;
import flash.utils.getTimer;

import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;

import starling.events.KeyboardEvent;

import view.events.ViewEvent;
import view.user_controlls.move_controller.MoveControllerEvent;

	public class StarlingStageMediator extends StarlingMediator implements IPausable
	{
        private var keyDowns:Object = {};
        private var prevKeyDowns:Object = {};

		public function StarlingStageMediator()
		{

		}
		
		override public function initialize():void
		{
			super.initialize();

            PauseManager.instance.starlingStageMediator = this;

			resetKeyDowns();

            addControllListeners();

            addContextListener(ViewEvent.EXIT_REACHED, onExitReachedHandler);

            GlobalEventDispatcher.instance.addEventListener(MoveControllerEvent.CONTROOLL_ACTION, onGlobalEventHandler);
		}

        private function onExitReachedHandler(event:ViewEvent):void {
            removeControllListeners();
        }

        private function onGlobalEventHandler(event:MoveControllerEvent):void
        {
            dispatch(event.data as ViewEvent);
        }

		override public function destroy():void
		{
			super.destroy();

            removeControllListeners();

            removeContextListener(ViewEvent.EXIT_REACHED, onExitReachedHandler);

            // TODO Application instance is null here
            GlobalEventDispatcher.instance.removeEventListener(MoveControllerEvent.CONTROOLL_ACTION, onGlobalEventHandler);
		}

        private function addControllListeners():void {
            viewComponent.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
            viewComponent.addEventListener(KeyboardEvent.KEY_UP, onKeyUpHandler);
        }

        private function removeControllListeners():void {
            viewComponent.removeEventListener(KeyboardEvent.KEY_DOWN, onKeyDownHandler);
            viewComponent.removeEventListener(KeyboardEvent.KEY_UP, onKeyUpHandler);
        }

        private function onKeyDownHandler(event:KeyboardEvent):void
        {
            if (isKey(event.keyCode))
            {
                memPrevKeyDowns();

                resetKeyDowns();
                keyDowns[event.keyCode] = true;

                if (!prevKeyDowns[event.keyCode] &&  keyDowns[event.keyCode])
                {
                    for (var keyCode:String in prevKeyDowns)
                    {
                        if (prevKeyDowns[keyCode])
                        {
                            prevKeyDowns[keyCode] = false;
                            dispatch(new ViewEvent(ViewEvent.KEY_UP, keyCode));
                        }
                    }

                    dispatch(new ViewEvent(ViewEvent.KEY_DOWN, event.keyCode));
                }
            }else
            if (event.keyCode == Keyboard.SPACE)
            {
                dispatch(new ViewEvent(ViewEvent.KEY_DOWN, event.keyCode));
            }
        }

        private function onKeyUpHandler(event:KeyboardEvent):void
        {
            if (isKey(event.keyCode))
            {
                if (keyDowns[event.keyCode])
                {
                    keyDowns[event.keyCode] = false;
                    dispatch(new ViewEvent(ViewEvent.KEY_UP, event.keyCode));
                }
            }
        }

        private function memPrevKeyDowns():void
        {
            prevKeyDowns[Keyboard.UP] = keyDowns[Keyboard.UP];
            prevKeyDowns[Keyboard.DOWN] = keyDowns[Keyboard.DOWN];
            prevKeyDowns[Keyboard.LEFT] = keyDowns[Keyboard.LEFT];
            prevKeyDowns[Keyboard.RIGHT] = keyDowns[Keyboard.RIGHT];
        }

        private function resetKeyDowns():void
        {
            keyDowns[Keyboard.UP] = false;
            keyDowns[Keyboard.DOWN] = false;
            keyDowns[Keyboard.LEFT] = false;
            keyDowns[Keyboard.RIGHT] = false;
        }

        private function isKey(keyCode:int):Boolean
        {
            return keyCode == Keyboard.UP || keyCode == Keyboard.DOWN || keyCode == Keyboard.LEFT || keyCode == Keyboard.RIGHT
        }

        public function pause():void {
            removeControllListeners();
        }

        public function resume():void {
            addControllListeners();
        }
    }
}
