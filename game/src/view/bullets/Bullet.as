/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.07.15
 * Time: 12:47
 * To change this template use File | Settings | File Templates.
 */
package view.bullets {
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.geom.Point;

import starling.display.Image;
import starling.textures.Texture;

public class Bullet extends Image{

    private var _speed: Point;
    private var _isActive: Boolean;

    public function Bullet() {
        super(createBulletTexture());

        _speed = new Point();
        _isActive = true;

        pivotX = 10;
        pivotY = 2;
    }

    private function createBulletTexture():Texture
    {
        var sprite:Sprite = new Sprite();
        sprite.graphics.beginFill(0xCCCCCC);
        sprite.graphics.drawRect(0,0,20,4);
        sprite.graphics.endFill();

        var bd:BitmapData = new BitmapData(20,4);
        bd.draw(sprite);

        return Texture.fromBitmapData(bd);
    }

    override public function dispose():void {
        super.dispose();

        texture.dispose();
    }

    public function get speed():Point {
        return _speed;
    }

    public function set speed(value:Point):void {
        _speed = value;
    }

    public function get isActive():Boolean {
        return _isActive;
    }

    public function set isActive(value:Boolean):void {
        _isActive = value;
    }
}
}
