package view.moveble_units.character {
    import controller.states.movableunit.move.MovableUnitMoveController;

import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherEvent;


import model.map.MapModel;
    import robotlegs.extensions.starlingViewMap.impl.StarlingMediator;
    import view.events.ViewEvent;



	public class CharacterViewMediator extends StarlingMediator
	{
		public function CharacterViewMediator()
		{
			
		}
		
		override public function initialize():void
		{
			super.initialize();

            GlobalEventDispatcher.instance.addEventListener(GlobalEventDispatcherEvent.TYPE_TO_GAME, onSpawnCharacterHandler, false,0, true);
		}

        private function onSpawnCharacterHandler(event:GlobalEventDispatcherEvent):void {
            if (event.data.type == GlobalEventDispatcherEvent.SPAWN_CHARACTER)
            {
                dispatch(new ViewEvent(ViewEvent.SPAWN_CHARACTER));
            }
        }

		override public function destroy():void
		{
			super.destroy();

            //GlobalEventDispatcher.instance.removeEventListener(GlobalEventDispatcherEvent.SPAWN_CHARACTER, onSpawnCharacterHandler);
        }
	}
}
