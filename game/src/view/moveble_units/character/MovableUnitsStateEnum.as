package view.moveble_units.character
{
    public class MovableUnitsStateEnum
    {
        public static const STATE_SPAWN:String = "Spawn";
        public static const STATE_IDLE:String = "Idle";
        public static const STATE_MOVE:String = "Move";
        public static const STATE_CLIMB:String = "Climb";
        public static const STATE_CLIMB_IDLE:String = "ClimbIdle";
        public static const STATE_CLIMB_MOVE:String = "ClimbMove";
        public static const STATE_FALL:String = "Fall";
        public static const STATE_DEAD:String = "Dead";

        public static const STATES_ALL:Array = [
            STATE_SPAWN, STATE_IDLE, STATE_MOVE, STATE_CLIMB, STATE_CLIMB_IDLE, STATE_CLIMB_MOVE, STATE_FALL,STATE_DEAD
        ];
    }
}
