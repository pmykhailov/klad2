/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.07.15
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
package view.moveble_units.character {
public class CharacterViewEnum {

    public static const LEFT:String = "left";
    public static const RIGHT:String = "right";
    public static const CLIMB:String = "climb";
    public static const FALL:String = "fall";

    public function CharacterViewEnum() {
    }
}
}
