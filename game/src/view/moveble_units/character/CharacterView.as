/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.04.15
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
package view.moveble_units.character {

import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.ui.Keyboard;

import model.config.game.movable_items.base.MovableUnitTypeEnum;

import sound.GameSoundEnum;
import sound.SoundGroupEnum;

import starling.core.Starling;
import starling.display.Image;

import starling.display.MovieClip;
import starling.display.Quad;
import starling.display.Sprite;
import starling.textures.Texture;
import starling.textures.TextureAtlas;

import treefortress.sound.SoundAS;

import view.ViewDimensions;
import view.moveble_units.common.BaseMovableUnitView;
import view.moveble_units.common.MovableUnitAnimationsEnum;
import view.moveble_units.common.animations.MovableUnitBaseAnimationController;
import view.moveble_units.common.animations.MovableUnitMovieClipAnimationController;
import view.moveble_units.common.animations.MovebleUnitImageAnimationController;

public class CharacterView extends BaseMovableUnitView {

    [Embed(source = "../../../../res/sprite_sheets/character/character_fall.png")]
    public const FallSpriteSheet:Class;

    [Embed(source="../../../../res/sprite_sheets/character/character_fall.xml", mimeType="application/octet-stream")]
    public const FallSpriteSheetXML:Class;

    [Embed(source = "../../../../res/sprite_sheets/character/character_climb.png")]
    public const ClimbSpriteSheet:Class;

    [Embed(source="../../../../res/sprite_sheets/character/character_climb.xml", mimeType="application/octet-stream")]
    public const ClimbSpriteSheetXML:Class;

    [Embed(source = "../../../../res/sprite_sheets/character/character_move.png")]
    public const MoveSpriteSheet:Class;

    [Embed(source="../../../../res/sprite_sheets/character/character_move.xml", mimeType="application/octet-stream")]
    public const MoveSpriteSheetXML:Class;

    public function CharacterView() {
        super();
    }

    override protected function init():void {

        _animations = [
          new MovableUnitMovieClipAnimationController(MovableUnitAnimationsEnum.MOVE, MoveSpriteSheet, MoveSpriteSheetXML, 10),
          new MovableUnitMovieClipAnimationController(MovableUnitAnimationsEnum.FALL, FallSpriteSheet, FallSpriteSheetXML, 5),
          new MovableUnitMovieClipAnimationController(MovableUnitAnimationsEnum.CLIMB, ClimbSpriteSheet, ClimbSpriteSheetXML,5)
        ];

        super.init();
    }

    override public function startAnimation(name:String):void
    {
        super.startAnimation(name);

        switch (name) {
            case MovableUnitAnimationsEnum.MOVE:
                SoundAS.group(SoundGroupEnum.GROUP_SFX).playLoop(GameSoundEnum.WALK);
                break;

            case MovableUnitAnimationsEnum.FALL:
                break;

            case MovableUnitAnimationsEnum.CLIMB:
                break;
        }
    }

    override  public function pauseAnimation():void
    {
       super.pauseAnimation();

        switch (currentAnimationName) {
            case MovableUnitAnimationsEnum.MOVE:
                SoundAS.group(SoundGroupEnum.GROUP_SFX).getSound(GameSoundEnum.WALK).stop();
                break;
            case MovableUnitAnimationsEnum.FALL:

                break;
            case MovableUnitAnimationsEnum.CLIMB:
                break;
        }
    }

    override public function removeAnimation():void {

        switch (currentAnimationName) {
            case MovableUnitAnimationsEnum.MOVE:
                SoundAS.group(SoundGroupEnum.GROUP_SFX).getSound(GameSoundEnum.WALK).stop();
                break;
            case MovableUnitAnimationsEnum.FALL:
                break;
            case MovableUnitAnimationsEnum.CLIMB:
                break;
        }

        super.removeAnimation();
    }
}
}
