/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.04.15
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */
package view.moveble_units.ai {

import starling.display.MovieClip;

import treefortress.sound.SoundAS;

import view.moveble_units.common.BaseMovableUnitView;
import view.moveble_units.common.MovableUnitAnimationsEnum;
import view.moveble_units.common.animations.MovableUnitMovieClipAnimationController;

use namespace SoundAS;

public class AIView extends BaseMovableUnitView {

    private var _id:int;

    [Embed(source = "../../../../res/sprite_sheets/guard/guard_climb.png")]
    public const ClimbSpriteSheet:Class;

    [Embed(source="../../../../res/sprite_sheets/guard/guard_climb.xml", mimeType="application/octet-stream")]
    public const ClimbSpriteSheetXML:Class;

    [Embed(source = "../../../../res/sprite_sheets/guard/guard_fall.png")]
    public const FallSpriteSheet:Class;

    [Embed(source="../../../../res/sprite_sheets/guard/guard_fall.xml", mimeType="application/octet-stream")]
    public const FallSpriteSheetXML:Class;

    [Embed(source = "../../../../res/sprite_sheets/guard/guard_move.png")]
    public const MoveSpriteSheet:Class;

    [Embed(source="../../../../res/sprite_sheets/guard/guard_move.xml", mimeType="application/octet-stream")]
    public const MoveSpriteSheetXML:Class;

    public function AIView(id:int) {
        super();

        _id = id;

        init();
    }

    override protected function init():void {

/*        var quad:Quad = new Quad(40,40,0xCCCCCC);
        addChild(quad);*/

        _animations = [
            new MovableUnitMovieClipAnimationController(MovableUnitAnimationsEnum.MOVE, MoveSpriteSheet, MoveSpriteSheetXML, 8),
            new MovableUnitMovieClipAnimationController(MovableUnitAnimationsEnum.FALL, FallSpriteSheet, FallSpriteSheetXML, 5),
            new MovableUnitMovieClipAnimationController(MovableUnitAnimationsEnum.CLIMB, ClimbSpriteSheet, ClimbSpriteSheetXML, 5)
        ];

        super.init();
    }

    public function get id():int {
        return _id;
    }

}
}
