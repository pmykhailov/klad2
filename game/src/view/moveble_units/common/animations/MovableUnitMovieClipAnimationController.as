/**
 * Created by Pasha on 14.05.2016.
 */
package view.moveble_units.common.animations {
import controller.managers.base.MoveDirectionsEnum;

import flash.display.Bitmap;
import flash.ui.Keyboard;

import starling.core.Starling;
import starling.display.DisplayObject;

import starling.display.MovieClip;

import starling.textures.Texture;
import starling.textures.TextureAtlas;

public class MovableUnitMovieClipAnimationController extends MovableUnitBaseAnimationController {

    private var _movieClip:MovieClip;

    public function MovableUnitMovieClipAnimationController(name:String, SpriteSheet:Class, SpriteSheetXML:Class,fps:int) {
        super(name);
        init(SpriteSheet,SpriteSheetXML,fps);
    }


    protected function init(SpriteSheet:Class, SpriteSheetXML:Class, fps:int):void {

        var bitmap:Bitmap = new SpriteSheet();
        var texture:Texture = Texture.fromBitmap(bitmap);

        var xml:XML = XML(new SpriteSheetXML());
        var sTextureAtlas:TextureAtlas = new TextureAtlas(texture, xml);

        var frames:Vector.<Texture> = sTextureAtlas.getTextures();

        _movieClip = new MovieClip(frames, fps);

        Starling.juggler.add(_movieClip);

        _movieClip.stop();
    }


    override public function get displayObject():DisplayObject {
        return _movieClip;
    }

    override public function play():void {
        super.play();
        _movieClip.play();
    }

    override public function pause():void {
        super.pause();
        _movieClip.pause();
    }

    override public function stop():void {
        super.stop();
        _movieClip.pause();
    }

    override public function set lookDirection(value:int):void {
        super.lookDirection = value;

        if (value == MoveDirectionsEnum.RIGHT) {
            _movieClip.scaleX = 1;
        } else
        if (value == MoveDirectionsEnum.LEFT) {
            _movieClip.scaleX = -1;
        }
    }
}
}
