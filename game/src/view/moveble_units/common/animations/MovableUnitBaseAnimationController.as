/**
 * Created by Pasha on 14.05.2016.
 */
package view.moveble_units.common.animations {
import starling.display.DisplayObject;

public class MovableUnitBaseAnimationController {

    protected var _lookDirection:int;
    private var _name:String;
    private var _isPlaying:Boolean;

    public function MovableUnitBaseAnimationController(name:String) {
        _name =  name;
    }

    public function play():void {
        _isPlaying = true;
    }

    public function pause():void {
        _isPlaying = false;
    }

    public function stop():void {
        _isPlaying = false;
    }

    public function get displayObject():DisplayObject {
        return null;
    }


    public function get lookDirection():int {
        return _lookDirection;
    }

    public function set lookDirection(value:int):void {
        _lookDirection = value;
    }

    public function get name():String {
        return _name;
    }

    public function get isPlaying():Boolean {
        return _isPlaying;
    }
}
}
