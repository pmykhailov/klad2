/**
 * Created by Pasha on 14.05.2016.
 */
package view.moveble_units.common.animations {
import flash.display.Bitmap;


import starling.display.DisplayObject;
import starling.display.Image;
import starling.textures.Texture;

public class MovebleUnitImageAnimationController extends MovableUnitBaseAnimationController {

    private var _image:Image;

    public function MovebleUnitImageAnimationController(name:String, ImageClass:Class) {
        super(name);
        init(ImageClass);
    }

    private function init(ImageClass:Class):void {
        var bitmap:Bitmap = new ImageClass();
        var texture:Texture = Texture.fromBitmap(bitmap);
        _image = new Image(texture);
    }

    override public function stop():void {
        super.stop();
    }

    override public function pause():void {
        super.pause();
    }

    override public function play():void {
        super.play();
    }

    override public function get displayObject():DisplayObject {
        return _image;
    }
}
}
