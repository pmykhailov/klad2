/**
 * Created by Pasha on 12.05.2016.
 */
package view.moveble_units.common {
public class MovableUnitAnimationsEnum {
   public static const MOVE:String = "move";
   public static const FALL:String = "fall";
   public static const CLIMB:String = "climb";
}
}
