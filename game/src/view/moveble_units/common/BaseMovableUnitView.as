/**
 * Created by Pasha on 26.07.2015.
 */
package view.moveble_units.common {
import controller.managers.base.MoveDirectionsEnum;
import controller.managers.pause.IPausable;

import starling.display.Sprite;

import view.ViewDimensions;

import view.moveble_units.common.animations.MovableUnitBaseAnimationController;

public class BaseMovableUnitView extends Sprite implements IPausable{

    private var _lookDirection:int = MoveDirectionsEnum.NONE;

    [ArrayElementType("view.moveble_units.common.animations.MovableUnitBaseAnimationController")]
    protected var _animations:Array;

    protected var _currentAnimation:MovableUnitBaseAnimationController;

    private var _playAnimationWhenResume:Boolean;

    public function BaseMovableUnitView() {
        super();
        init();
    }

    public function get currentAnimationName():String {
        if (_currentAnimation) {
            return _currentAnimation.name;
        }

        return "";
    }

    public function get lookDirection():int {
        return _lookDirection;
    }

    public function set lookDirection(value:int):void {
        _lookDirection = value;
    }

    protected function init():void {
        for (var i:int = 0; i < _animations.length; i++) {
            _animations[i].displayObject.x = _animations[i].displayObject.pivotX = ViewDimensions.CELL.width / 2;
            _animations[i].displayObject.y = _animations[i].displayObject.pivotY = ViewDimensions.CELL.height / 2;
        }
    }

    protected function getAnimationControllerByName(name:String):MovableUnitBaseAnimationController {
        for (var i:int = 0; i < _animations.length; i++) {
            if (_animations[i].name == name) {
                return _animations[i];
            }
        }

        return null;
    }

    public function startAnimation(name:String):void
    {
        _currentAnimation = getAnimationControllerByName(name);
        _currentAnimation.lookDirection = _lookDirection;
        _currentAnimation.play();
        addChild(_currentAnimation.displayObject);
    }

    public function pauseAnimation():void
    {
        _currentAnimation.pause();
    }

    public function removeAnimation():void {
        _currentAnimation.pause();
        removeChild(_currentAnimation.displayObject);
        _currentAnimation = null;
    }

    public function pause():void {
        _playAnimationWhenResume = false;

        if (_currentAnimation && _currentAnimation.isPlaying ) {
            _currentAnimation.pause();
            _playAnimationWhenResume = true;
        }
    }

    public function resume():void {
        if (_playAnimationWhenResume) {
            _currentAnimation && _currentAnimation.play();
        }
    }
}
}
