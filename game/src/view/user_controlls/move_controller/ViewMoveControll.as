/**
 * Created by Pasha on 13.08.2015.
 */
package view.user_controlls.move_controller {
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.ui.Keyboard;


public class ViewMoveControll extends Sprite {

    private var _view:Sprite;
    private var _r:Number;

    public function ViewMoveControll() {
        super();
        init();
    }

    private function init():void {
        _view = new View_MoveControll();
        addChild(_view);

        _r = circle0.width / 2;

        mouseChildren = false;
        mouseEnabled = false;

        // Maybe we will still need it
        circle1.visible = false;
    }

    public function get circle0():Sprite
    {
        return _view["_circle0"];
    }

    public function get circle1():Sprite
    {
        return _view["_circle1"];
    }

    public function get r():Number {
        return _r;
    }

    public function set direction(value:int):void
    {
        unselect();

        if (value == Keyboard.UP)
        {
            (circle0["_up"] as MovieClip).gotoAndStop(2);
        }else
        if (value == Keyboard.DOWN)
        {
            (circle0["_down"] as MovieClip).gotoAndStop(2);
        }else
        if (value == Keyboard.LEFT)
        {
            (circle0["_left"] as MovieClip).gotoAndStop(2);
        }else
        if (value == Keyboard.RIGHT)
        {
            (circle0["_right"] as MovieClip).gotoAndStop(2);
        }
    }

    private function unselect():void {
        (circle0["_up"] as MovieClip).gotoAndStop(1);
        (circle0["_down"] as MovieClip).gotoAndStop(1);
        (circle0["_left"] as MovieClip).gotoAndStop(1);
        (circle0["_right"] as MovieClip).gotoAndStop(1);
    }
}
}
