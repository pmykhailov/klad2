/**
 * Created by Pasha on 20.08.2015.
 */
package view.user_controlls.move_controller {
import controller.managers.pause.IPausable;
import controller.managers.pause.PauseManager;

import events.GlobalEventDispatcher;

import flash.display.Stage;

import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.ui.Keyboard;

import misc.IDestroyable;

import view.events.ViewEvent;
import view.user_controlls.UserControllsManager;

public class MoveController implements IDestroyable, IPausable{

    private var _eventDispatcher:IEventDispatcher;
    private var mouseDownPoint:Point;
    private var prevDirection:int;

    public var viewMoveControll:ViewMoveControll;

    public function MoveController(eventDispatcher:IEventDispatcher) {
        _eventDispatcher = eventDispatcher;
        init();
    }

    private function init():void {

        mouseDownPoint = new Point();
        viewMoveControll = new ViewMoveControll();
        viewMoveControll.visible = false;

        viewMoveControll.alpha = UserControllsManager.SHOW_FOR_DEBUG_REASON ? 1 : 0;
        addListeners();
    }

    private function addListeners():void {
        _eventDispatcher.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        _eventDispatcher.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
    }

    private function removeListeners():void {
        _eventDispatcher.removeEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
        _eventDispatcher.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
        _eventDispatcher.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
    }

    private function onMouseDownHandler(event:MouseEvent):void {
        if (!(event.target is Stage)) return;
        trace("MOUSE DOWN ");
        mouseDownPoint.x = event.localX;
        mouseDownPoint.y = event.localY;

        viewMoveControll.x = mouseDownPoint.x;
        viewMoveControll.y = mouseDownPoint.y;

        viewMoveControll.visible = true;

        _eventDispatcher.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
    }

    private function onMouseUpHandler(event:MouseEvent):void {

        trace("MOUSE UP");
        viewMoveControll.visible = false;
        viewMoveControll.circle1.x = 0;
        viewMoveControll.circle1.y = 0;

        viewMoveControll.direction = -1;
        prevDirection = -1;

        dispatch(new ViewEvent(ViewEvent.KEY_UP));

        _eventDispatcher.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMoveHandler);
    }

    private function onMouseMoveHandler(event:MouseEvent):void {
        trace("MOUSE MOVE");

        var circleCoordinates:Point = new Point();

        circleCoordinates.x = event.localX - mouseDownPoint.x;
        circleCoordinates.y = event.localY - mouseDownPoint.y;

        if (circleCoordinates.x*circleCoordinates.x + circleCoordinates.y*circleCoordinates.y <= viewMoveControll.r * viewMoveControll.r)
        {
            viewMoveControll.circle1.x = circleCoordinates.x;
            viewMoveControll.circle1.y = circleCoordinates.y;
        }

        showDegrees(new Point(0,0), circleCoordinates);
    }

    private function showDegrees(centerPoint:Object, point:Object):void {
        var part:int;


        if (point.x >= centerPoint.x && point.y <= centerPoint.x)
        {
            part  = 1;
        }

        if (point.x <= centerPoint.x && point.y <= centerPoint.y)
        {
            part = 2;
        }

        if (point.x <= centerPoint.x && point.y >= centerPoint.y)
        {
            part = 3;
        }

        if (point.x >= centerPoint.x && point.y >= centerPoint.y)
        {
            part = 4;
        }

        var d:Number = Math.sqrt((centerPoint.x - point.x)*(centerPoint.x - point.x) + (centerPoint.y - point.y)*(centerPoint.y - point.y));
        var a:Number = Math.abs(centerPoint.y - point.y);
        var cos:Number = a/d;

        if (part == 1)
        {
            outDirection(cos*90);
        }else
        if (part == 2)
        {
            outDirection(90 + (1-cos)*90);
        }else
        if (part == 3)
        {
            outDirection(180 + cos*90);
        }else
        if (part == 4)
        {
            outDirection(270 + (1-cos)*90);
        }
    }

    private function outDirection(degrees:Number):void
    {
        var direction:int;

        if ((degrees >=0 && degrees <=45) || (degrees > 315 && degrees <= 360))
        {
            direction = Keyboard.RIGHT;
        }else
        if (degrees > 45 && degrees <= 135)
        {
            direction = Keyboard.UP;
        }else
        if (degrees > 135 && degrees <= 225)
        {
            direction = Keyboard.LEFT;
        }else
        if (degrees > 225 && degrees <= 315)
        {
            direction = Keyboard.DOWN;
        }

        if (direction != prevDirection)
        {
            prevDirection = direction;

            viewMoveControll.direction = direction;

            dispatch(new ViewEvent(ViewEvent.KEY_UP));
            dispatch(new ViewEvent(ViewEvent.KEY_DOWN, direction));
        }
    }

    private function dispatch(event:Event):void
    {
        GlobalEventDispatcher.instance.dispatchEvent(new MoveControllerEvent(MoveControllerEvent.CONTROOLL_ACTION, event));
    }

    public function destroy():void {
        removeListeners();
        _eventDispatcher = null;
    }

    public function pause():void {
        removeListeners();
    }

    public function resume():void {
        addListeners();
    }
}
}
