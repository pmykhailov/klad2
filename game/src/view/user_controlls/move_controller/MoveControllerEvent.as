/**
 * Created by Pasha on 20.08.2015.
 */
package view.user_controlls.move_controller {
import flash.events.Event;

public class MoveControllerEvent extends Event {

    public static const CONTROOLL_ACTION:String = "controllAction";

    private var _data:Object;

    public function MoveControllerEvent(type:String, data:Object, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }
}
}
