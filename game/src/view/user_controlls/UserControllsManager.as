/**
 * Created by Pasha on 25.08.2015.
 */
package view.user_controlls {
import controller.managers.pause.PauseManager;

import flash.display.DisplayObjectContainer;
import flash.display.Stage;
import flash.system.Capabilities;

import misc.IDestroyable;

import utils.ScreenUtils;

import view.user_controlls.fire_controller.FireController;
import view.user_controlls.move_controller.MoveController;

public class UserControllsManager implements IDestroyable{

    public static const SHOW_FOR_DEBUG_REASON:Boolean = false;

    private var _moveController:MoveController;
    private var _fireController:FireController;

    public function UserControllsManager() {

    }

    public function init(stage:Stage):void
    {
        _moveController = new MoveController(stage);
        stage.addChild(_moveController.viewMoveControll);

        _fireController = new FireController();
        stage.addChild(_fireController.fireButton);

        var yy:int = ScreenUtils.instance.screenDimensions.height;
        var xx:int = ScreenUtils.instance.screenDimensions.width;

        _fireController.fireButton.y = yy;
        _fireController.fireButton.x = xx;

        var scale:Number = xx / 1920;
        _moveController.viewMoveControll.scaleX = _moveController.viewMoveControll.scaleY = scale;
        _fireController.fireButton.scaleX = _fireController.fireButton.scaleY = scale;
    }


    public function destroy():void
    {
        var viewMoveControllerParent:DisplayObjectContainer = _moveController.viewMoveControll.parent;
        var viewFireButtonParent:DisplayObjectContainer = _fireController.fireButton.parent;

        viewMoveControllerParent && viewMoveControllerParent.removeChild(_moveController.viewMoveControll);
        viewFireButtonParent && viewFireButtonParent.removeChild(_fireController.fireButton);

        _moveController.destroy();
        _fireController.destroy();
    }

    public function get moveController():MoveController {
        return _moveController;
    }

    public function get fireController():FireController {
        return _fireController;
    }
}
}
