/**
 * Created by Pasha on 22.08.2015.
 */
package view.user_controlls.fire_controller {
import controller.managers.pause.IPausable;

import events.GlobalEventDispatcher;

import flash.display.MovieClip;
import flash.events.MouseEvent;
import flash.ui.Keyboard;

import misc.IDestroyable;

import view.events.ViewEvent;
import view.user_controlls.UserControllsManager;

import view.user_controlls.move_controller.MoveControllerEvent;

public class FireController implements IDestroyable, IPausable{

    private var _fireButton:MovieClip;

    public function FireController() {
        _fireButton = new View_FireButton();
        addListeners();
        _fireButton.alpha = UserControllsManager.SHOW_FOR_DEBUG_REASON ? 1 : 0;
    }

    private function onFireButtonMouseUpHandler(event:MouseEvent):void {
        _fireButton.gotoAndStop(1);
    }

    private function onFireButtonMouseDownHandler(event:MouseEvent):void {
        _fireButton.gotoAndStop(2);

        var viewEvent:ViewEvent = new ViewEvent(ViewEvent.KEY_DOWN, Keyboard.SPACE);
        GlobalEventDispatcher.instance.dispatchEvent(new MoveControllerEvent(MoveControllerEvent.CONTROOLL_ACTION, viewEvent));
    }

    private function addListeners():void {
        _fireButton.addEventListener(MouseEvent.MOUSE_DOWN, onFireButtonMouseDownHandler);
        _fireButton.addEventListener(MouseEvent.MOUSE_UP, onFireButtonMouseUpHandler);

    }

    private function removeListeners():void {
        _fireButton.removeEventListener(MouseEvent.MOUSE_DOWN, onFireButtonMouseDownHandler);
        _fireButton.removeEventListener(MouseEvent.MOUSE_UP, onFireButtonMouseUpHandler);

    }

    public function get fireButton():MovieClip {
        return _fireButton;
    }


    public function destroy():void {
        removeListeners();
        _fireButton = null;
    }

    public function pause():void {
        removeListeners();
    }

    public function resume():void {
        addListeners();
    }
}
}
