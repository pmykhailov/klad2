/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 03.06.15
 * Time: 18:22
 * To change this template use File | Settings | File Templates.
 */
package model
{
import flash.events.IEventDispatcher;

import misc.IDestroyable;

import model.ai.AIModel;
import model.config.game.GameConfig;
import model.config.game.movable_items.AIConfig;
import model.map.MapModel;
import model.map.cell.CellModel;

    public class GameModel implements IDestroyable
    {
        [Inject]
        public var dispatcher: IEventDispatcher;

        private var _mapModel:MapModel;
        private var _exitCell:CellModel;
        private var _aiModels:Vector.<AIModel>;
        private var _characterSpawnCellModel:CellModel;
        private var _keyHasCollected: Boolean;
        private var _characterSpeed: Number;
        private var _aiSpeed: Number;
        private var _bulletSpeed: int;
        private var _maxBullets: int;

        public function GameModel()
        {
            _mapModel = new MapModel();
            _aiModels = new Vector.<AIModel>();
            _characterSpeed = 5.5;
            _aiSpeed = 5;
            _bulletSpeed = 20;
            _maxBullets = 1;
        }

        public function init(config:GameConfig):void {
            _mapModel.init(config, dispatcher);

            _exitCell = config.exitCell;

            _characterSpawnCellModel = config.characterData.spawnCellModel;
            
            var aisData:Vector.<AIConfig> = config.aisData;
            var currentID:int;
            var aiModel:AIModel;
            var aiConfig:AIConfig;

            for (var i:int = 0; i < aisData.length; i++) {
                aiConfig = aisData[i];
                aiModel = new AIModel();
                aiModel.spawnCellModel = aiConfig.spawnCellModel;
                aiModel.id = currentID++;
                _aiModels.push(aiModel);
            }
        }

        public function get mapModel():MapModel
        {
            return _mapModel;
        }

        public function get characterSpeed():Number
        {
            return _characterSpeed;
        }

        public function get aiSpeed():Number {
            return _aiSpeed;
        }

        public function get keyHasCollected():Boolean {
            return _keyHasCollected;
        }

        public function set keyHasCollected(value:Boolean):void {
            _keyHasCollected = value;
        }

        public function get bulletSpeed():int {
            return _bulletSpeed;
        }

        public function get maxBullets():int {
            return _maxBullets;
        }

        public function get aiModels():Vector.<AIModel> {
            return _aiModels;
        }

        public function get characterSpawnCellModel():CellModel {
            return _characterSpawnCellModel;
        }

        public function get exitCell():CellModel {
            return _exitCell;
        }

        public function destroy():void {
            dispatcher = null;
            mapModel.destroy();
        }
    }
}
