/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
package model.map.cell.featured {
import model.map.cell.*;
import model.map.cell_options.DoorCellOptionsModel;
import model.map.cell_options.ICellOptionsModel;

public class DoorCellModel extends CellModel implements IOptionableCellModel{

    public static const ORIENTATION_LEFT:String = "Left";
    public static const ORIENTATION_RIGHT:String = "Right";
    public static const ORIENTATION_BOTH:String = "Both";

    private var _isOpen:Boolean;
    private var _requireKey:Boolean;
    private var _orientation:String;

    public function DoorCellModel(type:int, row:int, col:int) {
        super(type, row, col);

        _isOpen = false;
        _requireKey = false;
    }

    public function get isOpen():Boolean {
        return _isOpen;
    }

    public function set isOpen(value:Boolean):void {
        _isOpen = value;
    }

    public function set type(value:int):void {
        _type = value;
    }

    public function get orientation():String {
        return _orientation;
    }

    public function set orientation(value:String):void {
        _orientation = value;
    }

    public function get requireKey():Boolean {
        return _requireKey;
    }

    public function set requireKey(value:Boolean):void {
        _requireKey = value;
    }

    public function initFromOptionsModel(cellOptionModel:ICellOptionsModel):void {
        var doorCellOptionModel:DoorCellOptionsModel = cellOptionModel as DoorCellOptionsModel;

        _requireKey = doorCellOptionModel.requireKey;
    }
}
}
