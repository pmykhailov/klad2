/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 10:46
 * To change this template use File | Settings | File Templates.
 */
package model.map.cell.featured {
import model.map.cell.*;

public class ScoreItemCellModel extends CellModel {

    private var _collected:Boolean;
    private var _hasKey:Boolean;

    public function ScoreItemCellModel(type:int, row:int, col:int) {
        super(type, row, col);
    }

    public function get collected():Boolean {
        return _collected;
    }

    public function set collected(value:Boolean):void {
        _collected = value;
    }

    public function get hasKey():Boolean {
        return _hasKey;
    }

    public function set hasKey(value:Boolean):void {
        _hasKey = value;
    }
}
}
