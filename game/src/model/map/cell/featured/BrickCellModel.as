/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.07.15
 * Time: 10:46
 * To change this template use File | Settings | File Templates.
 */
package model.map.cell.featured {
import controller.managers.pause.IPausable;

import flash.events.TimerEvent;
import flash.utils.Timer;

import model.events.GameModelEvent;

import model.map.cell.*;

public class BrickCellModel extends CellModel implements IPausable{

    private var _isHited:Boolean;

    private var _restoreBrickTimer:Timer;
    private var _restorePhaseCurrent:int;
    private var _restorePhaseTotal:int;
    private var _currentHitCount:int;
    private var _maxHitCount:int;

    public function BrickCellModel(type:int, row:int, col:int) {
        super(type, row, col);

        _restorePhaseTotal = 3;

        _currentHitCount = 0;
        _maxHitCount = 3;
    }

    public function get isHited():Boolean {
        return _isHited;
    }

    public function hit():void {
        _currentHitCount++;

        if (_currentHitCount == _maxHitCount) {
            _isHited = true;

            _type = CellTypeEnum.TYPE_EMPTY;

            _restorePhaseCurrent = 0;
            startRestoreTimer();
        }
    }

    private function onRestoreBrickTimerHandler(event:TimerEvent):void {
        _restorePhaseCurrent++;

        if (_restorePhaseCurrent == _restorePhaseTotal + 1)
        {
            stopRestoreTimer();

            _isHited = false;
            _currentHitCount = 0;
            _type = CellTypeEnum.TYPE_BRICK;
        }

        _dispatcher.dispatchEvent(new GameModelEvent(GameModelEvent.BRICK_RESTORE, this));
    }

    private function startRestoreTimer():void
    {
        _restoreBrickTimer = new Timer(5000);
        _restoreBrickTimer.addEventListener(TimerEvent.TIMER, onRestoreBrickTimerHandler);
        _restoreBrickTimer.start();
    }

    private function stopRestoreTimer():void
    {
        _restoreBrickTimer.removeEventListener(TimerEvent.TIMER, onRestoreBrickTimerHandler);
        _restoreBrickTimer.stop();
        _restoreBrickTimer = null;
    }

    public function get restorePhaseCurrent():int {
        return _restorePhaseCurrent;
    }

    public function get restorePhaseTotal():int {
        return _restorePhaseTotal;
    }

    public function get currentHitCount():int {
        return _currentHitCount;
    }

    public function get maxHitCount():int {
        return _maxHitCount;
    }

    public function pause():void {
        if (_restoreBrickTimer) {
            _restoreBrickTimer.stop();
        }
    }

    public function resume():void {
        if (_restoreBrickTimer) {
            _restoreBrickTimer.start();
        }
    }

    override public function destroy():void {
        super.destroy();

        if (_restoreBrickTimer) {
            stopRestoreTimer();
        }
    }
}
}
