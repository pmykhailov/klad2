/**
 * Created by Pasha on 19.06.2016.
 */
package model.map.cell.featured {
import model.map.cell_options.ICellOptionsModel;

public interface IOptionableCellModel {
    function initFromOptionsModel(optionModel:ICellOptionsModel):void
}
}
