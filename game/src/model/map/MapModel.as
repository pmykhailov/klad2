/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.04.15
 * Time: 18:50
 * To change this template use File | Settings | File Templates.
 */
package model.map {

import flash.events.IEventDispatcher;

import misc.IDestroyable;

import model.config.game.GameConfig;
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;
import model.map.cell.featured.BrickCellModel;
import model.map.cell.featured.DoorCellModel;
import model.map.cell.featured.ExitCellModel;
import model.map.cell.featured.IOptionableCellModel;
import model.map.cell.featured.ScoreItemCellModel;
import model.map.cell_options.ICellOptionsModel;

public class MapModel implements IDestroyable{

    private var _cols:int;
    private var _rows:int;

    private var _cells:Array;

    private var _config:GameConfig;
    private var _dispatcher:IEventDispatcher;

    public function MapModel() {
    }

    public function init(config:GameConfig, dispatcher: IEventDispatcher):void {
        _config = config;
        _dispatcher = dispatcher;

        _cols = _config.cols;
        _rows = _config.rows;
        initCells();
    }

    public function get cols():int {
        return _cols;
    }

    public function get rows():int {
        return _rows;
    }

    public function getCell(row:int, col:int):CellModel {
        return _cells[row][col];
    }

    private function initCells():void {
        var type:int;

        _cells = [];

        for (var i:int = 0; i < _rows; i++) {

            _cells[i] = [];

            for (var j:int = 0; j < _cols; j++) {

                type = _config.map[i][j];
                switch (type)
                {
                    case CellTypeEnum.TYPE_SCORE_ITEM:
                    case CellTypeEnum.TYPE_SCORE_ITEM_WITH_KEY:
                        _cells[i][j] = new ScoreItemCellModel(CellTypeEnum.TYPE_EMPTY, i, j);
                        if (type == CellTypeEnum.TYPE_SCORE_ITEM_WITH_KEY) {
                            (_cells[i][j] as ScoreItemCellModel).hasKey = true;
                        }
                    break;

                    case CellTypeEnum.TYPE_DOOR:
                        _cells[i][j] = new DoorCellModel(CellTypeEnum.TYPE_CONCRETE, i, j);
                    break;

                    case CellTypeEnum.TYPE_BRICK:
                        _cells[i][j] = new BrickCellModel(CellTypeEnum.TYPE_BRICK, i, j);
                    break;

                    case CellTypeEnum.TYPE_EXIT:
                        _cells[i][j] = new ExitCellModel(CellTypeEnum.TYPE_EMPTY, i, j);
                    break;

                    default :
                        _cells[i][j] = new CellModel(type, i, j);
                    break;
                }

                _cells[i][j].dispatcher = _dispatcher;
            }
        }

        // Init exit cell
        var exitCellRow:int = _config.exitCell.row;
        var exitCellCol:int =_config.exitCell.col;
        if (exitCellRow < 0 || exitCellRow >= _rows) {
            _cells[exitCellRow] = [];
        }
        _cells[exitCellRow][exitCellCol] = new ExitCellModel(CellTypeEnum.TYPE_EMPTY,exitCellRow,exitCellCol);

        //Apply cell options
        var count:int = _config.gridCellsOptions.length;
        for (var k:int = 0; k < count; k++) {
            var cellOptionModel:ICellOptionsModel = _config.gridCellsOptions[k];

            (_cells[cellOptionModel.row][cellOptionModel.col] as IOptionableCellModel).initFromOptionsModel(cellOptionModel);
        }

        trace(_cells);
    }

    public function destroy():void {
        _dispatcher = null;

        for (var i:int = 0; i<rows; i++) {
            for (var j:int = 0; j<cols; j++) {
                getCell(i,j).destroy();
            }
        }
    }
}
}
