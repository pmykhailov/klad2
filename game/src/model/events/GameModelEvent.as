package model.events {
    import flash.events.Event;

    /**
     * LevelEditorModelEvent class.
     * User: Paul Makarenko
     * Date: 05.10.2014
     */
    public class GameModelEvent extends Event {

        public static const BRICK_RESTORE:String = "gameModelEventBrickRestore";

        private var _data:Object;

        public function GameModelEvent(type: String, data:Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);

            _data = data;
        }


        override public function clone(): Event {
            return new GameModelEvent(type, _data, bubbles, cancelable);
        }

        public function get data():Object {
            return _data;
        }
    }
}