/**
 * Created by Pasha on 31.07.2015.
 */
package model.ai {
import model.map.cell.CellModel;

public class AIModel {

    private var _spawnCellModel:CellModel;
    private var _id:int;

    public function AIModel() {

    }

    public function get spawnCellModel():CellModel {
        return _spawnCellModel;
    }

    public function set spawnCellModel(value:CellModel):void {
        _spawnCellModel = value;
    }

    public function get id():int {
        return _id;
    }

    public function set id(value:int):void {
        _id = value;
    }
}
}
