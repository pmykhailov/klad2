/**
 * Created by Pasha on 16.06.2016.
 */
package misc {
import view.map.item.simple.SimpleCellView;
import view.map.item.simple.View_WallCell;
import view.map.item.simple.View_WaterCell;

public class MapUtils {

    public static function setTopWaterCells(cells:Array, rows:int, cols:int):void {
        var flags:Array = [];
        var i:int;
        var j:int;
        var k:int;

        for (i = 0; i < rows; i++) {
            flags[i] = [];
            for (j = 0; j < cols; j++){
                flags[i][j] = false;
            }
        }

        for (i = rows - 1; i >= 0; i--) {
            for (j = 0; j < cols; j++){
                if ((cells[i][j] is View_WaterCell) && !flags[i][j]) {
                    var candidates:Array = [];
                    var candidate:SimpleCellView;
                    var candidatesCount;
                    var hasMoreWaterAbove:Boolean;

                    for (k = j; k < cols; k++){
                        if (cells[i][k] is View_WaterCell) {
                            candidates.push(cells[i][k]);
                            flags[i][k] = true;
                        } else {
                            break;
                        }
                    }

                    hasMoreWaterAbove = false;
                    candidatesCount = candidates.length;

                    for (k = 0; k < candidatesCount; k++){
                        candidate = candidates[k] as SimpleCellView;

                        if (candidate.row - 1 >= 0 && (cells[candidate.row - 1][candidate.col] is View_WallCell)) {
                            hasMoreWaterAbove = true;
                            break;
                        }
                    }

                    if (!hasMoreWaterAbove) {
                        for (k = 0; k < candidatesCount; k++){
                            (candidates[k] as View_WaterCell).isWaterTop = true;
                        }
                    }
                }
            }
        }
    }
 }
}
