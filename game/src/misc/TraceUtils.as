/**
 * Created by Pasha on 12.07.2015.
 */
package misc {
import controller.managers.base.MoveDirectionsEnum;

import flash.ui.Keyboard;

public class TraceUtils {
    public function TraceUtils() {
    }

    public static function getKeyName(keyCode: uint):String
    {
        switch (keyCode)
        {
            case Keyboard.UP:
                return "UP";
            break;

            case Keyboard.DOWN:
                return "DOWN";
            break;

            case Keyboard.LEFT:
                return "LEFT";
            break;

            case Keyboard.RIGHT:
                return "RIGHT";
            break;

            case Keyboard.SPACE:
                return "SPACE";
            break;

        }
        return null;
    }

    public static function getDirectionName(direction: int):String {
        if (direction == MoveDirectionsEnum.NONE) {
            return "NONE";
        }

        return getKeyName(direction);
    }
}
}
