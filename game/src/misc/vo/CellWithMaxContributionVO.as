/**
 * Created by Pasha on 08.08.2015.
 */
package misc.vo {
import model.map.cell.CellModel;

public class CellWithMaxContributionVO {

    private var _cellModel:CellModel;
    private var _contribution:int;

    public function CellWithMaxContributionVO(cellModel:CellModel, contribution:int) {
        _cellModel = cellModel;
        _contribution = contribution;
    }

    public function get cellModel():CellModel {
        return _cellModel;
    }

    public function get contribution():int {
        return _contribution;
    }
}
}
