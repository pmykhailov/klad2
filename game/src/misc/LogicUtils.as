/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 21.04.15
 * Time: 13:46
 * To change this template use File | Settings | File Templates.
 */
package misc {
import flash.geom.Point;
import flash.geom.Rectangle;

import misc.vo.CellWithMaxContributionVO;

import model.map.cell.CellModel;

import model.map.MapModel;
import model.map.cell.CellTypeEnum;

import view.ViewDimensions;
import view.map.MapView;
import view.map.item.ICellView;

public class LogicUtils{

    public function LogicUtils() {
    }

    /*
     We can move through empty cell or through ladder cell
     */
    public static function areCellsForMoving(characterPosition:Object, mapModel:MapModel):Boolean
    {
        var characterCells:Vector.<CellModel> = LogicUtils.getCharacterCells(characterPosition, mapModel);

        for (var i:int = 0; i < characterCells.length; i++)
        {
            var cellModel:CellModel = characterCells[i];

            if (cellModel && (cellModel.type != CellTypeEnum.TYPE_EMPTY && cellModel.type != CellTypeEnum.TYPE_LADDER && cellModel.type != CellTypeEnum.TYPE_WOOD))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Possible closest cells combinations are
     *
     * [0]   [null]
     * [null][null]
     *
     * [0]    [1]
     * [null][null]
     *
     * [0] [null]
     * [3] [null]
     *
     *
     * [0][1]
     * [3][2]
     *
     * @param characterPosition
     * @param mapModel
     * @return
     */
    public static function getCharacterCells(characterPosition:Object, mapModel:MapModel):Vector.<CellModel>
    {
        var res:Vector.<CellModel> = new Vector.<CellModel>();
        var cellWidth:Number = ViewDimensions.CELL.width;
        var cellHeight:Number = ViewDimensions.CELL.height;
        var rect:Rectangle = new Rectangle(0,0,cellWidth, cellHeight);

        var col:int = Math.floor(characterPosition.x / cellWidth);
        var row:int = Math.floor(characterPosition.y / cellHeight);
        var cellToCheck:Array = [
                                    {row:row     , col:col + 1, point_to_check: new Point(characterPosition.x + cellWidth - 1, characterPosition.y)},
                                    {row:row + 1 , col:col + 1, point_to_check: new Point(characterPosition.x + cellWidth - 1, characterPosition.y + cellHeight - 1)},
                                    {row:row + 1 , col:col,     point_to_check: new Point(characterPosition.x,                 characterPosition.y + cellHeight - 1)}
                                ];

        res.push(mapModel.getCell(row,col));

        for (var i:int = 0; i < cellToCheck.length; i++) {
            var cell:Object = cellToCheck[i];

            rect.x = cell.col * cellWidth;
            rect.y = cell.row * cellHeight;

            if (rect.contains(cell.point_to_check.x, cell.point_to_check.y))
            {
                res.push(mapModel.getCell(cell.row,cell.col));
            }else
            {
                res.push(null);
            }
        }

        return res;
    }

    public static function calcContributions(characterPosition:Object, mapModel:MapModel, mapView:MapView):Array
    {
        var cells:Vector.<CellModel> = getCharacterCells(characterPosition, mapModel);
        var firstCellModel:CellModel = cells[0] as CellModel;
        var firstCellView:ICellView = mapView.getCellView(firstCellModel.row, firstCellModel.col);
        var centerPoint:Point = new Point(firstCellView.x + ViewDimensions.CELL.width, firstCellView.y + ViewDimensions.CELL.height);
        var contributions:Array = [];
        var characterAcreage:Number = ViewDimensions.CHARACTER.width * ViewDimensions.CHARACTER.height;

        var calculateAcreage:Function = function(point1:Point, point2:Point):Number
        {
            return Math.abs(point1.x - point2.x) * Math.abs(point1.y - point2.y);
        };

        var points:Array = [];

        points[0] = new Point(characterPosition.x, characterPosition.y);

        //if (cells.length == 4)
        //{
            points[1] = new Point(characterPosition.x + ViewDimensions.CHARACTER.width, characterPosition.y);
            points[2] = new Point(characterPosition.x + ViewDimensions.CHARACTER.width, characterPosition.y + ViewDimensions.CHARACTER.height);
            points[3] = new Point(characterPosition.x, characterPosition.y + ViewDimensions.CHARACTER.height);
        //}

        //var stringToTrace:String = "";

        for (var i:int = 0; i < points.length; i++)
        {
            contributions[i] = ((calculateAcreage(points[i], centerPoint) / characterAcreage) * 100).toFixed(2);
            //stringToTrace += contributions[i] + "% ";
        }

        //trace(stringToTrace);


        return contributions;
    }

    public static function getCellWithMaxContributionVO(characterPosition:Object, mapModel:MapModel, mapView:MapView):CellWithMaxContributionVO
    {
        var cells:Vector.<CellModel> = getCharacterCells(characterPosition, mapModel);
        var contributions:Array = calcContributions(characterPosition, mapModel,mapView);
        var cellWithMaxContributionIndex:int;
        var maxValue:int;

        for (var i:int = 0; i < contributions.length; i++)
        {
            if (maxValue < contributions[i])
            {
                maxValue = contributions[i];
                cellWithMaxContributionIndex = i;
            }
        }

        return new CellWithMaxContributionVO(cells[cellWithMaxContributionIndex], maxValue);
    }

    public static function isInMapRange(row:int, col:int, mapModel:MapModel):Boolean
    {
        return row >=0 && row < mapModel.rows && col >=0 && col < mapModel.cols;
    }

    /**
     * Get cell model that contains point
     * @param point
     */
    public static function getCellModelByPoint(point: Object, mapModel:MapModel):CellModel
    {
        var col: int = int(point.x / ViewDimensions.CELL.width);
        var row: int = int(point.y / ViewDimensions.CELL.height);

        return mapModel.getCell(row, col);
    }
}
}
