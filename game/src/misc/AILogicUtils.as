/**
 * Created by Pasha on 27.07.2015.
 */
package misc {
import controller.managers.GameManagers;
import controller.managers.ai.AIManager;
import controller.managers.base.MoveDirectionsEnum;
import controller.managers.pause.IPausable;
import controller.states.movableunit.base.Signal;
import controller.states.movableunit.base.events.MovableUnitStateEvent;
import controller.states.movableunit.base.vo.MovableUnitStateEventVO;

import flash.events.IEventDispatcher;

import model.GameModel;

import model.config.game.movable_items.base.MovableUnitTypeEnum;

import flash.ui.Keyboard;

import starling.core.Starling;

import starling.display.Sprite;
import starling.events.Event;

import view.ViewDimensions;

import view.game.StarlingGameView;
import view.moveble_units.character.MovableUnitsStateEnum;

public class AILogicUtils implements IPausable, IDestroyable
{
    private var _gameManagers:GameManagers;
    private var _gameModel:GameModel;
    private var _dispatcher:IEventDispatcher;

    private static var _instance:AILogicUtils;
    private var _enterFrameProvider:Sprite;

    public function AILogicUtils() {
        _instance = this;

        _enterFrameProvider = new Sprite();
        _gameView.addChild(_enterFrameProvider);
    }

    // ------------------------------------------
    // --------- GETTERS / SETTERS --------------
    // ------------------------------------------

    public static function get instance():AILogicUtils {
        return _instance;
    }

    public function set gameManagers(value:GameManagers):void {
        _gameManagers = value;
    }

    public function set gameModel(value:GameModel):void {
        _gameModel = value;
    }

    public function set dispatcher(value:IEventDispatcher):void {
        _dispatcher = value;
    }

    // ------------------------------------------
    // ------------ PUBLIC METHODS --------------
    // ------------------------------------------

    public function run():void {
        if (!_enterFrameProvider.hasEventListener(Event.ENTER_FRAME)) {
            _enterFrameProvider.addEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
            moveAI();
        }
    }

    public function stop():void {
        if (_enterFrameProvider.hasEventListener(Event.ENTER_FRAME)) {
            _enterFrameProvider.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
        }
    }

    public function hits(character:Object, ai:Object):Boolean {
        return (Math.abs(character.x - ai.x) < ViewDimensions.CELL.width / 2) && (Math.abs(character.y - ai.y) < ViewDimensions.CELL.height / 2);
    }


    public function pause():void {
        stop();
    }

    public function resume():void {
        run();
    }

    public function destroy():void {
        stop();

        _gameView.removeChild(_enterFrameProvider);
        _gameManagers = null;
        _gameModel = null;
        _dispatcher = null;
        _instance = null;
        _enterFrameProvider = null;
    }

    // ------------------------------------------
    // ------------ PRIVATE METHODS -------------
    // ------------------------------------------


    private function onEnterFrameHandler(event:Event):void {
        moveAI();
    }

    private function moveAI():void
    {
        var aiManagers: Vector.<AIManager> = _gameManagers.aiManagers;

        for (var i:int = 0; i < aiManagers.length; i++) {
            var aiManager:AIManager = aiManagers[i];

            if (hits(_characterView, getAIView(aiManager.id))) {
                var vo:MovableUnitStateEventVO;

                vo = new MovableUnitStateEventVO(MovableUnitsStateEnum.STATE_DEAD, aiManager);
                _dispatcher.dispatchEvent(new MovableUnitStateEvent(MovableUnitStateEvent.STATE_CHANGE, vo));

                vo = new MovableUnitStateEventVO(MovableUnitsStateEnum.STATE_DEAD, _gameManagers.characterManager);
                _dispatcher.dispatchEvent(new MovableUnitStateEvent(MovableUnitStateEvent.STATE_CHANGE, vo));
            }else
            {
                var nextAIMoveDirection:uint = getNextAIMoveDirection(_gameView, aiManager);

                if (nextAIMoveDirection != MoveDirectionsEnum.NONE)
                {
                    if (aiManager.stateController)
                    {

                        // [       ] [       ]
                        // [   L   ] [   B   ]
                        // [     [      ]    ]
                        //       [  AI  ] -> moves right to the player
                        // [     [      ]    ]
                        // [   L   ] [       ]
                        // [       ] [       ]

                        // [       ] [       ]                          [          ]
                        // [   L   ] [   B   ]                          [  PLAYER  ]
                        // [       ] [       ]                          [          ]

                        // AI is on the la ladder (has climb move state) and we moves to the right towards the player
                        // In this case AI stops, cuz he is trying to be on the same vertical position as a player according
                        // to the logic.
                        //
                        // So let AI move UP or DOWN in this case, to prevent it's stopping

                        if (nextAIMoveDirection == aiManager.currentMoveDirection && aiManager.stateID == MovableUnitsStateEnum.STATE_CLIMB_IDLE)
                        {
                            var ai:Sprite = getAIView(aiManager.id);
                            var character: Sprite = _characterView;

                            if (character.y > ai.y)
                            {
                                nextAIMoveDirection = Keyboard.DOWN;
                            }else
                            if (character.y < ai.y)
                            {
                                nextAIMoveDirection = Keyboard.UP;
                            }
                        }

                        if (nextAIMoveDirection != aiManager.currentMoveDirection)
                        {
                            trace("AI :: chose direction " + TraceUtils.getDirectionName(nextAIMoveDirection));

                            // Simulate AI moving as if someone pressed keyboard keys

                            aiManager.stateController.reactOnSignal(new Signal(Signal.TYPE_KEY_UP));

                            aiManager.currentMoveDirection = nextAIMoveDirection;
                            aiManager.stateController.reactOnSignal(new Signal(Signal.TYPE_KEY_DOWN, nextAIMoveDirection));
                        }

                    }
                } else
                {
                    aiManager.stateController.reactOnSignal(new Signal(Signal.TYPE_KEY_UP));
                    _enterFrameProvider.removeEventListener(Event.ENTER_FRAME, onEnterFrameHandler);
                }
            }
        }
    }

    private function getNextAIMoveDirection(gameView:StarlingGameView, aiManager:AIManager):int
    {
        var id:int = aiManager.id;
        var character: Sprite = _characterView;
        var ai:Sprite = getAIView(id);
        //var characterMoveDirection:int = _gameManagers.characterManager.currentMoveDirection;
        var deltaX:Number = Math.abs(character.x - ai.x);
        var deltaY:Number = Math.abs(character.y - ai.y);

        // trace("Calculating getNextAIMoveDirection based on character (" + character.x + "," + character.y + ") ai ( " + ai.x + "," + ai.y + ")" );

        if (aiManager.stateID == MovableUnitsStateEnum.STATE_CLIMB) {
            if (deltaX >=  ViewDimensions.CELL.width - _gameModel.characterSpeed && deltaX <= ViewDimensions.CELL.width && deltaY >= 2*ViewDimensions.CELL.height)
            {
                if (ai.x < character.x)
                {
                    return Keyboard.RIGHT;
                }else
                if (ai.x > character.x)
                {
                    return Keyboard.LEFT;
                }
            } else
            {
                if (character.y > ai.y)
                {
                    return Keyboard.DOWN;
                }else
                if (character.y < ai.y)
                {
                    return Keyboard.UP;
                }
            }
        } else
        if (deltaX > _gameModel.characterSpeed) {
            if (ai.x < character.x)
            {
                return Keyboard.RIGHT;
            }else
            if (ai.x > character.x)
            {
                return Keyboard.LEFT;
            }
        } else {
            if (character.y > ai.y)
            {
                return Keyboard.DOWN;
            }else
            if (character.y < ai.y)
            {
                return Keyboard.UP;
            }
        }

        return MoveDirectionsEnum.NONE;
    }

    private function get _gameView():StarlingGameView {
        return Starling.current.root as StarlingGameView;
    }

    private function get _characterView():Sprite {
        return _gameView.getMovableUnitViewByType(MovableUnitTypeEnum.TYPE_CHARACTER);
    }

    private function getAIView(id:int):Sprite {
        return _gameView.getMovableUnitViewByType(MovableUnitTypeEnum.TYPE_AI, id)
    }

}
}
