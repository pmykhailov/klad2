package level_editor.controller {

import flash.events.MouseEvent;

import level_editor.controller.commands.LoadLevelsCommand;
    import level_editor.controller.commands.SaveLevelsCommand;
    import level_editor.model.LevelEditorLevelModel;
    import level_editor.model.LevelEditorModel;
    import level_editor.model.events.LevelEditorModelEvent;
    import level_editor.view.LevelEditorView;
    import level_editor.view.events.GridEvent;
    import level_editor.view.events.LevelEditorViewEvent;
    import level_editor.view.events.LevelSettingsEvent;
    import level_editor.view.events.LevelsControllEvent;
import level_editor.view.events.GameSettingsEvent;
import level_editor.view.grid.GridCell;
import level_editor.view.level.settings.game.available_cells.item_options.CellOptionsEvent;

import model.config.game.GameConfig;
import model.config.game.movable_items.AIConfig;
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.map.cell.CellModel;
import model.map.cell.CellTypeEnum;
import model.map.cell_options.CellOptionsModelFactory;
import model.map.cell_options.DoorCellOptionsModel;
import model.map.cell_options.ICellOptionsModel;


/**
     * LevelEditorController class.
     * User: Paul Makarenko
     * Date: 26.09.2014
     */
    public class LevelEditorController {

        private var _model: LevelEditorModel;
        private var _view: LevelEditorView;


        public function LevelEditorController(model: LevelEditorModel, view: LevelEditorView) {
            _model = model;
            _view = view;
            init();
        }


        private function init(): void {
            _model.addEventListener(LevelEditorModelEvent.LEVELS_DATA_UPDATED, onLevelsDataUpdatedHandler);
            _model.addEventListener(LevelEditorModelEvent.CURRENT_LEVEL_SELECTED_UPDATED, onCurrentLevelSelectedHandler);

            _view.addEventListener(LevelEditorViewEvent.SAVE_BUTTON_CLICKED, onSaveButtonClickedHandler);
            _view.addEventListener(LevelEditorViewEvent.LOAD_BUTTON_CLICKED, onLoadButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.ADD_LEVEL_BUTTON_CLICKED, onAddLevelButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.REMOVE_LEVEL_BUTTON_CLICKED, onRemoveLevelButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.MOVE_LEVEL_UP_BUTTON_CLICKED, onMoveLevelUpButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.MOVE_LEVEL_DOWN_BUTTON_CLICKED, onMoveLevelDownButtonClickedHandler);
            _view.addEventListener(LevelsControllEvent.LEVEL_SELECTED, onLevelSelectedHandler);
            _view.addEventListener(LevelSettingsEvent.CHANGE_LEVEL_NAME, onChangeLevelNameHandler);

            _view.addEventListener(GameSettingsEvent.APPLY_GRID_DIMENSIONS , onApplyGridDimensionsHandler);
            _view.addEventListener(GameSettingsEvent.APPLY_SPAWN_CELL , onApplySpawnCellHandler);
            _view.addEventListener(GameSettingsEvent.APPLY_EXIT_CELL , onApplyExitCellHandler);
            _view.addEventListener(GameSettingsEvent.AI_CONFIG_ADD , onAIConfigAddHandler);
            _view.addEventListener(GameSettingsEvent.AI_CONFIG_REMOVE , onAIConfigRemovedHandler);

            _view.addEventListener(GridEvent.GRID_CELL_CLICKED, onGridCellClickedHandler);
            _view.addEventListener(CellOptionsEvent.OPTIONS_CHANGED, onCellOptionsChangedHandler);
            _view.addEventListener(GameSettingsEvent.CELL_TYPE_SELECTED, onCellTypeSelectedHandler);
            _view.addEventListener(MouseEvent.MOUSE_OVER, onGridCellMouseOverHandler);

        }


        private function onLevelsDataUpdatedHandler(event: LevelEditorModelEvent): void {
            _view.data = _model;
        }

        private function onCurrentLevelSelectedHandler(event:LevelEditorModelEvent):void {
            _view.data = _model;
        }

        private function onCellTypeSelectedHandler(event: GameSettingsEvent): void {
            var type:int = event.data.type;

            _model.selectedCellOptionsModel = null;
        }

        private function onCellOptionsChangedHandler(event: CellOptionsEvent): void {
            _model.selectedCellOptionsModel = CellOptionsModelFactory.getCellOptionsModel(event.data.type, -1, -1, event.data);
        }

        private function onGridCellClickedHandler(event: GridEvent): void {
            var cellType: int = event.data.type as int;
            var cellView:GridCell = event.data.cell as GridCell;

            _model.currentLevel.gameConfig.map[cellView.row][cellView.col] = cellType;

            // Remove current cell options
            if (_model.currentLevel.gameConfig.hasGridCellOptions(cellView.row, cellView.col)) {
                _model.currentLevel.gameConfig.removeGridCellOptions(cellView.row, cellView.col);
            }

            // And apply new cell options
            if (_model.selectedCellOptionsModel) {
                // Define that current cell options will be applied to clicked grid cell
                _model.selectedCellOptionsModel.row = cellView.row;
                _model.selectedCellOptionsModel.col = cellView.col;

                // Save this options
                _model.currentLevel.gameConfig.addGridCellOptions(_model.selectedCellOptionsModel);

                // new cell options will be applied for future
                _model.selectedCellOptionsModel = _model.selectedCellOptionsModel.clone();
                _model.selectedCellOptionsModel.row = -1;
                _model.selectedCellOptionsModel.col = -1;
            }
        }

        private function onGridCellMouseOverHandler(event:MouseEvent):void {
            var cell:GridCell = event.target as GridCell;
            var gridCellOptionsModel:ICellOptionsModel;
            if (!cell) return;

            gridCellOptionsModel =_model.currentLevel.gameConfig.getGridCellOptions(cell.row, cell.col);
            if (gridCellOptionsModel) {
                _view.gridCellTf.text += " " + gridCellOptionsModel.toString();
            }
        }

        private function onAddLevelButtonClickedHandler(event:LevelsControllEvent):void {
            var levels:Array = _model.levels;

            var newLevelModel:LevelEditorLevelModel = new LevelEditorLevelModel();

            levels.push(newLevelModel);
            _model.levels = levels;
            _model.currentLevelNumber = _model.levels.length - 1;

            const DEFAULT_ROWS:int = 23;
            const DEFAULT_COLS:int = 32;
            onApplyGridDimensionsHandler(new GameSettingsEvent(GameSettingsEvent.APPLY_GRID_DIMENSIONS, false, false, {rows: DEFAULT_ROWS, cols: DEFAULT_COLS}));
        }

        private function onMoveLevelUpButtonClickedHandler(event:LevelsControllEvent):void {
            var index:int = event.data as int;
            var levels:Array = _model.levels;
            var tmp:Object;

            tmp = levels[index - 1];
            levels[index - 1] = levels[index];
            levels[index] = tmp;

            _model.levels = levels;
            _model.currentLevelNumber = index - 1;
        }

        private function onMoveLevelDownButtonClickedHandler(event:LevelsControllEvent):void {
            var index:int = event.data as int;
            var levels:Array = _model.levels;
            var tmp:Object;

            tmp = levels[index + 1];
            levels[index + 1] = levels[index];
            levels[index] = tmp;

            _model.levels = levels;
            _model.currentLevelNumber = index + 1;
        }

        private function onRemoveLevelButtonClickedHandler(event:LevelsControllEvent):void {
            var index:int = event.data as int;
            var levels:Array = _model.levels;

            levels.splice(index, 1);

            _model.levels = levels;
            _model.currentLevelNumber = index;
        }

        private function onLevelSelectedHandler(event:LevelsControllEvent):void {
            var index:int = int(event.data);

            _model.currentLevelNumber = index;
        }

        private function onApplySpawnCellHandler(event:GameSettingsEvent):void {
            var data:Object = event.data;
            var gameConfig: GameConfig = _model.currentLevel.gameConfig;
            var spawnCellModel:CellModel = new CellModel(-1, data.row, data.col);

            switch (data.type)
            {
                case MovableUnitTypeEnum.TYPE_CHARACTER:
                    gameConfig.characterData.spawnCellModel = spawnCellModel;
                        break;

                case MovableUnitTypeEnum.TYPE_AI:
                    gameConfig.aisData[data.id].spawnCellModel = spawnCellModel;
                        break;
            }
        }

        private function onApplyExitCellHandler(event:GameSettingsEvent):void {
            var data:Object = event.data;
            var gameConfig: GameConfig = _model.currentLevel.gameConfig;
            var exitCellModel:CellModel = new CellModel(-1, data.row, data.col);

            gameConfig.exitCell = exitCellModel;
        }


        private function onAIConfigAddHandler(event:GameSettingsEvent):void {
            var aisData:Vector.<AIConfig> = _model.currentLevel.gameConfig.aisData;
            var aiConfig:AIConfig = new AIConfig();
            var id:int;

            if (aisData.length > 0) {
                id = aisData[aisData.length - 1].id + 1;
            }

            aiConfig.type = MovableUnitTypeEnum.TYPE_AI;
            aiConfig.id = id;
            aiConfig.spawnCellModel = new CellModel(-1,0,0);

            aisData.push(aiConfig);

            _view.data = _model;
        }

        private function onAIConfigRemovedHandler(event:GameSettingsEvent):void {
            var aisData:Vector.<AIConfig> = _model.currentLevel.gameConfig.aisData;
            var id:int = event.data.id;
            var ii:int;
            var i:int

            for (i = 0; i < aisData.length; i++) {
                var aiConfig:AIConfig = aisData[i];

                if (aiConfig.id == id) {
                    ii = i;
                    aisData.splice(i,1);
                    break;
                }
            }

            for (i = ii; i < aisData.length; i++) {
                aisData[i].id -= 1;
            }

            _view.data = _model;
        }

        private function onApplyGridDimensionsHandler(event:GameSettingsEvent):void {
            var newRows:int = event.data.rows;
            var newCols:int = event.data.cols;

            var rows:int = _model.currentLevel.gameConfig.rows;
            var cols:int = _model.currentLevel.gameConfig.cols;

            var i:int;
            var j:int;

            var rowCells:Array;

            if (_model.currentLevel.gameConfig.map.length == 0)
            {
                for (i = 0; i < newRows; i++) {

                    rowCells = new Array();

                    for (j = 0; j < newCols; j++) {
                        rowCells[j] = CellTypeEnum.TYPE_EMPTY;
                    }

                    _model.currentLevel.gameConfig.map[i] = rowCells;
                }
            }else
            {
                // Add more rows
                if (newRows > rows)
                {
                    for (i = rows; i < newRows; i++) {

                        rowCells = new Array();

                        for (j = 0; j < cols; j++) {
                            rowCells[j] = CellTypeEnum.TYPE_EMPTY;
                        }

                        _model.currentLevel.gameConfig.map[i] = rowCells;
                    }
                }

                // Add more cols
                if (newCols > cols)
                {
                    for (i = 0; i < newRows; i++) {
                        for (j = cols; j < newCols; j++) {
                            _model.currentLevel.gameConfig.map[i][j] = CellTypeEnum.TYPE_EMPTY;
                        }
                    }
                }
            }

            _model.currentLevel.gameConfig.rows = newRows;
            _model.currentLevel.gameConfig.cols = newCols;

            _view.data = _model;
        }

        private function onChangeLevelNameHandler(event:LevelSettingsEvent):void {
            var name:String = event.data.name as String;

            _model.currentLevel.levelConfig.name = name;
        }

        private function onLoadButtonClickedHandler(event: LevelEditorViewEvent): void {
            // TODO Can be eaten by GC. Check this somehow
            (new LoadLevelsCommand(_model, _view)).execute();
        }

        private function onSaveButtonClickedHandler(event: LevelEditorViewEvent): void {
            // TODO Can be eaten by GC. Check this somehow
            (new SaveLevelsCommand(_model, _view)).execute();
        }

    }
}