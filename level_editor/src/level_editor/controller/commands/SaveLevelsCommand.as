package level_editor.controller.commands {
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.net.FileReference;
    import flash.filesystem.File;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import level_editor.controller.commands.base.BaseLevelEditorCommand;

    import level_editor.model.LevelEditorModel;
    import level_editor.model.LevelEditorLevelModel;
    import level_editor.view.LevelEditorView;

    import model.config.game.movable_items.AIConfig;

    import model.config.game.movable_items.CharacterConfig;
import model.map.cell.CellModel;
import model.map.cell_options.ICellOptionsModel;

import utils.ScreenUtils;


/**
     * Takes data from model and saves all levels data
     * to XML file
     *
     * User: Paul Makarenko
     * Date: 22.09.2014
     */
    public class SaveLevelsCommand extends BaseLevelEditorCommand{

        public function SaveLevelsCommand(model: LevelEditorModel, view: LevelEditorView) {
            super(model, view);
        }


        override public function execute(): void {
            var levelsData: XML = <levels></levels>;
            var levels: Array = _model.levels;
            var levelCount: int = levels.length;

            for (var i: int = 0; i < levelCount; i++) {
                levelsData.appendChild(_getlevelData(levels[i]));
            }

            if (ScreenUtils.instance.environment == ScreenUtils.PC) {
                _showSaveDialog(levelsData);
            } else {
                _showSaveDialog2(levelsData);
            }
        }


        private function _showSaveDialog2(data: XML): void {
            var prefsFile = File.applicationStorageDirectory;
            prefsFile = prefsFile.resolvePath("levels.xml");
            var stream:FileStream = new FileStream();
            stream.open(prefsFile, FileMode.WRITE);
            stream.writeUTFBytes(data);

            /*
            var tf:TextField = new TextField();
            tf.autoSize = TextFieldAutoSize.CENTER;
            tf.text = prefsFile.nativePath;
            var tformat:TextFormat = new TextFormat();
            tformat.size = 30;
            tf.setTextFormat(tformat);
            tf.y = 100;
            tf.x = 100;
            _view.stage.addChild(tf);
            */
        }

        private function _showSaveDialog(data: XML): void {
            var file: FileReference = new FileReference();
            file.save(data);
        }


        private function _getlevelData(levelModel: LevelEditorLevelModel): XML {

            var levelData: XML = <level></level>;

            var levelConfigData: XML = <level_config></level_config>
            var gameConfigData: XML = <game_config></game_config>

            var dimensionsData: XML = <dimensions></dimensions>;

            // Add level settings data
            levelConfigData.level_name = levelModel.levelConfig.name;

            // Add game settings data
            dimensionsData.@rows = levelModel.gameConfig.rows;
            dimensionsData.@cols = levelModel.gameConfig.cols;
            gameConfigData.appendChild(dimensionsData);

            var gridData: XML = _getGridData(levelModel.gameConfig.map);
            gameConfigData.appendChild(gridData);

            var cellsOptionsData: XML = _getGridCellsOptionsData(levelModel.gameConfig.gridCellsOptions);
            gameConfigData.appendChild(cellsOptionsData);

            var exitCellData: XML = _getExitCellData(levelModel.gameConfig.exitCell);
            gameConfigData.appendChild(exitCellData);

            var characterData:XML = _getCharacterData(levelModel.gameConfig.characterData);
            gameConfigData.appendChild(characterData);

            var aisData:XML = _getAIsData(levelModel.gameConfig.aisData);
            gameConfigData.appendChild(aisData);

            // Fill level data
            levelData.appendChild(levelConfigData);
            levelData.appendChild(gameConfigData);

            return levelData;
        }


        private function _getCharacterData(characterConfig:CharacterConfig):XML {
            var characterData: XML = <character></character>;
            var spawnCellData: XML = <spawn_cell></spawn_cell>;

            spawnCellData.@row = characterConfig.spawnCellModel.row;
            spawnCellData.@col = characterConfig.spawnCellModel.col;

            characterData.appendChild(spawnCellData);

            return characterData;
        }

        private function _getAIsData(aisConfigs:Vector.<AIConfig>):XML {
            var aisData: XML = <ais></ais>;


            for (var i:int = 0; i < aisConfigs.length; i++) {
                var aiConfig:AIConfig = aisConfigs[i];
                var aiData: XML = <ai></ai>;
                var spawnCellData: XML = <spawn_cell></spawn_cell>;

                spawnCellData.@row = aiConfig.spawnCellModel.row;
                spawnCellData.@col = aiConfig.spawnCellModel.col;


                aiData.appendChild(spawnCellData);
                aisData.appendChild(aiData);
            }

            return aisData;
        }

        private function _getGridData(map: Array): XML {
            var res: XML = <grid></grid>;
            var rowChild: XML;
            var rowString: String;

            for (var i: int = 0; i < map.length; i++) {
                rowString = map[i][0] + "";

                for (var j: int = 1; j < map[i].length; j++) {
                    rowString += "," + map[i][j];
                }
                rowChild = <row></row>;
                rowChild.@data = rowString;
                res.appendChild(rowChild);
            }

            return res;

        }

        private function _getGridCellsOptionsData(gridCellsOptions:Array): XML {
            var res: XML = <grid_cells_options></grid_cells_options>;
            var cellOptionsXML: XML;

            for (var i: int = 0; i < gridCellsOptions.length; i++) {
                var gridCellOptions:ICellOptionsModel = gridCellsOptions[i] as ICellOptionsModel;
                var options:Object = gridCellOptions.options;
                cellOptionsXML = <cell></cell>;
                cellOptionsXML.@row = gridCellOptions.row;
                cellOptionsXML.@col = gridCellOptions.col;
                for (var option in options) {
                    var optionXML:XML;
                    var optionString:String;

                    optionString = "<"+option+">" + options[option] + "</"+option+">";
                    optionXML = new XML(optionString);

                    cellOptionsXML.appendChild(optionXML);
                }
                res.appendChild(cellOptionsXML);
            }

            return res;
        }

        private function _getExitCellData(exitCellModel: CellModel): XML {
            var res: XML = <exit_cell></exit_cell>;

            res.@row = exitCellModel.row;
            res.@col = exitCellModel.col;

            return res;
        }

    }
}