
package level_editor.controller.commands.base {
import level_editor.model.LevelEditorModel;
import level_editor.view.LevelEditorView;

public class BaseLevelEditorCommand {

    protected var _model: LevelEditorModel;
    protected var _view: LevelEditorView;


    public function BaseLevelEditorCommand(model: LevelEditorModel, view: LevelEditorView) {
        _model = model;
        _view = view;
    }

    public function execute():void
    {

    }
}
}
