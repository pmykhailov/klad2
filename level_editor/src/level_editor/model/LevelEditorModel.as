package level_editor.model {
    import flash.events.EventDispatcher;

    import level_editor.model.events.LevelEditorModelEvent;

import model.map.cell_options.ICellOptionsModel;


/**
     * LevelsList class.
     * User: Paul Makarenko
     * Date: 22.09.2014
     */
    public class LevelEditorModel extends EventDispatcher{

        [ArrayElementType("level_editor.model.LevelEditorLevelModel")]
        private var _levels: Array;

        private var _currentLevelNumber: int;

        private var _selectedCellOptionsModel:ICellOptionsModel;

        public function LevelEditorModel() {
            _levels = [];
            _currentLevelNumber = -1;
        }


        public function get levels(): Array {
            return _levels;
        }


        public function set levels(value: Array): void {
            _levels = value;

            _levels.length > 0 ? _currentLevelNumber = 0 : _currentLevelNumber = -1;

            dispatchEvent(new LevelEditorModelEvent(LevelEditorModelEvent.LEVELS_DATA_UPDATED));
        }


        public function get currentLevelNumber(): int {
            return _currentLevelNumber;
        }


        public function set currentLevelNumber(value: int): void {
            _currentLevelNumber = value;

            dispatchEvent(new LevelEditorModelEvent(LevelEditorModelEvent.CURRENT_LEVEL_SELECTED_UPDATED));
        }


        public function get currentLevel(): LevelEditorLevelModel {
            return _levels[_currentLevelNumber] as LevelEditorLevelModel;
        }

        public function get selectedCellOptionsModel():ICellOptionsModel {
            return _selectedCellOptionsModel;
        }

        public function set selectedCellOptionsModel(value:ICellOptionsModel):void {
            _selectedCellOptionsModel = value;
        }
}
}