/**
 * Created by Pasha on 05.08.2015.
 */
package level_editor.view.level.settings.game.movable_units.ais {


import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.MouseEvent;

import level_editor.view.events.GameSettingsEvent;

import model.config.game.movable_items.AIConfig;

public class AIsConfigControll extends Sprite {

    private var _aiConfigControlls:Vector.<AIConfigControll>;
    private var _addButton:SimpleButton;

    public function AIsConfigControll() {
        super();
        init();
    }

    private function init():void {
        _aiConfigControlls = new Vector.<AIConfigControll>();

        _addButton = new View_IncButton();
        _addButton.addEventListener(MouseEvent.CLICK, onAddAIConfigButtonClickHandler);
        addChild(_addButton);
    }

    public function set data(aisData:Vector.<AIConfig>):void {

        _clear();

        var n:int = aisData.length;

        for (var i:int = 0; i < n; i++) {
            var aiConfig:AIConfig = aisData[i];
            var aiConfigControll:AIConfigControll = new AIConfigControll();

            aiConfigControll.data = aiConfig;

            aiConfigControll.y += i*20;
            addChild(aiConfigControll);

            _aiConfigControlls.push(aiConfigControll);
        }

        if (_aiConfigControlls.length > 0)
        {
            _addButton.y = _aiConfigControlls[_aiConfigControlls.length - 1].y + _aiConfigControlls[_aiConfigControlls.length - 1].height;
        }else
        {
            _addButton.y = 0;
        }

    }

    private function _clear():void {
        if (_aiConfigControlls) {
            for (var i:int = 0; i < _aiConfigControlls.length; i++) {
                _aiConfigControlls[i].destroy();
                removeChild(_aiConfigControlls[i]);
            }
            _aiConfigControlls = new Vector.<AIConfigControll>();
        }
    }

    private function onAddAIConfigButtonClickHandler(event:MouseEvent):void {
        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.AI_CONFIG_ADD, true));
    }

}
}
