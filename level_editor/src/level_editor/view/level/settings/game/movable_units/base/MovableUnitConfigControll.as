/**
 * Created by Pasha on 05.08.2015.
 */
package level_editor.view.level.settings.game.movable_units.base {

import flash.display.Sprite;

import level_editor.view.events.GameSettingsEvent;

import model.config.game.movable_items.base.MovableUnitConfig;

public class MovableUnitConfigControll extends Sprite {

    protected var _id:int;

    protected var _type:String;

    protected var _spawnCellControll:SpawnCellControll;

    public function MovableUnitConfigControll() {
        super();
        init();
    }

    protected function init():void {
        _spawnCellControll = new SpawnCellControll();
        _spawnCellControll.addEventListener(GameSettingsEvent.APPLY_SPAWN_CELL, onApplySpawnCellHandler);
        addChild(_spawnCellControll);
    }

    public function set data(value:Object):void {
        var movableUnitConfig:MovableUnitConfig = value as MovableUnitConfig;
        _id = movableUnitConfig.id;
        _type = movableUnitConfig.type;

        _spawnCellControll.data = movableUnitConfig.spawnCellModel;
    }

    public function destroy():void
    {
        _spawnCellControll.removeEventListener(GameSettingsEvent.APPLY_SPAWN_CELL, onApplySpawnCellHandler);
        _spawnCellControll.destroy();
    }

    private function onApplySpawnCellHandler(event:GameSettingsEvent):void {
        var dimensionsObject:Object = event.data;

        dimensionsObject.id = _id;
        dimensionsObject.type = _type;

        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.APPLY_SPAWN_CELL, true, false, dimensionsObject));
    }

}
}
