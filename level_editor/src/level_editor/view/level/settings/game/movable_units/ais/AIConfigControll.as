/**
 * Created by Pasha on 05.08.2015.
 */
package level_editor.view.level.settings.game.movable_units.ais {

import flash.display.SimpleButton;
import flash.events.MouseEvent;

import level_editor.view.events.GameSettingsEvent;

import level_editor.view.level.settings.game.movable_units.base.MovableUnitConfigControll;

public class AIConfigControll extends MovableUnitConfigControll {

    private var _removeButton:SimpleButton;

    public function AIConfigControll() {
        super();
    }

    override protected function init():void {
        super.init();

        _removeButton = new View_DecButton();
        _removeButton.x = _spawnCellControll.x + _spawnCellControll.width + 10;
        _removeButton.addEventListener(MouseEvent.CLICK, onRemoveButtonClickHandler);
        addChild(_removeButton);
    }

    private function onRemoveButtonClickHandler(event:MouseEvent):void {
        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.AI_CONFIG_REMOVE, true, false, {id:_id}));
    }

    override public function destroy():void {
        _removeButton.removeEventListener(MouseEvent.CLICK, onRemoveButtonClickHandler);
        super.destroy();
    }
}
}
