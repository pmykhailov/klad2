/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:33
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.game.movable_units.base {
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;

import level_editor.view.events.LevelSettingsEvent;
import level_editor.view.events.GameSettingsEvent;

import model.config.game.GameConfig;
import model.map.cell.CellModel;

// TODO ExitCellConfigControll is also use SpawnCellControll. So rename this class to SelectCellControll
 public class SpawnCellControll extends Sprite {

    private var _view:Sprite;
    private var _rowTextField:TextField;
    private var _colTextField:TextField;
    private var _applyButton:SimpleButton;

    public function SpawnCellControll() {
        super();
        init();
    }

    private function init():void {
        _view = new View_SpawnCellControll();

        _rowTextField = _view["_row"] as TextField;
        _colTextField = _view["_col"] as TextField;
        _applyButton = _view["_applyButton"] as SimpleButton;

        _applyButton.addEventListener(MouseEvent.CLICK, onApplyButtonClickHandler);

        addChild(_view);
    }

    public function set data(spawnCellModel:CellModel):void {
        _rowTextField.text = spawnCellModel.row + "";
        _colTextField.text = spawnCellModel.col + "";
    }

    public function destroy():void {
        _applyButton.removeEventListener(MouseEvent.CLICK, onApplyButtonClickHandler);
    }

    private function onApplyButtonClickHandler(event:MouseEvent):void {
        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.APPLY_SPAWN_CELL, false, false, dimensionsObject));
    }

    private function get dimensionsObject(): Object {
        return {row: int(_rowTextField.text), col: int(_colTextField.text)};
    }

}
}
