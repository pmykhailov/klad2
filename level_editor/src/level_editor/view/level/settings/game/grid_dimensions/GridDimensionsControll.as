/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:33
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.game.grid_dimensions {
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;

import level_editor.view.events.LevelSettingsEvent;
import level_editor.view.events.GameSettingsEvent;

import model.config.game.GameConfig;

public class GridDimensionsControll extends Sprite {

    private var _view:Sprite;
    private var _rowsTextField:TextField;
    private var _colsTextField:TextField;
    private var _applyButton:SimpleButton;

    public function GridDimensionsControll() {
        super();
        init();
    }

    private function init():void {
        _view = new View_GridDimensionsControll();

        _rowsTextField = _view["_rows"] as TextField;
        _colsTextField = _view["_cols"] as TextField;
        _applyButton = _view["_applyButton"] as SimpleButton;

        _applyButton.addEventListener(MouseEvent.CLICK, onApplyButtonClickHandler);

        addChild(_view);
    }

    public function set data(model:GameConfig):void {
        _rowsTextField.text = model.rows + "";
        _colsTextField.text = model.cols + "";
    }

    private function onApplyButtonClickHandler(event:MouseEvent):void {
        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.APPLY_GRID_DIMENSIONS, true, false, dimensionsObject));
    }

    private function get dimensionsObject(): Object {
        return {rows: int(_rowsTextField.text), cols: int(_colsTextField.text)};
    }

}
}
