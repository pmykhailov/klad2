/**
 * Created by Pasha on 18.06.2016.
 */
package level_editor.view.level.settings.game.available_cells.item_options {
import flash.display.Sprite;

public class CellOptionsView extends Sprite {

    private var _type:int;

    public function CellOptionsView(type:int) {
        super();
        _type = type;
    }

    public function get type():int {
        return _type;
    }
}
}
