package level_editor.view.level.settings.game.available_cells.vo {

public class ItemsListVO {

    [ArrayElementType("level_editor.view.level.settings.game.available_cells.vo.ItemsListIRVO")]
    private var _cells:Array;

    public function ItemsListVO(visualCells:Array, visualCellsLinkages:Array) {

        _cells = [];

        for (var i:int = 0; i < visualCells.length; i++) {
            var itemsListIRVO:ItemsListIRVO = new ItemsListIRVO(visualCells[i], visualCellsLinkages[i]);

            _cells.push(itemsListIRVO);
        }
    }

    public function get cells():Array {
        return _cells;
    }
}
}
