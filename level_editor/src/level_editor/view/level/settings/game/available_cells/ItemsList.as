/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.10.14
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.game.available_cells {

import flash.display.Sprite;
import flash.events.MouseEvent;
import level_editor.view.events.GameSettingsEvent;
import level_editor.view.level.settings.game.available_cells.vo.ItemsListIRVO;
import level_editor.view.level.settings.game.available_cells.vo.ItemsListVO;

import model.map.cell.CellTypeEnum;

public class ItemsList extends Sprite {

    [ArrayElementType("level_editor.view.level.settings.game.available_cells.ItemsListIR")]
    private var _items:Array;

    public function ItemsList() {
        addEventListener(MouseEvent.CLICK, onItemClickHandler);
    }

    private function onItemClickHandler(event:MouseEvent):void {
        var target: ItemsListIR = event.target as ItemsListIR;

        if (!target) return;

        deselectAllCells();

        target.isSelected = true;

        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.CELL_TYPE_SELECTED, true, false, {type: target.type}));
    }

    public function selectCellType(type:int):void {
        deselectAllCells();

        for (var i:int = 0; i < _items.length; i++) {
            var ir:ItemsListIR = _items[i];
            if (ir.type == type) {
                ir.isSelected = true;

                dispatchEvent(new GameSettingsEvent(GameSettingsEvent.CELL_TYPE_SELECTED, true, false, {type: type}));
                break;
            }
        }
    }

    public function set data(itemsListVO:ItemsListVO):void {
        var xx:int;

        clearItemsList();

        _items = [];
        for (var i:int = 0; i < itemsListVO.cells.length; i++) {
            var itemsListIRVO:ItemsListIRVO = itemsListVO.cells[i];
            var itemsListIR:ItemsListIR = new ItemsListIR();

            itemsListIR.data = itemsListIRVO;
            itemsListIR.x = xx;

            xx += itemsListIR.width + 5;

            _items[i] = itemsListIR;
            addChild(itemsListIR);
        }
    }

    private function clearItemsList():void {
        if (_items) {
            for (var i:int = 0; i < _items.length; i++) {
                var itemsListIR:ItemsListIR = _items[i];
                removeChild(itemsListIR);
            }

            _items = null;
        }
    }

    private function deselectAllCells():void {
        for (var i:int = 0; i < _items.length; i++) {
            var ir:ItemsListIR = _items[i];
            ir.isSelected = false;
        }
    }

}
}
