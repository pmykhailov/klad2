/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 16.10.14
 * Time: 10:03
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.game.available_cells {

import flash.display.Sprite;

import level_editor.view.events.GameSettingsEvent;

import level_editor.view.level.settings.game.available_cells.item_options.CellOptionsView;
import level_editor.view.level.settings.game.available_cells.item_options.DoorCellOptionsView;
import level_editor.view.level.settings.game.available_cells.vo.ItemsListVO;

import model.map.cell.CellTypeEnum;


public class AvailableCellsControll extends Sprite{

    private var _cellsList:ItemsList;
    private var _itemOptions:CellOptionsView;

    public function AvailableCellsControll() {
        super();
        init();
    }

    public function update():void {
        var visualCells:Array = CellsTypeAdditionalData.VISUAL_CELLS.concat(CellTypeEnum.TYPE_SCORE_ITEM_WITH_KEY);
        var visualCellsLinkages:Array = [];

        for (var i:int = 0; i < visualCells.length; i++) {
            visualCellsLinkages[i] = CellsTypeAdditionalData.CELLS_GFX_ACCORDANCE[visualCells[i]];
        }

        _cellsList.data = new ItemsListVO(visualCells, visualCellsLinkages);
        _cellsList.selectCellType(CellTypeEnum.TYPE_EMPTY);
    }

    private function init():void {
        _cellsList = new ItemsList();
        _cellsList.addEventListener(GameSettingsEvent.CELL_TYPE_SELECTED, onCellTypeSelectedHandler);
        addChild(_cellsList);
    }

    private function onCellTypeSelectedHandler(event:GameSettingsEvent):void {
        var type:int = event.data.type as int;

        removeItemOptions();
        addItemOptions(type);
    }


    private function addItemOptions(type:int):void {
        var itemOptionsClass:Class;

        switch (type) {
            case CellTypeEnum.TYPE_DOOR:
                itemOptionsClass = DoorCellOptionsView;
                break;
        }

        if (itemOptionsClass) {
            _itemOptions = new itemOptionsClass(type);
            _itemOptions.x = _cellsList.x + _cellsList.width;
            addChild(_itemOptions);
        }
    }

    private function removeItemOptions():void {
        if (_itemOptions) {
            removeChild(_itemOptions);
            _itemOptions = null;
        }
    }

}
}
