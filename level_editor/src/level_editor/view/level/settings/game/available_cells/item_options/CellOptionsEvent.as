/**
 * Created by Pasha on 19.06.2016.
 */
package level_editor.view.level.settings.game.available_cells.item_options {
import flash.events.Event;

public class CellOptionsEvent extends Event {

    public static const OPTIONS_CHANGED:String = "cellOptionsEventOptionsChanged";
    private var _data:Object;

    public function CellOptionsEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new CellOptionsEvent(type, _data, bubbles, cancelable);
    }
}
}
