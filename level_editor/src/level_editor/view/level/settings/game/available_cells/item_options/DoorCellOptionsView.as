/**
 * Created by Pasha on 18.06.2016.
 */
package level_editor.view.level.settings.game.available_cells.item_options {
import fl.controls.CheckBox;

import flash.events.Event;
import flash.events.MouseEvent;


public class DoorCellOptionsView extends CellOptionsView {

    public static const DOOR_ITEM_OPTIONS_CHANGED:String = "doorItemOptionsChanged";

    private var _requireKeyCheckBox:CheckBox;

    public function DoorCellOptionsView(type:int) {
        super(type);
        init();
    }

    private function init():void {
        _requireKeyCheckBox = new CheckBox();
        _requireKeyCheckBox.label = "Require key";
        _requireKeyCheckBox.addEventListener(MouseEvent.CLICK, onRequireCheckBoxClickHandler);
        addChild(_requireKeyCheckBox);

        addEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStageHandler);
    }

    public function set data(value:Object):void {
        _requireKeyCheckBox.selected = value;
    }

    private function onRequireCheckBoxClickHandler(event:MouseEvent):void {
        dispatchEvent(new CellOptionsEvent(CellOptionsEvent.OPTIONS_CHANGED, {type:type, require_key:_requireKeyCheckBox.selected}, true));
    }

    private function onRemoveFromStageHandler(event:Event):void {
        removeEventListener(Event.REMOVED_FROM_STAGE, onRemoveFromStageHandler);
        _requireKeyCheckBox.removeEventListener(MouseEvent.CLICK, onRequireCheckBoxClickHandler);
    }
}
}
