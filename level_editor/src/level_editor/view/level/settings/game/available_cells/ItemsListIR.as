/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 17.10.14
 * Time: 11:56
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.game.available_cells {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;
import flash.utils.getDefinitionByName;

import level_editor.view.level.settings.game.available_cells.vo.ItemsListIRVO;


public class ItemsListIR extends Sprite{

    private var _bitmap:Bitmap
    private var _selection:Sprite;
    private var _isSelected:Boolean;
    private var _type:int;

    public function ItemsListIR() {
        super();
        init();
    }

    private function init():void {

        mouseChildren = false;
        buttonMode = true;

        _bitmap = new Bitmap();
        addChild(_bitmap);

        _selection = new Sprite();
        addChild(_selection);
    }

    public function set data(itemsListIRVO:ItemsListIRVO):void {
        _type = itemsListIRVO.type;

        var BDClass:Class = getDefinitionByName(itemsListIRVO.bitmapDataLinkageName) as Class;
        var bd:BitmapData = new BDClass() as BitmapData;
        _bitmap.bitmapData = bd;
    }

    public function set isSelected(value:Boolean):void{
        _isSelected = value;

        updateSelection();
    }

    public function get isSelected():Boolean {
        return _isSelected;
    }

    public function get type():int {
        return _type;
    }

    private function updateSelection():void {
        _selection.graphics.clear();

        if (_isSelected) {
            _selection.graphics.beginFill(0xFFFFFF, 0.5);
            _selection.graphics.drawRect(0,0,_bitmap.width,_bitmap.height);
            _selection.graphics.endFill();
        }
    }
}
}
