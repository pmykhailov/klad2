package level_editor.view.level.settings.game.available_cells.vo {
public class ItemsListIRVO {
    private var _bitmapDataLinkageName:String;
    private var _type:int;

    public function ItemsListIRVO(type:int, bitmapDataLinkageName:String) {
        _type = type;
        _bitmapDataLinkageName = bitmapDataLinkageName;
    }

    public function get bitmapDataLinkageName():String {
        return _bitmapDataLinkageName;
    }

    public function get type():int {
        return _type;
    }
}
}
