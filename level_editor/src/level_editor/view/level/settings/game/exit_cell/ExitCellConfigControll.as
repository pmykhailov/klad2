/**
 * Created by Pasha on 16.05.2016.
 */
package level_editor.view.level.settings.game.exit_cell {
import flash.display.Sprite;

import level_editor.view.events.GameSettingsEvent;

import level_editor.view.level.settings.game.movable_units.base.SpawnCellControll;

import model.map.cell.CellModel;


public class ExitCellConfigControll extends Sprite {

    protected var _spawnCellControll:SpawnCellControll;

    public function ExitCellConfigControll() {
        super();
        init();
    }

    protected function init():void {
        _spawnCellControll = new SpawnCellControll();
        _spawnCellControll.addEventListener(GameSettingsEvent.APPLY_SPAWN_CELL, onApplySpawnCellHandler);
        addChild(_spawnCellControll);
    }

    public function set data(value:Object):void {

        _spawnCellControll.data = value as CellModel;
    }

    public function destroy():void
    {
        _spawnCellControll.removeEventListener(GameSettingsEvent.APPLY_SPAWN_CELL, onApplySpawnCellHandler);
        _spawnCellControll.destroy();
    }

    private function onApplySpawnCellHandler(event:GameSettingsEvent):void {
        var dimensionsObject:Object = event.data;

        dispatchEvent(new GameSettingsEvent(GameSettingsEvent.APPLY_EXIT_CELL, true, false, dimensionsObject));
    }
}
}
