/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:32
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.game {
import flash.display.Sprite;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

import level_editor.model.LevelEditorModel;
import level_editor.view.level.settings.game.available_cells.AvailableCellsControll;
import level_editor.view.level.settings.game.exit_cell.ExitCellConfigControll;
import level_editor.view.level.settings.game.grid_dimensions.GridDimensionsControll;
import level_editor.view.level.settings.game.movable_units.ais.AIsConfigControll;
import level_editor.view.level.settings.game.movable_units.character.CharacterConfigControll;

public class GameSettingsPanel extends Sprite {

    private var _gridDimensionsControll: GridDimensionsControll;
    private var _availableItemsControll: AvailableCellsControll;
    private var _exitCellConfigControll: ExitCellConfigControll;
    private var _characterConfigControll: CharacterConfigControll;
    private var _aisConfigControll: AIsConfigControll;
    private var _exitCellConfigControllCaption:TextField;
    private var _characterConfigControllCaption:TextField;
    private var _aisConfigControllCaption:TextField;

    public function GameSettingsPanel() {
        super();
        init();
    }

    private function init():void {
        visible = false;

        _gridDimensionsControll = new GridDimensionsControll();
        addChild(_gridDimensionsControll);

        _availableItemsControll = new AvailableCellsControll();
        addChild(_availableItemsControll);

        _characterConfigControll = new CharacterConfigControll();
        addChild(_characterConfigControll);

        _exitCellConfigControll = new ExitCellConfigControll();
        addChild(_exitCellConfigControll);

        _aisConfigControll = new AIsConfigControll();
        addChild(_aisConfigControll);

        _exitCellConfigControllCaption = new TextField();
        _exitCellConfigControllCaption.text = "Exit";
        _exitCellConfigControllCaption.autoSize = TextFieldAutoSize.LEFT;
        addChild(_exitCellConfigControllCaption);

        _characterConfigControllCaption = new TextField();
        _characterConfigControllCaption.text = "Character";
        _characterConfigControllCaption.autoSize = TextFieldAutoSize.LEFT;
        addChild(_characterConfigControllCaption);

        _aisConfigControllCaption = new TextField();
        _aisConfigControllCaption.text = "AIs";
        _aisConfigControllCaption.autoSize = TextFieldAutoSize.LEFT;
        addChild(_aisConfigControllCaption);
    }

    public function set data(model:LevelEditorModel):void {
        if (model.currentLevel) {
            _gridDimensionsControll.data = model.currentLevel.gameConfig;
            _availableItemsControll.update();
            _exitCellConfigControll.data = model.currentLevel.gameConfig.exitCell;
            _characterConfigControll.data = model.currentLevel.gameConfig.characterData;
            _aisConfigControll.data = model.currentLevel.gameConfig.aisData;
        }

        _availableItemsControll.y = _gridDimensionsControll.y + _gridDimensionsControll.height + 10;
        _exitCellConfigControllCaption.y = _availableItemsControll.y + _availableItemsControll.height + 10;
        _exitCellConfigControll.y = _exitCellConfigControllCaption.y + _exitCellConfigControllCaption.textHeight + 10;
        _characterConfigControllCaption.y = _exitCellConfigControll.y + _exitCellConfigControll.height + 10;
        _characterConfigControll.y = _characterConfigControllCaption.y + _characterConfigControllCaption.textHeight + 10;
        _aisConfigControllCaption.y = _characterConfigControll.y + _characterConfigControll.height + 10;
        _aisConfigControll.y = _aisConfigControllCaption.y + _aisConfigControllCaption.textHeight + 10;

        model.currentLevel ? visible = true : false;
    }

}
}
