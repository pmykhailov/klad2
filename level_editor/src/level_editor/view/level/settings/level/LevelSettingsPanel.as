/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:32
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.level {
import flash.display.Sprite;
import level_editor.model.LevelEditorModel;
import level_editor.view.level.settings.level.level_name.LevelNameControll;

public class LevelSettingsPanel extends Sprite {

    private var _levelNameControll: LevelNameControll;

    public function LevelSettingsPanel() {
        super();
        init();
    }

    private function init():void {
        visible = false;

        _levelNameControll = new LevelNameControll();
        addChild(_levelNameControll);
    }

    public function set data(model:LevelEditorModel):void {
        if (model.currentLevel) {
            _levelNameControll.data = model.currentLevel;
        }

        model.currentLevel ? visible = true : false;
    }

}
}
