/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:33
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.settings.level.level_name {
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;

import level_editor.model.LevelEditorLevelModel;

import level_editor.view.events.LevelSettingsEvent;


public class LevelNameControll extends Sprite {

    private var _view:Sprite;
    private var _levelNameTextField:TextField;
    private var _applyButton:SimpleButton;

    public function LevelNameControll() {
        super();
        init();
    }

    private function init():void {
        _view = new View_LevelNameControll();

        _levelNameTextField = _view["_levelName"] as TextField;
        _applyButton = _view["_applyButton"] as SimpleButton;

        _applyButton.addEventListener(MouseEvent.CLICK, onApplyButtonClickHandler);

        addChild(_view);
    }

    public function set data(model:LevelEditorLevelModel):void {
        _levelNameTextField.text = model.levelConfig.name;
    }

    private function onApplyButtonClickHandler(event:MouseEvent):void {
        dispatchEvent(new LevelSettingsEvent(LevelSettingsEvent.CHANGE_LEVEL_NAME, true, false, {name:_levelNameTextField.text}));
    }

}
}
