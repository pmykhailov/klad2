/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.14
 * Time: 9:42
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.level.list {
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.geom.Point;
import flash.utils.Dictionary;

public class LevelsListControllButtons extends Sprite {

    public function LevelsListControllButtons() {
        super();
        init();
    }

    private var _moveUp:SimpleButton;
    private var _addLevel:SimpleButton;
    private var _removeLevel:SimpleButton;
    private var _moveDown:SimpleButton;
    private var _buttons:Array;

    private function init():void {
        _buttons = [new View_UpButton(), new View_IncButton(), new View_DecButton(), new View_DownButton()];

        var yy:int;
        var padding:Point = new Point(0, 5);

        _moveUp = _buttons[0];
        _addLevel = _buttons[1];
        _removeLevel = _buttons[2];
        _moveDown = _buttons[3];

        for (var i:int = 0; i < _buttons.length; i++) {
            var button:SimpleButton = _buttons[i] as SimpleButton;

            button.y = yy;

            yy += button.height + padding.y;

            addChild(button);
        }

    }

    public function get moveUp():SimpleButton {
        return _moveUp;
    }

    public function get addLevel():SimpleButton {
        return _addLevel;
    }

    public function get removeLevel():SimpleButton {
        return _removeLevel;
    }

    public function get moveDown():SimpleButton {
        return _moveDown;
    }

    public function get buttons():Array {
        return _buttons;
    }
}
}
