package level_editor.view {
import flash.display.SimpleButton;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;
import flash.text.TextFormat;

import level_editor.model.LevelEditorModel;
import level_editor.view.events.LevelEditorViewEvent;
import level_editor.view.events.GameSettingsEvent;
import level_editor.view.grid.Grid;
import level_editor.view.grid.GridCell;
import level_editor.view.level.list.LevelListControllPanel;
import level_editor.view.level.settings.game.GameSettingsPanel;
import level_editor.view.level.settings.level.LevelSettingsPanel;
import level_editor.view.statics.LevelEditorDimensions;

import model.map.cell.CellTypeEnum;

import mx.core.TextFieldAsset;

/**
 * View class.
 * User: Paul Makarenko
 * Date: 22.09.2014
 */
public class LevelEditorView extends Sprite {


    private var _saveButton:SimpleButton;
    private var _loadButton:SimpleButton;
    private var _levelListControllPanel:LevelListControllPanel;
    private var _levelSettingsPanel:LevelSettingsPanel;
    private var _gameSettingsPanel:GameSettingsPanel;
    private var _grid:Grid;
    private var _gridCellTf:TextField;

    public function LevelEditorView() {
        super();
        _init();
    }

    public function set data(model:LevelEditorModel):void {
        _levelListControllPanel.data = model;
        _grid.data = model;
        _levelSettingsPanel.data = model;
        _gameSettingsPanel.data = model;

        reLayoutComponents();
    }

    public function get gridCellTf():TextField {
        return _gridCellTf;
    }

    private function reLayoutComponents(): void {
        _levelSettingsPanel.x = _levelListControllPanel.x + _levelListControllPanel.width + 10;
        _gameSettingsPanel.x = _levelSettingsPanel.x;

        _gridCellTf.y = _gameSettingsPanel.y + _gameSettingsPanel.height + 10;
        _gridCellTf.x = 10;

        _grid.scaleX = 0.65;
        _grid.scaleY = 0.65;

        _grid.x = 10;
        _grid.y = _gridCellTf.y + _gridCellTf.height + 10;
    }

    private function _init():void {
        _initButtons();
        _initLevelsPanel();
        _initLevelSettingsPanel();
        _initGameSettingsPanel();
        _initGrid();
        _initGridCellTf();
    }

    private function _initGridCellTf():void {
        var tf:TextFormat = new TextFormat();
        tf.size = 18;

        _gridCellTf = new TextField();
        _gridCellTf.autoSize = TextFieldAutoSize.LEFT;
        _gridCellTf.textColor = 0x000000;
        _gridCellTf.defaultTextFormat = tf;
        _gridCellTf.text = " ";

        addChild(_gridCellTf);
    }

    private function _initLevelSettingsPanel():void {
        _levelSettingsPanel = new LevelSettingsPanel();
        _levelSettingsPanel.y = _levelListControllPanel.y;

        addChild(_levelSettingsPanel);
    }

    private function _initGameSettingsPanel():void {
        _gameSettingsPanel = new GameSettingsPanel();
        _gameSettingsPanel.y = _levelSettingsPanel.y + _levelSettingsPanel.height + 10;
        _gameSettingsPanel.addEventListener(GameSettingsEvent.CELL_TYPE_SELECTED, onCellTypeSelectedHandler);

        addChild(_gameSettingsPanel);
    }

    private function onCellTypeSelectedHandler(event:GameSettingsEvent):void {
        var cellType: int = event.data.type as int;

        _grid.activeCellType = cellType;
    }

    private function _initGrid():void {
        _grid = new Grid();
        _grid.addEventListener(MouseEvent.MOUSE_OVER, onGridCellMouseOverHandler);
        _grid.addEventListener(MouseEvent.MOUSE_OUT, onGridCellMouseOutHandler);
        addChild(_grid);
    }

    private function _initLevelsPanel():void {
        _levelListControllPanel = new LevelListControllPanel();
        _levelListControllPanel.x = 10;
        _levelListControllPanel.y = 10 + _saveButton.height + 10;
        addChild(_levelListControllPanel);
    }

    private function _initButtons():void {
        _saveButton = new View_SaveButton();
        _saveButton.x = 10;
        _saveButton.y = 10;
        _saveButton.addEventListener(MouseEvent.CLICK, onSaveButtonMouseClickHandler);

        _loadButton = new View_LoadButton();
        _loadButton.x = 10 + _saveButton.x + _saveButton.width + 10;
        _loadButton.y = _saveButton.y;
        _loadButton.addEventListener(MouseEvent.CLICK, onLoadButtonMouseClickHandler);

        addChild(_saveButton);
        addChild(_loadButton);
    }

    private function onGridCellMouseOutHandler(event:MouseEvent):void {
        _gridCellTf.text = " ";
    }

    private function onGridCellMouseOverHandler(event:MouseEvent):void {
        var cell:GridCell = event.target as GridCell;

        _gridCellTf.text = "(" + cell.row + "," + cell.col + ")";
    }

    private function onSaveButtonMouseClickHandler(event:MouseEvent):void {
        dispatchEvent(new LevelEditorViewEvent(LevelEditorViewEvent.SAVE_BUTTON_CLICKED));
    }

    private function onLoadButtonMouseClickHandler(event:MouseEvent):void {
        dispatchEvent(new LevelEditorViewEvent(LevelEditorViewEvent.LOAD_BUTTON_CLICKED));
    }


}

}