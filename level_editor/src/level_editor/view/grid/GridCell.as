package level_editor.view.grid {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.MovieClip;
    import flash.events.MouseEvent;
import flash.utils.getDefinitionByName;


    /**
     * GridCell class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class GridCell extends MovieClip {

        private var _view: MovieClip;
        private var _row: int;
        private var _col: int;
        private var _type: int;
        private var _bitmap:Bitmap;

        public function GridCell() {
            super();
            init();
        }


        public function get row(): int {
            return _row;
        }


        public function set row(value: int): void {
            _row = value;
        }


        public function get col(): int {
            return _col;
        }


        public function set col(value: int): void {
            _col = value;
        }


        public function get type():int {
            return _type;
        }

        public function set type(value:int):void {
            _type = value;

            var linkageName:String = CellsTypeAdditionalData.CELLS_GFX_ACCORDANCE[value];
            var BDClass:Class = getDefinitionByName(linkageName) as Class;
            var bd:BitmapData = new BDClass() as BitmapData;

            _bitmap.bitmapData = bd;
        }

        private function init(): void {
            mouseChildren = false;

            _view = new View_GridCell() as MovieClip;

            _bitmap = new Bitmap();

            _view.addChild(_bitmap);
            addChild(_view);
        }

    }
}