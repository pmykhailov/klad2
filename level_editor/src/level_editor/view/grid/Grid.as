package level_editor.view.grid {
    import flash.display.Sprite;
    import flash.events.MouseEvent;

import level_editor.model.LevelEditorModel;

import level_editor.view.events.GridEvent;

    /**
     * Grid class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    public class Grid extends Sprite {

        private var _cols: int;
        private var _rows: int;
        private var _cells: Array;
        private var _activeCellType:int;

        public function Grid() {
            super();
            init();
        }

        public function get activeCellType():int {
            return _activeCellType;
        }

        public function set activeCellType(value:int):void {
            _activeCellType = value;
        }

        public function set data(value: Object): void {
            clear();

            var model:LevelEditorModel = value as LevelEditorModel;
            var map:Array = model.currentLevel.gameConfig.map;

            _cols = model.currentLevel.gameConfig.cols;
            _rows = model.currentLevel.gameConfig.rows;
            _cells = new Array();

            for (var i: int = 0; i < _rows; i++) {
                _cells[i] = new Array();
                for (var j: int = 0; j < _cols; j++) {
                    var gridCell: GridCell = new GridCell();

                    gridCell.x = j * gridCell.width;
                    gridCell.y = i * gridCell.height;
                    gridCell.row = i;
                    gridCell.col = j;
                    gridCell.type = map[i][j];

                    addChild(gridCell);

                    _cells[i][j] = gridCell;
                }
            }
        }

        private function clear():void {
            _cells = [];

            while (numChildren > 0) {
                removeChildAt(0);
            }
        }


        public function getGridCell(row: int, col: int): GridCell {
            return _cells[row][col];
        }


        private function init(): void {
            addEventListener(MouseEvent.MOUSE_DOWN, onCellMouseDownHandler);
            addEventListener(MouseEvent.MOUSE_UP, onCellMouseUpHandler);
        }


        private function onCellMouseOverHandler(event: MouseEvent): void {
            var cell:GridCell = event.target as GridCell;
            cell.type = _activeCellType;
            dispatchEvent(new GridEvent(GridEvent.GRID_CELL_CLICKED, true, false, {cell: cell, type: _activeCellType}));
        }


        private function onCellMouseDownHandler(event: MouseEvent): void {
            var cell:GridCell = event.target as GridCell;
            cell.type = _activeCellType;
            dispatchEvent(new GridEvent(GridEvent.GRID_CELL_CLICKED, true, false, {cell: cell, type: _activeCellType}));

            addEventListener(MouseEvent.MOUSE_OVER, onCellMouseOverHandler);
        }

        private function onCellMouseUpHandler(event: MouseEvent): void {
            removeEventListener(MouseEvent.MOUSE_OVER, onCellMouseOverHandler);
        }

    }
}