package level_editor.view.events {
    import flash.events.Event;

    /**
     * GridEvent class.
     * User: Paul Makarenko
     * Date: 26.09.2014
     */
    public class GridEvent extends Event {

        /** Data of this event is object with properties grid_type and cell **/
        public static const GRID_CELL_CLICKED: String = "gridCellClicked";

        private var _data: Object;


        public function GridEvent(type: String, bubbles: Boolean = false, cancelable: Boolean = false, data: Object = null) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new GridEvent(type, bubbles, cancelable, _data);
        }
    }
}