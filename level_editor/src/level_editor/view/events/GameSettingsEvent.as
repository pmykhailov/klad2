/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.14
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.events {
import flash.events.Event;

public class GameSettingsEvent extends Event {

    public static const APPLY_SPAWN_CELL:String = "applySpawnCell";
    public static const APPLY_EXIT_CELL:String = "applyExitCell";
    public static const APPLY_GRID_DIMENSIONS:String = "applyGridDimensions";

    // Internal events
    public static const CELL_TYPE_SELECTED:String = "cellTypeSelected";
    public static const AI_CONFIG_REMOVE:String = "aiRemove";
    public static const AI_CONFIG_ADD:String = "aiAdd";

    private var _data:Object;

    public function GameSettingsEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, data:Object = null) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone(): Event {
        return new LevelSettingsEvent(type, bubbles, cancelable, _data);
    }
}
}
