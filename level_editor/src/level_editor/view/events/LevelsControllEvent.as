/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.14
 * Time: 10:10
 * To change this template use File | Settings | File Templates.
 */
package level_editor.view.events {
import flash.events.Event;

public class LevelsControllEvent extends Event {

    public static const ADD_LEVEL_BUTTON_CLICKED:String = "addLevelButtonClicked";
    public static const REMOVE_LEVEL_BUTTON_CLICKED:String = "removeLevelButtonClicked";
    public static const MOVE_LEVEL_UP_BUTTON_CLICKED:String = "moveLevelUpButtonClicked";
    public static const MOVE_LEVEL_DOWN_BUTTON_CLICKED:String = "moveLevelDownButtonClicked";
    public static const LEVEL_SELECTED:String = "levelSelected";

    public function LevelsControllEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false, data:Object = null) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    private var _data:Object;

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new LevelsControllEvent(type, bubbles, cancelable, _data);
    }
}
}
