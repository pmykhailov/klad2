package level_editor {
import flash.display.MovieClip;
import flash.display.Sprite;

    import level_editor.controller.LevelEditorController;
    import level_editor.model.LevelEditorModel;
    import level_editor.model.LevelEditorLevelModel;
    import level_editor.view.LevelEditorView;

    /**
     * Main class.
     * User: Paul Makarenko
     * Date: 18.04.14
     */
    [SWF(width='900', height='1000', backgroundColor='0xCCCCCC')]
    public class Main extends Sprite {

        private var _view: LevelEditorView;
        private var _model: LevelEditorModel;
        private var _controller: LevelEditorController;


        public function Main() {
            super();

            init();
        }


        private function init(): void {

            CellsTypeAdditionalData.init();

            _model = new LevelEditorModel();

            _view = new LevelEditorView();
            addChild(_view);

            _controller = new LevelEditorController(_model, _view);
        }

    }
}