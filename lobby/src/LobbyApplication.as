    /**
 * Created by Pasha on 30.07.2015.
 */
package {
import ad.AdMobManager;

import flash.display.Sprite;
import flash.events.Event;

import lobby.LobbyApplicationConfig;

import robotlegs.bender.bundles.mvcs.MVCSBundle;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.impl.Context;

import screens.ScreensConfig;

import utils.ScreenUtils;

import windows.controller.WindowsConfig;

[SWF(backgroundColor="0x000000", width="1280", height="720", frameRate="30")]
public class LobbyApplication extends Sprite {

    private var _context: IContext;

    public function LobbyApplication() {
        super();

        addEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);
    }

    private function onAddedToStageHandler(event: Event): void {
        removeEventListener(Event.ADDED_TO_STAGE, onAddedToStageHandler);

        ScreenUtils.instance.stage = stage;

        initAdMob();

        _context = new Context()
                .install(MVCSBundle)
                .configure(LobbyApplicationConfig, ScreensConfig, WindowsConfig)
                .configure(new ContextView(this));
    }

    private function initAdMob():void
    {
        AdMobManager.instance.init();
    }

}
}
