package screens.controller.init {
import flash.display.DisplayObject;
import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IInjector;

import screens.events.ScreensContextEvent;

import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;

/**
     * InitScreensCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class InitScreensCommand extends Command {

        [Inject]
        public var contextView:ContextView;

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var injector: IInjector;

        [Inject]
        public var lobbyModel:LobbyModel;

        public function InitScreensCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var screensLayerView: ScreensLayerView = new ScreensLayerView();

            screensLayerView.name = "ScreensLayerView";

            contextView.view.addChild(screensLayerView);

            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREENS_LAYER_INIT_COMPLETE));
        }
    }
}