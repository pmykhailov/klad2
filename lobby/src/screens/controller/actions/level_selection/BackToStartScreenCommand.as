/**
 * Created by Pasha on 14.06.2016.
 */
package screens.controller.actions.level_selection {
import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.vo.ShowScreenVO;

public class BackToStartScreenCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;

    public function BackToStartScreenCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.START)));

    }
}
}
