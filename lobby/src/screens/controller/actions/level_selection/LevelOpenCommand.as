package screens.controller.actions.level_selection {

import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.ShowWindowVO;

public class LevelOpenCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var lobbyModel:LobbyModel;

        public function LevelOpenCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var levelIndex:int = event.data as int;

            if (lobbyModel.userModel.livesLeft > 0) {

                dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.GAME)));
                dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_START, levelIndex));
            } else {
                dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.SHOW_WINDOW, new ShowWindowVO(WindowsTypeEnum.NO_LIVES_TO_PLAY, {levelIndex:levelIndex})));
            }
        }
    }
}
