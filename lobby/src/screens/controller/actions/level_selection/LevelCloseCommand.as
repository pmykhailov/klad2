package screens.controller.actions.level_selection {

import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.ShowWindowVO;
import windows.view.settings.vo.SettingsScreenVO;

public class LevelCloseCommand extends Command {

        [Inject]
        public var event: ScreensContextEvent;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var lobbyModel:LobbyModel;

        public function LevelCloseCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();
            // Pause game ...
            dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_PAUSE));

            // ... and show game paused window
            var vo:SettingsScreenVO = new SettingsScreenVO(lobbyModel.fxVolume, lobbyModel.musicVolume);
            var data:Object = {settingsScreenVO:vo};
            dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.SHOW_WINDOW, new ShowWindowVO(WindowsTypeEnum.GAME_PAUSED, data)));
        }
    }
}
