/**
 * Created by Pasha on 21.05.2016.
 */
package screens.controller.actions.loading {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.loading.LoadingScreen;

public class RetryCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var contextView:ContextView;

    public function RetryCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        loadingScreen.state = LoadingScreen.STATE_LOADING;

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.RETRY_USER_CREATION));
    }

    protected function get loadingScreen():LoadingScreen {
        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;

        return screensLayerView.activeScreen as LoadingScreen;
    }

}
}
