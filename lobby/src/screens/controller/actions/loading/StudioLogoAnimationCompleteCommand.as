/**
 * Created by Pasha on 21.05.2016.
 */
package screens.controller.actions.loading {
import flash.events.IEventDispatcher;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.vo.ShowScreenVO;

public class StudioLogoAnimationCompleteCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;


    public function StudioLogoAnimationCompleteCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.START)));
    }
}
}
