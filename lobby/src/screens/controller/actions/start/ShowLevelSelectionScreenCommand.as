/**
 * Created by Pasha on 07.08.2015.
 */
package screens.controller.actions.start {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;

public class ShowLevelSelectionScreenCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;


    public function ShowLevelSelectionScreenCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.LEVEL_SELECTION, {lobbyModel:lobbyModel})));
    }
}
}
