/**
 * Created by Pasha on 07.08.2015.
 */
package screens.controller.actions.start {
import flash.events.IEventDispatcher;
import flash.net.URLRequest;
import flash.net.navigateToURL;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.ShowWindowVO;
import windows.view.settings.vo.SettingsScreenVO;

public class MoreScreenCommand extends Command {


    public function MoreScreenCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var link:String = "https://play.google.com/store/apps/developer?id=Pavel+Mykhailov";
        navigateToURL(new URLRequest(link));
    }
}
}
