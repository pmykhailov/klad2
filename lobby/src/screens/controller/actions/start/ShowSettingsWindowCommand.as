/**
 * Created by Pasha on 07.08.2015.
 */
package screens.controller.actions.start {
import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.ShowWindowVO;
import windows.view.settings.vo.SettingsScreenVO;

public class ShowSettingsWindowCommand extends Command {

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var lobbyModel: LobbyModel;


    public function ShowSettingsWindowCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var vo:SettingsScreenVO = new SettingsScreenVO(lobbyModel.fxVolume, lobbyModel.musicVolume);
        var data:Object = {settingsScreenVO:vo};
        dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.SHOW_WINDOW, new ShowWindowVO(WindowsTypeEnum.SETTINGS, data)));
    }
}
}
