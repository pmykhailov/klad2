package screens.events {
    import flash.events.Event;

    /**
     * ScreensContextEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensContextEvent extends Event {

        public static const INIT_SCREENS_LAYER: String = "screensContextEventInitScreensLayer";
        public static const SCREENS_LAYER_INIT_COMPLETE: String = "screensContextEventScreensLayerInitComplete";

        // Screens general
        public static const SHOW_SCREEN: String = "screensContextEventShowScreen";

       // Events from different screens
        public static const SCREEN_CONTEXT_ACTION: String = "screensContextEventScreenContextAction";
        // Loading
        public static const ACTION_STUDIO_LOGO_ANIMATION_COMPLETE: String = "screensContextEventStudioLogoAnimationComplete";
        public static const ACTION_RETRY: String = "screensContextEventRetry";
        // Level selection screen
        public static const ACTION_LEVEL_OPEN: String = "screensContextEventLevelOpen";
        public static const ACTION_BACK: String = "screensContextEventBack";
        public static const ACTION_LEVEL_CLOSE: String = "screensContextEventLevelClose";
        // Start screen
        public static const ACTION_START: String = "screensContextEventStart";
        public static const ACTION_SETTINGS: String = "screensContextEventSettings";
        public static const ACTION_MORE: String = "screensContextEventMore";
        // Level complete screen and failed screen
        public static const ACTION_START_LEVEL: String = "screensContextEventStartNextLevel";
        public static const ACTION_BACK_TO_LEVEL_SELECTION: String = "screensContextEventBackToLevelSelection";


        private var _data: Object;


        public function ScreensContextEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new ScreensContextEvent(type, _data, bubbles, cancelable);
        }
    }
}