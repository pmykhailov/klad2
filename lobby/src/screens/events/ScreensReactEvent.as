package screens.events {
    import flash.events.Event;

    public class ScreensReactEvent extends Event {

        private var _data: Object;

        public function ScreensReactEvent(type: String, data: Object = null, bubbles: Boolean = false, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new ScreensReactEvent(type, _data, bubbles, cancelable);
        }
    }
}