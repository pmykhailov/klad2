package screens {


    import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

import screens.controller.actions.level_selection.BackToStartScreenCommand;

import screens.controller.actions.level_selection.LevelCloseCommand;
import screens.controller.actions.loading.RetryCommand;
import screens.controller.actions.loading.StudioLogoAnimationCompleteCommand;
import screens.controller.actions.start.MoreScreenCommand;
import screens.controller.actions.start.ShowLevelSelectionScreenCommand;
import screens.controller.actions.start.ShowSettingsWindowCommand;


import screens.controller.init.InitScreensCommand;
    import screens.controller.actions.level_selection.LevelOpenCommand;

    import screens.events.ScreensContextEvent;
    import screens.view.core.layer.ScreenLayerMediator;

    import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;
import windows.view.core.WindowsManager;

public class ScreensConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(ScreensContextEvent.INIT_SCREENS_LAYER).toCommand(InitScreensCommand);

            eventCommandMap.map(ScreensContextEvent.ACTION_STUDIO_LOGO_ANIMATION_COMPLETE).toCommand(StudioLogoAnimationCompleteCommand);
            eventCommandMap.map(ScreensContextEvent.ACTION_RETRY).toCommand(RetryCommand);

            eventCommandMap.map(ScreensContextEvent.ACTION_LEVEL_OPEN).toCommand(LevelOpenCommand);
            eventCommandMap.map(ScreensContextEvent.ACTION_BACK).toCommand(BackToStartScreenCommand);
            eventCommandMap.map(ScreensContextEvent.ACTION_LEVEL_CLOSE).toCommand(LevelCloseCommand);

            eventCommandMap.map(ScreensContextEvent.ACTION_START).toCommand(ShowLevelSelectionScreenCommand);
            eventCommandMap.map(ScreensContextEvent.ACTION_SETTINGS).toCommand(ShowSettingsWindowCommand);
            eventCommandMap.map(ScreensContextEvent.ACTION_MORE).toCommand(MoreScreenCommand);

            eventCommandMap.map(ScreensContextEvent.ACTION_BACK_TO_LEVEL_SELECTION).toCommand(ShowLevelSelectionScreenCommand);
            eventCommandMap.map(ScreensContextEvent.ACTION_START_LEVEL).toCommand(LevelOpenCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(ScreensLayerView).toMediator(ScreenLayerMediator);
        }


        private function mapInjections(): void {

        }


        private function init(): void {
            dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.INIT_SCREENS_LAYER));
        }
    }
}
