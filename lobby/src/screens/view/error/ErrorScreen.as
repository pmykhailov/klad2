package screens.view.error {
import screens.view.core.BaseScreen;

public class ErrorScreen extends BaseScreen {

    public function ErrorScreen() {
        super(new View_ErrorScreen());
    }
}
}