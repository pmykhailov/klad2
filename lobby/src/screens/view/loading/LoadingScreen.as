package screens.view.loading {
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import screens.events.ScreensContextEvent;

import screens.view.core.BaseScreen;
import screens.view.core.vo.ScreenContextActionVO;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

use namespace SoundAS;

public class LoadingScreen extends BaseScreen {

    public static const STATE_LOADING:String = "_stateLoading";
    public static const STATE_LOADING_FAILED:String = "_stateLoadingFailed";
    public static const STATE_LOADING_COMPLETE:String = "_stateLoadingComplete";

    private var _state:String;

    public function LoadingScreen() {
        super(new View_LoadingScreen());
    }

    override protected function init():void {
        super.init();
        _view.addEventListener(Event.COMPLETE, onStudioLogoAnimationCompleteHandler);
    }

    override public function destroy():void {
        super.destroy();
        _view.removeEventListener(Event.COMPLETE, onStudioLogoAnimationCompleteHandler);
    }

    public function set state(value:String):void {
        beforeStateChange();

        _state = value;
        _loadingScreenView.gotoAndStop(value);

        afterStateChange();
    }

    private function beforeStateChange():void {
        if (_state == STATE_LOADING_FAILED) {
            _buttonRetry.removeEventListener(MouseEvent.CLICK, onRetryButtonClickHandler);
        }
    }

    private function afterStateChange():void {
        if (_state == STATE_LOADING_FAILED) {
            _buttonRetry.addEventListener(MouseEvent.CLICK, onRetryButtonClickHandler);
        }
    }

    private function onRetryButtonClickHandler(event:MouseEvent):void {
        var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_RETRY);
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
    }

    private function get _loadingScreenView():MovieClip {
        return _view as MovieClip;
    }

    private function get _buttonRetry():DisplayObject {
        return _view["_buttonRetry"] as DisplayObject;
    }

    private function onStudioLogoAnimationCompleteHandler(event:Event):void {
        var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_STUDIO_LOGO_ANIMATION_COMPLETE);
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
    }

}
}