package screens.view.level_complete {
import screens.view.level_selection.*;

import flash.display.Sprite;
import flash.events.MouseEvent;

import lobby.model.levels.LevelModel;

import screens.events.ScreensContextEvent;

import screens.view.core.BaseScreen;
import screens.view.core.vo.ScreenContextActionVO;
import screens.view.level_selection.levels_list.StarsController;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

use namespace SoundAS;


/**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class LevelCompleteScreen extends BaseScreen {

        private var _nextLevelIndex:int;
        private var _completesLevelIndex:int;
        private var _starsController:StarsController;

        public function LevelCompleteScreen() {
            super(new View_LevelCompleteScreen());
        }


        override protected function init():void {
            super.init();

            _starsController = new StarsController(_view["_starsHolder"] as Sprite);

            buttonNextLevel.addEventListener(MouseEvent.CLICK, onButtonNextLevelClickHandler);
            buttonLevelSelection.addEventListener(MouseEvent.CLICK, onButtonLevelSelectionClickHandler);
            buttonRetryLevel.addEventListener(MouseEvent.CLICK, onButtonRetryLevelClickHandler);
        }

        override public function set data(value:Object):void
        {
            _nextLevelIndex = value.nextLevelIndex as int;
            _completesLevelIndex = value.completesLevelIndex as int;

            _starsController.data = value;
        }

        override public function destroy():void {
            super.destroy();

            _starsController.destroy();

            buttonNextLevel.removeEventListener(MouseEvent.CLICK, onButtonNextLevelClickHandler);
            buttonLevelSelection.removeEventListener(MouseEvent.CLICK, onButtonLevelSelectionClickHandler);
            buttonRetryLevel.removeEventListener(MouseEvent.CLICK, onButtonRetryLevelClickHandler);
        }

        private function onButtonLevelSelectionClickHandler(event:MouseEvent):void {
            SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

            var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_BACK_TO_LEVEL_SELECTION);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
        }

        private function onButtonRetryLevelClickHandler(event:MouseEvent):void {
            SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

            var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_START_LEVEL, _completesLevelIndex);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
        }

        private function onButtonNextLevelClickHandler(event:MouseEvent):void {
            SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

            var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_START_LEVEL, _nextLevelIndex);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
        }

        private function get buttonRetryLevel():Sprite
        {
            return _view["_buttonRetryLevel"] as Sprite;
        }

        private function get buttonNextLevel():Sprite
        {
            return _view["_buttonNextLevel"] as Sprite;
        }

        private function get buttonLevelSelection():Sprite
        {
            return _view["_buttonLevelSelection"] as Sprite;
        }
}
}