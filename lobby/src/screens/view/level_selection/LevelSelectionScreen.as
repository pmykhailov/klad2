package screens.view.level_selection {

import flash.display.Sprite;
import flash.events.MouseEvent;

import lobby.LobbyApplicationDimensions;

import lobby.model.LobbyModel;

import lobby.model.levels.LevelModel;

import screens.events.ScreensContextEvent;
import screens.view.common.LivesInfoView;

import screens.view.core.BaseScreen;
import screens.view.core.vo.ScreenContextActionVO;
import screens.view.game.InfoPanel;
import screens.view.level_selection.levels_list.LevelIR;
import screens.view.level_selection.levels_list.LevelIREvent;
import screens.view.level_selection.levels_list.LevelsList;
import screens.view.level_selection.levels_list.SmallBricksController;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import utils.ScreenUtils;

use namespace SoundAS;


/**
     * LevelSelectionScreen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class LevelSelectionScreen extends BaseScreen {

        private var _levelsList:LevelsList;
        private var _livesInfoView:LivesInfoView;

        public function LevelSelectionScreen() {
            super(new View_LevelSelectionScreen());
        }


        override protected function init():void {
            super.init();

            _levelsList = new LevelsList(_view["_levelsList"]);
            _view["_levelsList"].addEventListener(LevelIREvent.LEVEL_IR_CLICKED, onLevelIRClickedHandler);
            _view["_buttonBack"].addEventListener(MouseEvent.CLICK, onButtonBackClicked);

            var livesInfo:Sprite = _view["_livesInfo"] as Sprite;
            _livesInfoView = new LivesInfoView(livesInfo);
            _livesInfoView.nextLiveTf.visible = false;
        }

        override public function set data(value:Object):void
        {
            var lobbyModel:LobbyModel = value.lobbyModel as LobbyModel;
            var levelModels:Vector.<LevelModel> = lobbyModel.levelModels;

            _levelsList.data = levelModels;

            _livesInfoView.setLives(lobbyModel.userModel.livesLeft, lobbyModel.userModel.livesTotal);
        }

        private function onLevelIRClickedHandler(event:LevelIREvent):void {
            SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

            var levelIndex:int = (event.data as int) - 1;
            var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_LEVEL_OPEN, levelIndex);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
        }

        private function onButtonBackClicked(event:MouseEvent):void {
            var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_BACK);
            dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
        }

        override public function destroy():void {
            super.destroy();

            _levelsList.destroy();
            _levelsList = null;

            _view["_levelsList"].removeEventListener(LevelIREvent.LEVEL_IR_CLICKED, onLevelIRClickedHandler);
            _view["_buttonBack"].removeEventListener(MouseEvent.CLICK, onButtonBackClicked);

        }

        public function get livesInfoView():LivesInfoView {
            return _livesInfoView;
        }
}
}