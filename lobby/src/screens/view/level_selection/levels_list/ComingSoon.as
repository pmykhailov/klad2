/**
 * Created by Pasha on 15.06.2016.
 */
package screens.view.level_selection.levels_list {
import com.greensock.TweenLite;

import flash.display.Sprite;
import flash.geom.Point;

public class ComingSoon extends LevelsLayout {

    private var _comingSoon:Sprite;

    public function ComingSoon(view:Sprite, layoutNumber:int) {
        super(view, layoutNumber);
    }

    override protected function _init():void {
        _comingSoon = _view["_comingSoon"] as Sprite;

        _coordinates = [new Point(_comingSoon.x, _comingSoon.y)];
    }

    override public function set visible(value:Boolean):void {
        _comingSoon.visible = value;
    }

    override public function destroy():void {
        super.destroy();
    }

    override public function shift(deltaX:Number, animate:Boolean = false):void {

        if (!animate) {
            _comingSoon.x = _coordinates[0].x + deltaX;
            if (_comingSoon.x > _coordinates[0].x) {
                _comingSoon.x = _coordinates[0].x;
            }
        } else {
            TweenLite.to(_comingSoon, 0.5, {x:_coordinates[0].x + deltaX});
        }
    }

    override public function playShowAnimation():void {
        _comingSoon.x = _coordinates[0].x - offset;
        TweenLite.to(_comingSoon,10,{x:_coordinates[0].x, useFrames:true});
    }

    override public function playHideAnimation(direction:int):void {
        TweenLite.to(_comingSoon,10,{x:_coordinates[0].x + direction*offset, useFrames:true});
    }

    override public function set data(value:Object) {
        visible = value != null;
    }
}
}
