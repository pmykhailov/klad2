/**
 * Created by Pasha on 01.05.2016.
 */
package screens.view.level_selection.levels_list {
import flash.display.Sprite;

public class SmallBrick {

    private var _view:Sprite;
    private var _baseX:Number;
    private var _baseY:Number;

    public function SmallBrick(view:Sprite) {
        _view = view;
        _init();
    }

    private function _init():void {
        _baseX = _view.x;
        _baseY = _view.y;
    }

    public function get view():Sprite {
        return _view;
    }

    public function get baseX():Number {
        return _baseX;
    }

    public function get baseY():Number {
        return _baseY;
    }
}
}
