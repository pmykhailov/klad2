/**
 * Created by Pasha on 30.12.2015.
 */
package screens.view.level_selection.levels_list {
import com.greensock.TimelineMax;
import com.greensock.TweenLite;
import com.greensock.easing.Linear;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.geom.Point;

import lobby.model.levels.LevelModel;

import misc.IDestroyable;

import screens.view.level_selection.levels_list.LevelIR;

public class LevelsLayout implements IDestroyable{

    public static var CAPACITY : int;
    protected var _view:Sprite;
    protected var _layoutNumber:int;
    [ArrayElementType("screens.view.level_selection.levels_list.LevelIR")]
    protected var _levelIRs:Array;
    [ArrayElementType("flash.geom.Point")]
    protected var _coordinates:Array;
    protected const offset:Number = 440;

    public function LevelsLayout(view:Sprite, layoutNumber:int) {
        _view = view;
        _layoutNumber = layoutNumber;
        _init();
    }

    protected function _init():void {
        var i:int = 0;
        var levelIRView:DisplayObject;

        _levelIRs = [];
        _coordinates = [];

        while (levelIRView = _view.getChildByName("_layout" + _layoutNumber + "_level" + i))
        {
            _levelIRs.push(new LevelIR(levelIRView as Sprite));
            _coordinates.push(new Point(levelIRView.x, levelIRView.y));
            i++;
        }

        CAPACITY = _levelIRs.length;
    }

    public function set data(value:Object)
    {
        var levelModels:Vector.<LevelModel> = value as Vector.<LevelModel>;
        var i:int;

        for (i = 0; i < _levelIRs.length; i++) {
            _levelIRs[i].visible = false;
        }

        for (i = 0; i < levelModels.length; i++) {
            var levelModel:LevelModel = levelModels[i];
            _levelIRs[i].data = levelModel;
            _levelIRs[i].levelNumber = levelModel.number;
            _levelIRs[i].visible = true;
        }
    }

    public function set visible(value:Boolean):void
    {
        for (var i:int = 0; i < _levelIRs.length; i++) {
            var ir:LevelIR = _levelIRs[i];
            ir.visible = value;
        }
    }

    public function playShowAnimation():void
    {
        var timeline:TimelineMax = new TimelineMax({useFrames:true});

        for (var i:int = 0; i < _levelIRs.length; i++) {
            var ir:LevelIR = _levelIRs[i];
            ir.view.x = _coordinates[i].x - offset;
            ir.view.y = _coordinates[i].y;

            timeline.insert(new TweenLite(ir.view,15,{x:_coordinates[i].x, delay:i*2/*, ease:Linear.easeIn*/}))
        }
    }

    public function playHideAnimation(direction:int):void
    {
        var timeline:TimelineMax = new TimelineMax({useFrames:true});

        for (var i:int = 0; i < _levelIRs.length; i++) {
            var ir:LevelIR = _levelIRs[i];
            timeline.insert(new TweenLite(ir.view,10,{x:_coordinates[i].x + direction*offset}))
        }

    }

    public function shift(deltaX:Number, animate:Boolean = false):void {
        for (var i:int = 0; i < _levelIRs.length; i++) {
            var ir:LevelIR = _levelIRs[i];

            if (!animate) {
                ir.view.x = _coordinates[i].x + deltaX;
            } else {
                TweenLite.to(ir.view, 0.5, {x:_coordinates[i].x + deltaX});
            }
        }
    }

    public function destroy():void {
    }
}
}
