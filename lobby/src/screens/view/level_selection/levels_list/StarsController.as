/**
 * Created by Pasha on 02.07.2016.
 */
package screens.view.level_selection.levels_list {
import flash.display.MovieClip;
import flash.display.Sprite;

import lobby.model.levels.LevelModel;

import misc.IDestroyable;

public class StarsController implements IDestroyable{
    private var _starsHolder:Sprite;

    public function StarsController(starsHolder:Sprite) {
        _starsHolder = starsHolder;
    }

    public function set data(value:Object):void {
        var i:int;

        if (value.isLocked) {
            for (i = 0; i < value.starsGot; i++) {
                getStar(i).gotoAndStop("_invisible");
            }
        } else {
            for (i = 0; i < value.starsGot; i++) {
                getStar(i).gotoAndStop("_active");
            }

            for (i = value.starsGot; i < value.starsTotal; i++) {
                getStar(i).gotoAndStop("_inactive");
            }
        }

    }

    private function getStar(i:int):MovieClip {
        return _starsHolder["_star" + i] as MovieClip;
    }

    public function destroy():void {
        _starsHolder = null;
    }
}
}
