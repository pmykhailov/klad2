/**
 * Created by Pasha on 20.01.2016.
 */
package screens.view.level_selection.levels_list.selector {
import flash.display.Sprite;
import flash.events.MouseEvent;

public class LevelsPageSelector extends Sprite{

    [ArrayElementType("screens.view.level_selection.levels_list.selector.LevelPageSelectorIR")]
    private var _items:Array;
    private var _pagesCount:int;
    private var _selectedPage:int;

    public function LevelsPageSelector() {
    }

    public function set pagesCount(value:int):void {
        _pagesCount = value;
        _initItems();
    }

    public function get selectedPage():int {
        return _selectedPage;
    }

    public function set selectedPage(value:int):void {

        _selectedPage = value;

        for (var i:int = 0; i < _pagesCount; i++) {
            _items[i].state = LevelPageSelectorIR.UNSELECTED;
        }

        _items[value].state = LevelPageSelectorIR.SELECTED;
    }

    private function _initItems():void {
        _items = [];

        var item:LevelPageSelectorIR;
        for (var i:int = 0; i < _pagesCount; i++) {
            item = new LevelPageSelectorIR();
            item.x = i * 50;
            addChild(item);
            _items[i] = item;
        }

        _selectedPage = -1;

        addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownHandler);
    }

    private function onMouseDownHandler(event:MouseEvent):void {
        var item: LevelPageSelectorIR = event.target as LevelPageSelectorIR;
        var pageIndex:int;

        for (var i:int = 0; i < _pagesCount; i++) {
            _items[i].state = LevelPageSelectorIR.UNSELECTED;
            if (_items[i] == item) {
                pageIndex = i;
            }
        }

        item.state = LevelPageSelectorIR.SELECTED;

        dispatchEvent(new LevelPageSelectorEvent(LevelPageSelectorEvent.PAGE_SELECTED, pageIndex));
    }


}
}
