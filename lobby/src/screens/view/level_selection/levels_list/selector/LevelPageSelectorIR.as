/**
 * Created by Pasha on 20.01.2016.
 */
package screens.view.level_selection.levels_list.selector {
import flash.display.MovieClip;
import flash.display.Sprite;

public class LevelPageSelectorIR extends Sprite{

    public static const SELECTED: String = "_selected";
    public static const UNSELECTED: String = "_unselected";

    private var _view:MovieClip;

    public function LevelPageSelectorIR() {
        _init();
    }

    private function _init():void {
        mouseChildren = false;

        _view = new View_LevelPageSelectorIR();
        _view.gotoAndStop(1);

        addChild(_view);
    }

    public function set state(value:String) {
        _view.gotoAndStop(value);
    }

}
}
