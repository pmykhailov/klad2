/**
 * Created by Pasha on 20.01.2016.
 */
package screens.view.level_selection.levels_list.selector {
import flash.events.Event;

public class LevelPageSelectorEvent extends Event {

    public static const PAGE_SELECTED:String = "pageSelected";

    private var _data:Object;

    public function LevelPageSelectorEvent(type:String, data:Object, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }


    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new LevelPageSelectorEvent(type, _data, bubbles, cancelable);
    }
}
}
