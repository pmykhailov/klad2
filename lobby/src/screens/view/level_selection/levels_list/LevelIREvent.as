/**
 * Created by Pasha on 08.03.2016.
 */
package screens.view.level_selection.levels_list {
import flash.events.Event;

public class LevelIREvent extends Event {

    public static const LEVEL_IR_CLICKED:String = "levelIREventLevelIRClicked";

    private var _data:Object;

    public function LevelIREvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    override public function clone():Event {
        return new LevelIREvent(type, data, bubbles, cancelable);
    }

    public function get data():Object {
        return _data;
    }
}
}
