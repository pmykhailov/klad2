/**
 * Created by Pasha on 07.08.2015.
 */
package screens.view.level_selection.levels_list {
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.text.TextField;

import misc.IDestroyable;

import lobby.model.levels.LevelModel;

public class LevelIR implements IDestroyable{

    private var _view:Sprite;
    private var _levelNumber:int;
    private var _starsController:StarsController;

    public function LevelIR(view:Sprite) {
        super();
        _view = view;
        init();
    }

    private function init():void {
        _starsController = new StarsController(_view["_starsHolder"] as Sprite);
        levelNumberTf.mouseEnabled = false;
        _view.addEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
    }

    public function set data(value:Object):void {
        var levelModel:LevelModel = value as LevelModel;

        if (levelModel.isLocked)
        {
            _view.alpha = 0.5;
            _view.mouseEnabled = false;
            _view.mouseChildren = false;
        }

        _starsController.data = value;
    }

    public function set levelNumber(value:int):void {
        _levelNumber = value;
        levelNumberTf.text = value + "";
    }

    private function get levelNumberTf():TextField {
        return _view["_levelNumberTf"] as TextField;
    }

    private function get starsHolder():Sprite {
        return _view["_starsHolder"] as Sprite;
    }

    public function get levelNumber():int {
        return _levelNumber;
    }

    public function destroy():void {
        _starsController.destroy();
        _view.removeEventListener(MouseEvent.MOUSE_UP, onMouseUpHandler);
    }

    public function set visible(value:Boolean):void
    {
        _view.visible = value;
    }

    private function onMouseUpHandler(event:MouseEvent):void {
        _view.dispatchEvent(new LevelIREvent(LevelIREvent.LEVEL_IR_CLICKED, _levelNumber, true));
    }

    public function get view():Sprite {
        return _view;
    }
}
}
