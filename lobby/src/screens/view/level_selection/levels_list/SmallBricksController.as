/**
 * Created by Pasha on 01.05.2016.
 */
package screens.view.level_selection.levels_list {
import com.greensock.TweenLite;

import flash.display.DisplayObjectContainer;
import flash.display.Sprite;

import misc.IDestroyable;

public class SmallBricksController implements IDestroyable{

    private var _view:DisplayObjectContainer;

    [ArrayElementType("screens.view.level_selection.levels_list.SmallBrick")]
    private var _bricks:Array;

    public function SmallBricksController(view:DisplayObjectContainer) {
        _view = view;
        _init();
    }

    private function _init():void {
        _bricks = [];
        for (var i:int = 0; i<_view.numChildren; i++) {
            _bricks[i] = new SmallBrick(_view.getChildAt(i) as Sprite);
        }
    }

    public function animate():void {
        var n = _bricks.length;
        var shiftValue:int = 2;

        for (var i:int = 0; i<n; i++) {
            TweenLite.to(_bricks[i].view, 1, {x:_bricks[i].baseX + shiftValue*rndDirection, y:_bricks[i].baseY + shiftValue*rndDirection});
        }
    }

    private function get rndDirection():int {
        return Math.random() > 0.5 ? 1 : -1;
    }

    public function destroy():void {
    }
}
}
