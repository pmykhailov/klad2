/**
 * Created by Pasha on 07.08.2015.
 */
package screens.view.level_selection.levels_list {
import flash.events.MouseEvent;
import flash.geom.Point;

import screens.view.level_selection.*;

import flash.display.Sprite;

import misc.IDestroyable;

import lobby.model.levels.LevelModel;

import screens.view.level_selection.levels_list.selector.LevelPageSelectorEvent;

import screens.view.level_selection.levels_list.selector.LevelsPageSelector;

public class LevelsList implements IDestroyable{

    private var _levelModels:Vector.<LevelModel>;
    private var _pagesCount:int;

    private var _view:Sprite;

    [ArrayElementType("screens.view.level_selection.levels_list.LevelsLayout")]
    private var _layouts:Array;
    private var _layoutsCount:int;
    private var _currentLayoutIndex:int;

    private var _pageSelector:LevelsPageSelector;
    private var _smallBricksController:SmallBricksController;

    private var _swipeZone:Sprite;
    private var _swipeZoneHitX:Number;
    private var _swipeDeltaX:Number;

    public function LevelsList(view:Sprite) {
        super();

        _view = view;

        init();
    }

    private function init():void {
        // init layouts
        _currentLayoutIndex = -1;
        _layoutsCount = 2;
        _layouts = [];
        for (var i:int = 0; i < _layoutsCount; i++) {
            _layouts[i] = new LevelsLayout(_view, i);
            _layouts[i].visible = false;
        }

        // And coming soon as a new layout
        var comingSoon:ComingSoon = new ComingSoon(_view, i);
        comingSoon.visible = false;
        _layouts.push(comingSoon);
        _layoutsCount++;

        // init page selector
        _pageSelector = new LevelsPageSelector();
        _pageSelector.addEventListener(LevelPageSelectorEvent.PAGE_SELECTED, onPageSelectedHandler);
        _pageSelector.x = 900;
        _pageSelector.y = 600;
        _view.addChild(_pageSelector);

        // init additional animations
        _smallBricksController = new SmallBricksController(_view["_smallBricks"]);

        // swipe zone
        _swipeZone = _view["_swipeZone"] as Sprite;
        _swipeZone.addEventListener(MouseEvent.MOUSE_DOWN, onSwipeZoneMouseDownHandler);
    }

    private function onSwipeZoneMouseDownHandler(event:MouseEvent):void {
        _swipeZoneHitX = _swipeZone.mouseX;
        _swipeDeltaX = 0;

        _swipeZone.mouseEnabled = _swipeZone.mouseChildren = false;
        _swipeZone.stage.addEventListener(MouseEvent.MOUSE_MOVE, onSwipeZoneMouseMoveHandler);
        _swipeZone.stage.addEventListener(MouseEvent.MOUSE_UP, onSwipeZoneUpHandler, true);
    }

    private function onSwipeZoneMouseMoveHandler(event:MouseEvent):void {
        _swipeDeltaX = _swipeZone.mouseX - _swipeZoneHitX;
        _layouts[_currentLayoutIndex].shift(_swipeDeltaX);
    }

    private function onSwipeZoneUpHandler(event:MouseEvent):void {

        if (Math.abs(_swipeDeltaX) > 10) {
            var sign:int = _swipeDeltaX > 0 ? 1 : -1;
            var newPageIndex:int = _pageSelector.selectedPage + sign;

            if (newPageIndex >= 0 && newPageIndex <= _pagesCount) {
                selectPageByIndex(newPageIndex);
            }else {
                _layouts[_currentLayoutIndex].shift(0, true);
            }

            // Don't let MOUSE_UP event appear in LevelIR
            // In other case level will be opened on MOUSE_UP simultaneously with swipe action
            event.stopImmediatePropagation();
        }else {
            _layouts[_currentLayoutIndex].shift(0);
        }
        _swipeZone.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onSwipeZoneMouseMoveHandler);
        _swipeZone.stage.removeEventListener(MouseEvent.MOUSE_UP, onSwipeZoneUpHandler, true);
        _swipeZone.mouseEnabled = _swipeZone.mouseChildren = true;
    }

    public function set data(value:Object):void {
        clear();

        _levelModels = value as Vector.<LevelModel>;
        _pagesCount = Math.ceil(_levelModels.length / LevelsLayout.CAPACITY);

        _pageSelector.pagesCount = _pagesCount + 1; // + 1 is for coming soon page

        selectPageByIndex(0);
    }

    private function selectPageByIndex(pageIndex:int):void {
        // Update Page
        var pageData:Vector.<LevelModel> = new Vector.<LevelModel>();
        var startIndex:int = pageIndex*LevelsLayout.CAPACITY;
        var endIndex:int = Math.min((pageIndex+1)*LevelsLayout.CAPACITY, _levelModels.length);

        for (var i:int = startIndex; i < endIndex; i++) {
            var levelModel:LevelModel = _levelModels[i];
            pageData.push(levelModel);
        }

        if (_currentLayoutIndex >=0) {
            _layouts[_currentLayoutIndex].playHideAnimation(pageIndex - _pageSelector.selectedPage > 0 ? 1 : -1);
        }

        _currentLayoutIndex++;
        // If it's a page after last page when show coming soon
        if (pageIndex == _pagesCount) {
            _currentLayoutIndex = _layoutsCount - 1;
        } else
        if (_currentLayoutIndex >= _layoutsCount - 1) {
            _currentLayoutIndex = 0;
        }

        _layouts[_currentLayoutIndex].playShowAnimation();
        _layouts[_currentLayoutIndex].data = pageData;

        // Update selector
        _pageSelector.selectedPage = pageIndex;

        // Animate bricks
        _smallBricksController.animate();
    }

    private function onPageSelectedHandler(event:LevelPageSelectorEvent) {
        var pageIndex: int = event.data as int;

        selectPageByIndex(pageIndex);
    }

    private function clear():void {

    }


    public function destroy():void {
        _smallBricksController.destroy();
        _smallBricksController = null;

        _swipeZone.removeEventListener(MouseEvent.MOUSE_DOWN, onSwipeZoneMouseDownHandler);
        _swipeZone.stage.removeEventListener(MouseEvent.MOUSE_UP, onSwipeZoneUpHandler, true);
        _swipeZone.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onSwipeZoneMouseMoveHandler);
        _swipeZone = null;
    }
}
}
