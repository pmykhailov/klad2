package screens.view.level_failed {
import com.greensock.TweenLite;

import flash.display.MovieClip;

import flash.text.TextField;

import lobby.model.LobbyModel;

import lobby.model.levels.LevelModelEvent;

import screens.view.common.LivesInfoView;

import screens.view.level_selection.*;

import flash.display.Sprite;
import flash.events.MouseEvent;

import lobby.model.levels.LevelModel;

import screens.events.ScreensContextEvent;

import screens.view.core.BaseScreen;
import screens.view.core.vo.ScreenContextActionVO;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import utils.Format;

use namespace SoundAS;


/**
 * LevelSelectionScreen class.
 * User: Paul Makarenko
 * Date: 08.10.13
 */
public class LevelFailedScreen extends BaseScreen {

    private var _levelIndex:int;
    private var _livesInfoView:LivesInfoView;

    public function LevelFailedScreen() {
        super(new View_LevelFailedScreen());
    }

    // -------------------------------------------------
    // ---------------- GETTERS / SETTERS --------------
    // -------------------------------------------------

    override public function set data(value:Object):void {
        super.data = value;
        var lobbyModel:LobbyModel = value.lobbyModel as LobbyModel;

        _levelIndex = lobbyModel.activeLevel;
        _livesInfoView.setLives(lobbyModel.userModel.livesLeft, lobbyModel.userModel.livesTotal);
    }

    public function get livesInfoView():LivesInfoView {
        return _livesInfoView;
    }

    private function get _buttonRetryLevel():Sprite {
        return _buttonRetryLevelAnimation["_buttonRetryLevel"] as Sprite;
    }

    private function get _buttonLevelSelection():Sprite {
        return _view["_buttonLevelSelection"] as Sprite;
    }
    private function get _buttonRetryLevelAnimation():MovieClip {
        return _view["_buttonRetryLevelAnimation"] as MovieClip;
    }

    // ---------------------------------------
    // ---------------- INIT -----------------
    // ---------------------------------------

    override protected function init():void {
        super.init();

        _buttonRetryLevel.mouseEnabled = false;
        _buttonRetryLevelAnimation.alpha = 0.5;

        _livesInfoView = new LivesInfoView(_view["_livesInfo"]);
    }

    override protected function initViewListeners():void {
        super.initViewListeners();

        _buttonRetryLevel.addEventListener(MouseEvent.CLICK, onButtonRetryLevelClickHandler);
        _buttonLevelSelection.addEventListener(MouseEvent.CLICK, onButtonLevelSelectionClickHandler);

        addContextListener(LevelModelEvent.LIFE_RESTORED, onLifeRestored);
    }

    // ---------------------------------------
    // ---------------- DESTROY --------------
    // ---------------------------------------

    override public function destroy():void {
        super.destroy();

        _buttonRetryLevel.removeEventListener(MouseEvent.CLICK, onButtonRetryLevelClickHandler);
        _buttonLevelSelection.removeEventListener(MouseEvent.CLICK, onButtonLevelSelectionClickHandler);

        removeContextListener(LevelModelEvent.LIFE_RESTORED, onLifeRestored);
    }

    // ----------------------------------------------
    // ---------------- EVENT HANDLERS --------------
    // ----------------------------------------------

    private function onButtonLevelSelectionClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_BACK_TO_LEVEL_SELECTION);
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
    }

    private function onButtonRetryLevelClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_START_LEVEL, _levelIndex);
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
    }

    private function onLifeRestored(event:LevelModelEvent):void {
        _buttonRetryLevel.mouseEnabled = true;
        _buttonRetryLevelAnimation.alpha = 1;
        _buttonRetryLevelAnimation.gotoAndPlay(2);
    }

}
}