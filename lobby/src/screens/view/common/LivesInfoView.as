/**
 * Created by Pasha on 08.06.2016.
 */
package screens.view.common {
import com.greensock.TimelineLite;
import com.greensock.TweenLite;
import com.greensock.easing.Linear;

import flash.display.DisplayObject;
import flash.display.Sprite;
import flash.text.TextField;

import localization.LobbyTexts;

import utils.Format;

public class LivesInfoView {

    private var _view:Sprite;
    private var _livesViews:Array;
    private var _livesCount:int;

    public function LivesInfoView(view:Sprite) {
        _view = view;

        var live:DisplayObject;
        var i:int;

        _livesViews = [];

        while (live = _view.getChildByName("_life" + i)) {
            _livesViews[i] = live;
            i++;
        }
    }


    public function setLives(lives:int, totalLives:int):void
    {
        var i:int = 0;

        for (i = 0; i < lives; i++) {
            setLiveFill(i,1);
        }

        for (i = lives; i < totalLives; i++) {
            setLiveFill(i,0);
        }

        _livesCount = lives;
    }

    public function setLifeUpdateProgress(progress:Number):void
    {
        setLiveFill(_livesCount,progress);

        if (progress == 1) {
            var timeline:TimelineLite = new TimelineLite();
            var liveView:DisplayObject = _livesViews[_livesCount]
            timeline.append(new TweenLite(liveView, 0.2, {scaleX:1.2, scaleY:1.2, ease:Linear.easeNone}));
            timeline.append(new TweenLite(liveView, 0.2, {scaleX:0.8, scaleY:0.8, ease:Linear.easeNone}));
            timeline.append(new TweenLite(liveView, 0.1, {scaleX:1.1, scaleY:1.1, ease:Linear.easeNone}));
            timeline.append(new TweenLite(liveView, 0.05, {scaleX:1, scaleY:1, ease:Linear.easeNone}));
        }
    }

    public function setNextLifeIn(time:int):void
    {
        nextLiveTf.text = Format.time(time);
    }


    public function get livesTf():TextField {
        return _view.getChildByName("_livesTf") as TextField;
    }

    public function get nextLiveTf():TextField {
        return _view.getChildByName("_nextLiveTf") as TextField;
    }

    private function setLiveFill(index:int, value:Number):void {
        _livesViews[index]["_fill"].scaleY = value;
    }

  }
}
