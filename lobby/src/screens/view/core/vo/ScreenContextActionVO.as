/**
 * Created by Pasha on 31.07.2015.
 */
package screens.view.core.vo {
public class ScreenContextActionVO {

    private var _type:String;
    private var _data:Object;

    public function ScreenContextActionVO(type:String, data:Object = null) {
        _type = type;
        _data = data;
    }

    public function get type():String {
        return _type;
    }

    public function get data():Object {
        return _data;
    }
}
}
