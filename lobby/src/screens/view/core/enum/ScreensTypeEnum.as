package screens.view.core.enum  {

    /**
     * ScreensTypeEnum class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensTypeEnum {

        public static const START:String = "StartScreen";
        public static const LOADING:String = "LoadingScreen";
        public static const LEVEL_SELECTION:String = "LevelSelectionScreen";
        public static const GAME:String = "GameScreen";
        public static const LEVEL_COMPLETE:String = "LevelComplete";
        public static const LEVEL_FAILED:String = "LevelFailed";
        public static const ERROR:String = "ErrorScreen";

        public function ScreensTypeEnum() {
        }

    }
}