/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 09.09.15
 * Time: 16:19
 * To change this template use File | Settings | File Templates.
 */
package screens.view.core {
import flash.events.Event;

public class BaseScreenEvent extends Event {

    private var _data:Object;

    public function BaseScreenEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }


    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new BaseScreenEvent(type, data, bubbles, cancelable);
    }
}
}
