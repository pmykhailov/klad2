package screens.view.core.layer {
    import flash.display.Sprite;

import screens.events.ScreensContextEvent;

import screens.view.core.factory.ScreensFactory;

    import screens.view.core.BaseScreen;
import screens.view.core.vo.ShowScreenVO;

/**
     * ScreensManager class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensLayerView extends Sprite {

        private var _activeScreen: BaseScreen;
        private var _factory: ScreensFactory;


        public function ScreensLayerView() {
            super();

            _factory = new ScreensFactory();
        }


        public function get activeScreen(): BaseScreen {
            return _activeScreen;
        }


        public function showScreen(screenData: ShowScreenVO): void {
            if (_activeScreen) {
                removeActiveScreenListeners();

                _activeScreen.destroy();

                removeChild(_activeScreen);
            }

            _activeScreen = _factory.createScreen(screenData.type);

            if (screenData.data) {
                _activeScreen.data = screenData.data;
            }

            addActiveScreenListeners();

            addChild(_activeScreen);
        }


        private function addActiveScreenListeners(): void {
            _activeScreen.addEventListener(ScreensContextEvent.SCREEN_CONTEXT_ACTION, dispatchEvent);
        }

        private function removeActiveScreenListeners(): void {
            _activeScreen.removeEventListener(ScreensContextEvent.SCREEN_CONTEXT_ACTION, dispatchEvent);
        }
    }
}