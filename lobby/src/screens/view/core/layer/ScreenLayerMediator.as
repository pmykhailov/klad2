package screens.view.core.layer {

import flash.events.Event;

import robotlegs.bender.bundles.mvcs.Mediator;

import screens.events.ScreensContextEvent;
import screens.view.core.vo.ScreenContextActionVO;
import screens.view.core.vo.ShowScreenVO;
import screens.view.game.GameScreen;

/**
     * ScreenLayerMediator class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreenLayerMediator extends Mediator {

        [Inject]
        public var _screensLayer:ScreensLayerView;

        public function ScreenLayerMediator() {
            super();
        }

        override public function initialize(): void {
            super.initialize();

            addContextListener(ScreensContextEvent.SHOW_SCREEN, onShowScreenHandler);
            addViewListener(ScreensContextEvent.SCREEN_CONTEXT_ACTION, onScreenContextActionHandler);
        }

        private function onScreenContextActionHandler(event: ScreensContextEvent): void {
            var vo:ScreenContextActionVO = event.data as ScreenContextActionVO;

            dispatch(new ScreensContextEvent(vo.type, vo.data));
        }

        private function onShowScreenHandler(event: ScreensContextEvent): void {
            var vo:ShowScreenVO = event.data as ShowScreenVO;
            if (vo.data == null) {
                vo.data = {};
            }

            vo.data.addContextListener = addContextListener;
            vo.data.removeContextListener = removeContextListener;

            _screensLayer.showScreen(vo);
        }
}
}