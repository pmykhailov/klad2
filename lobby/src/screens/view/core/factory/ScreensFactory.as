package screens.view.core.factory  {

import screens.view.core.BaseScreen;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.error.ErrorScreen;
import screens.view.game.GameScreen;
import screens.view.level_complete.LevelCompleteScreen;
import screens.view.level_failed.LevelFailedScreen;
import screens.view.level_selection.LevelSelectionScreen;
import screens.view.loading.LoadingScreen;
import screens.view.start.StartScreen;

/**
     * ScreensFactory class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class ScreensFactory {

        public function ScreensFactory() {
        }


        public function createScreen(type: String): BaseScreen {
            switch (type) {

                case ScreensTypeEnum.LOADING:
                    return new LoadingScreen();
                    break;

                case ScreensTypeEnum.START:
                    return new StartScreen();
                    break;

                case ScreensTypeEnum.LEVEL_SELECTION:
                    return new LevelSelectionScreen();
                    break;

                case ScreensTypeEnum.GAME:
                    return new GameScreen();
                    break;

                case ScreensTypeEnum.LEVEL_COMPLETE:
                    return new LevelCompleteScreen();
                    break;

                case ScreensTypeEnum.LEVEL_FAILED:
                    return new LevelFailedScreen();
                    break;

                case ScreensTypeEnum.ERROR:
                    return new ErrorScreen();
                    break;

            }

            return null;
        }

    }
}