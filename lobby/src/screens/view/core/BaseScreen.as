
package screens.view.core {
import flash.display.DisplayObject;
import flash.display.Sprite;

    /**
     * Screen class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class BaseScreen extends Sprite {

        protected var _view:DisplayObject;
        private var _data:Object;

        public function BaseScreen(view:DisplayObject) {
            _view = view;
            addChild(_view);
            init();
        }

        protected function init():void {

        }

        public function set data(value:Object):void
        {
            _data = value;
            initViewListeners();
        }

        protected function initViewListeners():void {

        }

        protected function addContextListener(eventString:String, listener:Function, eventClass:Class = null):void {
            _data.addContextListener(eventString, listener, eventClass);
        }


        protected function removeContextListener(eventString:String, listener:Function, eventClass:Class = null) {
            _data.removeContextListener(eventString, listener, eventClass);
        }

        public function destroy():void
        {

        }
    }
}