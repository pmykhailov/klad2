package screens.view.start {
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.events.MouseEvent;
import flash.events.TimerEvent;
import flash.utils.Timer;

import screens.events.ScreensContextEvent;

import screens.view.core.BaseScreen;
import screens.view.core.vo.ScreenContextActionVO;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

use namespace SoundAS;

/**
 * StartScreen class.
 * User: Paul Makarenko
 * Date: 08.10.13
 */
public class StartScreen extends BaseScreen {

    public function StartScreen() {
        super(new View_StartScreen());
    }

    override protected function init():void {
        super.init();
        (_view as MovieClip).gotoAndPlay(1);
        _view.addEventListener(MouseEvent.CLICK, onButtonClickHandler);
    }

    override public function destroy():void {
        super.destroy();

        _view.removeEventListener(MouseEvent.CLICK, onButtonClickHandler);
    }


    private function onButtonClickHandler(event:MouseEvent):void
    {
        var actionName:String;

        switch (event.target.name)
        {
            case "_buttonStart":
                actionName = ScreensContextEvent.ACTION_START;
                break;

            case "_buttonSettings":
                actionName = ScreensContextEvent.ACTION_SETTINGS;
                break;

            case "_buttonMore":
                actionName = ScreensContextEvent.ACTION_MORE;
                break;
            default :
                return;
        }

        var animation:MovieClip = _view[event.target.name + "Animation"] as MovieClip;
        animation.gotoAndPlay("_press");
        animation.mouseChildren = animation.mouseEnabled = false;

        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var timer : Timer = new Timer(500);
        timer.addEventListener(TimerEvent.TIMER,
                    function(event:TimerEvent)
                {
                    timer.removeEventListener(TimerEvent.TIMER, arguments.callee);

                    var vo:ScreenContextActionVO = new ScreenContextActionVO(actionName);
                    dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));

                    animation.gotoAndPlay("_release");
                    animation.mouseChildren = animation.mouseEnabled = true;
                }
        );
        timer.start();
    }


}
}