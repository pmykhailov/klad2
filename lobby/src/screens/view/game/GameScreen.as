package screens.view.game {


import events.GlobalEventDispatcherEvent;

import flash.display.DisplayObject;

import flash.display.Loader;
import flash.display.Sprite;
import flash.events.MouseEvent;

import screens.events.ScreensContextEvent;
import screens.view.common.LivesInfoView;

import screens.view.core.BaseScreen;
import screens.view.core.vo.ScreenContextActionVO;
import screens.view.game.InfoPanel;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

use namespace SoundAS;

/**
 * LevelSelectionScreen class.
 * User: Paul Makarenko
 * Date: 08.10.13
 */
public class GameScreen extends BaseScreen {

    private var _loading:Sprite;

    private var _loader:Loader;
    private var _infoPanel:InfoPanel;

    public function GameScreen() {
        super(new View_GameScreen());
    }

    override protected function init():void {
        super.init();

        buttonClose.addEventListener(MouseEvent.CLICK, onCloseButtonClickHandler);

        _infoPanel = new InfoPanel(_view["_infoPanel"] as Sprite);

        _loading = _view["_loading"] as Sprite;
        _loading.mouseChildren = _loading.mouseEnabled = false;
        _loading.visible = false;
    }

    private function onCloseButtonClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var vo:ScreenContextActionVO = new ScreenContextActionVO(ScreensContextEvent.ACTION_LEVEL_CLOSE);
        dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SCREEN_CONTEXT_ACTION, vo));
    }

    public function showLoading():void
    {
        _infoPanel.view.visible = false;
        buttonClose.visible = false;
        _loading.visible = true;
    }

    public function hideLoading():void
    {
        _infoPanel.view.visible = true;
        buttonClose.visible = true;
        _loading.visible = false;
    }

    override public function destroy():void {
        super.destroy();
        buttonClose.removeEventListener(MouseEvent.CLICK, onCloseButtonClickHandler);
    }

    public function get livesInfoView():LivesInfoView {
        return _infoPanel.livesInfoView;
    }

    public function get infoPanel():InfoPanel {
        return _infoPanel;
    }

    public function get loader():Loader {
        return _loader;
    }

    public function set loader(value:Loader):void {
        _loader = value;
    }

    public function get buttonClose():DisplayObject
    {
        return _view["_buttonClose"] as DisplayObject;
    }

    public function get loading():Sprite {
        return _loading;
    }
}
}