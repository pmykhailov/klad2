/**
 * Created by Pasha on 01.08.2015.
 */
package screens.view.game {
import localization.LobbyTexts;

import flash.display.Sprite;
import flash.text.TextField;

import screens.view.common.LivesInfoView;

public class InfoPanel {

    public static const PERCENT_OF_TOTAL_SCREEN_HEIGHT:Number = 10;

    private var _view:Sprite;
    private var _livesInfoView:LivesInfoView;

    public function InfoPanel(view:Sprite) {
        _view = view;
        _livesInfoView = new LivesInfoView(_view["_livesInfo"]);
    }

    public function get view():Sprite {
        return _view;
    }

    public function setScore(score:int, totalScore:int):void
    {
        (_view["_scoresTf"] as TextField).text = LobbyTexts.fillTextWithArguments(LobbyTexts.SCORES, score, totalScore);
    }

    public function set level(value:int):void
    {
        (_view["_levelTf"] as TextField).text = LobbyTexts.fillTextWithArguments(LobbyTexts.LEVEL, value);
    }

    public function get livesInfoView():LivesInfoView {
        return _livesInfoView;
    }
}
}
