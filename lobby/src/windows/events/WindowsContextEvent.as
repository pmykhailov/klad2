package windows.events {
import screens.events.*;
    import flash.events.Event;

    /**
     * ScreensContextEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class WindowsContextEvent extends Event {

        public static const INIT_WINDOWS_LAYER: String = "windowsContextEventInitScreensLayer";

        // Windows general
        public static const SHOW_WINDOW: String = "windowsContextEventShowWindow";
        public static const HIDE_WINDOW: String = "windowsContextEventHideWindow";
        public static const HIDE_ALL_WINDOWS: String = "windowsContextEventHideAllWindows";

        // Events from different windows
        public static const WINDOW_CONTEXT_ACTION: String = "windowsContextEventWindowContextAction";
        public static const WINDOW_CLOSE: String = "windowsContextEventWindowClose";

        // Common
        public static const ACTION_VOLUME_CHANGED: String = "windowsContextEventActionVolumeChanged";

        // No lives to play window
        public static const ACTION_PLAY: String = "windowsContextEventActionPlay";

        // Settings window
        public static const ACTION_SETTINGS_CLOSE: String = "windowsContextEventActionSettingsClose";

        // Game paused window
        public static const ACTION_LEVEL_SELECTION: String = "windowsContextEventActionLevelSelection";
        public static const ACTION_REPLAY: String = "windowsContextEventActionReply";
        public static const ACTION_CONTINUE: String = "windowsContextEventActionContinue";

        private var _data: Object;


        public function WindowsContextEvent(type: String, data: Object = null, bubbles: Boolean = true, cancelable: Boolean = false) {
            super(type, bubbles, cancelable);
            _data = data;
        }


        public function get data(): Object {
            return _data;
        }


        override public function clone(): Event {
            return new WindowsContextEvent(type, _data, bubbles, cancelable);
        }
    }
}