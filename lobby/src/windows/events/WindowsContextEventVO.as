package windows.events {
import screens.events.*;
    import flash.events.Event;

    /**
     * ScreensContextEvent class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class WindowsContextEventVO {

        private var _data: Object;
        private var _type:String;


        public function WindowsContextEventVO(type: String, data: Object = null) {
            _type = type;
            _data = data;
        }

        public function get data(): Object {
            return _data;
        }

        public function get type():String {
            return _type;
        }
    }
}