/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 10.09.15
 * Time: 9:55
 * To change this template use File | Settings | File Templates.
 */
package windows.view.gamePaused {
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.MouseEvent;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import windows.events.WindowsContextEvent;
import windows.events.WindowsContextEventVO;
import windows.view.common.volumeManager.VolumeManager;
import windows.view.core.vo.ShowWindowVO;

import windows.view.core.vo.WindowContextActionVO;

import windows.view.core.window.BaseWindow;
import windows.view.settings.tumbler.TumblerController;
import windows.view.settings.tumbler.TumblerControllerEvent;
import windows.view.settings.vo.SettingsScreenVO;

use namespace SoundAS;

public class GamePausedWindow extends BaseWindow {


    private var _volumeManager:VolumeManager;

    public function GamePausedWindow(view:DisplayObject, type:String) {
        super(view, type);
    }

    override protected function initView():void {
        super.initView();

        _volumeManager = new VolumeManager(_view as Sprite);
        _volumeManager.addListeners();

        _buttonLevelSelection.addEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
        //_buttonReplay.addEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
        _buttonContinue.addEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);

    }

    override public function destroy():void {
        super.destroy();

        _volumeManager.removeListeners();

        _buttonLevelSelection.removeEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
        //_buttonReplay.removeEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
        _buttonContinue.removeEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
    }


    override protected function _initFromData():void {
        super._initFromData();

        _volumeManager.data = _data;
    }

    private function onButtonConfirmClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var action:String;
        switch (event.target) {
            case _buttonLevelSelection:
                action = WindowsContextEvent.ACTION_LEVEL_SELECTION;
                break;
            case _buttonReplay:
                action = WindowsContextEvent.ACTION_REPLAY;
                break;
            case _buttonContinue:
                action = WindowsContextEvent.ACTION_CONTINUE;
                break;
        }

        var vo:WindowsContextEventVO = new WindowsContextEventVO(action);
        dispatchEvent(new WindowsContextEvent(WindowsContextEvent.WINDOW_CONTEXT_ACTION, vo));

        dispatchEvent(new WindowsContextEvent(WindowsContextEvent.WINDOW_CLOSE));
    }

    private function get _buttonLevelSelection():DisplayObject {
        return _view["_buttonLevelSelection"] as DisplayObject;
    }

    private function get _buttonReplay():DisplayObject {
        return _view["_buttonReplay"] as DisplayObject;
    }

    private function get _buttonContinue():DisplayObject {
        return _view["_buttonContinue"] as DisplayObject;
    }


}
}
