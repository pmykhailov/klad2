/**
 * Created by Pasha on 31.07.2015.
 */
package windows.view.core.vo {
public class HideWindowVO {

    private var _type:String;

    public function HideWindowVO(type:String) {
        _type = type;
    }

    public function get type():String {
        return _type;
    }
}
}
