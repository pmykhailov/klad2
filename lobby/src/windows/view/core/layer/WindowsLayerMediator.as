package windows.view.core.layer {
import windows.events.WindowsContextEventVO;
import windows.view.core.*;

import screens.view.core.layer.*;

import flash.events.Event;

import robotlegs.bender.bundles.mvcs.Mediator;
    import screens.events.ScreensContextEvent;
import screens.view.core.vo.ScreenContextActionVO;
import screens.view.core.vo.ShowScreenVO;
import screens.view.game.GameScreen;

import windows.events.WindowsContextEvent;
import windows.view.core.WindowsManager;
import windows.view.core.vo.HideWindowVO;
import windows.view.core.vo.ShowWindowVO;
import windows.view.core.vo.WindowContextActionVO;
import windows.view.core.window.BaseWindow;

/**
     * ScreenLayerMediator class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class WindowsLayerMediator extends Mediator {

        [Inject]
        public var _windowsLayer:WindowsLayerView;

         [Inject]
        public var _windowsManager:WindowsManager;



        public function WindowsLayerMediator() {
            super();
        }

        override public function initialize(): void {
            super.initialize();

            _windowsManager.init(_windowsLayer);

            addContextListener(WindowsContextEvent.SHOW_WINDOW, onShowWindowHandler);
            addContextListener(WindowsContextEvent.HIDE_WINDOW, onHideWindowHandler);
            addContextListener(WindowsContextEvent.HIDE_ALL_WINDOWS, onHideAllWindowsHandler);

            addViewListener(WindowsContextEvent.WINDOW_CONTEXT_ACTION, onWindowContextActionHandler);
            addViewListener(WindowsContextEvent.WINDOW_CLOSE, onWindowCloseHandler);
        }

        private function onWindowCloseHandler(event: WindowsContextEvent): void {
            _windowsManager.hideWindow((event.target as BaseWindow).type);
        }

        private function onWindowContextActionHandler(event: WindowsContextEvent): void {
            var vo:WindowsContextEventVO = event.data as WindowsContextEventVO;

            dispatch(new WindowsContextEvent(vo.type, vo.data, false));
        }

        private function onShowWindowHandler(event: WindowsContextEvent): void {
            var vo:ShowWindowVO = event.data as ShowWindowVO;

            if (vo.data == null) {
                vo.data = {};
            }

            vo.data.addContextListener = addContextListener;
            vo.data.removeContextListener = removeContextListener;

            _windowsManager.showWindow(vo);
        }

        private function onHideWindowHandler(event: WindowsContextEvent): void {
            _windowsManager.hideWindow((event.data as HideWindowVO).type);
        }

        private function onHideAllWindowsHandler(event: WindowsContextEvent): void {
            _windowsManager.hideAllWindows();
        }
    }
}