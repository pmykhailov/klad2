package windows.view.core.layer {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Sprite;

import lobby.LobbyApplicationDimensions;

import screens.events.ScreensContextEvent;

import screens.view.core.factory.ScreensFactory;

    import screens.view.core.BaseScreen;
import screens.view.core.vo.ShowScreenVO;

/**
     * ScreensManager class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class WindowsLayerView extends Sprite {

        private var _darkness:Sprite;

        public function WindowsLayerView() {
            super();
            init();
        }

        private function init():void {

            var bitmap:Bitmap = new Bitmap();
            var bitmapData:BitmapData = new BitmapData(LobbyApplicationDimensions.WIDTH, LobbyApplicationDimensions.HEIGHT, true, 0x00000000);

            _darkness = new Sprite();
            _darkness.graphics.beginFill(0x000000, 0.5);
            _darkness.graphics.drawRect(0,0,LobbyApplicationDimensions.WIDTH, LobbyApplicationDimensions.HEIGHT);
            _darkness.graphics.endFill();

            bitmapData.draw(_darkness);
            bitmap.bitmapData = bitmapData;

            _darkness.graphics.clear();

            _darkness.addChild(bitmap);
        }

        public function get darkness():Sprite {
            return _darkness;
        }
}
}