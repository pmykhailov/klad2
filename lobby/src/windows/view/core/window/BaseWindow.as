
package windows.view.core.window {
import flash.display.DisplayObject;
import flash.display.Sprite;

import misc.IDestroyable;

import robotlegs.bender.bundles.mvcs.Mediator;


public class BaseWindow extends Sprite implements IDestroyable{

        protected var _view:DisplayObject;
        protected var _type:String;
        protected var _data:Object;
        private var _enable:Boolean;
        private var _mediator:Mediator;

        public function BaseWindow(view:DisplayObject, type:String) {
            _view = view;
            _type = type;

            initView();
        }

        protected function addContextListener(eventString:String, listener:Function, eventClass:Class = null):void {
            _data.addContextListener(eventString, listener, eventClass);
        }


        protected function removeContextListener(eventString:String, listener:Function, eventClass:Class = null) {
            _data.removeContextListener(eventString, listener, eventClass);
        }

        protected function initView():void {
            addChild(_view);
        }

        public function get type():String
        {
            return _type;
        }

        public function set data(value:Object):void
        {
            _data = value;

            _initViewListeners();
            _initFromData();
        }

        protected function _initViewListeners():void {

        }

        protected function _initFromData():void {

        }

        public function get enable():Boolean {
            return _enable;
        }

        /**
         * Used when window is animating.
         * It is not possible to interact with window when it is animating
         * @param value
         */
        public function set enable(value:Boolean):void {
            _enable = value;
            mouseChildren = mouseEnabled = _enable;
        }

        public function destroy():void
        {
            _mediator = null;
        }
    }
}