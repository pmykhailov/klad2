/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 10.09.15
 * Time: 9:20
 * To change this template use File | Settings | File Templates.
 */
package windows.view.core {
import lobby.LobbyApplicationDimensions;

import robotlegs.bender.bundles.mvcs.Mediator;

import windows.view.core.factory.*;
import windows.view.core.*;

import com.greensock.TimelineLite;

import windows.view.core.vo.ShowWindowVO;

import windows.view.core.window.BaseWindow;
import windows.view.core.layer.WindowsLayerView;

public class WindowsManager {

    private var _queue:Array;
    private var _layer:WindowsLayerView;
    private var _factory:WindowFactory;

    public function WindowsManager() {

    }

    public function init(layer:WindowsLayerView):void {
        _queue = [];
        _layer = layer;
        _factory = new WindowFactory();
    }

    public function showWindow(vo:ShowWindowVO):void {

        var window:BaseWindow = _factory.getWindow(vo.type);

        window.data = vo.data;

        // Add

        _layer.scaleX = _layer.stage.stageWidth / LobbyApplicationDimensions.WIDTH;
        _layer.scaleY = _layer.stage.stageHeight / LobbyApplicationDimensions.HEIGHT;

        if (_queue.length == 0)
        {
            _layer.addChild(_layer.darkness);
        }

        window.x = (LobbyApplicationDimensions.WIDTH - window.width) / 2;
        window.y = (LobbyApplicationDimensions.HEIGHT - window.height) / 2;
        _layer.addChild(window);

        _queue.push(window);
    }

    public function hideWindow(type:String):void {
        var window:BaseWindow;

        // Remove from queue
        var n:uint = _queue.length;
        for (var i:int = 0; i < n; i++) {
            window = _queue[i];
            if (window.type == type) {
                _queue.splice(i,1);
                break;
            }
        }

        _layer.removeChild(window);

        if (_queue.length == 0)
        {
            _layer.removeChild(_layer.darkness);
        }
    }

    public function hideAllWindows():void {
        while (_queue.length > 0) {
            hideWindow(_queue[0].type);
        }
    }

}
}
