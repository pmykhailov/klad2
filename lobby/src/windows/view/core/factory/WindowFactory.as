/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 10.09.15
 * Time: 9:29
 * To change this template use File | Settings | File Templates.
 */
package windows.view.core.factory {
import windows.view.alert.Alert;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.window.BaseWindow;
import windows.view.gamePaused.GamePausedWindow;
import windows.view.noLivesToPlay.NoLivesToPlayWindow;
import windows.view.settings.SettingsWindow;

public class WindowFactory {
    public function WindowFactory() {
    }

    public function getWindow(type:String):BaseWindow {

        switch (type)
        {
            case WindowsTypeEnum.ALERT:
                    return new Alert(new View_Alert(), type);
            break;

            case WindowsTypeEnum.SETTINGS:
                    return new SettingsWindow(new View_Settings(), type);
            break;

            case WindowsTypeEnum.NO_LIVES_TO_PLAY:
                    return new NoLivesToPlayWindow(new View_NoLivesToPlay(), type);
            break;

            case WindowsTypeEnum.GAME_PAUSED:
                    return new GamePausedWindow(new View_GamePaused(), type);
            break;
        }

        return null;
    }
}
}
