package windows.view.core.enum  {

    /**
     * ScreensTypeEnum class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class WindowsTypeEnum {

        public static const ALERT:String = "AlertWindow";
        public static const SETTINGS:String = "SettingsWindow";
        public static const NO_LIVES_TO_PLAY:String = "NoLivesToPlayWindow";
        public static const GAME_PAUSED:String = "GamePaused";

        public function WindowsTypeEnum() {
        }

    }
}