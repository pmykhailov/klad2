/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 10.09.15
 * Time: 9:55
 * To change this template use File | Settings | File Templates.
 */
package windows.view.noLivesToPlay {
import flash.display.DisplayObject;
import flash.display.SimpleButton;
import flash.events.MouseEvent;
import flash.text.TextField;

import lobby.model.levels.LevelModelEvent;

import localization.LobbyTexts;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import utils.Format;

import windows.events.WindowsContextEvent;

import windows.events.WindowsContextEventVO;
import windows.view.core.vo.WindowContextActionVO;

import windows.view.core.window.BaseWindow;

use namespace SoundAS;

public class NoLivesToPlayWindow extends BaseWindow {
    public function NoLivesToPlayWindow(view:DisplayObject, type:String) {
        super(view, type);
    }

    override protected function _initFromData():void {
        super._initFromData();
    }

    // -------------------------------------------------
    // ---------------- GETTERS / SETTERS --------------
    // -------------------------------------------------

    private function get _buttonPlay():SimpleButton {
        return _view["_buttonPlay"] as SimpleButton;
    }

    private function get _buttonBack():SimpleButton {
        return _view["_buttonBack"] as SimpleButton;
    }

    private function get _tfTime():TextField {
        return _view["_tfTime"] as TextField;
    }


    private function get _tfCaption():TextField {
        return _view["_tfCaption"] as TextField;
    }

    // ---------------------------------------
    // ---------------- INIT -----------------
    // ---------------------------------------

    override protected function initView():void {
        super.initView();

        _buttonPlay.alpha = 0.5;
        _buttonPlay.mouseEnabled = false;

        _tfCaption.text = LobbyTexts.NEXT_LIFE_IN;
    }

    override protected function _initViewListeners():void {
        super._initViewListeners();

        _buttonPlay.addEventListener(MouseEvent.CLICK, onButtonPlayClickHandler);
        _buttonBack.addEventListener(MouseEvent.CLICK, onButtonBackClickHandler);

        addContextListener(LevelModelEvent.LIFE_RESTORE_PROGRESS, onLifeRestoreProgress);
        addContextListener(LevelModelEvent.LIFE_RESTORED, onLifeRestored);
    }

    // ---------------------------------------
    // ---------------- DESTROY --------------
    // ---------------------------------------

    override public function destroy():void {
        super.destroy();

        _buttonPlay.removeEventListener(MouseEvent.CLICK, onButtonPlayClickHandler);
        _buttonBack.removeEventListener(MouseEvent.CLICK, onButtonBackClickHandler);

        removeContextListener(LevelModelEvent.LIFE_RESTORE_PROGRESS, onLifeRestoreProgress);
        removeContextListener(LevelModelEvent.LIFE_RESTORED, onLifeRestored);
    }

    // ----------------------------------------------
    // ---------------- EVENT HANDLERS --------------
    // ----------------------------------------------

    private function onButtonBackClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);
        dispatchEvent(new WindowsContextEvent(WindowsContextEvent.WINDOW_CLOSE));
    }

    private function onButtonPlayClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var vo:WindowsContextEventVO = new WindowsContextEventVO(WindowsContextEvent.ACTION_PLAY, _data.levelIndex);
        dispatchEvent(new WindowsContextEvent(WindowsContextEvent.WINDOW_CONTEXT_ACTION, vo));
    }

    private function onLifeRestored(event:LevelModelEvent):void {
        _buttonPlay.mouseEnabled = true;
        _buttonPlay.alpha = 1;

        _tfCaption.text = LobbyTexts.LIFE_RESTORED;

        _tfTime.visible = false;
    }

    private function onLifeRestoreProgress(event:LevelModelEvent):void {
        var time:int = event.data.secondsLeft as int;

        _tfTime.text = Format.time(time);
    }

}
}
