/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.15
 * Time: 14:04
 * To change this template use File | Settings | File Templates.
 */
package windows.view.settings.tumbler {
import flash.display.MovieClip;
import flash.events.EventDispatcher;
import flash.events.MouseEvent;

import misc.IDestroyable;

public class TumblerController extends EventDispatcher implements IDestroyable{

    public static const ON:String = "_on";
    public static const OFF:String = "_off";

    private var _view:MovieClip;
    private var _currentPosition:String;

    public function TumblerController(view:MovieClip, position:String) {
        _view = view;
        _currentPosition = position;
        init();
    }

    private function init():void {
        _view.addEventListener(MouseEvent.CLICK, onMouseClickHandler);

        currentPosition = OFF;
    }

    private function onMouseClickHandler(event:MouseEvent):void {
        currentPosition = (_currentPosition == ON) ? OFF : ON;

        dispatchEvent(new TumblerControllerEvent(TumblerControllerEvent.CHANGED, _currentPosition));
    }

    public function destroy():void {
        _view.removeEventListener(MouseEvent.CLICK, onMouseClickHandler);
    }

    public function get currentPosition():String {
        return _currentPosition;
    }

    public function set currentPosition(value:String):void {
        _currentPosition = value;
        _view.gotoAndStop(_currentPosition);
    }
}
}
