/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.15
 * Time: 14:04
 * To change this template use File | Settings | File Templates.
 */
package windows.view.settings.tumbler {
import flash.events.Event;

public class TumblerControllerEvent extends Event {

    public static const CHANGED:String = "TumblerControllerEventChanged";

    private var _data:Object;

    public function TumblerControllerEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new TumblerControllerEvent(type, data, bubbles, cancelable);
    }
}
}
