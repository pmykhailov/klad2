/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 08.10.15
 * Time: 12:06
 * To change this template use File | Settings | File Templates.
 */
package windows.view.settings.vo {
public class SettingsScreenVO {

    private var _fxVolume:Number;
    private var _musicVolume:Number;

    public function SettingsScreenVO(fxVolume:Number, musicVolume:Number) {
        _fxVolume = fxVolume;
        _musicVolume = musicVolume;
    }

    public function get fxVolume():Number {
        return _fxVolume;
    }

    public function get musicVolume():Number {
        return _musicVolume;
    }
}
}
