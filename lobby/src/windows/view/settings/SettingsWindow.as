/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 10.09.15
 * Time: 9:55
 * To change this template use File | Settings | File Templates.
 */
package windows.view.settings {
import flash.display.DisplayObject;
import flash.display.MovieClip;
import flash.display.Sprite;
import flash.events.MouseEvent;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import windows.events.WindowsContextEvent;
import windows.events.WindowsContextEventVO;
import windows.view.common.volumeManager.VolumeManager;
import windows.view.core.vo.ShowWindowVO;

import windows.view.core.vo.WindowContextActionVO;

import windows.view.core.window.BaseWindow;
import windows.view.settings.tumbler.TumblerController;
import windows.view.settings.tumbler.TumblerControllerEvent;
import windows.view.settings.vo.SettingsScreenVO;

use namespace SoundAS;

public class SettingsWindow extends BaseWindow {

    private var _volumeManager:VolumeManager;

    public function SettingsWindow(view:DisplayObject, type:String) {
        super(view, type);
    }

    override protected function initView():void {
        super.initView();
        _volumeManager = new VolumeManager(_view as Sprite);
        _volumeManager.addListeners();

        buttonConfirm.addEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
    }

    override public function destroy():void {
        super.destroy();

        _volumeManager.removeListeners();

        buttonConfirm.removeEventListener(MouseEvent.CLICK, onButtonConfirmClickHandler);
    }

    override protected function _initFromData():void {
        super._initFromData();

        _volumeManager.data = _data;
    }

    private function onButtonConfirmClickHandler(event:MouseEvent):void {
        SoundAS.group(SoundGroupEnum.GROUP_SFX).playFx(LobbySoundEnum.BUTTON_CLICK);

        var vo:WindowsContextEventVO = new WindowsContextEventVO(WindowsContextEvent.ACTION_SETTINGS_CLOSE);
        dispatchEvent(new WindowsContextEvent(WindowsContextEvent.WINDOW_CONTEXT_ACTION, vo));
    }

    private function get buttonConfirm():DisplayObject {
        return _view["_buttonConfirm"] as DisplayObject;
    }
}
}
