/**
 * Created by Pasha on 14.06.2016.
 */
package windows.view.common.volumeManager {
import flash.display.MovieClip;
import flash.display.Sprite;

import windows.events.WindowsContextEvent;

import windows.events.WindowsContextEventVO;

import windows.view.settings.tumbler.TumblerController;
import windows.view.settings.tumbler.TumblerControllerEvent;
import windows.view.settings.vo.SettingsScreenVO;

public class VolumeManager {

    private var _fxTumblerController:TumblerController;
    private var _musicTumblerController:TumblerController;

    private var _view:Sprite;


    public function VolumeManager(view:Sprite) {
        _view = view;
    }

    public function addListeners():void {
        _fxTumblerController = new TumblerController(_view["_fxTumbler"] as MovieClip, TumblerController.ON);
        _musicTumblerController = new TumblerController(_view["_musicTumbler"] as MovieClip, TumblerController.ON);

        _fxTumblerController.addEventListener(TumblerControllerEvent.CHANGED, onFXTumblerChangedHandler);
        _musicTumblerController.addEventListener(TumblerControllerEvent.CHANGED, onMusicTumblerChangedHandler);
    }

    public function removeListeners():void {
        _fxTumblerController.removeEventListener(TumblerControllerEvent.CHANGED, onFXTumblerChangedHandler);
        _musicTumblerController.removeEventListener(TumblerControllerEvent.CHANGED, onMusicTumblerChangedHandler);
    }

    public function set data(value:Object):void {
        var vo:SettingsScreenVO = value.settingsScreenVO as SettingsScreenVO;

        _fxTumblerController.currentPosition = vo.fxVolume == 0 ? TumblerController.OFF : TumblerController.ON;
        _musicTumblerController.currentPosition = vo.musicVolume == 0 ? TumblerController.OFF : TumblerController.ON;
    }

    private function onMusicTumblerChangedHandler(event:TumblerControllerEvent):void {
        dispatchVolumeChanged();
    }

    private function onFXTumblerChangedHandler(event:TumblerControllerEvent):void {
        dispatchVolumeChanged();
    }

    private function dispatchVolumeChanged():void
    {
        var fxVolume:Number = _fxTumblerController.currentPosition == TumblerController.ON ? 1 : 0;
        var musicVolume:Number = _musicTumblerController.currentPosition == TumblerController.ON ? 1 : 0;;
        var volumeVO:SettingsScreenVO = new SettingsScreenVO(fxVolume, musicVolume);
        var eventVO:WindowsContextEventVO = new WindowsContextEventVO(WindowsContextEvent.ACTION_VOLUME_CHANGED, volumeVO);

        _view.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.WINDOW_CONTEXT_ACTION, eventVO));
    }

}
}
