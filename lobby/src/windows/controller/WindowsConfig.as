package windows.controller {


    import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.extensions.contextView.ContextView;
    import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
    import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
    import robotlegs.bender.framework.api.IConfig;
    import robotlegs.bender.framework.api.IContext;
    import robotlegs.bender.framework.api.IInjector;

import screens.controller.actions.level_selection.LevelCloseCommand;
import screens.controller.actions.start.ShowLevelSelectionScreenCommand;


import screens.controller.init.InitScreensCommand;
    import screens.controller.actions.level_selection.LevelOpenCommand;

    import screens.events.ScreensContextEvent;
    import screens.view.core.layer.ScreenLayerMediator;

    import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;

import windows.controller.actions.BackToLevelSelectionWindowCommand;

import windows.controller.actions.ChangeVolumeCommand;

import windows.controller.actions.CloseSettingsWindowCommand;
import windows.controller.actions.ContinueWindowCommand;
import windows.controller.actions.PlayLevelAfterRestoreWindowCommand;
import windows.controller.actions.ReplayWindowCommand;
import windows.controller.actions.SaveSettingsFromWindowCommand;

import windows.controller.init.InitWindowsCommand;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.layer.WindowsLayerMediator;
import windows.view.core.layer.WindowsLayerView;
import windows.view.core.WindowsManager;
import windows.view.core.vo.ShowWindowVO;

public class WindowsConfig implements IConfig {

        [Inject]
        public var context: IContext;
        [Inject]
        public var eventCommandMap: IEventCommandMap;
        [Inject]
        public var mediatorMap: IMediatorMap;
        [Inject]
        public var dispatcher: IEventDispatcher;
        [Inject]
        public var injector: IInjector;
        [Inject]
        public var contextView: ContextView;


        public function configure(): void {

            mapCommands();
            mapMediators();
            mapInjections();

            context.afterInitializing(init);
        }


        private function mapCommands(): void {
            eventCommandMap.map(WindowsContextEvent.INIT_WINDOWS_LAYER).toCommand(InitWindowsCommand);

            // No lives to play actions
            eventCommandMap.map(WindowsContextEvent.ACTION_PLAY).toCommand(PlayLevelAfterRestoreWindowCommand);

            // Settings window actions
            eventCommandMap.map(WindowsContextEvent.ACTION_SETTINGS_CLOSE).toCommand(CloseSettingsWindowCommand);
            eventCommandMap.map(WindowsContextEvent.ACTION_VOLUME_CHANGED).toCommand(ChangeVolumeCommand);

            //Game paused window
            eventCommandMap.map(WindowsContextEvent.ACTION_LEVEL_SELECTION).toCommand(BackToLevelSelectionWindowCommand);
            eventCommandMap.map(WindowsContextEvent.ACTION_CONTINUE).toCommand(ContinueWindowCommand);
            eventCommandMap.map(WindowsContextEvent.ACTION_REPLAY).toCommand(ReplayWindowCommand);
        }


        private function mapMediators(): void {
            mediatorMap.map(WindowsLayerView).toMediator(WindowsLayerMediator);
        }


        private function mapInjections(): void {
            injector.map(WindowsManager).asSingleton();
        }


        private function init(): void {
            dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.INIT_WINDOWS_LAYER));
        }
    }
}
