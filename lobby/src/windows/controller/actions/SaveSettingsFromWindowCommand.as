/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.15
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
package windows.controller.actions {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

public class SaveSettingsFromWindowCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var eventCommandMap: IEventCommandMap;

    public function SaveSettingsFromWindowCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        // Save settings not after window close but after each settings is changed

        //eventCommandMap.map(LobbyApplicationEvent.BD_SETTINGS_DATA_UPDATED).toCommand(CloseSettingsWindowCommand).once();
        //dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_SETTINGS_DATA));
    }
}
}
