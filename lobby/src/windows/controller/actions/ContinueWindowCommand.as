/**
 * Created by Pasha on 14.06.2016.
 */
package windows.controller.actions {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import robotlegs.bender.bundles.mvcs.Command;

public class ContinueWindowCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    public function ContinueWindowCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_RESUME));
    }
}
}
