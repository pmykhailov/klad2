/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.15
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
package windows.controller.actions {
import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.HideWindowVO;
import windows.view.core.vo.ShowWindowVO;

public class CloseSettingsWindowCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    public function CloseSettingsWindowCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.HIDE_WINDOW, new HideWindowVO(WindowsTypeEnum.SETTINGS)));
    }
}
}
