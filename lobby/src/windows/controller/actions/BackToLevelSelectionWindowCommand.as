/**
 * Created by Pasha on 14.06.2016.
 */
package windows.controller.actions {
import ad.AdMobManager;

import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.vo.ShowScreenVO;

public class BackToLevelSelectionWindowCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    public function BackToLevelSelectionWindowCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_STOP));
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.LEVEL_SELECTION, {lobbyModel:lobbyModel})));
    }
}
}
