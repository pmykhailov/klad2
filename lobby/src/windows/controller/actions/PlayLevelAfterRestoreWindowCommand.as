/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.15
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
package windows.controller.actions {
import flash.events.Event;
import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.HideWindowVO;

public class PlayLevelAfterRestoreWindowCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var event:WindowsContextEvent;

    public function PlayLevelAfterRestoreWindowCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.HIDE_WINDOW, new HideWindowVO(WindowsTypeEnum.NO_LIVES_TO_PLAY)));

        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.ACTION_START_LEVEL, event.data/*levelIndex*/));
    }
}
}
