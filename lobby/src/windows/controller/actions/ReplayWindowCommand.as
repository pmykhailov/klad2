/**
 * Created by Pasha on 14.06.2016.
 */
package windows.controller.actions {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

public class ReplayWindowCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;
    [Inject]
    public var lobbyModel:LobbyModel;

    public function ReplayWindowCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        // Replay is not needed but maybe will be useful later

        /*
        var levelIndex:int = lobbyModel.activeLevel;

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_STOP));
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_START, levelIndex));
        */
    }
}
}
