/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 07.10.15
 * Time: 13:56
 * To change this template use File | Settings | File Templates.
 */
package windows.controller.actions {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import windows.events.WindowsContextEvent;
import windows.view.core.enum.WindowsTypeEnum;
import windows.view.core.vo.HideWindowVO;
import windows.view.core.vo.ShowWindowVO;
import windows.view.settings.vo.SettingsScreenVO;

use namespace SoundAS;

public class ChangeVolumeCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var event:WindowsContextEvent;

    [Inject]
    public var lobbyModel: LobbyModel;

    public function ChangeVolumeCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        var vo:SettingsScreenVO = event.data as SettingsScreenVO;

        lobbyModel.musicVolume = vo.musicVolume;
        lobbyModel.fxVolume = vo.fxVolume;

        SoundAS.group(SoundGroupEnum.GROUP_MUSIC).masterVolume = lobbyModel.musicVolume;
        SoundAS.group(SoundGroupEnum.GROUP_SFX).masterVolume = lobbyModel.fxVolume;

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_SETTINGS_DATA));
    }
}
}
