package windows.controller.init {
import flash.display.DisplayObject;
import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
    import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IInjector;

import screens.events.ScreensContextEvent;

import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;

import windows.view.core.layer.WindowsLayerView;

/**
     * InitScreensCommand class.
     * User: Paul Makarenko
     * Date: 08.10.13
     */
    public class InitWindowsCommand extends Command {

        [Inject]
        public var contextView:ContextView;

        [Inject]
        public var dispatcher: IEventDispatcher;

        [Inject]
        public var injector: IInjector;

        [Inject]
        public var lobbyModel:LobbyModel;

        public function InitWindowsCommand() {
            super();
        }


        override public function execute(): void {
            super.execute();

            var windowsLayerView: WindowsLayerView = new WindowsLayerView();

            windowsLayerView.name = "WindowsLayerView";

            contextView.view.addChild(windowsLayerView);
        }
    }
}