/**
 * Created by Pasha on 27.08.2015.
 */
package localization {
public class LobbyTexts {

    public static const SCORES : String = "SCORES {0}/{1}";
    public static const LIVES : String = "LIVES {0}/{1}";
    public static const NEXT_LIFE_IN : String = "NEXT LIFE IN";
    public static const LIFE_RESTORED : String = "LIFE RESTORED";
    public static const NEXT_LIFE_IN_WITH_PARAM : String = NEXT_LIFE_IN + " {0}";
    public static const LEVEL : String = "LEVEL {0}";

    // TODO LEVEL_COMPLETE & LEVEL_FAILFD
    public static const LEVEL_COMPLETE : String = "LEVEL {0}";
    public static const LEVEL_FAILFD : String = "LEVEL {0}";

    public function LobbyTexts() {
    }

    public static function fillTextWithArguments(s:String, ...args):String
    {
        for (var i:int = 0; i < args.length; i++) {
            var argIndex:int = s.indexOf("{" + i + "}");
            s = s.substr(0,argIndex) + args[i] + s.substr(argIndex + 3);
        }

        return s;
    }
}
}
