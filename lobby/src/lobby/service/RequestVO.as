package lobby.service
{
	import flash.net.URLRequest;

	public class RequestVO
	{
		private var _onComplete:Function;
		private var _onError:Function;
		private var _urlRequest:URLRequest;
		
		public function RequestVO(onComplete:Function, onError:Function)
		{
			_onComplete = onComplete;
			_onError = onError;
		}
		
		public function get urlRequest():URLRequest
		{
			return _urlRequest;
		}
		
		public function set urlRequest(value:URLRequest):void
		{
			_urlRequest = value;
		}
		
		public function get onError():Function
		{
			return _onError;
		}
		
		public function get onComplete():Function
		{
			return _onComplete;
		}
	}
}