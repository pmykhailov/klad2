package lobby.service {
import lobby.model.*;


import flash.net.SharedObject;

public class SharedObjectService {

    private var so:SharedObject;

    public function SharedObjectService() {
        _init();
    }

    private function _init():void {
        so = SharedObject.getLocal("com.supergames.ruffer");
    }

    public function initLevelsData(lobbyModel:LobbyModel):void
    {
        /*
        var levelsLockArray:Array;

        if (so.data.hasOwnProperty("levelsLockArray")){
            levelsLockArray =  so.data.levelsLockArray;
        }
        */

        if (so.data.hasOwnProperty("fxVolume")){
            lobbyModel.fxVolume = so.data.fxVolume;
        }

        if (so.data.hasOwnProperty("musicVolume")){
            lobbyModel.musicVolume = so.data.musicVolume;
        }

        /*
        if (levelsLockArray){
            for (var i:int = 0; i < levelsLockArray.length; i++) {
                var levelModel:LevelModel = levelsModel.getLevelModelByNumber(i + 1);
                levelModel.isLocked = false; //levelsLockArray[i];
            }
        }else{
            levelsModel.getLevelModelByNumber(1).isLocked = false;
            saveLevelsData(levelsModel);
        }
        */
    }


    public function saveData(lobbyModel:LobbyModel):void{
        /*
        var levelsLockArray:Array = [];

        for (var i:int = 0; i < levelsModel.levelsCount; i++) {
            var levelModel:LevelModel = levelsModel.getLevelModelByNumber(i + 1);
            levelsLockArray[i] = levelModel.isLocked;
        }
        so.data.levelsLockArray = levelsLockArray;
        */

        so.data.soundVolume = lobbyModel.fxVolume;
        so.data.musicVolume = lobbyModel.musicVolume;

        so.flush();
    }



}
}
