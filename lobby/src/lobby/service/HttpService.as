package lobby.service
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;

	public class HttpService
	{
		private var BASE_URL:String = "http://mikhaylove.com.ua/games/roofer/";
		private var USER:String = "user.php";
		private var USER_ACTION_CREATE:String = "create";
		private var USER_ACTION_UPDATE:String = "update";

		private var _loader:URLLoader;
		
		private var _queue:Array;
		private var _currentRequest:RequestVO;
		private var _isBusy:Boolean;
		
		public function HttpService()
		{
			_loader = new URLLoader();
			
			_queue = [];
		}

		// ---------------------------------------------------------------------
		// --------------------- PUBLIC METHODS --------------------------------
		// ---------------------------------------------------------------------

		public function createUser(requestVO:RequestVO):void
		{
			var urlVariables:URLVariables = createRequestData();
			
			urlVariables.action_type = USER_ACTION_CREATE;

            prepareRequest(requestVO, urlVariables, USER);

            addRequestToQueue(requestVO);
		}

        public function sendUserData(requestVO:RequestVO, userID:int, data:String):void {

            var urlVariables:URLVariables = createRequestData();

            urlVariables.user_id = userID;
            urlVariables.user_data = data;
            urlVariables.action_type = USER_ACTION_UPDATE;

            prepareRequest(requestVO, urlVariables, USER);

            addRequestToQueue(requestVO);
        }

		/*
		public function updateUser(requestVO:RequestVO, userVO:UserVO):void
		{
			var urlValiables:URLVariables = new URLVariables();
			
			urlValiables.action_type = "update";
			urlValiables.user_name = userVO.name;
			urlValiables.user_id = userVO.id;
			
			requestVO.urlRequest = new URLRequest(BASE_URL + USER);
			requestVO.urlRequest.data = urlValiables;
			requestVO.urlRequest.method = URLRequestMethod.POST;
			
			_queue.push(requestVO);
			loadNextFromQueue();
		}

         public function submitScore(requestVO:RequestVO, scoreVO:SubmitLevelCompleteVO):void
         {
         var urlValiables:URLVariables = new URLVariables();

         urlValiables.action_type = "submit";
         urlValiables.user_id = scoreVO.user_id;
         urlValiables.score = scoreVO.score;
         urlValiables.date = scoreVO.date;
         urlValiables.date_full = scoreVO.date_full;

         requestVO.urlRequest = new URLRequest(BASE_URL + SCORE);
         requestVO.urlRequest.data = urlValiables;
         requestVO.urlRequest.method = URLRequestMethod.POST;

         _queue.push(requestVO);
         loadNextFromQueue();
         }


         public function getUser(requestVO:RequestVO, userVO:UserVO):void
		{
			var urlValiables:URLVariables = new URLVariables();
			
			urlValiables.action_type = "get";
			urlValiables.user_id = userVO.id;

			requestVO.urlRequest = new URLRequest(BASE_URL + USER);
			requestVO.urlRequest.data = urlValiables;
			requestVO.urlRequest.method = URLRequestMethod.POST;
			
			_queue.push(requestVO);
			loadNextFromQueue();
		}

		public function getUserScore(requestVO:RequestVO, userVO:UserVO):void
		{
			var urlValiables:URLVariables = new URLVariables();
			
			urlValiables.action_type = "get_user_score";
			urlValiables.user_id = userVO.id;
			
			requestVO.urlRequest = new URLRequest(BASE_URL + SCORE);
			requestVO.urlRequest.data = urlValiables;
			requestVO.urlRequest.method = URLRequestMethod.POST;
			
			_queue.push(requestVO);
			loadNextFromQueue();
		}
		
		public function getLeaders(requestVO:RequestVO, userVO:UserVO):void
		{
			var urlValiables:URLVariables = new URLVariables();
			
			urlValiables.action_type = "get_leaders";
			urlValiables.user_id = userVO.id;

			requestVO.urlRequest = new URLRequest(BASE_URL + SCORE);
			requestVO.urlRequest.data = urlValiables;
			requestVO.urlRequest.method = URLRequestMethod.POST;
			
			_queue.push(requestVO);
			loadNextFromQueue();
		}
        */

        // ---------------------------------------------------------------------
        // --------------------- PRIVATE METHODS -------------------------------
        // ---------------------------------------------------------------------


        private function createRequestData():URLVariables {
            var urlVariables:URLVariables = new URLVariables();

            urlVariables.no_cache = noCache;

            return urlVariables
        }

        private function prepareRequest(requestVO:RequestVO, urlVariables:URLVariables, urlEnding:String):void {
            requestVO.urlRequest = new URLRequest(BASE_URL + urlEnding);
            requestVO.urlRequest.data = urlVariables;
            requestVO.urlRequest.method = URLRequestMethod.POST;
        }

        private function addRequestToQueue(requestVO:RequestVO):void {
            _queue.push(requestVO);
            loadNextFromQueue();
        }

        private function loadNextFromQueue():void
		{
			if (!_isBusy && _queue.length > 0)
			{
				_isBusy = true;
				
				_currentRequest = _queue.pop() as RequestVO;
				
				addLoaderListeners();
				_loader.load(_currentRequest.urlRequest)
			}
		}

        private function onLoaderCompleteHandler(event:Event):void
		{
			removeLoaderListeners();
			
			_currentRequest.onComplete && _currentRequest.onComplete(_loader.data);
			
			_isBusy = false;
			_currentRequest = null;
			loadNextFromQueue();
		}

        private function onLoaderErrorHandler(event:IOErrorEvent):void
		{
			removeLoaderListeners();
			
			_currentRequest.onError && _currentRequest.onError();
			
			_isBusy = false;
			_currentRequest = null;
			loadNextFromQueue();
		}
		
		private function addLoaderListeners():void
		{
			_loader.addEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			_loader.addEventListener(IOErrorEvent.IO_ERROR, onLoaderErrorHandler);
		}
		
		private function removeLoaderListeners():void
		{
			_loader.removeEventListener(Event.COMPLETE, onLoaderCompleteHandler);
			_loader.removeEventListener(IOErrorEvent.IO_ERROR, onLoaderErrorHandler);			
		}

        private function get noCache():String {
            return (new Date()).time.toString();
        }
	}
}