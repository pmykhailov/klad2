/**
 * Created by Pasha on 30.07.2015.
 */
package lobby {
import flash.events.Event;

public class LobbyApplicationEvent extends Event {

    public static const INIT:String = "lobbyApplicationEventInit";
    public static const INIT_COMPLETE:String = "lobbyApplicationEventInitComplete";

    public static const LEVEL_START:String = "levelStart";
    public static const LEVEL_PAUSE:String = "levelPause";
    public static const LEVEL_RESUME:String = "levelResume";
    public static const LEVEL_STOP:String = "levelStop";
    public static const LEVEL_PROGRESS:String = "levelProgress";

    // Server communication events
    public static const CONNECTION_ERROR:String = "connectionError";
    public static const SEND_DATA_TO_SERVER:String = "sendDataToServer";
    public static const RETRY_USER_CREATION:String = "retryUserCreation";

    // Data base events
    public static const CONNECT_TO_DB:String = "connectToBD";
    public static const UPDATE_LEVELS_DATA:String = "updateLevelsData";
    public static const UPDATE_USER_DATA:String = "updateUserData";
    public static const UPDATE_SETTINGS_DATA:String = "updateSettingsData";
    public static const UPDATE_INFO_DATA:String = "updateInfoData";

    public static const BD_CONNECTED:String = "dbConnected";
    public static const BD_TABLES_CREATED:String = "dbLevelsTablesCreated";
    public static const BD_TABLE_USER_INITED:String = "dbLevelsTableUserInited";
    public static const BD_TABLE_LEVELS_INITED:String = "dbLevelsTableLevelsrInited";
    public static const BD_TABLE_SETTINGS_INITED:String = "dbLevelsTableSettingsInited";
    public static const BD_TABLE_INFO_INITED:String = "dbLevelsTableInfoInited";
    public static const BD_LEVELS_TABLE_DATA_GOT:String = "dbLevelsTableDataGot";
    public static const BD_USER_TABLE_DATA_GOT:String = "dbUserTableDataGot";
    public static const BD_SETTINGS_TABLE_DATA_GOT:String = "dbSettingsTableDataGot";

    public static const BD_LEVELS_DATA_UPDATED:String = "dbLevelsDataUpdated";
    public static const BD_USER_DATA_UPDATED:String = "dbUserDataUpdated";
    public static const BD_SETTINGS_DATA_UPDATED:String = "dbSettingsDataUpdated";
    public static const BD_INFO_DATA_UPDATED:String = "dbInfoDataUpdated";

    public static const BD_ERROR:String = "dbError";

    private var _data:Object;

    public function LobbyApplicationEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }
}
}
