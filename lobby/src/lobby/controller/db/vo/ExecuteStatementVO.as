/**
 * Created by Pasha on 06.06.2016.
 */
package lobby.controller.db.vo {
import flash.data.SQLStatement;

public class ExecuteStatementVO {

    private var _statement:SQLStatement;
    private var _onComplete:Function;

    public function ExecuteStatementVO(statement:SQLStatement, onComplete:Function = null) {
        _statement = statement;
        _onComplete = onComplete;
    }

    public function get statement():SQLStatement {
        return _statement;
    }

    public function get onComplete():Function {
        return _onComplete;
    }

}
}
