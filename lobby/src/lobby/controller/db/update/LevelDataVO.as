/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.09.15
 * Time: 12:57
 * To change this template use File | Settings | File Templates.
 */
package lobby.controller.db.update {
public class LevelDataVO {

    private var _levelNumber:int;
    private var _isLocked:int;
    private var _scoreCollected:int;
    private var _scoreTotal:int;

    public function LevelDataVO(levelID:int, isLocked:int, scoreCollected:int = 0, scoreTotal:int = 0) {
        _levelNumber = levelID;
        _isLocked = isLocked;
        _scoreCollected = scoreCollected;
        _scoreTotal = scoreTotal;
    }

    public function get levelNumber():int {
        return _levelNumber;
    }

    public function get isLocked():int {
        return _isLocked;
    }

    public function get scoreCollected():int {
        return _scoreCollected;
    }

    public function get scoreTotal():int {
        return _scoreTotal;
    }
}
}
