/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.update {

import flash.data.SQLConnection;

import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBUpdateLevelsDataCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    [Inject]
    public var event:LobbyApplicationEvent;

    public function DBUpdateLevelsDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        context.detain(this);
        update();
    }


    private function update():void {
        var selectStmt:SQLStatement = new SQLStatement();
        var levelDataVO:LevelDataVO = (event.data as LevelDataVO);

        selectStmt.text = "UPDATE " + DBManager.DB_LEVELS_TABLE_NAME + " " +
                          "SET "    + DBManager.DB_LEVELS_TABLE_FIELD_IS_LOCKED + " = " + levelDataVO.isLocked + "," +
                                      DBManager.DB_LEVELS_TABLE_FIELD_SCORE_COLLECTED + " = " + "\'" + (levelDataVO.scoreCollected + "/" + levelDataVO.scoreTotal) + "\' " +
                          "WHERE "  + DBManager.DB_LEVELS_TABLE_FIELD_ID + " = " + levelDataVO.levelNumber;

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt,selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_LEVELS_DATA_UPDATED));
        context.release(this);
    }

}
}
