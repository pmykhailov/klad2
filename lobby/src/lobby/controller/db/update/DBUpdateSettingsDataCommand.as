/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.update {

import flash.data.SQLConnection;

import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;
import lobby.model.LobbyModel;
import lobby.model.user.UserModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBUpdateSettingsDataCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var event:LobbyApplicationEvent;

    public function DBUpdateSettingsDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        context.detain(this);

        update();
    }

    private function update():void {


        var selectStmt:SQLStatement = new SQLStatement();

        selectStmt.text = "UPDATE " + DBManager.DB_SETTINGS_TABLE_NAME + " " +
                          "SET " + DBManager.DB_SETTINGS_TABLE_FIELD_MUSIC + " = " + lobbyModel.musicVolume + "," +
                                   DBManager.DB_SETTINGS_TABLE_FIELD_SOUND + " = " + lobbyModel.fxVolume + " " +
                          "WHERE " + DBManager.DB_SETTINGS_TABLE_FIELD_ID + " = " + DBManager.DB_SETTINGS_TABLE_FIELD_ID_VALUE

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt,selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_SETTINGS_DATA_UPDATED));
        context.release(this);
    }

}
}
