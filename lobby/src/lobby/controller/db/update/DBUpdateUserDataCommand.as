/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.update {

import flash.data.SQLConnection;

import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;
import lobby.model.LobbyModel;
import lobby.model.user.UserModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBUpdateUserDataCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var event:LobbyApplicationEvent;

    public function DBUpdateUserDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        context.detain(this);
        update();
    }

    private function update():void {

        var selectStmt:SQLStatement = new SQLStatement();

        // TODO : Only id is updated here. Make name updatable also. Configure building request

        if (event.data && (event.data as UserDataVO).reason == UserDataVO.REASON_UPDATE_USER_ID) {
            selectStmt.text = "UPDATE " + DBManager.DB_USER_TABLE_NAME + " " +
                              "SET " + DBManager.DB_USER_TABLE_FIELD_USER_ID + " = " + lobbyModel.userModel.id + " " +
                              "WHERE " + DBManager.DB_USER_TABLE_FIELD_USER_ID + " = " + UserModel.DEFAULT_ID
        } else {
            selectStmt.text = "UPDATE " + DBManager.DB_USER_TABLE_NAME + " " +
                              "SET " +    DBManager.DB_USER_TABLE_FIELD_LIVES + " = " + lobbyModel.userModel.livesLeft + "," +
                                          DBManager.DB_USER_TABLE_FIELD_LIFE_RESTORE_START + " = " + lobbyModel.userModel.lifeRestoreStartTime + " " +
                               "WHERE " + DBManager.DB_USER_TABLE_FIELD_USER_ID + " = " + lobbyModel.userModel.id
        }

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt,selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_USER_DATA_UPDATED));
        context.release(this);
    }
}
}
