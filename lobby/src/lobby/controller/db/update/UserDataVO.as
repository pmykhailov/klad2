/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.09.15
 * Time: 12:57
 * To change this template use File | Settings | File Templates.
 */
package lobby.controller.db.update {
public class UserDataVO {

    public static const REASON_UPDATE_USER_ID:int = 0;

    private var _reason:int;


    public function UserDataVO(reason:int) {
        _reason = reason;
    }

    public function get reason():int {
        return _reason;
    }
}
}
