/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.09.15
 * Time: 11:38
 * To change this template use File | Settings | File Templates.
 */
package lobby.controller.db.error {

import flash.events.IEventDispatcher;

import lobby.controller.db.DBManager;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.vo.ShowScreenVO;

import windows.events.WindowsContextEvent;

public class DBErrorHandleCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;
    public function DBErrorHandleCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        trace("DATA BASE :: Error");

        DBManager.instance.disconnect();

        dispatcher.dispatchEvent(new WindowsContextEvent(WindowsContextEvent.HIDE_ALL_WINDOWS));
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.ERROR)));
    }
}
}
