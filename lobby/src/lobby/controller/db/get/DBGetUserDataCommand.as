/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.get {

import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;
import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBGetUserDataCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    [Inject]
    public var lobbyModel: LobbyModel;

    public function DBGetUserDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        context.detain(this);
        select();
    }

    private function select():void
    {
        var selectStmt:SQLStatement = new SQLStatement();
        selectStmt.text = "SELECT * FROM " + DBManager.DB_USER_TABLE_NAME;

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt,selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void
    {
        var selectStmt:SQLStatement = event.target as SQLStatement;
        var result:SQLResult = selectStmt.getResult();

        var row:Object = result.data[0];

        lobbyModel.userModel.id = row[DBManager.DB_USER_TABLE_FIELD_USER_ID];
        lobbyModel.userModel.name = row[DBManager.DB_USER_TABLE_FIELD_NAME];
        lobbyModel.userModel.lifeRestoreStartTime = row[DBManager.DB_USER_TABLE_FIELD_LIFE_RESTORE_START];
        lobbyModel.userModel.livesLeft = row[DBManager.DB_USER_TABLE_FIELD_LIVES];

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_USER_TABLE_DATA_GOT));

        context.release(this);
    }

}
}
