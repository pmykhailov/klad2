/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.get {

import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;
import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;
import lobby.model.logic.LogicLobbyFacade;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBGetLevelsDataCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    [Inject]
    public var lobbyModel: LobbyModel;


    public function DBGetLevelsDataCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        context.detain(this);
        select();
    }

    private function select():void
    {
        var selectStmt:SQLStatement = new SQLStatement();
        selectStmt.text = "SELECT * FROM " + DBManager.DB_LEVELS_TABLE_NAME;

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt, selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void
    {
        var selectStmt:SQLStatement = event.target as SQLStatement;
        var result:SQLResult = selectStmt.getResult();

        var numResults:int = result.data.length;
        for (var i:int = 0; i < numResults; i++)
        {
            var row:Object = result.data[i];
            var levelModel:LevelModel = lobbyModel.levelModels[i];

            levelModel.isLocked = row[DBManager.DB_LEVELS_TABLE_FIELD_IS_LOCKED];
            levelModel.maxScoreCollected = int(row[DBManager.DB_LEVELS_TABLE_FIELD_SCORE_COLLECTED].split("/")[0]);
            levelModel.starsGot = LogicLobbyFacade.instance.logicLevelStars.getStarsCount(levelModel, true);
        }

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_LEVELS_TABLE_DATA_GOT));

        context.release(this);
    }

}
}
