/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db {
import flash.data.SQLConnection;
import flash.data.SQLSchemaResult;
import flash.data.SQLStatement;
import flash.data.SQLTableSchema;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.vo.ExecuteStatementVO;

public class DBManager {

    public static const DB_FILE_NAME:String = "roofer.db";

    public static const DB_USER_TABLE_NAME:String = "user";
    public static const DB_USER_TABLE_FIELD_USER_ID:String = "user_id";
    public static const DB_USER_TABLE_FIELD_NAME:String = "name";
    public static const DB_USER_TABLE_FIELD_LIVES:String = "lives";
    public static const DB_USER_TABLE_FIELD_LIFE_RESTORE_START:String = "life_restore_start";

    public static const DB_LEVELS_TABLE_NAME:String = "levels";
    public static const DB_LEVELS_TABLE_FIELD_ID:String = "level_id";
    public static const DB_LEVELS_TABLE_FIELD_IS_LOCKED:String = "is_locked";
    public static const DB_LEVELS_TABLE_FIELD_SCORE_COLLECTED:String = "score_collected";

    public static const DB_SETTINGS_TABLE_NAME:String = "settings";
    public static const DB_SETTINGS_TABLE_FIELD_ID_VALUE:int = 1;
    public static const DB_SETTINGS_TABLE_FIELD_ID:String = "id";
    public static const DB_SETTINGS_TABLE_FIELD_MUSIC:String = "music";
    public static const DB_SETTINGS_TABLE_FIELD_SOUND:String = "sound";

    public static const DB_INFO_TABLE_NAME:String = "info";
    public static const DB_INFO_TABLE_FIELD_ID:String = "id";
    public static const DB_INFO_TABLE_FIELD_VERSION:String = "version";
    public static const DB_INFO_TABLE_SYNC_TIME:String = "sync_time";

    private static var _instance:DBManager;
    private var _connection:SQLConnection;
    private var _dispatcher:IEventDispatcher;
    [ArrayElementType("lobby.controller.db.vo.ExecuteStatementVO")]
    private var _queue:Array;
    private var _isBusy:Boolean;
    private var _currentExecuteStatementVO:ExecuteStatementVO;

    public function DBManager() {
        _queue = [];
        _isBusy = false;
    }

    // ---------------------------------------------------------
    // ---------------- Getters / setters ----------------------
    // ---------------------------------------------------------

    public static function get instance():DBManager {
        if (!_instance) _instance = new DBManager();
        return _instance;
    }

    public function set dispatcher(value:IEventDispatcher):void {
        _dispatcher = value;
    }

    public function get connection():SQLConnection {
        return _connection;
    }

    public function set connection(value:SQLConnection):void {
        _connection = value;
    }

    // ---------------------------------------------------------
    // ---------------- Public methods -------------------------
    // ---------------------------------------------------------

    public function connect():void {

        // https://github.com/sqlitebrowser/sqlitebrowser/releases/tag/v3.7.0
        // http://help.adobe.com/en_US/as3/dev/WS5b3ccc516d4fbf351e63e3d118676a5497-7fb4.html

        _connection = new SQLConnection();

        _connection.addEventListener(SQLEvent.OPEN, connectionOpenComplete);
        _connection.addEventListener(SQLErrorEvent.ERROR, connectionOpenFail);

        // C:\Users\Pasha\AppData\Roaming\LobbyApplication\Local Store\roofer.db
        var folder:File = File.applicationStorageDirectory;
        var dbFile:File = folder.resolvePath(DBManager.DB_FILE_NAME);

        _connection.openAsync(dbFile);
    }

    public function disconnect():void {
        _connection && _connection.close();
    }

    public function get isConnected():Boolean {
        return connection && connection.connected;
    }

    public function addRequest(vo:ExecuteStatementVO):void {
        if (isConnected) {
            _queue.push(vo);
            executeNextStatement();
        } else {
            dispatchError();
        }
    }

    // ---------------------------------------------------------
    // ---------------- Private methods ------------------------
    // ---------------------------------------------------------

    private function executeNextStatement():void {
        if (!_isBusy && _queue.length > 0) {
            _isBusy = true;

            _currentExecuteStatementVO = _queue.shift() as ExecuteStatementVO;

            _currentExecuteStatementVO.statement.sqlConnection = _connection;

            _currentExecuteStatementVO.statement.addEventListener(SQLEvent.RESULT, executeStatementResultHandler);
            _currentExecuteStatementVO.statement.addEventListener(SQLErrorEvent.ERROR, executeStatementErrorHandler);

            trace("DATA BASE :: Executing " + _currentExecuteStatementVO.statement.text);

            _currentExecuteStatementVO.statement.execute();
        }
    }

    private function removeCurrentExecuteStatementListeners():void {
        _currentExecuteStatementVO.statement.removeEventListener(SQLEvent.RESULT, executeStatementResultHandler);
        _currentExecuteStatementVO.statement.removeEventListener(SQLErrorEvent.ERROR, executeStatementErrorHandler);
    }

    private function dispatchError():void {
        _dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_ERROR));
    }
    // ---------------------------------------------------------
    // ---------------- Event handlers -------------------------
    // ---------------------------------------------------------

    private function connectionOpenComplete(event:SQLEvent):void
    {
        _dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_CONNECTED));
    }

    private function connectionOpenFail(event:SQLErrorEvent):void
    {
        dispatchError();
    }

    private function executeStatementResultHandler(event:SQLEvent):void {
        _isBusy = false;

        removeCurrentExecuteStatementListeners();

        _currentExecuteStatementVO.onComplete && _currentExecuteStatementVO.onComplete(event);

        executeNextStatement();
    }

    private function executeStatementErrorHandler(event:SQLErrorEvent):void {
        dispatchError();
    }

}
}
