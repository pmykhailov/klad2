/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.09.15
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */
package lobby.controller.db.init.init_tables {

import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;
import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBInitLevelsTableCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher

    [Inject]
    public var context:IContext;

    [Inject]
    public var lobbyModel: LobbyModel;

    public function DBInitLevelsTableCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        context.detain(this);

        var selectStmt:SQLStatement = new SQLStatement();
        selectStmt.text = "SELECT * FROM " + DBManager.DB_LEVELS_TABLE_NAME;

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt, selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void
    {
        var selectStmt:SQLStatement = event.target as SQLStatement;
        var result:SQLResult = selectStmt.getResult();

        if (result.data == null) {
            fillTable();
        } else {
            context.release(this);
            dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_TABLE_LEVELS_INITED));
        }

    }

    private function fillTable():void {
        var insertStmt:SQLStatement;

        var levelsCount:int = lobbyModel.levelModels.length;

        for (var i:int = 0; i < levelsCount; i++) {
            insertStmt = new SQLStatement();

            var sql:String = "INSERT INTO " + DBManager.DB_LEVELS_TABLE_NAME +
                             "(" + DBManager.DB_LEVELS_TABLE_FIELD_ID + "," +
                                   DBManager.DB_LEVELS_TABLE_FIELD_IS_LOCKED + "," +
                                   DBManager.DB_LEVELS_TABLE_FIELD_SCORE_COLLECTED +
                              ") VALUES";

            sql += " (" + (i + 1) + ",";

            // First level is unlocked
            if (i == 0)
            {
                sql += "0";
            }else
            {
                // TODO: Remove this comment when releasing
                sql += "0"; //"1";
            }

            sql += "," + "\'0/" + lobbyModel.levelModels[i].scoreTotal + "\'" + ")";

            insertStmt.text = sql;

            // INSERT INTO levels (is_locked) VALUES (0), (0)

            // But in AIR sql lite you should write
            // INSERT INTO levels (is_locked)
            // SELECT 1 UNION
            // SELECT 2 UNION
            // SELECT 3

            // And this is not working if
            // values to select are equal
            // Like this
            // SELECT 1 UNION
            // SELECT 1 UNION

            if ( i == levelsCount - 1 ) {
                DBManager.instance.addRequest(new ExecuteStatementVO(insertStmt, insertResult));
            } else {
                DBManager.instance.addRequest(new ExecuteStatementVO(insertStmt));
            }
        }
    }

    private function insertResult(event:SQLEvent):void {
        context.release(this);
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_TABLE_LEVELS_INITED));
    }



}
}
