/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 02.09.15
 * Time: 11:39
 * To change this template use File | Settings | File Templates.
 */
package lobby.controller.db.init.init_tables {

import flash.data.SQLResult;
import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.vo.ExecuteStatementVO;
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBInitInfoTableCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher

    [Inject]
    public var context:IContext;

    [Inject]
    public var lobbyModel: LobbyModel;


    public function DBInitInfoTableCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        context.detain(this);

        var selectStmt:SQLStatement = new SQLStatement();
        selectStmt.text = "SELECT * FROM " + DBManager.DB_INFO_TABLE_NAME;

        DBManager.instance.addRequest(new ExecuteStatementVO(selectStmt, selectResultHandler));
    }

    private function selectResultHandler(event:SQLEvent):void
    {
        var selectStmt:SQLStatement = event.target as SQLStatement;
        var result:SQLResult = selectStmt.getResult();

        if (result.data == null) {
            fillTable();
        } else {
            context.release(this);
            dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_TABLE_INFO_INITED));
        }

    }

    private function fillTable():void {
        var insertStmt:SQLStatement = new SQLStatement();

        var sql:String = "INSERT INTO " + DBManager.DB_INFO_TABLE_NAME;
        sql += "(" + DBManager.DB_INFO_TABLE_FIELD_VERSION + ") VALUES ";
        sql += "(" + "\"" + lobbyModel.DBVersion + "\"" + ")";

        insertStmt.text = sql;

        DBManager.instance.addRequest(new ExecuteStatementVO(insertStmt, insertResult));
    }

    private function insertResult(event:SQLEvent):void {
        context.release(this);
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_TABLE_INFO_INITED));
    }


}
}
