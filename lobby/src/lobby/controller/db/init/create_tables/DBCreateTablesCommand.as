/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.init.create_tables {

import flash.data.SQLStatement;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.init.create_tables.vo.CreateTableVO;
import lobby.controller.db.init.create_tables.vo.FieldVO;
import lobby.controller.db.vo.ExecuteStatementVO;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBCreateTablesCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    private var _tablesVOs:Array;

    public function DBCreateTablesCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        context.detain(this);

        _tablesVOs = [
                new CreateTableVO(DBManager.DB_USER_TABLE_NAME, [
                    new FieldVO(DBManager.DB_USER_TABLE_FIELD_USER_ID,"INTEGER PRIMARY KEY"),
                    new FieldVO(DBManager.DB_USER_TABLE_FIELD_NAME,"TEXT"),
                    new FieldVO(DBManager.DB_USER_TABLE_FIELD_LIVES,"NUMERIC"),
                    new FieldVO(DBManager.DB_USER_TABLE_FIELD_LIFE_RESTORE_START,"NUMERIC")
                ])
            ,
                new CreateTableVO(DBManager.DB_LEVELS_TABLE_NAME, [
                    new FieldVO(DBManager.DB_LEVELS_TABLE_FIELD_ID,"INTEGER"),
                    new FieldVO(DBManager.DB_LEVELS_TABLE_FIELD_IS_LOCKED,"NUMERIC"),
                    new FieldVO(DBManager.DB_LEVELS_TABLE_FIELD_SCORE_COLLECTED,"TEXT")
                ])
            ,
                new CreateTableVO(DBManager.DB_SETTINGS_TABLE_NAME, [
                    new FieldVO(DBManager.DB_SETTINGS_TABLE_FIELD_ID,"NUMERIC"),
                    new FieldVO(DBManager.DB_SETTINGS_TABLE_FIELD_MUSIC,"NUMERIC"),
                    new FieldVO(DBManager.DB_SETTINGS_TABLE_FIELD_SOUND,"NUMERIC")
                ])
            ,
            new CreateTableVO(DBManager.DB_INFO_TABLE_NAME, [
                new FieldVO(DBManager.DB_INFO_TABLE_FIELD_VERSION,"TEXT")
            ])
        ];

        createTables();
    }

    private function createTables():void {
        var fieldVO:FieldVO;
        var tableVO:CreateTableVO;
        var createStmt:SQLStatement

        for (var ii:int = 0; ii < _tablesVOs.length; ii++) {
            tableVO = _tablesVOs[ii];

            createStmt = new SQLStatement();

            var sql:String = "CREATE TABLE IF NOT EXISTS " + tableVO.name + " (";

            for (var i:int = 0; i < tableVO.fieldsVOs.length; i++) {
                fieldVO = tableVO.fieldsVOs[i];
                sql += " " + fieldVO.name + " " + fieldVO.type + ",";
            }

            sql = sql.substr(0,sql.length - 1) + ")";

            createStmt.text = sql;

            if (ii == _tablesVOs.length - 1) {
                DBManager.instance.addRequest(new ExecuteStatementVO(createStmt, createResult));
            } else {
                DBManager.instance.addRequest(new ExecuteStatementVO(createStmt));
            }

        }
    }

    function createResult(event:SQLEvent):void {
        context.release(this);
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_TABLES_CREATED));
    }

    /*
    private function createLevelsTable():void
    {
        // ... create and open the SQLConnection instance named _connection ...

        var createStmt:SQLStatement = new SQLStatement();
        createStmt.sqlConnection = DBManager.instance.connection;

        var sql:String =
                "CREATE TABLE IF NOT EXISTS " + DBManager.DB_LEVELS_TABLE_NAME + " (" +
                " " + DBManager.DB_LEVELS_TABLE_FIELD_ID + " INTEGER " + //" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                " " + DBManager.DB_LEVELS_TABLE_FIELD_IS_LOCKED + " NUMERIC" +
                ")";
        createStmt.text = sql;

        createStmt.addEventListener(SQLEvent.RESULT, createResult);
        createStmt.addEventListener(SQLErrorEvent.ERROR, createError);

        createStmt.execute();
    }

    function createResult(event:SQLEvent):void
    {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_LEVELS_TABLE_CREATED));

        //trace("Table created");

        context.release(this);
    }

    function createError(event:SQLErrorEvent):void
    {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.BD_ERROR));

        //trace("Error message:", event.error.message);
        //trace("Details:", event.error.details);

        context.release(this);
    }
    */

}
}
