/**
 * Created by Pasha on 22.05.2016.
 */
package lobby.controller.db.init.create_tables.vo {
public class FieldVO {

    private var _name:String;
    private var _type:String;

    public function FieldVO(name:String, type:String) {
        _name = name;
        _type = type;
    }

    public function get name():String {
        return _name;
    }

    public function get type():String {
        return _type;
    }
}
}
