/**
 * Created by Pasha on 22.05.2016.
 */
package lobby.controller.db.init.create_tables.vo {
public class CreateTableVO {
    private var _name:String;
    private var _fieldsVOs:Array;

    public function CreateTableVO(name:String, fieldsVOs:Array) {
        _name = name;
        _fieldsVOs = fieldsVOs;
    }

    public function get name():String {
        return _name;
    }

    public function get fieldsVOs():Array {
        return _fieldsVOs;
    }
}
}
