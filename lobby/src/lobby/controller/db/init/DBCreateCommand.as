/**
 * Created by Pasha on 02.09.2015.
 */
package lobby.controller.db.init {

import flash.data.SQLConnection;
import flash.events.IEventDispatcher;
import flash.events.SQLErrorEvent;
import flash.events.SQLEvent;
import flash.filesystem.File;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.framework.api.IContext;

public class DBCreateCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var context:IContext;

    public function DBCreateCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        DBManager.instance.connect();
    }

}
}
