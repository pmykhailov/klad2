/**
 * Created by Pasha on 04.06.2016.
 */
package lobby.controller.init {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.controller.level.misc.LevelLifeRestoreProgressCommand;
import lobby.controller.level.misc.LevelLifeRestoreStartTimeUpdatedCommand;

import lobby.controller.level.misc.LevelLivesUpdatedCommand;
import lobby.controller.level.progress.LevelScoreUpdatedCommand;
import lobby.model.LobbyModel;
import lobby.model.levels.LevelModelEvent;
import lobby.model.logic.LogicLivesRestore;
import lobby.model.logic.LogicLobbyFacade;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

import screens.view.core.layer.ScreensLayerView;
import screens.view.loading.LoadingScreen;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

use namespace SoundAS;

public class LobbyInitCompleteCommand extends Command {

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var eventCommandMap: IEventCommandMap;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    public function LobbyInitCompleteCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        sendUserDataToServer();
        mapRestCommands();
        startLifeRestore();
        hideLoadingScreen();
        someSoundSetup();
    }

    private function someSoundSetup():void {
        SoundAS.group(SoundGroupEnum.GROUP_MUSIC).masterVolume = lobbyModel.musicVolume;
        SoundAS.group(SoundGroupEnum.GROUP_SFX).masterVolume = lobbyModel.fxVolume;

        SoundAS.group(SoundGroupEnum.GROUP_MUSIC).playLoop(LobbySoundEnum.AMBIANCE_LOBBY);
    }

    private function startLifeRestore():void {
        LogicLobbyFacade.instance.logicLivesRestore.onApplicationStart();
    }

    private function sendUserDataToServer():void {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.SEND_DATA_TO_SERVER));
    }

    private function mapRestCommands():void {
        // Game commands
        eventCommandMap.map(LevelModelEvent.SCORE_UPDATED).toCommand(LevelScoreUpdatedCommand);
        eventCommandMap.map(LevelModelEvent.LIVES_UPDATED).toCommand(LevelLivesUpdatedCommand);
        eventCommandMap.map(LevelModelEvent.LIFE_RESTORE_STAR_TIME_UPDATED).toCommand(LevelLifeRestoreStartTimeUpdatedCommand);
        eventCommandMap.map(LevelModelEvent.LIFE_RESTORE_PROGRESS).toCommand(LevelLifeRestoreProgressCommand);
    }

    private function hideLoadingScreen():void {
        loadingScreen.state = LoadingScreen.STATE_LOADING_COMPLETE;
    }

    protected function get loadingScreen():LoadingScreen {
        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;

        return screensLayerView.activeScreen as LoadingScreen;
    }

}
}
