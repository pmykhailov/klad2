/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 06.10.15
 * Time: 10:47
 * To change this template use File | Settings | File Templates.
 */
package lobby.controller.init {
import flash.desktop.NativeApplication;
import flash.events.Event;
import flash.media.AudioPlaybackMode;
import flash.media.SoundMixer;
import flash.media.SoundTransform;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import utils.ClassUtils;

use namespace SoundAS;

public class InitLobbySoundsCommand extends Command {

    [Inject]
    public var lobbyModel: LobbyModel;

    public function InitLobbySoundsCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        button_click;

        ambiance_lobby;

        NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, activate);
        NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, deactivate);

        //SoundMixer.audioPlaybackMode = AudioPlaybackMode.AMBIENT;

        var sounds: Array = LobbySoundEnum.ALL_FX;
        var n: int = sounds.length;
        for (var i: int = 0; i < n; i++) {
            _addSoundToGroup(sounds[i], SoundGroupEnum.GROUP_SFX);
        }

        _addSoundToGroup(LobbySoundEnum.AMBIANCE_LOBBY, SoundGroupEnum.GROUP_MUSIC);
    }

    private function _addSoundToGroup(soundClassName: String, group:String): void {

        SoundAS.group(group).addSound(soundClassName, ClassUtils.getInstanceByClassName(soundClassName));
    }

    private function activate(e: Event): void {
        SoundMixer.soundTransform = new SoundTransform(1);
    }

    private function deactivate(e: Event): void {
        SoundMixer.soundTransform = new SoundTransform(0);
    }

}
}
