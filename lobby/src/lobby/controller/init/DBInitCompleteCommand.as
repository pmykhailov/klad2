/**
 * Created by Pasha on 24.05.2016.
 */
package lobby.controller.init {
import flash.events.SQLEvent;

import lobby.controller.*;

import flash.events.IEventDispatcher;
import flash.net.URLVariables;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.DBManager;
import lobby.controller.db.update.UserDataVO;

import lobby.model.LobbyModel;
import lobby.model.user.UserModel;
import lobby.service.RequestVO;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.framework.api.IContext;

import screens.view.core.layer.ScreensLayerView;

import screens.view.loading.LoadingScreen;

public class DBInitCompleteCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var context:IContext;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var eventCommandMap: IEventCommandMap;

    public function DBInitCompleteCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        if (lobbyModel.userModel.id == UserModel.DEFAULT_ID) {
            context.detain(this);
            lobbyModel.httpService.createUser(new RequestVO(onUserCreated, onErrorHandler));
        } else {
            dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.INIT_COMPLETE));
        }
    }

    private function onUserCreated(data:Object):void {
        context.release(this);
        var vars:URLVariables = new URLVariables(data as String);

        lobbyModel.userModel.id = vars.id_user;
        lobbyModel.userModel.name = vars.name;

        eventCommandMap.map(LobbyApplicationEvent.BD_USER_DATA_UPDATED).toCommand(UserIDGivenCommand).once();
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_USER_DATA, new UserDataVO(UserDataVO.REASON_UPDATE_USER_ID)));
    }

    private function onErrorHandler():void {
        context.release(this);

        loadingScreen.state = LoadingScreen.STATE_LOADING_FAILED;

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.CONNECTION_ERROR));
    }

    protected function get loadingScreen():LoadingScreen {
        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;

        return screensLayerView.activeScreen as LoadingScreen;
    }

}
}
