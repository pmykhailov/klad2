/**
 * Created by Pasha on 30.07.2015.
 */
package lobby.controller.init {
import lobby.*;


import flash.events.IEventDispatcher;
import flash.net.URLVariables;

import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;
import lobby.model.logic.LogicLevelStars;
import lobby.model.logic.LogicLobbyFacade;

import model.level.parsers.LevelsDataParser;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.api.IInjector;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.loading.LoadingScreen;

public class LobbyInitCommand extends Command {

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var injector: IInjector;

    [Inject]
    public var dispatcher: IEventDispatcher;

    [Inject]
    public var lobbyModel: LobbyModel;

    [Inject]
    public var context:IContext;

    [Embed(source="../../../data/levels.xml")]
    private var levelsXMLClass: Class;

    public function LobbyInitCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        context.detain(this);

        // --------------------------------
        // ---- 1. Show loading screen ----
        // --------------------------------
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.LOADING)));

        loadingScreen.state = LoadingScreen.STATE_LOADING;

        // -------------------------------------------------------------------------------
        // ---- 2. Fill lobby model with levels. Init lobby model with default values ----
        // -------------------------------------------------------------------------------
        var levelsData:XML = XML(levelsXMLClass.data);
        var allLevels:Array = [];

        var parser: LevelsDataParser = new LevelsDataParser();
        var levelsCount: int = levelsData.level.length();

        for (var i: int = 0; i < levelsCount; i++) {
            var levelModel: LevelModel = new LevelModel();
            levelModel.number = i + 1;
            levelModel.starsTotal = LogicLevelStars.MAX_START_FOR_LEVEL;
            allLevels.push(levelModel);
        }

        parser.parse(levelsData, allLevels);

        lobbyModel.initLevels(allLevels);

        // -------------------------------
        // --- 3. Connect to data base ---
        // -------------------------------
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.CONNECT_TO_DB));

    }


    protected function get loadingScreen():LoadingScreen {
        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;

        return screensLayerView.activeScreen as LoadingScreen;
    }
}
}
