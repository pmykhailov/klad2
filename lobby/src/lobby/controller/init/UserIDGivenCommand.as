/**
 * Created by Pasha on 24.05.2016.
 */
package lobby.controller.init {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import robotlegs.bender.bundles.mvcs.Command;

public class UserIDGivenCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    public function UserIDGivenCommand() {
        super();
    }

    override public function execute():void {
        super.execute();
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.INIT_COMPLETE));
    }
}
}
