/**
 * Created by Pasha on 05.06.2016.
 */
package lobby.controller.sync {
import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;
import lobby.model.user.UserModel;
import lobby.service.RequestVO;

import robotlegs.bender.bundles.mvcs.Command;

public class SendDataToServerCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    public function SendDataToServerCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        if (lobbyModel.userModel.id != UserModel.DEFAULT_ID) {
            lobbyModel.httpService.sendUserData(new RequestVO(onRequestCallBack, onRequestCallBack), lobbyModel.userModel.id, createJSONString());
        }
    }

    private function onRequestCallBack(...args):void {
        // Don't reach on errors. Let user play offline
    }

    private function createJSONString():String {
        var levelModel:LevelModel;
        var levelsInfo:Object = new Object();
        var levelInfo:Object;
        var properties:Array = ["isLocked", "maxScoreCollected", "scoreTotal", "starsGot"];
        var propertiesCount:int = properties.length;
        var levelsCount:int = lobbyModel.levelModels.length;

        for (var i:int = 0; i < levelsCount; i++) {

            levelModel = lobbyModel.levelModels[i];
            levelInfo = new Object();

            for (var j:int = 0; j < propertiesCount; j++) {
                levelInfo[properties[j]] = levelModel[properties[j]];
            }

            levelsInfo[levelModel.number] = levelInfo;
        }

       return JSON.stringify(levelsInfo);
    }
}
}
