/**
 * Created by Pasha on 31.07.2015.
 */
package lobby.controller.level.start {
import ad.AdMobManager;

import com.greensock.TweenLite;

import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.display.Loader;
import flash.display.StageScaleMode;
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.net.URLRequest;
import flash.system.ApplicationDomain;
import flash.system.LoaderContext;

import lobby.LobbyApplicationDimensions;

import lobby.LobbyApplicationEvent;

import lobby.model.LobbyModel;
import lobby.model.levels.LevelModelEvent;

import model.config.game.movable_items.base.MovableUnitTypeEnum;
import lobby.model.levels.LevelModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.framework.api.IContext;

import screens.events.ScreensContextEvent;

import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.game.GameScreen;
import screens.view.game.InfoPanel;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

import utils.ScreenUtils;

use namespace SoundAS;

public class LevelStartCommand extends Command {

    [Inject]
    public var event:LobbyApplicationEvent;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var context: IContext;

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var dispatcher:IEventDispatcher;

    private var _loader:Loader;

    private var _levelIndex:int;

    private var _gameScreen:GameScreen;

    public function LevelStartCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        context.detain(this);

        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var urlRequest:URLRequest = new URLRequest("game.swf");
        var applicationDomain:ApplicationDomain = new ApplicationDomain(ApplicationDomain.currentDomain);
        var loaderContext:LoaderContext = new LoaderContext(false,applicationDomain);

        AdMobManager.instance.visible = false;

        _gameScreen = screensLayerView.activeScreen as GameScreen;
        _levelIndex = event.data as int;

        lobbyModel.activeLevel = _levelIndex;

        // Stop ambiance sound
        SoundAS.group(SoundGroupEnum.GROUP_MUSIC).pause(LobbySoundEnum.AMBIANCE_LOBBY);

        // Show loading
        _gameScreen.loading.x = (LobbyApplicationDimensions.WIDTH - _gameScreen.loading.width)/2;
        _gameScreen.loading.y = (LobbyApplicationDimensions.HEIGHT - _gameScreen.loading.height)/2;
        _gameScreen.stage.addEventListener(Event.RESIZE, onStageResizeHandler);
        _gameScreen.showLoading();

        _loader = new Loader();
        _loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoaderCompleteHandler);
        _loader.load(urlRequest, loaderContext);
    }

    private function onStageResizeHandler(event:Event):void {
        // When starling changed scaleMode - scale loading
        if (_gameScreen.stage.scaleMode == StageScaleMode.NO_SCALE) {
            //_gameScreen.loading.visible = false;

            _gameScreen.loading.scaleX = ScreenUtils.instance.screenDimensions.width/LobbyApplicationDimensions.WIDTH;
            _gameScreen.loading.scaleY = ScreenUtils.instance.screenDimensions.height/LobbyApplicationDimensions.HEIGHT;

            _gameScreen.loading.x = (ScreenUtils.instance.screenDimensions.width - _gameScreen.loading.width)/2;
            _gameScreen.loading.y = (ScreenUtils.instance.screenDimensions.height - _gameScreen.loading.height)/2; // - 0.03125*ScreenUtils.instance.screenDimensions.height; // Works for LG
        }
    }

    private function onLoaderCompleteHandler(event:Event):void {
        _loader.contentLoaderInfo.removeEventListener(Event.COMPLETE, onLoaderCompleteHandler);

        // Transfer data to game
        var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.INIT_GAME, lobbyModel.activeLevelModel.gameConfig);
        _loader.content.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_TO_GAME, data));

        // >>>> Init start level values and this will feel info panel <<<<
        dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.LIVES_UPDATED));

        lobbyModel.activeLevelModel.scoreCollected = 0;

        _gameScreen.infoPanel.level = _levelIndex;

        // >>> <<<<

        _loader.addEventListener(GlobalEventDispatcherEvent.TYPE_FROM_GAME, onGlobalEventHandler);

        _gameScreen.loader = _loader;
        _gameScreen.addChild(_loader);
    }

    private function onGlobalEventHandler(event:GlobalEventDispatcherEvent):void {

        if (GlobalEventDispatcherEvent.GAME_INIT_COMPLETE)
        {
            _gameScreen.stage.removeEventListener(Event.RESIZE, onStageResizeHandler);
            _gameScreen.hideLoading();

            // release this commnand
            _loader.removeEventListener(GlobalEventDispatcherEvent.TYPE_FROM_GAME, onGlobalEventHandler);
            context.release(this);

            // Scale info panel
            var percentForInfoPanel:Number = InfoPanel.PERCENT_OF_TOTAL_SCREEN_HEIGHT;
            var originalInfoPanelHeight:Number = _gameScreen.infoPanel.view.height;
            var infoPanelNewHeight:Number =  ScreenUtils.instance.screenDimensions.height * percentForInfoPanel / 100;
            var scaleValue:Number = infoPanelNewHeight / originalInfoPanelHeight;

            _gameScreen.infoPanel.view.scaleX = scaleValue;
            _gameScreen.infoPanel.view.scaleY = scaleValue;

            _gameScreen.buttonClose.scaleX = scaleValue;
            _gameScreen.buttonClose.scaleY = scaleValue;
            _gameScreen.buttonClose.y = (infoPanelNewHeight - _gameScreen.buttonClose.height)/2;
            _gameScreen.buttonClose.x = ScreenUtils.instance.screenDimensions.width - _gameScreen.buttonClose.width - _gameScreen.buttonClose.y;

            // Shift game cuz we have info panel on top of screen
            var paddingObject:Object = {paddingTop:int(infoPanelNewHeight)};
            var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.CHANGE_GAME_SCREEN, paddingObject);
            _loader.content.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_TO_GAME, data));

            //Process level events
            dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_PROGRESS));
        }
    }

}
}
