/**
 * Created by Pasha on 31.07.2015.
 */
package lobby.controller.level.stop {
import ad.AdMobManager;

import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.display.Stage;
import flash.display.StageScaleMode;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.view.core.layer.ScreensLayerView;
import screens.view.game.GameScreen;

import sound.LobbySoundEnum;

import sound.SoundGroupEnum;

import treefortress.sound.SoundAS;

use namespace SoundAS;

public class LevelStopCommand extends Command {

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var lobbyModel:LobbyModel;

    public function LevelStopCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        // Resume ambiance sound
        SoundAS.group(SoundGroupEnum.GROUP_MUSIC).resume(LobbySoundEnum.AMBIANCE_LOBBY);

        //
        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var gameScreen:GameScreen = screensLayerView.activeScreen as GameScreen;
        var stage:Stage = contextView.view.stage;

        var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.DESTROY_GAME);
        gameScreen.loader.content.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_TO_GAME, data));

        gameScreen.loader.unloadAndStop();
        gameScreen.loader = null;

        stage.scaleMode = StageScaleMode.SHOW_ALL;

        // Show ad
        AdMobManager.instance.visible = true;
    }
}
}
