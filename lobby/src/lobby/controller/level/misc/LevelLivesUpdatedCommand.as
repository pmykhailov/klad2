/**
 * Created by Pasha on 26.08.2015.
 */
package lobby.controller.level.misc {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.view.common.LivesInfoView;

import screens.view.core.layer.ScreensLayerView;

public class LevelLivesUpdatedCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var dispatcher:IEventDispatcher;

    public function LevelLivesUpdatedCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var livesInfoView:LivesInfoView;

        if (screensLayerView.activeScreen.hasOwnProperty("livesInfoView")) {
            livesInfoView = screensLayerView.activeScreen["livesInfoView"] as LivesInfoView;
            livesInfoView.setLives(lobbyModel.userModel.livesLeft, lobbyModel.userModel.livesTotal);
            livesInfoView.nextLiveTf.visible = (lobbyModel.userModel.livesLeft < lobbyModel.userModel.livesTotal);
        }

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_USER_DATA));
    }
}
}
