/**
 * Created by Pasha on 26.08.2015.
 */
package lobby.controller.level.misc {
import flash.events.IEventDispatcher;

import lobby.model.LobbyModel;
import lobby.model.levels.LevelModelEvent;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.view.common.LivesInfoView;

import screens.view.core.layer.ScreensLayerView;
import screens.view.game.GameScreen;
import screens.view.level_selection.LevelSelectionScreen;

public class LevelLifeRestoreProgressCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var event:LevelModelEvent;

    public function LevelLifeRestoreProgressCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var livesInfoView:LivesInfoView;

        if (screensLayerView.activeScreen.hasOwnProperty("livesInfoView")) {
            livesInfoView = screensLayerView.activeScreen["livesInfoView"] as LivesInfoView;
            livesInfoView.nextLiveTf.visible = true;
            livesInfoView.setNextLifeIn(event.data.secondsLeft as int);
            livesInfoView.setLifeUpdateProgress(event.data.progress as Number);
        }
    }
}
}
