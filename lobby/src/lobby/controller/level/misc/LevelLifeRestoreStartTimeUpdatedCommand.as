/**
 * Created by Pasha on 26.08.2015.
 */
package lobby.controller.level.misc {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.view.core.layer.ScreensLayerView;
import screens.view.game.GameScreen;

public class LevelLifeRestoreStartTimeUpdatedCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var dispatcher:IEventDispatcher;

    public function LevelLifeRestoreStartTimeUpdatedCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_USER_DATA));
    }
}
}
