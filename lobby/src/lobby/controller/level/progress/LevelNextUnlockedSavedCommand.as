/**
 * Created by Pasha on 26.05.2016.
 */
package lobby.controller.level.progress {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;
import lobby.model.logic.LogicLobbyFacade;

import robotlegs.bender.bundles.mvcs.Command;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.vo.ShowScreenVO;

public class LevelNextUnlockedSavedCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    public function LevelNextUnlockedSavedCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        showLevelCompleteScreen();
    }

    private function showLevelCompleteScreen():void
    {


        var completesLevelIndex:int = lobbyModel.activeLevel - 1;
        var nextLevelIndex:int = lobbyModel.activeLevel;
        var completedLevelModel:LevelModel = lobbyModel.levelModels[completesLevelIndex];

        var data:Object = {
            nextLevelIndex:nextLevelIndex,
            completesLevelIndex:completesLevelIndex,
            isLocked:false,
            starsGot:LogicLobbyFacade.instance.logicLevelStars.getStarsCount(completedLevelModel, false),
            starsTotal:lobbyModel.levelModels[completesLevelIndex].starsTotal
        };


        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.SEND_DATA_TO_SERVER));

        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.LEVEL_COMPLETE, data)));
    }

}
}
