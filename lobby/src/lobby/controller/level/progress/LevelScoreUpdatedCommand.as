/**
 * Created by Pasha on 26.08.2015.
 */
package lobby.controller.level.progress {
import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.view.core.layer.ScreensLayerView;
import screens.view.game.GameScreen;

public class LevelScoreUpdatedCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var contextView:ContextView;

    public function LevelScoreUpdatedCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var gameScreen:GameScreen = screensLayerView.activeScreen as GameScreen;

        gameScreen.infoPanel.setScore(lobbyModel.activeLevelModel.scoreCollected, lobbyModel.activeLevelModel.scoreTotal);
    }
}
}
