/**
 * Created by Pasha on 26.08.2015.
 */
package lobby.controller.level.progress {
import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;
import lobby.controller.db.update.LevelDataVO;

import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;
import lobby.model.logic.LogicLobbyFacade;

import model.config.game.movable_items.base.MovableUnitTypeEnum;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.framework.api.IContext;

import screens.events.ScreensContextEvent;

import screens.view.core.layer.ScreensLayerView;
import screens.view.core.vo.ShowScreenVO;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.game.GameScreen;

public class LevelUpdateProcessCommand extends Command {

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var context: IContext;

    [Inject]
    public var eventCommandMap: IEventCommandMap;

    private var _gameScreen:GameScreen;

    public function LevelUpdateProcessCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        context.detain(this);

        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var gameScreen:GameScreen = screensLayerView.activeScreen as GameScreen;

        gameScreen.loader.addEventListener(GlobalEventDispatcherEvent.TYPE_FROM_GAME, onGlobalEventHandler);

        _gameScreen = gameScreen;
    }

    private function onGlobalEventHandler(event:GlobalEventDispatcherEvent):void {
        switch (event.data.type)
        {
            case GlobalEventDispatcherEvent.SCORE_ITEM_COLLECTED:
                    onScoreItemCollected(event.data.data);
                break;

            case GlobalEventDispatcherEvent.MOVABLE_UNIT_DEAD:
                    onMovableUnitDead(event.data.data);
                break;

            case GlobalEventDispatcherEvent.EXIT_REACHED:
                    onExitReached(event.data.data);
                break;
        }
    }

    private function onScoreItemCollected(data:Object):void
    {
        //upadare game screen as well
        lobbyModel.activeLevelModel.scoreCollected++;
    }

    private function onMovableUnitDead(data:Object):void
    {
        var unitType:String = data.type as String;
        if (unitType == MovableUnitTypeEnum.TYPE_CHARACTER)
        {
            lobbyModel.userModel.livesLeft--;

            if (lobbyModel.userModel.livesLeft == 0)
            {
                stopLevel();
                showLevelFailedScreen();
                releaseCommand();
            }else
            {
                var spawnData:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.SPAWN_CHARACTER);
                _gameScreen.loader.content.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_TO_GAME, spawnData));
            }
        }
    }

    private function onExitReached(data:Object):void
    {
        var completedLevelModel:LevelModel = lobbyModel.activeLevelModel;

        // Set maxScoreCollected variable
        if (completedLevelModel.maxScoreCollected < completedLevelModel.scoreCollected) {
            completedLevelModel.maxScoreCollected = completedLevelModel.scoreCollected;
        }

        // Give stars
        LogicLobbyFacade.instance.logicLevelStars.getStarsCount(completedLevelModel, true);

        // Save to data base
        eventCommandMap.map(LobbyApplicationEvent.BD_LEVELS_DATA_UPDATED).toCommand(LevelProgressSavedCommand).once();

        var vo:LevelDataVO = new LevelDataVO(
                completedLevelModel.number,
                completedLevelModel.isLocked ? 1 : 0,
                completedLevelModel.maxScoreCollected,
                completedLevelModel.scoreTotal
        );

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_LEVELS_DATA, vo));

        // Misc
        stopLevel();
        releaseCommand();
    }

    private function stopLevel():void
    {
         _gameScreen.loader.removeEventListener(GlobalEventDispatcherEvent.TYPE_FROM_GAME, onGlobalEventHandler);
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.LEVEL_STOP));
    }

    private function showLevelFailedScreen():void
    {
        dispatcher.dispatchEvent(new ScreensContextEvent(ScreensContextEvent.SHOW_SCREEN, new ShowScreenVO(ScreensTypeEnum.LEVEL_FAILED, {lobbyModel:lobbyModel})));
    }

    private function releaseCommand():void
    {
        context.release(this);
    }
}
}
