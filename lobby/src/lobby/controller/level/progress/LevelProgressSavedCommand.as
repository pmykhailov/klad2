/**
 * Created by Pasha on 26.05.2016.
 */
package lobby.controller.level.progress {
import flash.events.IEventDispatcher;

import lobby.LobbyApplicationEvent;

import lobby.controller.db.update.LevelDataVO;

import lobby.model.LobbyModel;
import lobby.model.levels.LevelModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;

import screens.events.ScreensContextEvent;
import screens.view.core.enum.ScreensTypeEnum;
import screens.view.core.vo.ShowScreenVO;

public class LevelProgressSavedCommand extends Command {

    [Inject]
    public var dispatcher:IEventDispatcher;

    [Inject]
    public var lobbyModel:LobbyModel;

    [Inject]
    public var eventCommandMap: IEventCommandMap;

    public function LevelProgressSavedCommand() {
        super();
    }

    override public function execute():void {
        super.execute();

        // Move to next level
        var nextLevelModel:LevelModel;
        lobbyModel.activeLevel++;
        nextLevelModel = lobbyModel.activeLevelModel;
        nextLevelModel.isLocked = false;

        // Save to data base
        eventCommandMap.map(LobbyApplicationEvent.BD_LEVELS_DATA_UPDATED).toCommand(LevelNextUnlockedSavedCommand).once();

        var vo:LevelDataVO = new LevelDataVO(
                nextLevelModel.number,
                nextLevelModel.isLocked ? 1 : 0
        );

        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_LEVELS_DATA, vo));
    }

}
}
