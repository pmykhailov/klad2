/**
 * Created by Pasha on 31.07.2015.
 */
package lobby.controller.level.pause {
import events.GlobalEventDispatcher;
import events.GlobalEventDispatcherData;
import events.GlobalEventDispatcherEvent;

import flash.display.Stage;

import lobby.model.LobbyModel;

import robotlegs.bender.bundles.mvcs.Command;
import robotlegs.bender.extensions.contextView.ContextView;

import screens.view.core.layer.ScreensLayerView;
import screens.view.game.GameScreen;

public class LevelPauseCommand extends Command {

    [Inject]
    public var contextView:ContextView;

    [Inject]
    public var lobbyModel:LobbyModel;

    public function LevelPauseCommand() {
        super();
    }


    override public function execute():void {
        super.execute();

        var screensLayerView: ScreensLayerView = contextView.view.getChildByName("ScreensLayerView") as ScreensLayerView;
        var gameScreen:GameScreen = screensLayerView.activeScreen as GameScreen;
        var stage:Stage = contextView.view.stage;

        var data:GlobalEventDispatcherData = new GlobalEventDispatcherData(GlobalEventDispatcherEvent.PAUSE_GAME);
        gameScreen.loader.content.dispatchEvent(new GlobalEventDispatcherEvent(GlobalEventDispatcherEvent.TYPE_TO_GAME, data));
    }
}
}
