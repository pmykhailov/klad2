/**
 * Created by Pasha on 30.07.2015.
 */
package lobby {


import flash.events.IEventDispatcher;

import lobby.controller.db.DBManager;

import lobby.controller.db.update.DBUpdateSettingsDataCommand;

import lobby.controller.db.update.DBUpdateUserDataCommand;

import lobby.controller.init.InitLobbySoundsCommand;
import lobby.controller.init.LobbyInitCommand;
import lobby.controller.db.error.DBErrorHandleCommand;
import lobby.controller.db.get.DBGetLevelsDataCommand;
import lobby.controller.db.get.DBGetSettingsDataCommand;
import lobby.controller.db.get.DBGetUserDataCommand;
import lobby.controller.db.init.DBCreateCommand;
import lobby.controller.init.DBInitCompleteCommand;
import lobby.controller.db.init.create_tables.DBCreateTablesCommand;
import lobby.controller.db.init.init_tables.DBInitInfoTableCommand;
import lobby.controller.db.init.init_tables.DBInitLevelsTableCommand;
import lobby.controller.db.init.init_tables.DBInitSettingsTableCommand;
import lobby.controller.db.init.init_tables.DBInitUserTableCommand;
import lobby.controller.db.update.DBUpdateLevelsDataCommand;
import lobby.controller.init.LobbyInitCompleteCommand;
import lobby.controller.level.misc.LevelLivesUpdatedCommand;
import lobby.controller.level.pause.LevelPauseCommand;
import lobby.controller.level.progress.LevelScoreUpdatedCommand;
import lobby.controller.level.progress.LevelUpdateProcessCommand;
import lobby.controller.level.resume.LevelResumeCommand;
import lobby.controller.level.start.LevelStartCommand;
import lobby.controller.level.stop.LevelStopCommand;
import lobby.controller.sync.SendDataToServerCommand;
import lobby.model.LobbyModel;
import lobby.model.logic.LogicLevelStars;
import lobby.model.logic.LogicLivesRestore;
import lobby.model.levels.LevelModelEvent;
import lobby.model.logic.LogicLobbyFacade;

import robotlegs.bender.extensions.contextView.ContextView;
import robotlegs.bender.extensions.eventCommandMap.api.IEventCommandMap;
import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
import robotlegs.bender.framework.api.IConfig;
import robotlegs.bender.framework.api.IContext;
import robotlegs.bender.framework.api.IInjector;

import screens.events.ScreensContextEvent;

public class LobbyApplicationConfig implements IConfig{

    [Inject]
    public var context: IContext;
    [Inject]
    public var eventCommandMap: IEventCommandMap;
    [Inject]
    public var mediatorMap: IMediatorMap;
    [Inject]
    public var dispatcher: IEventDispatcher;
    [Inject]
    public var injector: IInjector;
    [Inject]
    public var contextView: ContextView;


    public function LobbyApplicationConfig() {
    }

    public function configure():void {
        mapCommands();
        mapMediators();
        mapInjections();

        context.afterInitializing(init);
    }

    private function mapCommands(): void {
        // Init
        eventCommandMap.map(LobbyApplicationEvent.INIT).toCommand(InitLobbySoundsCommand)
        eventCommandMap.map(LobbyApplicationEvent.INIT_COMPLETE).toCommand(LobbyInitCompleteCommand)
        eventCommandMap.map(ScreensContextEvent.SCREENS_LAYER_INIT_COMPLETE).toCommand(LobbyInitCommand)

        // Levels command pack
        eventCommandMap.map(LobbyApplicationEvent.LEVEL_START).toCommand(LevelStartCommand);
        eventCommandMap.map(LobbyApplicationEvent.LEVEL_PAUSE).toCommand(LevelPauseCommand);
        eventCommandMap.map(LobbyApplicationEvent.LEVEL_RESUME).toCommand(LevelResumeCommand);
        eventCommandMap.map(LobbyApplicationEvent.LEVEL_STOP).toCommand(LevelStopCommand);
        eventCommandMap.map(LobbyApplicationEvent.LEVEL_PROGRESS).toCommand(LevelUpdateProcessCommand);

        // Data base commands
        eventCommandMap.map(LobbyApplicationEvent.UPDATE_LEVELS_DATA).toCommand(DBUpdateLevelsDataCommand);
        eventCommandMap.map(LobbyApplicationEvent.UPDATE_USER_DATA).toCommand(DBUpdateUserDataCommand);
        eventCommandMap.map(LobbyApplicationEvent.UPDATE_SETTINGS_DATA).toCommand(DBUpdateSettingsDataCommand);

        // Init sequence
        // DBCreateCommand -> DBCreateTablesCommand -> DBInitUserTableCommand, DBInitLevelsTableCommand, DBInitSettingsTableCommand, DBInitInfoTableCommand ->
        // BDGetLevelsDataCommand,BDGetUserDataCommand,BDGetSettingsDataCommand -> DBInitCompleteCommand
        eventCommandMap.map(LobbyApplicationEvent.CONNECT_TO_DB).toCommand(DBCreateCommand);
        eventCommandMap.map(LobbyApplicationEvent.BD_CONNECTED).toCommand(DBCreateTablesCommand);
        eventCommandMap.map(LobbyApplicationEvent.BD_TABLES_CREATED).toCommand(DBInitUserTableCommand);
        eventCommandMap.map(LobbyApplicationEvent.BD_TABLE_USER_INITED).toCommand(DBInitLevelsTableCommand);
        eventCommandMap.map(LobbyApplicationEvent.BD_TABLE_LEVELS_INITED).toCommand(DBInitSettingsTableCommand);
        eventCommandMap.map(LobbyApplicationEvent.BD_TABLE_SETTINGS_INITED).toCommand(DBInitInfoTableCommand);
        eventCommandMap.map(LobbyApplicationEvent.BD_TABLE_INFO_INITED).toCommand(DBGetLevelsDataCommand).once();
        eventCommandMap.map(LobbyApplicationEvent.BD_LEVELS_TABLE_DATA_GOT).toCommand(DBGetUserDataCommand).once();
        eventCommandMap.map(LobbyApplicationEvent.BD_USER_TABLE_DATA_GOT).toCommand(DBGetSettingsDataCommand).once();
        eventCommandMap.map(LobbyApplicationEvent.BD_SETTINGS_TABLE_DATA_GOT).toCommand(DBInitCompleteCommand).once();

        eventCommandMap.map(LobbyApplicationEvent.BD_ERROR).toCommand(DBErrorHandleCommand);

        // Server communication
        eventCommandMap.map(LobbyApplicationEvent.SEND_DATA_TO_SERVER).toCommand(SendDataToServerCommand);
        eventCommandMap.map(LobbyApplicationEvent.RETRY_USER_CREATION).toCommand(DBInitCompleteCommand);

        // Some commands are mapped in LobbyInitCompleteCommand
    }


    private function mapMediators(): void {
    }


    private function mapInjections(): void {

        // DBManager
        DBManager.instance.dispatcher = dispatcher;

        // Lobby model
        injector.map(LobbyModel).asSingleton();
        var lobbyModel:LobbyModel = injector.getInstance(LobbyModel) as LobbyModel;
        lobbyModel.dispatcher = dispatcher;
        lobbyModel.init();

        // Logic facede
        injector.map(LogicLobbyFacade).asSingleton();
        var logicFacade:LogicLobbyFacade = injector.getInstance(LogicLobbyFacade) as LogicLobbyFacade;

        // LivesRestoreLogic
        logicFacade.logicLivesRestore  = new LogicLivesRestore();
        logicFacade.logicLivesRestore.dispatcher = dispatcher;
        logicFacade.logicLivesRestore.userModel = lobbyModel.userModel;

        logicFacade.logicLevelStars = new LogicLevelStars();
    }


    private function init(): void {
        dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.INIT));
    }
}
}
