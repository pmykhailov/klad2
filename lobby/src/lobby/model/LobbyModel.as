/**
 * Created by Pasha on 01.08.2015.
 */
package lobby.model {
import flash.events.IEventDispatcher;

import lobby.model.levels.LevelModel;
import lobby.model.user.UserModel;
import lobby.service.HttpService;

public class LobbyModel {

    private var _dispatcher: IEventDispatcher;

    private var _levelModels:Vector.<LevelModel>;

    private var _activeLevel:int;

    private var _fxVolume:Number;

    private var _musicVolume:Number;

    private var _userModel:UserModel;

    private var _httpService:HttpService;

    private var _DBVersion:String;

    public function LobbyModel() {

    }

    public function init():void {
        _activeLevel = -1;
        _levelModels = new Vector.<LevelModel>();

        _fxVolume = 1;
        _musicVolume = 1;

        _userModel = new UserModel(UserModel.DEFAULT_ID);
        _userModel.dispatcher = _dispatcher;

        _httpService = new HttpService();
        _DBVersion = "1.0.0";
    }

    public function initLevels(levels:Array):void
    {
        for (var i:int = 0; i < levels.length; i++) {
            _levelModels[i] = levels[i];
            _levelModels[i].dispatcher = _dispatcher;
        }
    }

    public function get levelModels():Vector.<LevelModel> {
        return _levelModels;
    }

    public function get activeLevel():int {
        return _activeLevel;
    }

    public function set activeLevel(value:int):void {
        _activeLevel = value;
    }

    public function get activeLevelModel():LevelModel
    {
        if (_activeLevel == -1) return null;

        return _levelModels[_activeLevel];
    }

    public function get fxVolume():Number {
        return _fxVolume;
    }

    public function set fxVolume(value:Number):void {
        _fxVolume = value;
    }

    public function get musicVolume():Number {
        return _musicVolume;
    }

    public function set musicVolume(value:Number):void {
        _musicVolume = value;
    }

    public function get httpService():HttpService {
        return _httpService;
    }

    public function get userModel():UserModel {
        return _userModel;
    }

    public function get DBVersion():String {
        return _DBVersion;
    }

    public function set DBVersion(value:String):void {
        _DBVersion = value;
    }

    public function set dispatcher(value:IEventDispatcher):void {
        _dispatcher = value;
    }
}
}
