/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 06.08.15
 * Time: 10:44
 * To change this template use File | Settings | File Templates.
 */
package lobby.model.levels {
import flash.events.IEventDispatcher;

import model.config.game.GameConfig;
import model.config.level.LevelConfig;
import model.level.CoreLevelModel;
import model.map.cell.CellTypeEnum;

public class LevelModel extends CoreLevelModel{

        private var _dispatcher: IEventDispatcher;

        /* From 1*/
        private var _number:int;

        private var _scoreCollected:int;
        private var _maxScoreCollected:int;
        private var _scoreTotal:int;

        private var _starsGot:int;
        private var _starsTotal:int;

        private var _isLocked:Boolean;

        public function LevelEditorLevelModel() {
        }

        public function get number():int {
            return _number;
        }

        public function set number(value:int):void {
            _number = value;
        }

        public function set dispatcher(value:IEventDispatcher):void {
            _dispatcher = value;
        }

        public function get isLocked():Boolean {
            return _isLocked;
        }

        public function set isLocked(value:Boolean):void {
            _isLocked = value;
            _dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.IS_LOCKED_UPDATED, _number));
        }

        public function get scoreCollected():int {
            return _scoreCollected;
        }

        public function set scoreCollected(value:int):void {
            _scoreCollected = value;
            _dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.SCORE_UPDATED));
        }

        public function get maxScoreCollected():int {
            return _maxScoreCollected;
        }

        public function set maxScoreCollected(value:int):void {
            _maxScoreCollected = value;
        }
        public function get scoreTotal():int {
            return _scoreTotal;
        }

        override public function set gameConfig(value:GameConfig):void {
            super.gameConfig = value;

            // Calculate score total
            _scoreTotal = 0;
            for (var i:int = 0; i < _gameConfig.rows; i++) {
                for (var j:int = 0; j < _gameConfig.cols; j++) {
                    var cellType:int = _gameConfig.map[i][j];
                    if (cellType == CellTypeEnum.TYPE_SCORE_ITEM || cellType == CellTypeEnum.TYPE_SCORE_ITEM_WITH_KEY) {
                        _scoreTotal++;
                    }
                }
            }
        }


    public function get starsGot():int {
        return _starsGot;
    }

    public function set starsGot(value:int):void {
        _starsGot = value;
    }

    public function get starsTotal():int {
        return _starsTotal;
    }

    public function set starsTotal(value:int):void {
        _starsTotal = value;
    }

}
}
