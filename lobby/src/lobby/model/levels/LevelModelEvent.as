/**
 * Created by Pasha on 26.08.2015.
 */
package lobby.model.levels {
import flash.events.Event;

public class LevelModelEvent extends Event {

    public static const SCORE_UPDATED:String = "scoreUpdated";
    public static const LIVES_UPDATED:String = "livesUpdated";
    public static const IS_LOCKED_UPDATED:String = "isLockedUpdated";
    public static const LIFE_RESTORE_STAR_TIME_UPDATED:String = "lifeRestoreStartTimeUpdated";

    public static const LIFE_RESTORED:String = "lifeRestored";
    public static const LIFE_RESTORE_PROGRESS:String = "lifeRestoreProgress";

    private var _data:Object;

    public function LevelModelEvent(type:String, data:Object = null, bubbles:Boolean = false, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():Object {
        return _data;
    }

    override public function clone():Event {
        return new LevelModelEvent(type, data, bubbles, cancelable);
    }
}
}
