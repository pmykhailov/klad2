/**
 * Created by Pasha on 23.05.2016.
 */
package lobby.model.user {
import flash.events.IEventDispatcher;

import lobby.model.levels.LevelModelEvent;

public class UserModel {
    public static const  DEFAULT_ID:uint = 0;
    public static const  LIVES_TOTAL:uint = 3;
    public static const  LIFE_RESTORE_TIME:uint = 20; //5*60; /*seconds*/ //TODO: Uncomment test time amount

    private var _id:uint;
    private var _name:String;
    private var _livesTotal:int;
    private var _livesLeft:int;
    private var _lifeRestoreStartTime:Number;

    private var _dispatcher: IEventDispatcher;

    public function UserModel(id:uint, name:String = "") {
        _id = id;
        _name = name;
        _livesTotal = LIVES_TOTAL;
        _lifeRestoreStartTime = -1;
    }

    public function get id():uint {
        return _id;
    }

    public function set id(value:uint):void {
        _id = value;
    }

    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }

    public function get livesTotal():int {
        return _livesTotal;
    }

    public function set livesTotal(value:int):void {
        _livesTotal = value;
    }

    public function get livesLeft():int {
        return _livesLeft;
    }

    public function set livesLeft(value:int):void {
        var previousValue:int = _livesLeft;

        _livesLeft = value;
        _dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.LIVES_UPDATED, {previousValue:previousValue, newValue:value}));
    }

    public function set dispatcher(value:IEventDispatcher):void {
        _dispatcher = value;
    }

    public function get lifeRestoreStartTime():Number {
        return _lifeRestoreStartTime;
    }

    public function set lifeRestoreStartTime(value:Number):void {
        _lifeRestoreStartTime = value;
        _dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.LIFE_RESTORE_STAR_TIME_UPDATED));
    }
}
}
