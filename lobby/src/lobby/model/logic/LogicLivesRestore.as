/**
 * Created by Pasha on 05.06.2016.
 */
package lobby.model.logic {

import flash.events.IEventDispatcher;
import flash.events.TimerEvent;
import flash.utils.Timer;

import lobby.model.levels.LevelModelEvent;
import lobby.model.user.UserModel;

public class LogicLivesRestore {

    private var _dispatcher:IEventDispatcher;
    private var _userModel:UserModel;
    private var _livesTimer:Timer;
    private var _secondsLeft:int;
    private var _isBusy:Boolean;

    public function LogicLivesRestore() {

    }

    public function set dispatcher(value:IEventDispatcher):void {
        _dispatcher = value;
    }

    public function set userModel(value:UserModel):void {
        _userModel = value;
    }

    public function get isBusy():Boolean {
        return _isBusy;
    }

    /**
     * Start life restore if there is saved life restore time start
     */
    public function onApplicationStart() {
        // Init timer
        _livesTimer = new Timer(1000);
        _livesTimer.addEventListener(TimerEvent.TIMER, onTimerHandler);

        // Start life restore
        if (_userModel.livesLeft < _userModel.livesTotal && _userModel.lifeRestoreStartTime > 0) {
            var time:Number = (new Date()).time - _userModel.lifeRestoreStartTime;
            var timeInSeconds:Number = Math.floor(time / 1000);
            var livesRestored:Number = Math.floor(timeInSeconds / UserModel.LIFE_RESTORE_TIME);

            // Full restore
            if (_userModel.livesLeft + livesRestored >= _userModel.livesTotal) {
                _userModel.lifeRestoreStartTime = -1;
                _userModel.livesLeft = _userModel.livesTotal;
            } else
            // Restore some lives
            {
                _userModel.livesLeft += livesRestored;
                restoreLifeInternal(UserModel.LIFE_RESTORE_TIME - (timeInSeconds - livesRestored * UserModel.LIFE_RESTORE_TIME), false);
            }
        }
        // Set this listener here in the end so it will not react on previous updates of_userModel.livesLeft property
        _dispatcher.addEventListener(LevelModelEvent.LIVES_UPDATED, onLivesUpdatedHandler);
    }

    private function onLivesUpdatedHandler(event:LevelModelEvent):void {
        restoreLifeInternal(UserModel.LIFE_RESTORE_TIME);
    }

    private function restoreLifeInternal(secondsLeft:Number, updateLifeRestoreStartTime:Boolean = true):void {
        if (_userModel.livesLeft < _userModel.livesTotal && !_livesTimer.running) {

            _secondsLeft = secondsLeft;

            if (updateLifeRestoreStartTime) {
                _userModel.lifeRestoreStartTime = (new Date()).time;
                //_dispatcher.dispatchEvent(new LobbyApplicationEvent(LobbyApplicationEvent.UPDATE_USER_DATA));
            }

            _livesTimer.start();
            _isBusy = true;

        }
    }

    private function onTimerHandler(event:TimerEvent):void {

        // Restore progress
        _secondsLeft--;
        _dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.LIFE_RESTORE_PROGRESS, {secondsLeft:_secondsLeft, progress:(UserModel.LIFE_RESTORE_TIME - _secondsLeft)/UserModel.LIFE_RESTORE_TIME}))

        trace("LIFE RESTORE " + _secondsLeft);

        // Restore complete
        if (_secondsLeft == 0) {

            trace("LIFE RESTORE COMPLETE");

            _livesTimer.stop();
            _isBusy = false;

            _dispatcher.dispatchEvent(new LevelModelEvent(LevelModelEvent.LIFE_RESTORED));

            _userModel.livesLeft++;
            if (_userModel.livesLeft == _userModel.livesTotal) {
                _userModel.lifeRestoreStartTime = -1;
            }
        }
    }

}
}
