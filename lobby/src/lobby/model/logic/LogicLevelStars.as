package lobby.model.logic {
import lobby.model.levels.LevelModel;

public class LogicLevelStars {

    public static const MAX_START_FOR_LEVEL:uint = 3;

    private const SCORE_PERSENTAGE_FOR_STARS:Array = [60,80,100];

    public function LogicLevelStars() {

    }

    public function getStarsCount(levelModel:LevelModel, useMaxScoreCollected:Boolean):int {
        var collectedScoreCount:int = useMaxScoreCollected ? levelModel.maxScoreCollected : levelModel.scoreCollected;
        var levelScoreCollectedPersantage:int = int((collectedScoreCount / levelModel.scoreTotal)*100);

        for (var i:int = SCORE_PERSENTAGE_FOR_STARS.length - 1; i >= 0 ; i--) {
            if (levelScoreCollectedPersantage >= SCORE_PERSENTAGE_FOR_STARS[i]) {
                return (i + 1);
            }
        }

        return 0;
    }
}
}
