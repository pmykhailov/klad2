/**
 * Created by Pasha on 01.07.2016.
 */
package lobby.model.logic {
public class LogicLobbyFacade {

    private static var _instance:LogicLobbyFacade;


    private var _logicLivesRestore:LogicLivesRestore;
    private var _logicLevelStars:LogicLevelStars;

    public function LogicLobbyFacade() {
        _instance = this;
    }

    public static function get instance():LogicLobbyFacade {
        return _instance;
    }

    public function get logicLivesRestore():LogicLivesRestore {
        return _logicLivesRestore;
    }

    public function set logicLivesRestore(value:LogicLivesRestore):void {
        _logicLivesRestore = value;
    }

    public function get logicLevelStars():LogicLevelStars {
        return _logicLevelStars;
    }

    public function set logicLevelStars(value:LogicLevelStars):void {
        _logicLevelStars = value;
    }
}
}
