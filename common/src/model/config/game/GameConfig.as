/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 20.04.15
 * Time: 9:52
 * To change this template use File | Settings | File Templates.
 */
package model.config.game {
import model.config.game.movable_items.AIConfig;
import model.config.game.movable_items.CharacterConfig;
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.map.cell.CellModel;
import model.map.cell_options.ICellOptionsModel;

public class GameConfig {

    private var _rows:int;
    private var _cols:int;
    private var _map:Array;
    [ArrayElementType("model.map.cell_options.ICellOptionsModel")]
    private var _gridCellsOptions:Array;
    private var _exitCell:CellModel;
    private var _aisData:Vector.<AIConfig>;
    private var _characterData:CharacterConfig;

    public function GameConfig() {
        init();
    }

    private function init():void {

        _map = [];
        _gridCellsOptions = [];

        _exitCell = new CellModel(-1,0,0);

        _characterData = new CharacterConfig();
        _characterData.type = MovableUnitTypeEnum.TYPE_CHARACTER;
        _characterData.spawnCellModel = new CellModel(-1,0,0);

        _aisData = new Vector.<AIConfig>();
    }

    public function get rows():int {
        return _rows;
    }

    public function set rows(value:int):void {
        _rows = value;
    }

    public function get cols():int {
        return _cols;
    }

    public function set cols(value:int):void {
        _cols = value;
    }

    public function get map():Array {
        return _map;
    }

    public function set map(value:Array):void {
        _map = value;
    }

    public function get aisData():Vector.<AIConfig> {
        return _aisData;
    }

    public function set aisData(value:Vector.<AIConfig>):void {
        _aisData = value;
    }

    public function get characterData():CharacterConfig {
        return _characterData;
    }

    public function set characterData(value:CharacterConfig):void {
        _characterData = value;
    }

    public function get exitCell():CellModel {
        return _exitCell;
    }

    public function set exitCell(value:CellModel):void {
        _exitCell = value;
    }

    public function get gridCellsOptions():Array {
        return _gridCellsOptions;
    }

    public function set gridCellsOptions(value:Array):void {
        _gridCellsOptions = value;
    }

    // --------------------------------------
    // --------- GRID CELL OPTIONS ----------
    // --------------------------------------

    public function addGridCellOptions(cellOptionsModel:ICellOptionsModel):void {
        var optionsExists:Boolean = false;
        for (var i:int = 0; i < _gridCellsOptions.length; i++) {
            var gridCellOptions:ICellOptionsModel = _gridCellsOptions[i];

            if (gridCellOptions.row == cellOptionsModel.row && gridCellOptions.col == cellOptionsModel.col) {
                gridCellOptions.options = cellOptionsModel.options;
                optionsExists = true;
                break;
            }
        }

        if (!optionsExists) {
            _gridCellsOptions.push(cellOptionsModel);
        }
    }

    public function hasGridCellOptions(row:int, col:int):Boolean {
        for (var i:int = 0; i < _gridCellsOptions.length; i++) {
            var gridCellOptions:ICellOptionsModel = _gridCellsOptions[i];

            if (gridCellOptions.row == row && gridCellOptions.col == col) {
                return true;
            }
        }

        return false;
    }

    public function getGridCellOptions(row:int, col:int):ICellOptionsModel {
        for (var i:int = 0; i < _gridCellsOptions.length; i++) {
            var gridCellOptions:ICellOptionsModel = _gridCellsOptions[i];

            if (gridCellOptions.row == row && gridCellOptions.col == col) {
                return gridCellOptions;
            }
        }

        return null;
    }

    public function removeGridCellOptions(row:int, col:int) {
        for (var i:int = 0; i < _gridCellsOptions.length; i++) {
            var gridCellOptions:ICellOptionsModel = _gridCellsOptions[i];

            if (gridCellOptions.row == row && gridCellOptions.col == col) {
                _gridCellsOptions.splice(i,1);
                break;
            }
        }
    }

    // --------------------------------------
    // -------------- NEXT BLOCK ------------
    // --------------------------------------

}
}
