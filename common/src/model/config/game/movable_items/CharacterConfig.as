/**
 * Created by Pasha on 31.07.2015.
 */
package model.config.game.movable_items {
import model.config.*;
import model.config.game.movable_items.base.MovableUnitConfig;
import model.config.game.movable_items.base.MovableUnitTypeEnum;
import model.map.cell.CellModel;

public class CharacterConfig extends MovableUnitConfig{
    public function CharacterConfig() {
        super();

        _type = MovableUnitTypeEnum.TYPE_CHARACTER;
    }
}
}
