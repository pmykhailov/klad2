/**
 * Created by Pasha on 26.07.2015.
 */
package model.config.game.movable_items.base {
public class MovableUnitTypeEnum {

    public static const TYPE_GENERAL: String = "MovableUnit";
    public static const TYPE_CHARACTER: String = "Character";
    public static const TYPE_AI: String = "AI";

    public static const TYPES_ALL: Array = [TYPE_CHARACTER, TYPE_AI];

    public function MovableUnitTypeEnum() {
    }
}
}
