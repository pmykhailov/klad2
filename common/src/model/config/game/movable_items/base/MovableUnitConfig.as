/**
 * Created by Pasha on 31.07.2015.
 */
package model.config.game.movable_items.base {
import model.map.cell.CellModel;

public class MovableUnitConfig {

    protected var _id:int;

    protected var _type:String;

    protected var _spawnCellModel:CellModel;

    public function MovableUnitConfig() {
    }

    public function get spawnCellModel():CellModel {
        return _spawnCellModel;
    }

    public function set spawnCellModel(value:CellModel):void {
        _spawnCellModel = value;
    }

    public function get id():int {
        return _id;
    }

    public function set id(value:int):void {
        _id = value;
    }

    public function get type():String {
        return _type;
    }

    public function set type(value:String):void {
        _type = value;
    }
}
}
