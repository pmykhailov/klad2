/**
 * Created by Pasha on 31.07.2015.
 */
package model.config.game.movable_items {
import model.config.game.movable_items.base.MovableUnitConfig;
import model.config.game.movable_items.base.MovableUnitTypeEnum;

public class AIConfig extends MovableUnitConfig{

    public function AIConfig() {
        super();

        _type = MovableUnitTypeEnum.TYPE_AI;
    }

}
}
