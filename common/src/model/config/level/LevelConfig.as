package model.config.level {
public class LevelConfig {

    private var _name:String;

    public function LevelConfig() {
        _name = "";
    }

    public function get name():String {
        return _name;
    }

    public function set name(value:String):void {
        _name = value;
    }

}
}
