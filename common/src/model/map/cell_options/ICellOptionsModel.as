/**
 * Created by Pasha on 19.06.2016.
 */
package model.map.cell_options {
public interface ICellOptionsModel {
    function get row():int;
    function set row(value:int):void;
    function get col():int;
    function set col(value:int):void;
    function set options(value:Object):void;
    function get options():Object;
    function clone():ICellOptionsModel;
    function toString():String;
}
}
