/**
 * Created by Pasha on 19.06.2016.
 */
package model.map.cell_options {
public class DoorCellOptionsModel implements ICellOptionsModel{

    private var _row:int;
    private var _col:int;
    private var _requireKey:Boolean;

    public function DoorCellOptionsModel(row:int, col:int) {
        _row = row;
        _col = col;
    }

    public function get row():int {
        return _row;
    }

    public function set row(value:int):void {
        _row = value;
    }

    public function get col():int {
        return _col;
    }

    public function set col(value:int):void {
        _col = value;
    }

    public function set options(value:Object):void {
        _requireKey = value.require_key;
    }

    public function get options():Object {
        return {require_key:_requireKey};
    }

    public function clone():ICellOptionsModel {
        var res:DoorCellOptionsModel = new DoorCellOptionsModel(row,col);
        res.options = options;
        return res;
    }


    public function toString():String {
        return "require_key:" + _requireKey;
    }

    public function get requireKey():Boolean {
        return _requireKey;
    }
}
}
