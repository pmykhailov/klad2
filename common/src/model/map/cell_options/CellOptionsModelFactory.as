/**
 * Created by Pasha on 19.06.2016.
 */
package model.map.cell_options {
import model.map.cell.CellTypeEnum;

public class CellOptionsModelFactory {
    public function CellOptionsModelFactory() {
    }


    public static function getCellOptionsModel(cellType:int, row:int, col:int, options:Object):ICellOptionsModel {

        var res:ICellOptionsModel;

        switch (cellType) {
            case CellTypeEnum.TYPE_DOOR :
                res = new DoorCellOptionsModel(row, col);
                break;
        }

        if (res) {
            res.options = options;
        }

        return res;
    }
}
}
