/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.04.15
 * Time: 18:51
 * To change this template use File | Settings | File Templates.
 */
package model.map.cell {
public class CellTypeEnum {

    public static const TYPE_EMPTY:int = 0;

    public static const TYPE_CONCRETE:int = 1;
    public static const TYPE_BRICK:int = 2;
    public static const TYPE_WOOD:int = 3;
    public static const TYPE_LADDER:int = 4;
    public static const TYPE_WATTER:int = 5;

    // WARNING: Values are used in config only
    // to check is cell is belong to this type
    // fo like this
    // e.g. cellModel is ScoreItemModel
    public static const TYPE_SCORE_ITEM:int = 6;
    public static const TYPE_SCORE_ITEM_WITH_KEY:int = 9;
    public static const TYPE_DOOR:int = 7;
    public static const TYPE_OPENED_DOOR:int = 8;
    public static const TYPE_EXIT:int = 10;

}
}
