/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.04.15
 * Time: 18:50
 * To change this template use File | Settings | File Templates.
 */
package model.map.cell {
import flash.events.IEventDispatcher;

import misc.IDestroyable;

public class CellModel implements IDestroyable{

    protected var _type:int;
    protected var _row:int;
    protected var _col:int;
    protected var _dispatcher: IEventDispatcher;

    public function CellModel(type:int, row:int, col:int) {
        _type = type;
        _row = row;
        _col = col;
    }

    public function get type():int {
        return _type;
    }

    public function get row():int {
        return _row;
    }

    public function get col():int {
        return _col;
    }

    public function set dispatcher(value:IEventDispatcher):void {
        _dispatcher = value;
    }

    public function destroy():void {
        _dispatcher = null;
    }
}
}
