package model.level  {
import model.config.game.GameConfig;
import model.config.level.LevelConfig;

/**
 * Level model data that will not be changed.
 * This data is a core (minimal) data to identify level
 */
public interface ICoreLevelModel {
    function get levelConfig():LevelConfig;
    function set levelConfig(value: LevelConfig):void;

    function get gameConfig():GameConfig;
    function set gameConfig(value: GameConfig):void;
}
}
