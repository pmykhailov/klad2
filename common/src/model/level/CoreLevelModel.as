/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 06.08.15
 * Time: 10:51
 * To change this template use File | Settings | File Templates.
 */
package model.level {
import model.config.game.GameConfig;
import model.config.level.LevelConfig;

public class CoreLevelModel implements ICoreLevelModel {

    protected var _gameConfig: GameConfig;
    protected var _levelConfig: LevelConfig;

    public function CoreLevelModel() {
        _gameConfig = new GameConfig();
        _levelConfig = new LevelConfig();
    }

    public function get levelConfig():LevelConfig {
        return _levelConfig;
    }

    public function set levelConfig(value:LevelConfig):void {
        _levelConfig = value;
    }

    public function get gameConfig():GameConfig {
        return _gameConfig;
    }

    public function set gameConfig(value:GameConfig):void {
        _gameConfig = value;
    }
}
}
