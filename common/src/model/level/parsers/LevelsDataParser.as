package model.level.parsers  {
import model.config.game.movable_items.AIConfig;
import model.config.game.movable_items.CharacterConfig;
import model.config.game.GameConfig;
import model.level.ICoreLevelModel;
import model.map.cell.CellModel;
import model.config.level.LevelConfig;
import model.map.cell.CellTypeEnum;
import model.map.cell_options.CellOptionsModelFactory;

/**
     * LevelDataParser class.
     *
     * Parses XML data to array of GameSettings
     * Each GameSettings instance contain set of setting to a game
     *
     * User: Paul Makarenko
     * Date: 06.10.2014
     */
    public class LevelsDataParser {

        public function LevelsDataParser() {
        }


        public function parse(levelsData: XML, levels:Array /*ICoreLevelModel*/): void {
            var levelsCount: int = levelsData.level.length();

            for (var i: int = 0; i < levelsCount; i++) {
                var levelData: XML = levelsData.level[i] as XML;
                var levelModel: ICoreLevelModel = levels[i] as ICoreLevelModel;

                var gameConfig: GameConfig = new GameConfig();
                var levelConfig: LevelConfig = new LevelConfig();

                fillLevelDimensions(gameConfig, levelData.game_config[0]);
                fillLevelGrid(gameConfig, levelData.game_config[0]);
                fillLevelCellsOptionsDataGrid(gameConfig, levelData.game_config[0]);
                fillExitCell(gameConfig, levelData.game_config[0]);
                fillCharacterData(gameConfig, levelData.game_config[0]);
                fillAIsData(gameConfig, levelData.game_config[0]);

                fillLevelSettings(levelConfig, levelData.level_config[0]);

                levelModel.gameConfig = gameConfig;
                levelModel.levelConfig = levelConfig;
            }
        }

        private function fillLevelSettings(levelSettings:LevelConfig, levelSettingsData:XML):void {
            levelSettings.name = levelSettingsData.level_name;
        }


        private function fillLevelDimensions(gameConfig: GameConfig, gameConfigData: XML): void {
            var rows: int = int(gameConfigData.dimensions.@rows);
            var cols: int = int(gameConfigData.dimensions.@cols);

            gameConfig.rows = rows;
            gameConfig.cols = cols;
        }

        private function fillLevelCellsOptionsDataGrid(gameConfig: GameConfig, gameConfigData: XML): void {
            var cellsOptionsData: XML = gameConfigData.grid_cells_options[0] as XML;
            var cellsCount:int = cellsOptionsData.cell.length();

            gameConfig.gridCellsOptions = [];

            for (var i: int = 0; i < cellsCount; i++) {
                var cellXML:XML = cellsOptionsData.cell[i] as XML;
                var row:int = int(cellXML.@row);
                var col:int = int(cellXML.@col);
                var cellType:int = gameConfig.map[row][col];
                var options:Object = {};

                for (var j:int = 0; j < cellXML.children().length(); j++) {
                    var cellChild:XML = cellXML.children()[j] as XML;
                    var name:String = cellChild.name();
                    var text:String = cellChild.text();
                    var value:Object;

                    if (text == "true") {
                        value = true;
                    }

                    if (text == "false") {
                        value = false;
                    }

                    if (!isNaN(Number(text))) {
                        value = Number(text);
                    }

                    options[name] = value;
                }

                gameConfig.gridCellsOptions.push(CellOptionsModelFactory.getCellOptionsModel(cellType, row, col, options));
            }
        }

        private function fillLevelGrid(gameConfig: GameConfig, gameConfigData: XML): void {
            var gridData: XML = gameConfigData.grid[0] as XML;
            var rowsCount: int = gridData.row.length();

            gameConfig.map = [];

            for (var i: int = 0; i < rowsCount; i++) {
                var rowData: XML = gridData.row[i] as XML;
                var typesData: String = rowData.@data;
                var typesArray = typesData.split(",");
                var cols: int = typesArray.length;

                gameConfig.map[i] = [];

                for (var j: int = 0; j < cols; j++) {
                    gameConfig.map[i][j] = int(typesArray[j]);
                }
            }
        }

        private function fillExitCell(gameConfig: GameConfig, gameConfigData: XML): void {
            var exitCellCellData: XML = gameConfigData.exit_cell[0] as XML;
            var row: int = int(exitCellCellData.@row);
            var col: int = int(exitCellCellData.@col);


            gameConfig.exitCell = new CellModel(CellTypeEnum.TYPE_EMPTY, row, col);
        }

        private function fillCharacterData(gameConfig: GameConfig, gameConfigData: XML): void {
            var characterData: XML = gameConfigData.character[0] as XML;
            var spawnCellData: XML = characterData.spawn_cell[0] as XML;
            var row: int = int(spawnCellData.@row);
            var col: int = int(spawnCellData.@col);


            gameConfig.characterData = new CharacterConfig();
            gameConfig.characterData.spawnCellModel = new CellModel(-1,row,col);
        }

        private function fillAIsData(gameConfig: GameConfig, gameConfigData: XML): void {
            var aisData: XML = gameConfigData.ais[0] as XML;
            var aiCount: int = aisData.ai.length();

            gameConfig.aisData = new Vector.<AIConfig>;

            for (var i:int = 0; i < aiCount; i++) {
                var aiData: XML = aisData.ai[i] as XML;
                var spawnCellData: XML = aiData.spawn_cell[0] as XML;
                var row: int = int(spawnCellData.@row);
                var col: int = int(spawnCellData.@col);
                var aiConfig:AIConfig = new AIConfig();

                aiConfig.spawnCellModel = new CellModel(-1,row,col);

                gameConfig.aisData.push(aiConfig);
            }


        }
    }
}
