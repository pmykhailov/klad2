/**
 * Created by Pasha on 01.08.2015.
 */
package events  {
import flash.events.Event;

public class GlobalEventDispatcherEvent extends Event {

    public static const TYPE_FROM_GAME:String = "typeFromGame";
    public static const TYPE_TO_GAME:String = "typeToGame";

    // From game
    public static const GAME_INIT_COMPLETE:String = "gameInitConmplete";
    public static const MOVABLE_UNIT_DEAD:String = "movableUnitDead";
    public static const SCORE_ITEM_COLLECTED:String = "scoreItemCollected";
    public static const EXIT_REACHED: String = "exitReached";

    // To game
    public static const INIT_GAME:String = "initGame";
    public static const CHANGE_GAME_SCREEN: String = "changeGameScreen"; // TODO: unite this with INIT_GAME
    public static const DESTROY_GAME:String = "destroyGame";
    public static const PAUSE_GAME:String = "pauseGame";
    public static const RESUME_GAME:String = "resumeGame";
    public static const SPAWN_CHARACTER:String = "spawnCharacter";

    private var _data:GlobalEventDispatcherData;

    public function GlobalEventDispatcherEvent(type:String, data:GlobalEventDispatcherData, bubbles:Boolean = true, cancelable:Boolean = false) {
        super(type, bubbles, cancelable);
        _data = data;
    }

    public function get data():GlobalEventDispatcherData {
        return _data;
    }
}
}
