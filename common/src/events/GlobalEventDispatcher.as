/**
 * Created by Pasha on 01.08.2015.
 */
package events {
import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.IEventDispatcher;

import misc.IDestroyable;

public class GlobalEventDispatcher implements IEventDispatcher, IDestroyable {

    private static var _instance:GlobalEventDispatcher;
    private var _application:EventDispatcher;
    [ArrayElementType("events.GlobalListenerVO")]
    private var _listeners:Array;

    public function GlobalEventDispatcher() {
        super();

        _listeners = [];
    }

    public static function get instance():GlobalEventDispatcher {
        if (!_instance) _instance = new GlobalEventDispatcher();
        return _instance;
    }

    public function init(application:EventDispatcher):void {
        _application = application;
    }

    public function destroy():void {
        // Remove application listeners
        while (_listeners.length > 0) {
            removeEventListener(_listeners[0].type, _listeners[0].listener, _listeners[0].useCapture);
        }

        _instance = null;
        _application = null;
        _listeners = null;
    }

    public function addEventListener(type:String, listener:Function, useCapture:Boolean = false, priority:int = 0, useWeakReference:Boolean = false):void {
        _listeners.push(new GlobalListenerVO(type, listener, useCapture));
        _application.addEventListener(type, listener, useCapture, priority, useWeakReference);
    }

    public function removeEventListener(type:String, listener:Function, useCapture:Boolean = false):void {
        for (var i:int = 0; i<_listeners.length; i++) {
            var listenerVO:GlobalListenerVO = _listeners[i];

            if (listenerVO.type == type && listenerVO.listener == listener && listenerVO.useCapture == useCapture) {
                _listeners.splice(i, 1);
                break;
            }
        }
        _application.removeEventListener(type, listener, useCapture);
    }

    public function dispatchEvent(event:Event):Boolean {
        return _application.dispatchEvent(event);
    }

    public function hasEventListener(type:String):Boolean {
        return _application.hasEventListener(type);
    }

    public function willTrigger(type:String):Boolean {
        return _application.willTrigger(type);
    }
}
}
