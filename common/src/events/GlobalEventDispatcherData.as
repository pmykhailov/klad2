/**
 * Created by Pasha on 01.08.2015.
 */
package events {
public class GlobalEventDispatcherData {

    private var _type:String;
    private var _data:Object;

    public function GlobalEventDispatcherData(type:String, data:Object = null) {
        _type = type;
        _data = data;
    }

    public function get type():String {
        return _type;
    }

    public function get data():Object {
        return _data;
    }
}
}
