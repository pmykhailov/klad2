/**
 * Created by Pasha on 10.07.2016.
 */
package events {
public class GlobalListenerVO {

    private var _type:String;
    private var _listener:Function;
    private var _useCapture:Boolean;

    public function GlobalListenerVO(type:String, listener:Function, useCapture:Boolean) {
        _type = type;
        _listener = listener;
        _useCapture = useCapture;
    }

    public function get type():String {
        return _type;
    }

    public function get listener():Function {
        return _listener;
    }

    public function get useCapture():Boolean {
        return _useCapture;
    }
}
}
