package utils {
import flash.display.Stage;
import flash.system.Capabilities;

import misc.Dimensions;
import misc.IDestroyable;

public class ScreenUtils implements IDestroyable{

    public static const PC:String = "pc";
    public static const DEVICE:String = "device";

    private static var _instance:ScreenUtils;

    private var _environment:String;
    private var _stage:Stage;
    private var _screenDimensions:Dimensions;

    public function ScreenUtils() {
        _instance = this;

        if (Capabilities.os.toLowerCase().indexOf("windows") != -1)
        {
            _environment = PC;
        }else
        {
            _environment = DEVICE;
        }
    }

    public static function get instance():ScreenUtils {
        if (!_instance) {
            _instance = new ScreenUtils();
        }

        return _instance;
    }

    public function get environment():String {
        return _environment;
    }

    public function set stage(value:Stage):void {
        _stage = value;
    }

    public function get screenDimensions():Dimensions {

        if (!_screenDimensions) {
            _screenDimensions = new Dimensions(0,0);

            if (_environment == DEVICE) {
                _screenDimensions.width = Capabilities.screenResolutionX;
                _screenDimensions.height = Capabilities.screenResolutionY;
            } else {
                _screenDimensions.width = _stage.stageWidth;
                _screenDimensions.height = _stage.stageHeight;
            }
        }

        return _screenDimensions;
    }

    public function destroy():void {
        _stage = null;
        _instance = null;
    }
}
}
