/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.09.15
 * Time: 12:29
 * To change this template use File | Settings | File Templates.
 */
package sound {
public class SoundGroupEnum {

    public static const GROUP_MUSIC: String = "group_music";
    public static const GROUP_SFX: String = "group_sfx";
}
}
