/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.09.15
 * Time: 12:21
 * To change this template use File | Settings | File Templates.
 */
package sound {
public class GameSoundEnum {

    public static const COIN:String = "coin";
    public static const EXIT:String = "exit";
    public static const LOSE:String = "lose";
    public static const SHOOT:String = "shoot";
    public static const WALK:String = "walk";

    public static const BACKGROUND:String = "background";

    public static const ALL_FX:Array = [COIN, EXIT, LOSE, SHOOT, WALK];

}
}
