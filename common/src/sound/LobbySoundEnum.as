/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 28.09.15
 * Time: 12:26
 * To change this template use File | Settings | File Templates.
 */
package sound {
public class LobbySoundEnum {

    public static const AMBIANCE_LOBBY:String = "ambiance_lobby";
    public static const AMBIANCE_GAME:String = "ambiance_game";

    public static const BUTTON_CLICK:String = "button_click";

    public static const ALL_FX:Array = [BUTTON_CLICK];
}
}
