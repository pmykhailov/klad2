/**
 * Created by Pasha on 02.08.2015.
 */
package {
import model.map.cell.CellTypeEnum;

public class CellsTypeAdditionalData {

    public static var CELLS_GFX_ACCORDANCE:Array;
    public static var VISUAL_CELLS:Array;

    public function CellsTypeAdditionalData() {
        EmtyCellBitmapData;
        WallCellBitmapData;
        BrickCellBitmapData;
        WoodCellBitmapData;
        LadderCellBitmapData;
        WaterCellBitmapData;
        ScoreItemBitmapData;
        KeyBitmapData;
        ClosedDoorBitmapData;
        OpenedDoorBitmapData;
    }

    public static function init():void {
        CELLS_GFX_ACCORDANCE = new Array();
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_EMPTY] = "EmtyCellBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_CONCRETE] = "WallCellBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_BRICK] = "BrickCellBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_WOOD] = "WoodCellBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_LADDER] = "LadderCellBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_WATTER] = "WaterCellBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_SCORE_ITEM] = "ScoreItemBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_SCORE_ITEM_WITH_KEY] = "KeyBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_DOOR] = "ClosedDoorBitmapData";
        CELLS_GFX_ACCORDANCE[CellTypeEnum.TYPE_OPENED_DOOR] = "OpenedDoorBitmapData";

        VISUAL_CELLS = new Array();
        VISUAL_CELLS = [
            CellTypeEnum.TYPE_EMPTY,
            CellTypeEnum.TYPE_CONCRETE,
            CellTypeEnum.TYPE_BRICK,
            CellTypeEnum.TYPE_WOOD,
            CellTypeEnum.TYPE_LADDER,
            CellTypeEnum.TYPE_WATTER,
            CellTypeEnum.TYPE_SCORE_ITEM,
            CellTypeEnum.TYPE_DOOR
        ];
    }

}
}
