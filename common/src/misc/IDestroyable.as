/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 31.08.15
 * Time: 10:01
 * To change this template use File | Settings | File Templates.
 */
package misc {
public interface IDestroyable {
    function destroy():void;
}
}
