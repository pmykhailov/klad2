/**
 * Created with IntelliJ IDEA.
 * User: pavel.mykhailov
 * Date: 15.05.15
 * Time: 11:27
 * To change this template use File | Settings | File Templates.
 */
package misc {
public class Dimensions {

    private var _width:Number;
    private var _height:Number;

    public function Dimensions(width:Number, height:Number) {
        _width = width;
        _height = height;
    }

    public function get width():Number {
        return _width;
    }

    public function set width(value:Number):void {
        _width = value;
    }

    public function get height():Number {
        return _height;
    }

    public function set height(value:Number):void {
        _height = value;
    }
}
}
